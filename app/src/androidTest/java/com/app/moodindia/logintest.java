package com.app.moodindia;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class logintest {

    @Rule
    public ActivityTestRule<IntroActivity> mActivityTestRule = new ActivityTestRule<>(IntroActivity.class);

    @Test
    public void logintest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.next), isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appIntroViewPager = onView(
                allOf(withId(R.id.view_pager), isDisplayed()));
        appIntroViewPager.perform(swipeLeft());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withId(R.id.next), isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction appIntroViewPager2 = onView(
                allOf(withId(R.id.view_pager), isDisplayed()));
        appIntroViewPager2.perform(swipeLeft());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.done), withText("DONE"), isDisplayed()));
        appCompatButton.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3598163);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction relativeLayout = onView(
                allOf(withClassName(is("android.widget.RelativeLayout")), isDisplayed()));
        relativeLayout.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3580013);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction magicTextView = onView(
                allOf(withId(R.id.txtaddlogin), withText("Existing User? Login ›"),
                        withParent(allOf(withId(R.id.llnologin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        magicTextView.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.etemail),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("kaly"), closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.btnsignup), withText("Email"), isDisplayed()));
        appCompatButton2.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3582249);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction frameLayout = onView(
                allOf(withId(R.id.dialogplus_view_container),
                        withParent(allOf(withId(R.id.dialogplus_outmost_container),
                                withParent(withId(R.id.dialogplus_content_container)))),
                        isDisplayed()));
        frameLayout.perform(click());

        ViewInteraction magicTextView2 = onView(
                allOf(withId(R.id.tv_noticont), withText("Continue"), isDisplayed()));
        magicTextView2.perform(click());

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3586509);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.etemail), withText("kaly"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText2.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.etemail), withText("kaly"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("kalyan@schenmaxtech"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.etemail), withText("kalyan@schenmaxtech"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText4.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.etemail), withText("kalyan@schenmaxtech"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText5.perform(click());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.etemail), withText("kalyan@schenmaxtech"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText6.perform(replaceText("kalyan@schenmaxtech.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.etemail), withText("kalyan@schenmaxtech.com"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText7.perform(click());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.etemail), withText("kalyan@schenmaxtech.com"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText8.perform(replaceText("kalyan@schemaxtech.com"), closeSoftKeyboard());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.etpwd),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText9.perform(replaceText("2787o"), closeSoftKeyboard());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3514954);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText10 = onView(
                allOf(withId(R.id.etpwd), withText("2787o"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText10.perform(click());

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.etpwd), withText("2787o"),
                        withParent(allOf(withId(R.id.lllogin),
                                withParent(withId(R.id.content_login)))),
                        isDisplayed()));
        appCompatEditText11.perform(replaceText("278799"), closeSoftKeyboard());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.btnlogin), withText("Login"), isDisplayed()));
        appCompatButton3.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3579607);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction viewPager = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager.perform(swipeLeft());

        ViewInteraction viewPager2 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager2.perform(swipeRight());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.tvviewAll), withText("View All ›"), isDisplayed()));
        appCompatTextView.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3574773);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        pressBack();

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(3597430);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction textView = onView(
                allOf(withText("Created"), isDisplayed()));
        textView.perform(click());

        ViewInteraction viewPager3 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager3.perform(swipeLeft());

        ViewInteraction viewPager4 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager4.perform(swipeRight());

        ViewInteraction viewPager5 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager5.perform(swipeRight());

        ViewInteraction viewPager6 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager6.perform(swipeRight());

        ViewInteraction viewPager7 = onView(
                allOf(withId(R.id.container),
                        withParent(allOf(withId(R.id.main_content),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        viewPager7.perform(swipeRight());

    }

}
