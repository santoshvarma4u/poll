package com.app.moodindia.webservices;

import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.KeywordsPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.UserCategories;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public interface CategoryAPI {

    @FormUrlEncoded
    @POST("/get_all_category")
    void getCategories(@Field("blank") String blank,
               Callback<List<CategoryRealmPojo>> response);

    @FormUrlEncoded
    @POST("/get_user_following_category")
    void getUserFollowing(@Field("user_id") String userid,
                          Callback<List<CategoryRealmPojo>> response);
    @FormUrlEncoded
    @POST("/get_user_categories")
    void getUserCatgories(@Field("user_id") String userid,
                        Callback<List<UserCategories>> response);
    @FormUrlEncoded
    @POST("/get_tags")
    void getHashTags(@Field("user_id") String userid,
                        Callback<KeywordsPojo> response);
    @FormUrlEncoded
    @POST("/update_user_following")
    void updateUserFollowing(@Field("user_id") String userid,
                             @Field("cat_id") String cat_id,
                             @Field("status") String status,
                          Callback<StatusPojo> response);
    @FormUrlEncoded
    @POST("/select_all_user_following")
    void updateAllUserFollowing(@Field("user_id") String userid,
                          Callback<StatusPojo> response);
}
