package com.app.moodindia.webservices;

import com.app.moodindia.model.ChangePwdPojo;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.model.SuccessPojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Santosh Varma on 27/09/16.
 */

public interface AuthenticationAPI {

    @FormUrlEncoded
    @POST("/user_login")
    void login(@Field("email") String email,
               @Field("pwd") String pwd,
               Callback<ProfilePojo> response);

    @FormUrlEncoded
    @POST("/social_login")
    void sociallogin(@Field("email") String email,
                     @Field("login_type") String login_type,
                     Callback<ProfilePojo> response);

    @POST("/user_register")
    @Multipart
    void register(@Query("email") String email,
                  @Query("name") String name,
                  @Query("pwd") String pwd,
                  @Query("phone") String phone,
                  @Query("country") String country,
                  @Query("gender") String gender,
                  @Query("login_type") String login_type,
                  @Query("State") String State,
                  @Query("maritial_status") String maritial_status,
                  @Query("City") String City,
                  @Query("income_level") String income_level,
                  @Query("user_type") String user_type,
                  @Query("age") String age,
                  @Query("qualification") String qualification,
                  @Query("latitude") String latitude,
                  @Query("longitude") String longitude,
                  @Part("image") TypedFile image,
                  @Field("referralId") String referralId,
                  @Field("deviceid") String deviceid,
                  Callback<ProfilePojo> response);

    @FormUrlEncoded
    @POST("/user_register")
    void register_no_image(@Field("email") String email,
                  @Field("name") String name,
                  @Field("pwd") String pwd,
                  @Field("phone") String phone,
                  @Field("country") String country,
                  @Field("gender") String gender,
                  @Field("login_type") String login_type,
                  @Field("State") String State,
                  @Field("maritial_status") String maritial_status,
                  @Field("City") String City,
                  @Field("income_level") String income_level,
                  @Field("user_type") String user_type,
                  @Field("age") String age,
                  @Field("qualification") String qualification,
                  @Field("latitude") String latitude,
                  @Field("longitude") String longitude,
                  @Field("referralId") String referralId,
                  @Field("deviceid") String deviceid,
                  Callback<ProfilePojo> response);

    @FormUrlEncoded
    @POST("/get_user_info")
    void getUserData(@Field("user_id") String user_id,
                  Callback<ProfilePojo> response);

    @FormUrlEncoded
    @POST("/update_profile")
    void editProfile(@Field("user_id") String user_id,
                     @Field("name") String name,
                     @Field("phone") String phone,
                     @Field("country") String country,
                     @Field("date_of_birth") String date_of_birth,
                     @Field("age") String age,
                     @Field("gender") String gender,
                     @Field("State") String state,
                     @Field("City") String city,
                     @Field("user_type") String user_type,
                     @Field("income_level") String income_level,
                     @Field("maritial_status") String maritial_status,
                     @Field("latitude") String latitude,
                     @Field("longitude") String longitude,
                     Callback<ProfilePojo> response);

    @Multipart
    @POST("/update_profile")
    void editProfileWithImage(@Query("user_id") String user_id,
                     @Query("name") String name,
                     @Query("phone") String phone,
                     @Query("country") String country,
                     @Query("date_of_birth") String date_of_birth,
                     @Query("age") String age,
                     @Query("gender") String gender,
                     @Query("State") String state,
                     @Query("City") String city,
                     @Query("user_type") String user_type,
                     @Query("income_level") String income_level,
                     @Query("maritial_status") String maritial_status,
                     @Query("latitude") String latitude,
                     @Query("longitude") String longitude,
                      @Part("image") TypedFile image,
                     Callback<ProfilePojo> response);

    @FormUrlEncoded
    @POST("/update_fcm")
    void updatefcm(@Field("user_id") String user_id,
                   @Field("fcm_id") String fcm_id,
                   @Field("device_id") String device_id,
                   @Field("device_type") String device_type,
                   @Field("device_name") String device_name,
                   Callback<SuccessPojo> response);

    @FormUrlEncoded
    @POST("/change_pwd")
    void changePassword(@Field("user_id") String user_id,
                     @Field("old_pwd") String old_pwd,
                     @Field("new_pwd") String new_pwd,
                     Callback<ChangePwdPojo> response);

    @FormUrlEncoded
    @POST("/forgot_pwd")
    void forgotPwd(@Field("email") String email,
                   Callback<ChangePwdPojo> response);

}
