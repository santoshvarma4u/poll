package com.app.moodindia.webservices;

import com.app.moodindia.model.CategoryPojo;
import com.app.moodindia.model.CreditsPojo;
import com.app.moodindia.model.MessagesPojo;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.model.SuggestPojo;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public interface PollAPI {

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPolls(@Field("user_id") String user_id,
                         @Field("page_no") Integer page_no,
                   Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_all_notifications")
    void getNotifications(@Field("user_id") String user_id,
                  Callback<List<MessagesPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsV2(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("favs") String favs,
                  Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/getSearchResults")
    void getSearchResults(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("querys") String querys,
                  Callback<List<PollPojo>> response);
    @FormUrlEncoded
    @POST("/getSearchByTags")
    void getTagResults(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("querys") String querys,
                  Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsV3(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("cat_id") String cat_id,
                    @Field("act_names")String act_names, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_web_all")
    void getPublicPolls(@Field("weball") String weball,
                        @Field("country_id") String country_id,
                        @Field("state_id") String state_id,
                        @Field("city_id") String city_id,
                        @Field("favs") String favs,
                     Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_trending_public_polls")
    void getPublicTrandingPolls(@Field("page_no") Integer page_no,
                          @Field("country_id") String country_id,
                          @Field("state_id") String state_id,
                          @Field("city_id") String city_id,
                          @Field("tranding") String tranding,
                          @Field("favs") String favs,
                          Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getTrandingPolls(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("tranding") String tranding,
                    @Field("favs") String favs,
                    Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getAnsweredPolls(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("answered_poll") String answered_poll,
                    Callback<List<PollPojo>> response);


    @FormUrlEncoded
    @POST("/get_current_polls")
    void getAnsweredPollsAll(@Field("user_id") String user_id,
                            @Field("categories") String categories,
                            @Field("page_no") Integer page_no,
                            @Field("country_id") String country_id,
                            @Field("state_id") String state_id,
                            @Field("city_id") String city_id,
                            @Field("answered_poll") String answered_poll,
                    Callback<List<PollPojo>> response);

    @POST("/set_suggest_poll")
    @Multipart
    void suggestPoll(@Query("user_id") String user_id,
                     @Query("poll_question") String poll_question,
                     @Query("poll_description") String poll_description,
                     @Query("poll_optiona") String poll_optiona,
                     @Query("poll_optionb") String poll_optionb,
                     @Query("poll_optionc") String poll_optionc,
                     @Query("poll_optiond") String poll_optiond,
                     @Query("poll_moreinfo") String poll_moreinfo,
                     @Query("preferred_type") String preferred_type,
                     @Query("preferred_state") String preferred_state,
                     @Query("preferred_city") String preferred_city,
                     @Part("image") TypedFile image,@Query("name") String name, Callback<List<SuggestPojo>> response);

    @FormUrlEncoded
    @POST("/set_suggest_poll")
    void suggestPollWithoutImage(@Field("user_id") String user_id,
                     @Field("poll_question") String poll_question,
                     @Field("poll_description") String poll_description,
                     @Field("poll_optiona") String poll_optiona,
                     @Field("poll_optionb") String poll_optionb,
                     @Field("poll_optionc") String poll_optionc,
                     @Field("poll_optiond") String poll_optiond,
                     @Field("poll_moreinfo") String poll_moreinfo,
                                 @Field("preferred_type") String preferred_type,
                                 @Field("preferred_state") String preferred_state,
                                 @Field("preferred_city") String preferred_city,
                     @Field("name") String name, Callback<List<SuggestPojo>> response);


    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsByCategory(@Field("user_id") String user_id,
                             @Field("page_no") Integer page_no,
                            @Field("country_id") String country_id,
                            @Field("state_id") String state_id,
                            @Field("city_id") String city_id,
                            @Field("cat_id") String cat_id,
                  Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_poll_option")
    void getPollOption(@Field("user_id") String user_id,
                       @Field("question_id") String question_id,
                       @Field("from") String from,
                       @Field("state") String state,
                       @Field("city") String city,
                       Callback<List<PollOptionPojo>> response);

    @FormUrlEncoded
    @POST("/submit_answer")
    void submitAns(@Field("question_id") String question_id,
                   @Field("option_id") String option_id,
                   @Field("user_id") String user_id,
                   Callback<SubmitAnsPojo> response);

    @FormUrlEncoded
    @POST("/report_poll")
    void reportPoll(@Field("user_id") String user_id,
                   @Field("report") String report,
                   @Field("pollid") String pollid,
                   @Field("description") String description,
                   Callback<SuccessPojo> response);

    @FormUrlEncoded
    @POST("/submit_reaction")
    void submitReaction(@Field("poll_id") String poll_id,
                   @Field("user_id") String user_id,
                   @Field("reaction") String reaction,
                   Callback<SuccessPojo> response);

    @FormUrlEncoded
    @POST("/verifyReferralUser")
    void verifyReferralUser(@Field("userid") String userid,
                            @Field("referralid") String referralid,
                   @Field("deviceid") String deviceid,
                   Callback<StatusPojo> response);

    @FormUrlEncoded
    @POST("/addCreditsforReferral")
    void addCreditsforReferral(@Field("referralid") String referralid,
                   @Field("userid") String userid,
                   Callback<StatusPojo> response);

 @FormUrlEncoded
    @POST("/updatePhoneNumber")
    void updatePhoneNumber(@Field("userid") String userid,
                   @Field("mobilenumber") String mobilenumber,
                   Callback<StatusPojo> response);
 @FormUrlEncoded
    @POST("/updateProfileVisiblity")
    void updateProfileVisiblity(@Field("userid") String userid,
                   @Field("visibilty") String visibilty,
                   Callback<StatusPojo> response);

 @FormUrlEncoded
    @POST("/addCredits")
    void addCredits(@Field("userid") String userid,
                   @Field("orderid") String orderid,
                   @Field("amount") String amount,
                   Callback<StatusPojo> response);
@FormUrlEncoded
    @POST("/removeCredits")
    void removeCredits(@Field("userid") String userid,
                   @Field("orderid") String orderid,
                   @Field("amount") String amount,
                   Callback<StatusPojo> response);
@FormUrlEncoded
    @POST("/updateCreditsAnon")
    void updateCreditsAnon(@Field("userid") String userid,
                   @Field("orderid") String orderid,
                   @Field("amount") String amount,
                   Callback<StatusPojo> response);

    @FormUrlEncoded
    @POST("/getCreditDetails")
    void getCreditDetails(@Field("userid") String userid,
                   Callback<List<CreditsPojo>> response);


    @FormUrlEncoded
    @POST("/get_history")
    void getHistory(@Field("page_no") Integer page_no,
                    @Field("country_id") String country_id, @Field("state_id") String state_id, @Field("city_id") String city_id, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/getCategoryHistory")
    void getCategoryHistory(@Field("page_no") Integer page_no, @Field("categoryid") String catid,@Field("country_id") String country_id, @Field("state_id") String state_id, @Field("city_id") String city_id, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/getFilterHistory")
    void getFilterCategoryHistory(@Field("page_no") Integer page_no,@Field("categoryid") String catid ,@Field("filtertxt") String filtertext, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_history_by_question_id")
    void getHistoryOption(@Field("question_id") String question_id,
                          @Field("user_id") String user_id,
                          @Field("optionid") String optionid,
                    Callback<List<PollOptionPojo>> response);

    @FormUrlEncoded
    @POST("/getCategories")
    void getCategories(Callback<List<CategoryPojo>> response);


    @FormUrlEncoded
    @POST("/getLocationBasedUsers")
    void getUserLocations(@Field("userid") String userid,
                          @Field("redius") String redius,
                          Callback<List<ProfilePojo>> response);
     @FormUrlEncoded
    @POST("/sendLocationInvite")
    void sendLocataionInvite(@Field("userid") String userid,
                          @Field("redius") String redius,
                          Callback<StatusPojo> response);


}
