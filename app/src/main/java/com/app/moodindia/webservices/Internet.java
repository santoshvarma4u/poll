package com.app.moodindia.webservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.moodindia.ConnectionDetector;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.ivankocijan.magicviews.views.MagicTextView;

public class Internet extends AppCompatActivity implements View.OnClickListener {

    protected Toolbar toolbar;
    protected ImageView imageView3;
    protected MagicTextView textView3;
    protected Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_internet);
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_retry) {
            ConnectionDetector cd=new ConnectionDetector(Internet.this);

            if(cd.isConnectingToInternet())
            {
                startActivity(new Intent(Internet.this, HomeActivity.class));
                finish();
            }
            else{
                Toast.makeText(Internet.this, "Not Yet Connected, try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        textView3 = (MagicTextView) findViewById(R.id.textView3);
        btnRetry = (Button) findViewById(R.id.btn_retry);
        btnRetry.setOnClickListener(Internet.this);
    }
}
