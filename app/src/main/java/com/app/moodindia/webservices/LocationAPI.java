package com.app.moodindia.webservices;

import com.app.moodindia.model.CityPojo;
import com.app.moodindia.model.CountryPojo;
import com.app.moodindia.model.StatePojo;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by tskva on 12/24/2016.
 */

public interface LocationAPI {

    @FormUrlEncoded
    @POST("/get_countries")
    void getCountries(@Field("blank") String blank,
                       Callback<List<CountryPojo>> response);

    @FormUrlEncoded
    @POST("/get_states")
    void getStates(@Field("country_id") String country_id,
                       Callback<List<StatePojo>> response);

    @FormUrlEncoded
    @POST("/get_states")
    void getStatesRegister(@Field("country_id") String country_id,
                           @Field("register") String register,
                       Callback<List<StatePojo>> response);

    @FormUrlEncoded
    @POST("/get_cities")
    void getCities(@Field("state_id") String state_id,
                       Callback<List<CityPojo>> response);

    @FormUrlEncoded
    @POST("/get_cities")
    void getCitiesRegister(@Field("state_id") String state_id,
                           @Field("register") String register,
                           Callback<List<CityPojo>> response);

}
