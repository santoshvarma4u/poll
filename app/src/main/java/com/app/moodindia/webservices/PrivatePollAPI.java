package com.app.moodindia.webservices;

import com.app.moodindia.model.CGImagePojo;
import com.app.moodindia.model.CategoryPojo;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.MessagesPojo;
import com.app.moodindia.model.ParticipantsPojo;
import com.app.moodindia.model.ParticipatedUsersPojo;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.model.PrivateOptions;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.model.SuggestPojo;
import com.app.moodindia.model.UserLog;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public interface PrivatePollAPI {

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPolls(@Field("user_id") String user_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_private_polls_own")
    void getPrivatePolls(@Field("userid") String user_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PrivatePollPojo>> response);
    @FormUrlEncoded
    @POST("/get_private_polls_own_closed")
    void getPrivatePollsClosed(@Field("userid") String user_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PrivatePollPojo>> response);
    @FormUrlEncoded
    @POST("/get_private_polls_options")
    void getPrivatePollOptions(@Field("pollid") String pollid,
                  Callback<List<PrivateOptions>> response);

    @FormUrlEncoded
    @POST("/get_private_poll_by_id")
    void getPrivatePollById(@Field("pollid") String poll_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PrivatePollPojo>> response);

    @FormUrlEncoded
    @POST("/get_private_polls_participated_users")
    void getPrivatePollUsers(@Field("questionid") String questionid,
                             @Field("page_no") Integer page_no,
                             Callback<List<ParticipatedUsersPojo>> response);

    @FormUrlEncoded
    @POST("/get_private_polls_participated_by_user")
    void getPrivatePollUsersByUserid(@Field("questionid") String questionid,
                                     @Field("optionid") String optionid,
                             @Field("page_no") Integer page_no,
                             Callback<List<ParticipatedUsersPojo>> response);


    @FormUrlEncoded
    @POST("/get_private_polls_answered")
    void getPrivateParticipatedPolls(@Field("userid") String user_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PrivatePollPojo>> response);

    @FormUrlEncoded
    @POST("/get_private_polls_answered_closed")
    void getPrivateParticipatedPollsClosed(@Field("userid") String user_id,
                  @Field("page_no") Integer page_no,
                  Callback<List<PrivatePollPojo>> response);


    @FormUrlEncoded
    @POST("/get_all_notifications")
    void getNotifications(@Field("user_id") String user_id,
                          Callback<List<MessagesPojo>> response);


    @FormUrlEncoded
    @POST("/get_all_photos.php")
    void getAllPhotos(@Field("user_id") String user_id,
                          Callback<List<CGImagePojo>> response);


    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsV2(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("favs") String favs,
                    Callback<List<PollPojo>> response);
    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsV3(@Field("user_id") String user_id,
                    @Field("page_no") Integer page_no,
                    @Field("country_id") String country_id,
                    @Field("state_id") String state_id,
                    @Field("city_id") String city_id,
                    @Field("cat_id") String cat_id,
                    @Field("act_names")String act_names, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getTrandingPolls(@Field("user_id") String user_id,
                          @Field("page_no") Integer page_no,
                          @Field("country_id") String country_id,
                          @Field("state_id") String state_id,
                          @Field("city_id") String city_id,
                          @Field("tranding") String tranding,
                          @Field("favs") String favs,
                          Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_current_polls")
    void getAnsweredPolls(@Field("user_id") String user_id,
                          @Field("page_no") Integer page_no,
                          @Field("country_id") String country_id,
                          @Field("state_id") String state_id,
                          @Field("city_id") String city_id,
                          @Field("answered_poll") String answered_poll,
                          Callback<List<PollPojo>> response);


    @FormUrlEncoded
    @POST("/get_current_polls")
    void getAnsweredPollsAll(@Field("user_id") String user_id,
                             @Field("categories") String categories,
                             @Field("page_no") Integer page_no,
                             @Field("country_id") String country_id,
                             @Field("state_id") String state_id,
                             @Field("city_id") String city_id,
                             @Field("answered_poll") String answered_poll,
                             Callback<List<PollPojo>> response);
    @FormUrlEncoded
    @POST("/get_accept_poll")
    void getAcceptPoll(@Field("user_id") String user_id,
                                 @Field("poll_id") String poll_id,
                                 Callback<List<StatusPojo>> response);

    @FormUrlEncoded
    @POST("/submit_accept_poll")
    void submitAcceptPoll(@Field("user_id") String user_id,
                                 @Field("poll_id") String poll_id,
                                 Callback<List<StatusPojo>> response);

    @FormUrlEncoded
    @POST("/banUnban_Poll")
    void BanPrivatePoll(@Field("user_id") String user_id,
                                 @Field("poll_id") String poll_id,
                                 @Field("ban") String ban,
                                 Callback<List<StatusPojo>> response);

    @POST("/set_suggest_poll")
    @Multipart
    void suggestPoll(@Query("user_id") String user_id,
                     @Query("poll_question") String poll_question,
                     @Query("poll_description") String poll_description,
                     @Query("poll_optiona") String poll_optiona,
                     @Query("poll_optionb") String poll_optionb,
                     @Query("poll_optionc") String poll_optionc,
                     @Query("poll_optiond") String poll_optiond,
                     @Query("poll_moreinfo") String poll_moreinfo,
                     @Query("preferred_type") String preferred_type,
                     @Query("preferred_state") String preferred_state,
                     @Query("preferred_city") String preferred_city,
                     @Part("image") TypedFile image,@Query("name") String name, Callback<List<SuggestPojo>> response);

    @FormUrlEncoded
    @POST("/set_suggest_poll")
    void suggestPollWithoutImage(@Field("user_id") String user_id,
                                 @Field("poll_question") String poll_question,
                                 @Field("poll_description") String poll_description,
                                 @Field("poll_optiona") String poll_optiona,
                                 @Field("poll_optionb") String poll_optionb,
                                 @Field("poll_optionc") String poll_optionc,
                                 @Field("poll_optiond") String poll_optiond,
                                 @Field("poll_moreinfo") String poll_moreinfo,
                                 @Field("preferred_type") String preferred_type,
                                 @Field("preferred_state") String preferred_state,
                                 @Field("preferred_city") String preferred_city,
                                 @Field("name") String name, Callback<List<SuggestPojo>> response);


   @POST("/submit_private_poll")
   @Multipart
   void submitPrivatePoll(@Query("user_id") String user_id,
                          @Query("poll_question") String poll_question,
                          @Query("poll_description") String poll_description,
                          @Query("poll_type") String poll_type,
                          @Query("poll_optiona") String poll_optiona,
                          @Query("poll_optionb") String poll_optionb,
                          @Query("poll_optionc") String poll_optionc,
                          @Query("poll_optiond") String poll_optiond,
                          @Query("poll_optione") String poll_optione,
                          @Query("poll_optionf") String poll_optionf,
                          @Query("poll_moreinfo") String poll_moreinfo,
                          @Query("groupid") String groupid,
                          @Part("poll_option_imagea") TypedFile imagea,
                          @Part("poll_option_imageb") TypedFile imageb,
                          @Part("poll_option_imagec") TypedFile imagec,
                          @Part("poll_option_imaged") TypedFile imaged,
                          @Part("poll_option_imagee") TypedFile imagee,
                          @Part("poll_option_imagef") TypedFile imagef,
                          @Query("poll_tags") String poll_tags,
                          @Query("youtube_link") String youtube_link,
                          @Part("image") TypedFile image,@Query("name") String name,@Query("anonymous") String anonymous, Callback<List<SuggestPojo>> response);

   @FormUrlEncoded
   @POST("/submit_private_poll_withoutimage")
   void submitPrivatePollNoImage(@Field("user_id") String user_id,
                          @Field("poll_question") String poll_question,
                          @Field("poll_description") String poll_description,
                          @Field("poll_type") String poll_type,
                          @Field("poll_optiona") String poll_optiona,
                          @Field("poll_optionb") String poll_optionb,
                          @Field("poll_optionc") String poll_optionc,
                          @Field("poll_optiond") String poll_optiond,
                          @Field("poll_optione") String poll_optione,
                          @Field("poll_optionf") String poll_optionf,
                          @Field("poll_moreinfo") String poll_moreinfo,
                          @Field("groupid") String groupid,
                          @Field("poll_option_imagea") String imagea,
                          @Field("poll_option_imageb") String imageb,
                          @Field("poll_option_imagec") String imagec,
                          @Field("poll_option_imaged") String imaged,
                          @Field("poll_option_imagee") String imagee,
                          @Field("poll_option_imagef") String imagef,
                          @Field("poll_tags") String poll_tags,
                          @Field("youtube_link") String youtube_link,
                          @Field("image") String image,@Field("name") String name,@Query("anonymous") String anonymous, Callback<List<SuggestPojo>> response);


    @FormUrlEncoded
    @POST("/get_current_polls")
    void getPollsByCategory(@Field("user_id") String user_id,
                            @Field("page_no") Integer page_no,
                            @Field("country_id") String country_id,
                            @Field("state_id") String state_id,
                            @Field("city_id") String city_id,
                            @Field("cat_id") String cat_id,
                            Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_poll_option")
    void getPollOption(@Field("user_id") String user_id,
                       @Field("question_id") String question_id,
                       @Field("from") String from,
                       Callback<List<PollOptionPojo>> response);
    @FormUrlEncoded
    @POST("/get_private_poll_option")
    void getPrivatePollOption(@Field("user_id") String user_id,
                       @Field("question_id") String question_id,
                       @Field("from") String from,
                       Callback<List<PollOptionPojo>> response);

    @FormUrlEncoded
    @POST("/submit_answer")
    void submitAns(@Field("question_id") String question_id,
                   @Field("option_id") String option_id,
                   @Field("user_id") String user_id,
                   Callback<SubmitAnsPojo> response);

    @FormUrlEncoded
    @POST("/endPersonalPoll")
    void endPersonalPoll(@Field("pollid") String pollid,
                   @Field("userid") String userid,
                   Callback<String> response);

    @FormUrlEncoded
    @POST("/submit_private_answer")
    void submitPrivateAns(@Field("question_id") String question_id,
                   @Field("option_id") String option_id,
                   @Field("user_id") String user_id,
                   Callback<SubmitAnsPojo> response);

    @FormUrlEncoded
    @POST("/submit_read_notificcation")
    void submitReadNotification(@Field("notificationId") String notificationId,
                                Callback<SuccessPojo> response);
    @FormUrlEncoded
    @POST("/submit_read_notificcation_public")
    void submitReadNotificationPublic(@Field("notificationId") String notificationId,
                                Callback<SuccessPojo> response);

    @FormUrlEncoded
    @POST("/report_poll")
    void reportPoll(@Field("user_id") String user_id,
                    @Field("report") String report,
                    @Field("pollid") String pollid,
                    @Field("description") String description,
                    Callback<SuccessPojo> response);



    @FormUrlEncoded
    @POST("/get_history")
    void getHistory(@Field("page_no") Integer page_no,
                    @Field("country_id") String country_id, @Field("state_id") String state_id, @Field("city_id") String city_id, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/getCategoryHistory")
    void getCategoryHistory(@Field("page_no") Integer page_no, @Field("categoryid") String catid,@Field("country_id") String country_id, @Field("state_id") String state_id, @Field("city_id") String city_id, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/getFilterHistory")
    void getFilterCategoryHistory(@Field("page_no") Integer page_no,@Field("categoryid") String catid ,@Field("filtertxt") String filtertext, Callback<List<PollPojo>> response);

    @FormUrlEncoded
    @POST("/get_history_by_question_id")
    void getHistoryOption(@Field("question_id") String question_id,
                          @Field("user_id") String user_id,
                          @Field("optionid") String optionid,
                          Callback<List<PollOptionPojo>> response);


    @FormUrlEncoded
    @POST("/getCategories")
    void getCategories(Callback<List<CategoryPojo>> response);


    @POST("/add_group")
    @Multipart
    void createGroup(@Query("user_id") String user_id,
                     @Query("name") String name,
                     @Query("description") String description,
                     @Query("type") String type,
                     @Query("latitude") String latitude,
                     @Query("longitude") String longitude,
                     @Query("address") String address,
                     @Part("image") TypedFile image, Callback<List<GroupPojo>> response);



    @FormUrlEncoded
    @POST("/add_group_participants")
    void addGroupParticipants(@Field("group_id") String group_id,
                              @Field("user_id") String user_id,
                              @Field("status") String status,
                              Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/getMyGroups")
    void getMyGroups(@Field("user_id") String user_id,
                     Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/getBusinessGroups")
    void getBusinessGroups(@Field("user_id") String user_id,
                           @Field("radius") String radius,
                           @Field("latitude") String latitude,
                           @Field("longitude") String longitude,
                     Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/getPublicGroups")
    void getPublicGroups(@Field("user_id") String user_id,
                         @Field("radius") String radius,
                         @Field("latitude") String latitude,
                         @Field("longitude") String longitude,
                     Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/getPersonalGroups")
    void getPersonalGroups(@Field("user_id") String user_id,
                         @Field("radius") String radius,
                         @Field("latitude") String latitude,
                         @Field("longitude") String longitude,
                     Callback<List<GroupPojo>> response);


 @FormUrlEncoded
    @POST("/checkUserinGroup")
    void checkinuserGroup(@Field("user_id") String user_id,
                          @Field("group_id") String group_id,
                     Callback<String> response);

    @FormUrlEncoded
    @POST("/getGroupDetails")
    void getGroupDetails(@Field("group_id") String group_id,
                     Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/getGroupId")
    void getGroupId(@Field("uidchat") String uidchat,
                     Callback<List<GroupPojo>> response);

    @FormUrlEncoded
    @POST("/get_private_polls_group")
    void getParticipatedGroupPolls(@Field("userid") String userid,
                                   @Field("group_id") String group_id,
                         Callback<List<PrivatePollPojo>> response);

    @FormUrlEncoded
    @POST("/getInviteParticipants")
    void getInviteParticipants(@Field("userid") String userid,
                                   @Field("group_id") String group_id,
                         Callback<List<ParticipantsPojo>> response);

@FormUrlEncoded
    @POST("/getGroupParticipants")
    void getGroupParticipants(@Field("group_id") String group_id,
                         Callback<List<ParticipantsPojo>> response);

@FormUrlEncoded
    @POST("/getUserGroupLog")
    void getGroupsLog(@Field("userid") String userid,
                         Callback<List<UserLog>> response);

@FormUrlEncoded
    @POST("/getUserPollLog")
    void getUserPollLog(@Field("userid") String userid,
                         Callback<List<UserLog>> response);


}
