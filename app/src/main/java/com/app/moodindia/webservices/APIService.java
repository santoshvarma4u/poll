package com.app.moodindia.webservices;

import com.app.moodindia.helper.AppConst;
import com.jakewharton.retrofit.Ok3Client;

import okhttp3.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/*import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;*/

public class APIService {

    public APIService() {
    }

    public static <S> S createService(Class<S> serviceClass, final String token) {

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AppConst.MAIN)
                .setLogLevel(RestAdapter.LogLevel.FULL);

        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("token", token);
            }
        });
        RestAdapter adapter = builder.build();
        adapter.setLogLevel(RestAdapter.LogLevel.FULL);
        return adapter.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setClient(new Ok3Client(new OkHttpClient()))
                .setEndpoint(AppConst.MAIN)
                .setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter adapter = builder.build();
        adapter.setLogLevel(RestAdapter.LogLevel.FULL);
        return adapter.create(serviceClass);
    }

    public static <S> S createServiceImages(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setClient(new Ok3Client(new OkHttpClient()))
                .setEndpoint(AppConst.ConsantURL)
                .setLogLevel(RestAdapter.LogLevel.FULL);
        RestAdapter adapter = builder.build();
        adapter.setLogLevel(RestAdapter.LogLevel.FULL);
        return adapter.create(serviceClass);
    }
}
