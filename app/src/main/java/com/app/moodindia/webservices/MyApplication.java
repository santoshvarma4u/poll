package com.app.moodindia.webservices;

import android.support.multidex.MultiDexApplication;

import com.ivankocijan.magicviews.MagicViews;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.ConnectionBuddyConfiguration;

import io.branch.referral.Branch;

/**
 * Created by vinay
 *
 */
public class MyApplication extends MultiDexApplication {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize the Branch object
        Branch.getAutoInstance(this);

      //  Realm.init(this);

        mInstance = this;
        MagicViews.setFontFolderPath(this, "fonts");
        MagicViews.setDefaultTypeFace(this, "ssr.ttf");



        ConnectionBuddyConfiguration networkInspectorConfiguration = new ConnectionBuddyConfiguration.Builder(this).build();
        ConnectionBuddy.getInstance().init(networkInspectorConfiguration);


    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}
