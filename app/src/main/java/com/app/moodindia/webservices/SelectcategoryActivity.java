package com.app.moodindia.webservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class SelectcategoryActivity extends AppCompatActivity {
    Button clearfilter;
    RecyclerView rv_category;
    SearchView sv_category;
    List<CategoryRealmPojo> categoryPojoList;
    CategoryListAdapter cla;
    List<CategoryRealmPojo> filteredcategorypojoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        rv_category=(RecyclerView)findViewById(R.id.rv_category);
        sv_category=(SearchView)findViewById(R.id.sv_category);
        clearfilter=(Button) findViewById(R.id.clearFilter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectcategoryActivity.this);
        rv_category.setLayoutManager(mLayoutManager);
        rv_category.setItemAnimator(new DefaultItemAnimator());
        categoryPojoList=new ArrayList<>();
        clearfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("Categoryname","0");
                intent.putExtra("CategoryId","0");
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        sv_category.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                cla.applyFilter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                cla.applyFilter(newText);
                return true;
            }
        });
    }


    public class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<CategoryRealmPojo> CategorypojoList;
        public CategoryListAdapter(List<CategoryRealmPojo> categorypojoList) {
            filteredcategorypojoList=new ArrayList<>();
            this.CategorypojoList = categorypojoList;
            filteredcategorypojoList.addAll(categorypojoList);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_country, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SelectcategoryActivity.ViewHolderFilter vh2 = (SelectcategoryActivity.ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        public void applyFilter(String query)
        {
            if(query.length() ==0)
                filteredcategorypojoList.addAll(categoryPojoList);
            else
            {
                filteredcategorypojoList.clear();
                for(CategoryRealmPojo cp:categoryPojoList)
                {
                    if(cp.getCat_name().toLowerCase().contains(query.toLowerCase()))
                    {
                        filteredcategorypojoList.add(cp);
                    }
                }

            }
            notifyDataSetChanged();

        }

        @Override
        public int getItemCount() {
            return filteredcategorypojoList.size();
        }
        CategoryRealmPojo listCategory;

        public void configureViewHolderFilter(final SelectcategoryActivity.ViewHolderFilter holder, final int position) {
            listCategory=filteredcategorypojoList.get(position);
            holder.CountryName.setText(listCategory.getCat_name());
            holder.CountryId=listCategory.getCat_id();
        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CountryName;
        String CountryId;


        public ViewHolderFilter(View itemView) {
            super(itemView);
            CountryName=(TextView)itemView.findViewById(R.id.tv_countryname);


            CountryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent();
                    intent.putExtra("Categoryname",filteredcategorypojoList.get(getAdapterPosition()).getCat_name());
                    intent.putExtra("CategoryId",filteredcategorypojoList.get(getAdapterPosition()).getCat_id());
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCategory();
    }

    private void getCategory() {
        M.showLoadingDialog(SelectcategoryActivity.this);

        CategoryAPI mAuthenticationAPI = APIService.createService(CategoryAPI.class);
        mAuthenticationAPI.getCategories("",new retrofit.Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(List<CategoryRealmPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        categoryPojoList=pojo;

                        cla=new SelectcategoryActivity.CategoryListAdapter(categoryPojoList);

                        rv_category.setAdapter(cla);

                        M.hideLoadingDialog();
                    }else{
                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("fsilure","fail"+error.getMessage());
                return;
            }
        });
    }
}
