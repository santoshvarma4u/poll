package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AboutUs  extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        addSlide(AppIntroFragment.newInstance("Mood India", "Mood India is the India's first independent real time polling application.And regional issues with complete transparency and is devised to create user experience like never before.", R.drawable.sintro1, R.color.white));
        addSlide(AppIntroFragment.newInstance("Mood India", "User can make a poll and learn about others opinion using various filters Location, Age, Gender, Income level, Qualifications, Occupation etc that enables users to customize information as they want it.", R.drawable.sintro2, R.color.white));
        addSlide(AppIntroFragment.newInstance("Mood India", "Now participate in democracy in most democratic way.", R.drawable.sintro3, R.color.white));


     //   setContentView(R.layout.activity_about_us);
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        TextView tv=(TextView)findViewById(R.id.tvabout_detail);
        tv.setText("Mood India is the India's first independent real time polling application to capture nation's mood and opinion on key national, and regional issues with complete transparency and is devised to create user experience like never before. User can make a poll and learn about others opinion using various filters Location, Age, Gender, Income level, Qualifications, Occupation etc that enables users to customize information as they want it. There are also other features such as trends, archives, different categories to chose and much more that make it distinctive and amazing to use. Now participate in democracy in most democratic way. ");
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssr.ttf");

        tv.setTypeface(custom_font);*/
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent it = new Intent(AboutUs.this, SettingActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent it = new Intent(AboutUs.this, SettingActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(AboutUs.this,SettingActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
}
