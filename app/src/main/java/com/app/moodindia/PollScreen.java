package com.app.moodindia;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.moodindia.adapter.OptionAdapter;
import com.app.moodindia.chatmod.adapters.MainAdapter;
import com.app.moodindia.chatmod.models.Message;
import com.app.moodindia.chatmod.utils.ProfanityFilter;
import com.app.moodindia.chatmod.utils.SCUtils;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.SimpleGestureFilter;
import com.app.moodindia.helper.SimpleGestureFilter.SimpleGestureListener;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.ui.ActivityLogs.Logs_activity;
import com.app.moodindia.ui.publicpolls.PublicHomePage;
import com.app.moodindia.ui.topics.SearchTopicsActivity;
import com.app.moodindia.util.DisplayUtil;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PollAPI;
import com.bluejamesbond.text.DocumentView;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.github.florent37.tutoshowcase.TutoShowcase;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wooplr.spotlight.SpotlightView;
import com.wooplr.spotlight.utils.SpotlightListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import retrofit.RetrofitError;
import xyz.hanks.library.SmallBang;


public class PollScreen extends YouTubeBaseActivity implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener,
        YouTubePlayer.OnInitializedListener,SimpleGestureListener, com.app.moodindia.Reactions.reaction.ReactionView.SelectedReaction,com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener{

    TextView tvque,tvdate,tvPollCount,txtExp,commentscount,tvprevpoll,categoryname,likecount;
    LinearLayout lloptions,lytmap;
    ScrollView svoptions;
    Button btnsubmit;
    //FrameLayout frameLayout;
    //ImageView ivpoll;
    ListView lvoption;
    Bitmap mbitmap;
    File  imagePath;
    String TAG="pollscreen",pollid="",userid="";
    String info,que,start,end,type,img="",queid,seloptions="",totalPolls,twittertag,youtubetag,chattag;
    int single;
    Boolean isvoted;
    Button tvinfo;
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    ArrayList<String> idlist=new ArrayList<>();
    private Button btnViewMaps;
    private Button btnSuggestPoll;
    MaterialDialog materialDialog;
    OptionAdapter adapter;
    ImageView ivpoll,ivprevpoll,ivnextpoll;
    boolean isConnected;
    private BottomSheetBehavior mBottomSheetBehavior;
    private WebView wv_chat_onscreen;
    private KenBurnsView kv_poll_background;

   private SimpleGestureFilter detector;

    private ImageView ivLike;
    private TextView tvLike;

    ImageView iv_backnav,iv_report_poll,iv_share_poll;


    //chat module inits
    public static final int ANTI_FLOOD_SECONDS = 3; //simple anti-flood
    private boolean IS_ADMIN = false; //set this to true for the admin app.
    String username = "anonymous"; //default username
    private boolean PROFANITY_FILTER_ACTIVE = true;
    private FirebaseDatabase database;
    private RecyclerView main_recycler_view;
    private PollScreen mContext;
    private MainAdapter madapter;
    private String userID;
    private DatabaseReference databaseRef;
    private ImageView imageButton_send,iv_chat_window_action;
    private EditText editText_message;
    ArrayList<Message> messageArrayList = new ArrayList<>();
    private ProgressBar progressBar;
    private long last_message_timestamp = 0;
    String nextpollid="0";
    String prevpollid="0";
    RelativeLayout navbtnlayout;
    SlidingUpPanelLayout sliding_layout;
    LinearLayout lytChatView;
    ProgressWheel progressWheelsending;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

       // supportPostponeEnterTransition();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_screen);

        detector = new SimpleGestureFilter(this,this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Fade().setDuration(500));
            getWindow().setExitTransition(new Fade().setDuration(500));
        }

        //chat mods inits
        mContext = PollScreen.this;
        username = M.getUsername(mContext);

        main_recycler_view = (RecyclerView) findViewById(R.id.main_recycler_view);
        imageButton_send = (ImageView) findViewById(R.id.imageButton_send);
        iv_chat_window_action = (ImageView) findViewById(R.id.iv_chat_window_action);
        progressWheelsending = (ProgressWheel) findViewById(R.id.progressWheelsending);
        editText_message = (EditText) findViewById(R.id.editText_message);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        database = FirebaseDatabase.getInstance();
        databaseRef = database.getReference();
        sliding_layout=(SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        navbtnlayout=(RelativeLayout)findViewById(R.id.navbtnlayout);
navbtnlayout.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, final MotionEvent event) {
        detector.onTouchEvent(event);
        return true;
    }
});




        sliding_layout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);
                if(newState.toString().equalsIgnoreCase("EXPANDED"))
                {
                    navbtnlayout.setVisibility(View.GONE);
                    iv_chat_window_action.setImageResource(R.drawable.ic_clear);
                }
                else if(newState.toString().equalsIgnoreCase("COLLAPSED"))
                {
                    navbtnlayout.setVisibility(View.VISIBLE);
                    iv_chat_window_action.setImageResource(R.drawable.ic_arrow_upward);
                }
                else
                {
                    navbtnlayout.setVisibility(View.GONE);
                }
            }
        });
        iv_chat_window_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sliding_layout.getPanelState().toString().equalsIgnoreCase("EXPANDED"))
                {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                else {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });


     /*   sliding_layout.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onPanelOpened(View panel) {
                //hide nav
                navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onPanelClosed(View panel) {
                //visible nav
                navbtnlayout.setVisibility(View.VISIBLE);
            }
        });*/
        ivLike=(ImageView) findViewById(R.id.ivliketype);
        ivprevpoll=(ImageView) findViewById(R.id.ivprevpoll);
        ivnextpoll=(ImageView) findViewById(R.id.ivnextpoll);
        tvLike=(TextView) findViewById(R.id.tvlikeview);
        likecount=(TextView) findViewById(R.id.likecount);
        categoryname=(TextView) findViewById(R.id.catname);
        commentscount=(TextView) findViewById(R.id.commentscount);

        lytChatView=(LinearLayout)findViewById(R.id.lytChatView);

        ivprevpoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextPoll();
            }
        });

        ivnextpoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevPoll();
            }
        });

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showReactionPopup(v);
                } else {
                    showReactionPopupPreLollipop(v);
                }

            }
        });
        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showReactionPopup(v);
                } else {
                    showReactionPopupPreLollipop(v);
                }
            }
        });

        ivLike.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showReactionPopup(v);
                } else {
                    showReactionPopupPreLollipop(v);
                }
                return false;
            }
        });
        tvLike.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    showReactionPopup(v);
                } else {
                    showReactionPopupPreLollipop(v);
                }
                return false;
            }
        });
        main_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        madapter = new MainAdapter(mContext, messageArrayList);
        main_recycler_view.setAdapter(madapter);


        imageButton_send.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(editText_message.getText().toString().trim().length() > 0)
                        {
                           // M.showLoadingDialog(PollScreen.this);

                            new backTask().execute();
                           //
                        }
                    }
                });
            }
        });

        editText_message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_SEND)) {
                    imageButton_send.performClick();
                }
                return false;
            }
        });




        iv_backnav = (ImageView) findViewById(R.id.iv_backnav);
        iv_report_poll = (ImageView) findViewById(R.id.iv_report_poll);
        iv_share_poll = (ImageView) findViewById(R.id.iv_share_poll);

        iv_backnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(M.getID(PollScreen.this).equalsIgnoreCase("0"))
                {
                    Intent it=new Intent(PollScreen.this,PublicHomePage.class);
                    startActivity(it);
                    finish();
                    //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }
                else if(getIntent().getExtras().containsKey("searchquery"))
                {
                    Intent it=new Intent(PollScreen.this,SearchTopicsActivity.class);
                    it.putExtra("searchquery",getIntent().getExtras().get("searchquery").toString());
                    startActivity(it);
                    finish();
                }

                else if(getIntent().getExtras().containsKey("fragName"))
                {
                    Intent it=new Intent(PollScreen.this,HomeActivity.class);
                    it.putExtra("fragName",getIntent().getExtras().get("fragName").toString());
                    startActivity(it);
                    // supportFinishAfterTransition();
                    finish();
                    overridePendingTransition(0,0);
                }
                else if(getIntent().getExtras().containsKey("by"))
                {
                    Intent it=new Intent(PollScreen.this,Logs_activity.class);
                    startActivity(it);
                    finish();
                    // supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }
                else
                {
                    Intent it=new Intent(PollScreen.this,HomeActivity.class);
                    startActivity(it);
                    finish();
                    //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }
            }
        });




        iv_report_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(PollScreen.this,ReportPollActivity.class);
                intent.putExtra("pollid",pollid);
                startActivity(intent);
            }
        });

        iv_share_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                    }
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                            View v1 = getWindow().getDecorView().getRootView();
                            v1.setDrawingCacheEnabled(true);
                            Bitmap myBitmap = v1.getDrawingCache();
                            saveBitmap(myBitmap);
                        }
                        else {
                            sharepollkitkat();
                        }
                    }
                }
                else
                {
                    sharepollkitkat();
                }
            }
        });


        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
       // window.setStatusBarColor(ContextCompat.getColor(PollScreen.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
/*        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        pollid=getIntent().getExtras().getString("pollid");
        //Log.d("poll",pollid);
        userid= M.getID(PollScreen.this);
        tvque=(TextView)findViewById(R.id.tvque);
        tvdate=(TextView)findViewById(R.id.tvdate);
        tvinfo=(Button)findViewById(R.id.tvinfo);
        txtExp=(TextView)findViewById(R.id.txtExp);
        tvPollCount=(TextView)findViewById(R.id.tvPollCount);
        lloptions=(LinearLayout)findViewById(R.id.lloption);
        svoptions=(ScrollView) findViewById(R.id.svoptions);
        btnsubmit=(Button)findViewById(R.id.btnsubmit);
        btnsubmit.setVisibility(View.GONE);
        kv_poll_background=(KenBurnsView)findViewById(R.id.kv_poll_background);

       /* frameLayout=(FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);*/
        btnSuggestPoll = (Button) findViewById(R.id.btnSuggestPoll);
        ivpoll=(ImageView) findViewById(R.id.ivpoll);
       // ivpoll.pause();
        lvoption=(ListView)findViewById(R.id.lvotedoption);
       // kvpoll=(KenBurnsView)findViewById(R.id.kvpoll);
        lvoption.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lytmap=(LinearLayout)findViewById(R.id.lytmap);
        setListViewHeightBasedOnChildren(lvoption);
       // getPollData();
        btnsubmit.setOnClickListener(this);
        tvinfo.setOnClickListener(this);
        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        btnViewMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PollScreen.this,ResultMapActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("from",getIntent().getExtras().getString("from"));
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PollScreen.this,SuggestPollActivity.class);
                startActivity(intent);
            }
        });

     //   mBottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet));
       // mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//mBottomSheetBehavior.setHideable(false);
        //wv_chat_onscreen =(WebView)findViewById(R.id.wv_chat_onscreen);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        checkConnection();

        if(isConnected)
        {
            if(getIntent().getExtras().containsKey("from"))
                getPollData(getIntent().getExtras().getString("from"));
        }

    }


    private boolean scaledDown;

    @Override
    public void onPlaying() {
        navbtnlayout.setVisibility(View.GONE);
    }

    @Override
    public void onPaused() {
        navbtnlayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopped() {
        navbtnlayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBuffering(boolean b) {
        navbtnlayout.setVisibility(View.GONE);
    }

    @Override
    public void onSeekTo(int i) {
        navbtnlayout.setVisibility(View.GONE);
    }


    private class backTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            process_new_message(editText_message.getText().toString().trim(), false);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressWheelsending.setVisibility(View.VISIBLE);
            imageButton_send.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            editText_message.setText("");
            progressWheelsending.setVisibility(View.GONE);
            imageButton_send.setVisibility(View.VISIBLE);
        }
    }


  /* @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        // Call onTouchEvent of SimpleGestureFilter class
    this.detector.onTouchEvent(me);
     return super.dispatchTouchEvent(me);
   }*/
    @Override
    public void onSwipe(int direction) {


        switch (direction) {

            case  SimpleGestureFilter.SWIPE_RIGHT : nextPoll();
                break;
            case SimpleGestureFilter.SWIPE_LEFT : prevPoll();
                break;

        }
       // Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDoubleTap() {
        //Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

    public void nextPoll()
    {
        if(getIntent().getExtras().containsKey("searchquery"))
        {
           //swipe disbaled for search results
        }
        else
        {
            if(nextpollid.equalsIgnoreCase("0"))
            {
                Toast.makeText(PollScreen.this, "No more polls found in this topic.", Toast.LENGTH_SHORT).show();
            }
            else {
                // Toast.makeText(this, "Next Poll", Toast.LENGTH_SHORT).show();
                Intent intent1=new Intent(PollScreen.this,PollScreen.class);
                intent1.putExtra("from","active");
                intent1.putExtra("pollid",nextpollid);
                finish();
                startActivity(intent1);
                this.overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);

            }
        }
    }
    public void prevPoll()
    {
        if(getIntent().getExtras().containsKey("searchquery"))
        {
            //swipe disbaled for search results
        }
        else
        {
            if(prevpollid.toLowerCase().equalsIgnoreCase("0"))
            {
                Toast.makeText(PollScreen.this, "No more polls found in this topic.", Toast.LENGTH_SHORT).show();
            }
            else {
                //  Toast.makeText(this, "Prev Poll", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(PollScreen.this,PollScreen.class);
                intent.putExtra("from","active");
                intent.putExtra("pollid",prevpollid);
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        }
    }
    private void loadChatWebview(WebView wvTlk,String pollid,String username)
    {

        WebSettings mWebSettings = wvTlk.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);
        mWebSettings.setLoadWithOverviewMode(true);
        // Other webview options
        mWebSettings.setLoadWithOverviewMode(true);

        wvTlk.setScrollbarFadingEnabled(false);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setPluginState(WebSettings.PluginState.ON);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setRenderPriority(
                WebSettings.RenderPriority.HIGH);
        mWebSettings.setCacheMode(
                WebSettings.LOAD_NO_CACHE);
        mWebSettings.setAppCacheEnabled(true);
        wvTlk.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSavePassword(true);
        mWebSettings.setSaveFormData(true);
        mWebSettings.setEnableSmoothTransition(true);
        //System.out.println("http://embed.tlk.io/MIPOLL"+pollid+"?nickname="+username+"&theme=theme--minimal");
        // wvTlk.loadUrl("http://moodindia.in/storyapi/chat/index.php?r="+mParam2+"&u="+M.getUsername(getActivity()));
        // http://moodindia.in/storyapi/chat/?r=MIPOLL44&u=kalyanu

        wvTlk.loadUrl("http://embed.tlk.io/MIPOLL"+pollid+"?nickname="+username+"&theme=theme--minimal");
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras().containsKey("searchquery"))
        {
            navbtnlayout.setVisibility(View.GONE);
        }

    }

    public void realtimeLoading()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds

                if(getIntent().getExtras().containsKey("from"))
                {
                    if(getIntent().getExtras().getString("from").equalsIgnoreCase("active"))
                    {
                        getRealTimePollData();
                        handler.postDelayed(this, 10000);
                    }
                }
                  //  getPollData(getIntent().getExtras().getString("from"));

            }
        }, 500);
    }

    int totalReactions=0;
    boolean isVoteLocal=false;
    public String catname="";
    public String pollState="",pollCity="";

    private SmallBang mSmallBang;
    Bitmap bmPollImage;
    private void getPollData(String active) {

        M.showLoadingDialog(PollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid,pollid,active,M.getMyStateID(PollScreen.this),M.getMyCityID(PollScreen.this),new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {
                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            tvinfo.setEnabled(true);
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            catname=pojo.getCat_name();
                            categoryname.setText(catname);
                            nextpollid=pojo.getNextid();
                            prevpollid=pojo.getPrevid();
                            totalReactions = Integer.parseInt(pojo.getLikecount());
                            pollState=pojo.getPollState();
                            pollCity=pojo.getPollCity();
                            likecount.setText(" "+totalReactions+"");

                            if(pojo.getYoutubelink().length()>5)
                            {
                                youTubeView.initialize(AppConst.YOUTUBE_API_KEY, PollScreen.this);
                                youtubeLink=pojo.getYoutubelink().toString().substring(pojo.getYoutubelink().toString().lastIndexOf("=") + 1);
                                Log.d("Youutube",youtubeLink);
                              // =pojo.getYoutubelink().toString();
                            }
                            else
                            {
                                youTubeView.setVisibility(View.GONE);
                                ivpoll.setVisibility(View.VISIBLE);
                            }

                            if(M.getID(PollScreen.this).equalsIgnoreCase("0"))
                                lytChatView.setVisibility(View.GONE);

                            if(isvoted)
                            {
                                btnViewMaps.setVisibility(View.VISIBLE);
                             //   frameLayout.setVisibility(View.VISIBLE);

                                if(getIntent().getExtras().containsKey("stats")){
                                    tvinfo.performClick();
                                }
                                else
                                {
                                        tvque.setBackgroundColor(Color.parseColor("#e3e9ef"));
                                         tvque.getBackground().setAlpha(51);
                                    tvque.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    //  tvque.setBackgroundColor(getResources().getColor(R.color.light_green));
                                    realtimeLoading();
                                }
                            }
                            else
                            {
                                btnViewMaps.setVisibility(View.GONE);
                            }

                            if(pojo.getReaction().length()>1)
                            {
                                if (pojo.getReaction().equalsIgnoreCase("like")){
                                    ivLike.setImageResource(R.drawable.like);
                                    tvLike.setText("Like");
                                }else if (pojo.getReaction().equalsIgnoreCase("love")){

                                    ivLike.setImageResource(R.drawable.love);
                                    tvLike.setText("Love");
                                }else if (pojo.getReaction().equalsIgnoreCase("haha")){

                                    ivLike.setImageResource(R.drawable.haha);
                                    tvLike.setText("Haha");
                                }else if (pojo.getReaction().equalsIgnoreCase("wow")){
                                    ivLike.setImageResource(R.drawable.wow);
                                    tvLike.setText("Wow");

                                }else if (pojo.getReaction().equalsIgnoreCase("sad")){
                                    ivLike.setImageResource(R.drawable.sad);
                                    tvLike.setText("Sad");

                                }else if (pojo.getReaction().equalsIgnoreCase("angry")){
                                    ivLike.setImageResource(R.drawable.angry);
                                    tvLike.setText("Angry");
                                }
                            }

                            tvque.setText(que);
                            if (img != "") {
                                Picasso.with(PollScreen.this)
                                        .load(AppConst.imgurl + img)
                                        .placeholder(R.drawable.default_image)
                                        .error(R.drawable.default_image)
                                        .into(ivpoll);

                                Handler uiHandler = new Handler(Looper.getMainLooper());
                                uiHandler.post(new Runnable(){
                                    @Override
                                    public void run() {
                                        try {
                                            Picasso.with(PollScreen.this).load(AppConst.imgurl+img).into(new Target() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                                    Bitmap afterBlur=fastblur(bitmap,1,20);
                                                    kv_poll_background.setImageBitmap(afterBlur);

                                                }

                                                @Override
                                                public void onBitmapFailed(Drawable errorDrawable) {

                                                }

                                                @Override
                                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                                }
                                            });




                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }

                            try {
                                tvdate.setText("Posted on "+dtfmt.format(defaultfmt.parse(start)));
                                Date dt=defaultfmt.parse(end);
                               // txtExp.setVisibility(View.VISIBLE);
                                RelativeTimeTextView v = (RelativeTimeTextView)findViewById(R.id.timestamp);
                                v.setReferenceTime(dt.getTime());

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(!isvoted) {
                               // getBrandDialog();
                               // mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

                                for (final Pollresult data : pojo.getPollresult()) {

                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(PollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(14);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(PollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 5);
                                    lloptions.addView(tv, layoutParams);
                                  //  lytmap.setVisibility(View.GONE);
                                }

                            }else{
                                lloptions.setVisibility(View.GONE);
                                svoptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);

                                 adapter = new OptionAdapter(PollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);

                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                            }
                        }


                        M.hideLoadingDialog();
                        nextPollSpotlightView();

                        // supportStartPostponedEnterTransition();

                        if(isvoted)
                        {
                            lytChatView.setVisibility(View.VISIBLE);
                            username=M.getUsername(PollScreen.this);
                            makeChatReady(queid,username);
                        }
                        else
                        {
                            lytChatView.setVisibility(View.GONE);
                        }

                    }
                    else {
                        Toast.makeText(PollScreen.this, "Poll Id is invalid, please try again", Toast.LENGTH_SHORT).show();
                        Intent it=new Intent(PollScreen.this,HomeActivity.class);
                        startActivity(it);
                        //  supportFinishAfterTransition();
                        overridePendingTransition(0,0);
                    }

                }
                else
                {
                    Toast.makeText(PollScreen.this, "Poll Id is invalid, please try again", Toast.LENGTH_SHORT).show();
                    Intent it=new Intent(PollScreen.this,HomeActivity.class);
                    startActivity(it);
                    //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }

               // ivpoll.resume();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }


    private void getRealTimePollData() {
        //AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
       // M.showLoadingDialog(PollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid,pollid,"active",M.getMyStateID(PollScreen.this),M.getMyCityID(PollScreen.this),new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {
                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            //Log.d("info",info);
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            catname=pojo.getCat_name();
                            nextpollid=pojo.getNextid();
                            prevpollid=pojo.getPrevid();
                            categoryname.setText(catname);
                            totalReactions = Integer.parseInt(pojo.getLikecount());

                            if(pojo.getReaction().length()>2)
                            {
                                if (pojo.getReaction().equalsIgnoreCase("like")){
                                    ivLike.setImageResource(R.drawable.like);
                                    tvLike.setText("Like");
                                }else if (pojo.getReaction().equalsIgnoreCase("love")){

                                    ivLike.setImageResource(R.drawable.love);
                                    tvLike.setText("Love");
                                }else if (pojo.getReaction().equalsIgnoreCase("haha")){

                                    ivLike.setImageResource(R.drawable.haha);
                                    tvLike.setText("Haha");
                                }else if (pojo.getReaction().equalsIgnoreCase("wow")){
                                    ivLike.setImageResource(R.drawable.wow);
                                    tvLike.setText("Wow");

                                }else if (pojo.getReaction().equalsIgnoreCase("sad")){
                                    ivLike.setImageResource(R.drawable.sad);
                                    tvLike.setText("Sad");

                                }else if (pojo.getReaction().equalsIgnoreCase("angry")){
                                    ivLike.setImageResource(R.drawable.angry);
                                    tvLike.setText("Angry");
                                }
                            }

                            if(!isvoted) {

                                btnViewMaps.setVisibility(View.GONE);

                                for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(PollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(14);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(PollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 25);
                                    lloptions.addView(tv, layoutParams);
                                    lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                btnViewMaps.setVisibility(View.VISIBLE);
                              //  frameLayout.setVisibility(View.VISIBLE);
                                lloptions.setVisibility(View.GONE);
                                svoptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                OptionAdapter adapter = new OptionAdapter(PollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
               // M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnsubmit){
            String ans="";
            for(int i=0;i<idlist.size();i++){
                if(i==0)
                    ans=idlist.get(i);
                else
                    ans=ans+","+idlist.get(i);
            }
            submitresult(ans);
        }else if(view.getId()==R.id.tvinfo){
            Intent it=new Intent(PollScreen.this,InfoActivity.class);
            it.putExtra("info",info);
            it.putExtra("question",que);
            it.putExtra("image",img);
            it.putExtra("screen","pollscreen");
            it.putExtra("twittertag",twittertag);
            it.putExtra("youtubetag",youtubetag);
            it.putExtra("chattag",chattag);
            it.putExtra("startdate",start);
            it.putExtra("enddate",end);
            it.putExtra("isvoted",isvoted);
            it.putExtra("from",getIntent().getExtras().getString("from"));
            it.putExtra("pollid",pollid);

            startActivity(it);
        }
    }

    public void submitresult(final String ans){
        //Log.d(TAG,"ans:"+ans);
        M.showLoadingDialog(PollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.submitAns(queid,ans,M.getID(PollScreen.this),new retrofit.Callback<SubmitAnsPojo>() {
            @Override
            public void success(SubmitAnsPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess()==1){
                       // AppConst.selpoll=null;
                        PollOptionPojo obj=new PollOptionPojo();
                        obj.setEndtime(end);
                        obj.setImage(img);
                        obj.setInfo(info);
                        obj.setIsvoted(true);
                        obj.setSelected_option(ans);
                        obj.setPollid(Integer.valueOf(queid));
                        obj.setPollresult(pojo.getPollresult());
                        obj.setQuestion(que);
                        obj.setStarttime(start);
                        obj.setType(type);
                        AppConst.selpoll=obj;
                        M.hideLoadingDialog();

                        if(M.getPollAnswerPopup(PollScreen.this).equalsIgnoreCase("1"))
                        {
                            Intent it=new Intent(PollScreen.this,PollScreen.class);
                            it.putExtra("pollid",pollid);
                            it.putExtra("from","active");
                            finish();
                            startActivity(it);
                        }
                        else {
                            final DialogPlus notificationDialog = DialogPlus.newDialog(PollScreen.this)
                                    .setGravity(Gravity.CENTER)
                                    .setContentHolder(new ViewHolder(R.layout.custom_layout_pollpop_alert))
                                    .setCancelable(true)
                                    .create();


                            View view=notificationDialog.getHolderView();

                            final TextView tvNoticont=(TextView) view.findViewById(R.id.tv_noticont);
                            final TextView tv_notification=(TextView) view.findViewById(R.id.tv_notification);
                            final CheckBox cb_dontsee=(CheckBox) view.findViewById(R.id.cb_dontsee);
                            final DocumentView dv=(DocumentView)view.findViewById(R.id.blogText) ;
                            Typeface type = Typeface.createFromAsset(getAssets(),"fonts/ssr.ttf");
                            dv.getDocumentLayoutParams().setTextTypeface(type);
                            dv.setText("Dear "+M.getUsername(PollScreen.this)+" \n\n We appreciate your vote and consider it as your proactive contribution to reflect the true MOOD of our democratic  nation. We believe your vote even at an unit level contributes a lot in building our nation at all levels social, political, economic, sports,  entertainment etc \nKindly, see our stats page to run your own analysis on the issue or topic in the most flexible way and see how citizens across India are thinking on the issue. \nPlease share your proclaim as proactive citizen on Facebook, one share worth inspiring others to join you in the wave to gauge unbiased and true mood of our nation.\nRaise your voice because your voice matters.");

                            tvNoticont.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(cb_dontsee.isChecked())
                                    {
                                        M.setNoPollAnswerPopup("1",PollScreen.this);
                                    }
                                    Intent it=new Intent(PollScreen.this,PollScreen.class);
                                    it.putExtra("pollid",pollid);
                                    it.putExtra("from","active");
                                    finish();
                                    startActivity(it);
                                    notificationDialog.dismiss();
                                }
                            });
                            notificationDialog.show();
                        }





                      /*  new SweetAlertDialog(PollScreen.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Mood India")
                                .setContentText("Thanyou for your vote.Do you want to suggest a poll?")
                                .setConfirmText("Yes,I want!")
                                .setCancelText("No")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        pollid=getIntent().getExtras().getString("pollid");

                                        Intent it=new Intent(PollScreen.this,PollScreen.class);
                                        it.putExtra("pollid",pollid);
                                        it.putExtra("from","active");
                                        finish();
                                        startActivity(it);
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {

                                        Intent intent=new Intent(PollScreen.this,SuggestPollActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .show();*/




                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_poll,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
      /*  if(id==android.R.id.home)
            supportFinishAfterTransition();
        else*/ if(id==R.id.share){


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(PollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else {
                        sharepollkitkat();
                    }
                }
            }
            else
            {
                sharepollkitkat();
            }

            /*Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            //this is the text that will be shared
           // sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));*/
            return true;
        }

        else if(id==R.id.report){
            Intent intent=new Intent(PollScreen.this,ReportPollActivity.class);
            intent.putExtra("pollid",pollid);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(PollScreen.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(M.getID(PollScreen.this).equalsIgnoreCase("0"))
        {
            Intent it=new Intent(this,PublicHomePage.class);
            startActivity(it);
            //  supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else if(getIntent().getExtras().containsKey("searchquery"))
        {
            Intent it=new Intent(PollScreen.this,SearchTopicsActivity.class);
            it.putExtra("searchquery",getIntent().getExtras().get("searchquery").toString());
            startActivity(it);
            finish();
        }

        else if(getIntent().getExtras().containsKey("fragName"))
        {
            Intent it=new Intent(this,HomeActivity.class);
            it.putExtra("fragName",getIntent().getExtras().get("fragName").toString());
            startActivity(it);
           // supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else if(getIntent().getExtras().containsKey("by"))
        {
            Intent it=new Intent(this,Logs_activity.class);
            startActivity(it);
            finish();
           // supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else
        {
            Intent it=new Intent(this,HomeActivity.class);
            startActivity(it);
          //  supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }

    }


    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

    public void sharepoll(String path) {

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/gif");
        Uri myUri = Uri.parse("file://" + path);

        //sharePollDialog(myUri,path);
        String shareText="I just saw this awesome poll. " +
                "Please participate and express your opinion. \nTopic: " + catname + " \n"+que+"" +
                " \nPoll Id: "+pollid+" \nRegion:\nCity : "+ pollCity +"\nState : "+pollState+" \nCountry : India \nhttp://moodindia.in/p/"+pollid+ " \n \nDownload Mood India \nhttps://play.google.com/store/apps/details?id=com.app.moodindia";

        sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (shareText));
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, (shareText));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }
    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        String shareText="I just saw this awesome poll. " +
                "Please participate and express your opinion. \nTopic: " + catname + " \n"+que+"" +
                " \nPoll Id: " + pollid + "\nRegion:\nCity : "+ pollCity +"\nState : "+pollState+" \nCountry : India \nhttp://moodindia.in/p/"+pollid+ " \n \nDownload Mood India \nhttps://play.google.com/store/apps/details?id=com.app.moodindia";

        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, (shareText));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }

    public void sharePollDialog(final Uri imageUri,final String path)
    {
        final String shareText="I just saw this awesome poll. " +
                "Please participate and express your opinion. \nTopic: " + catname + " \n"+que+"" +
                " \nPoll Id: "+pollid+" \nRegion:\nCity : "+ pollCity +"\nState : "+pollState+" \nCountry : India \nhttp://moodindia.in/p/"+pollid+ " \n \nDownload Mood India \nhttps://play.google.com/store/apps/details?id=com.app.moodindia";


        final DialogPlus shareDialog = DialogPlus.newDialog(PollScreen.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_share_layout))
                .setCancelable(true)
                .create();

        final String facebookSharemsg=que+" \n \n "+M.getUsername(PollScreen.this)+" .is proactive citizen of India contributing in nation's functioning proactively. We are proud of "+M.getUsername(PollScreen.this)+" and many congratulations from MOODINDIA for earning proactive citizenship badge and Paytm reward.  To earn your proactive citizenship badge and earn cash rewards download the MOODINDIA app today . \n \nDownload Mood India \nhttps://play.google.com/store/apps/details?id=com.app.moodindia";

        View view=shareDialog.getHolderView();
        final ShareDialog fshareDialog = new ShareDialog(this);
        final LinearLayout lytFacebook=(LinearLayout) view.findViewById(R.id.lytFacebook);
        final LinearLayout lytOthers=(LinearLayout) view.findViewById(R.id.lytothers);
        lytFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareDialog.dismiss();

                View v1 = getWindow().getDecorView().getRootView();
                v1.setDrawingCacheEnabled(true);
                Bitmap myBitmap = v1.getDrawingCache();

                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(myBitmap)
                        .setCaption(facebookSharemsg)
                        .build();

                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();

                fshareDialog.show(content);

              /*  ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentDescription(facebookSharemsg)
                        .setContentTitle("Android Facebook Integration")
                        .build();
                fshareDialog.show(content);*/


              /*  Uri uri = imageUri;
                Shareable imageShare = new Shareable.Builder(PollScreen.this)
                        .message(facebookSharemsg)
                        .socialChannel(Shareable.Builder.FACEBOOK)
                        .build();
                imageShare.share();*/
            }
        });
        lytOthers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareDialog.dismiss();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("image/gif");
                Uri myUri = Uri.parse("file://" + path);

                String shareText="I just saw this awesome poll. " +
                        "Please participate and express your opinion. \nTopic: " + catname + " \n"+que+"" +
                        " \nPoll Id: "+pollid+" \nRegion:\nCity : "+ pollCity +"\nState : "+pollState+" \nCountry : India \nhttp://moodindia.in/p/"+pollid+ " \n \nDownload Mood India \nhttps://play.google.com/store/apps/details?id=com.app.moodindia";
                sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
                sendIntent.putExtra(Intent.EXTRA_TEXT, (shareText));
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, (shareText));
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
            }
        });
        shareDialog.show();
    }


    public void getBrandDialog()
    {
        Intent intent=new Intent(PollScreen.this,BrandsActivity.class);
        startActivityForResult(intent,1);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }

    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) { return (null); } int w = bitmap.getWidth(); int h = bitmap.getHeight(); int[] pix = new int[w * h]; Log.e("pix", w + " " + h + " " + pix.length); bitmap.getPixels(pix, 0, w, 0, 0, w, h); int wm = w - 1; int hm = h - 1; int wh = w * h; int div = radius + radius + 1; int r[] = new int[wh]; int g[] = new int[wh]; int b[] = new int[wh]; int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; int vmin[] = new int[Math.max(w, h)]; int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) { p = pix[yi + Math.min(wm, Math.max(i, 0))]; sir = stack[i + radius]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) { r[yi] = dv[rsum]; g[yi] = dv[gsum]; b[yi] = dv[bsum]; rsum -= routsum; gsum -= goutsum; bsum -= boutsum; stackstart = stackpointer - radius + div; sir = stack[stackstart % div]; routsum -= sir[0]; goutsum -= sir[1]; boutsum -= sir[2]; if (y == 0) { vmin[x] = Math.min(x + radius + 1, wm); } p = pix[yw + vmin[x]]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) { yi = Math.max(0, yp) + x; sir = stack[i + radius]; sir[0] = r[yi]; sir[1] = g[yi]; sir[2] = b[yi]; rbs = r1 - Math.abs(i); rsum += r[yi] * rbs; gsum += g[yi] * rbs; bsum += b[yi] * rbs; if (i > 0) {
                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];
            } else {
                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];
            }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    //make chat module ready


  public void makeChatReady(String roomid, final String Pollusername)
  {

      databaseRef.child(roomid).addChildEventListener(new ChildEventListener() {
          @Override public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            //  M.hideLoadingDialog();
              progressWheelsending.setVisibility(View.GONE);
              imageButton_send.setVisibility(View.VISIBLE);
              Message new_message = dataSnapshot.getValue(Message.class);
              messageArrayList.add(new_message);
              madapter.notifyDataSetChanged();
              main_recycler_view.scrollToPosition(madapter.getItemCount() - 1);
              commentscount.setText("Comments ("+messageArrayList.size()+")");
          }

          @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {

              progressWheelsending.setVisibility(View.GONE);
              imageButton_send.setVisibility(View.VISIBLE);
          }

          @Override public void onChildRemoved(DataSnapshot dataSnapshot) {
              Log.d("REMOVED", dataSnapshot.getValue(Message.class).toString());
              messageArrayList.remove(dataSnapshot.getValue(Message.class));
              madapter.notifyDataSetChanged();

              progressWheelsending.setVisibility(View.GONE);
              imageButton_send.setVisibility(View.VISIBLE);
          }

          @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {

              progressWheelsending.setVisibility(View.GONE);
              imageButton_send.setVisibility(View.VISIBLE);
          }

          @Override public void onCancelled(DatabaseError databaseError) {

              progressWheelsending.setVisibility(View.GONE);
              imageButton_send.setVisibility(View.VISIBLE);
          }
      });

      userID = SCUtils.getUniqueID(getApplicationContext());

      databaseRef.child("users").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
          @Override public void onDataChange(DataSnapshot dataSnapshot) {
              if (!dataSnapshot.exists()) {

                  String new_username = M.getUsername(mContext);
               /*   if ((!new_username.equals(username)) && (!username.equals("anonymous"))) {
                      process_new_message(username + " changed it's nickname to " + new_username, true);
                  }*/
                  username = new_username;
                  databaseRef.child("users").child(userID).setValue(Pollusername);
              } else {
                  username = dataSnapshot.getValue(String.class);
                //  Snackbar.make(findViewById(android.R.id.content), "Logged in as " + username, Snackbar.LENGTH_SHORT).show();
              }
          }
          @Override public void onCancelled(DatabaseError databaseError) {
              Log.w("!!!", "username:onCancelled", databaseError.toException());
          }
      });
  }

    private void process_new_message(String new_message, boolean isNotification) {
        if (new_message.isEmpty()) {
            return;
        }



        //simple anti-flood protection
       /* if ((System.currentTimeMillis() / 1000L - last_message_timestamp) < ANTI_FLOOD_SECONDS) {
            SCUtils.showErrorSnackBar(mContext, findViewById(android.R.id.content), "You cannot send messages so fast.").show();
            return;
        }
*/
        //yes, admins can swear ;)
        if ((PROFANITY_FILTER_ACTIVE) && (!IS_ADMIN)) {
            new_message = new_message.replaceAll(ProfanityFilter.censorWords(ProfanityFilter.ENGLISH), ":)");
        }



        Message xmessage = new Message(userID, M.getUsername(mContext), new_message, System.currentTimeMillis() / 1000L, IS_ADMIN, isNotification);
        String key = databaseRef.child(pollid).push().getKey();
        databaseRef.child(pollid).child(key).setValue(xmessage);

        last_message_timestamp = System.currentTimeMillis() / 1000L;
    }

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private String youtubeLink="";
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.loadVideo(youtubeLink);
        }
        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

                //youTubePlayer.pause();
            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {

            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        });
        youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
            @Override
            public void onPlaying() {

              //  navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onPaused() {

                navbtnlayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStopped() {
                navbtnlayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onBuffering(boolean b) {

                //navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onSeekTo(int i) {

                //navbtnlayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
           /* String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();*/
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(AppConst.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    @Override
    public void onSelectReaction(String selectedReaction) {

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }

        tvLike.setText(selectedReaction);

        if (selectedReaction.equalsIgnoreCase("like")){
            ivLike.setImageResource(R.drawable.like);
            tvLike.setText("Like");
        }else if (selectedReaction.equalsIgnoreCase("love")){

                ivLike.setImageResource(R.drawable.love);
                tvLike.setText("Love");
        }else if (selectedReaction.equalsIgnoreCase("haha")){

                ivLike.setImageResource(R.drawable.haha);
                tvLike.setText("Haha");
        }else if (selectedReaction.equalsIgnoreCase("wow")){
                ivLike.setImageResource(R.drawable.wow);
                tvLike.setText("Wow");

        }else if (selectedReaction.equalsIgnoreCase("sad")){
                ivLike.setImageResource(R.drawable.sad);
                tvLike.setText("Sad");

        }else if (selectedReaction.equalsIgnoreCase("angry")){
                ivLike.setImageResource(R.drawable.angry);
                tvLike.setText("Angry");
              }
              else
        {
            ivLike.setImageResource(R.drawable.like);
            tvLike.setText("Like");
        }


        if(M.getID(PollScreen.this).equalsIgnoreCase("0"))
        {
            Toast.makeText(PollScreen.this, "You need to Login, To share your reaction", Toast.LENGTH_SHORT).show();
        }
        else
        {
            if(pollid.length()>0 )
            {
                submitReactionToServer(pollid,selectedReaction.toLowerCase().toString());
            }
        }

    }



    private PopupWindow popupWindow;
    private void showReactionPopup(View anchorView) {

        com.app.moodindia.Reactions.reaction.ReactionView layout = new com.app.moodindia.Reactions.reaction.ReactionView(PollScreen.this, this);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(layout, width, height, focusable);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setTouchable(true);

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        } else {
          popupWindow.showAsDropDown(anchorView,-20, -DisplayUtil.dpToPx(anchorView.getHeight() + DisplayUtil.dpToPx(8))-40);
        }
    }
    private void showReactionPopupPreLollipop(View anchorView) {

        View popUpView = getLayoutInflater().inflate(R.layout.popup_view, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;

        popupWindow = new PopupWindow(popUpView, width, height, focusable);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setTouchable(true);
        com.app.moodindia.Reactions.reaction.ReactionView layout = new com.app.moodindia.Reactions.reaction.ReactionView(PollScreen.this, this);

        RelativeLayout relativeLayout = (RelativeLayout) popUpView.findViewById(R.id.rlpopuplayout);
        relativeLayout.addView(layout);

        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        } else {
            popupWindow.showAsDropDown(anchorView, 0, -DisplayUtil.dpToPx(anchorView.getHeight() + DisplayUtil.dpToPx(8)));
        }
    }
    private void submitReactionToServer(String pollid,String reaction)
    {
        M.showLoadingDialog(PollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.submitReaction(pollid,M.getID(PollScreen.this),reaction,new retrofit.Callback<SuccessPojo>() {
            @Override
            public void success(SuccessPojo pojo, retrofit.client.Response response) {

                if(pojo.getLikecount().length()>0)
                {
                    likecount.setText(" "+pojo.getLikecount());
                }
                Toast.makeText(PollScreen.this, "Reaction Submitted Successfully", Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    public void nextPollSpotlightView()
    {
        new SpotlightView.Builder(this)
                .introAnimationDuration(400)
                .enableRevealAnimation(true)
                .performClick(true)
                .fadeinTextDuration(400)
                .headingTvColor(Color.parseColor("#eb273f"))
                .headingTvSize(32)
                .headingTvText("Next Polls")
                .subHeadingTvColor(Color.parseColor("#ffffff"))
                .subHeadingTvSize(16)
                .subHeadingTvText("Swipe Right or Click here to see next poll in this topic")
                .maskColor(Color.parseColor("#dc000000"))
                .target(ivnextpoll)
                .lineAnimDuration(400)
                .lineAndArcColor(Color.parseColor("#eb273f"))
                .dismissOnTouch(true)
                .dismissOnBackPress(true)
                .enableDismissAfterShown(true)
                .usageId("nextpollspotview3")
                .setListener(new SpotlightListener() {
                    @Override
                    public void onUserClicked(String s) {

                        TutoShowcase.from(PollScreen.this)
                                .setListener(new TutoShowcase.Listener() {
                                    @Override
                                    public void onDismissed() {
                                        prevScreenSpotLightView();
                                    }
                                })
                                .setFitsSystemWindows(true)
                                .on(tvque)
                                .displaySwipableRight()
                                .duration(3000)
                                .animated(true)
                                .showOnce("ivbextpoll2");

                        prevScreenSpotLightView();
                    }
                })//
                .show();
    }
    public void prevScreenSpotLightView()
    {
        new SpotlightView.Builder(this)
            .introAnimationDuration(400)
            .enableRevealAnimation(true)
            .performClick(true)
            .fadeinTextDuration(400)
            .headingTvColor(Color.parseColor("#eb273f"))
            .headingTvSize(32)
            .headingTvText("Previous Polls")
            .subHeadingTvColor(Color.parseColor("#ffffff"))
            .subHeadingTvSize(16)
                .subHeadingTvText("Swipe Left or Click here to see previous polls in this topic")
            .maskColor(Color.parseColor("#dc000000"))
            .target(ivprevpoll)
            .lineAnimDuration(400)
            .lineAndArcColor(Color.parseColor("#eb273f"))
            .dismissOnTouch(true)
            .dismissOnBackPress(true)
            .enableDismissAfterShown(true)
            .usageId("prevpollspotview")
            .setListener(new SpotlightListener() {
                @Override
                public void onUserClicked(String s) {

                    TutoShowcase.from(PollScreen.this)
                            .setListener(new TutoShowcase.Listener() {
                                @Override
                                public void onDismissed() {

                                }
                            })
                            .setFitsSystemWindows(true)
                            .on(tvque)
                            .displaySwipableLeft()
                            .duration(3000)
                            .animated(true)
                            .showOnce("ivprevpoll");
                }
            })//
            .show();

    }

    SpotlightView spotView;


    public void share(final String url, final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/jpeg");
                    share.putExtra(
                            Intent.EXTRA_TEXT,
                            text);

                    if (url != null) {
                        Bitmap bmp = null;
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                        try {
                            byte[] chunk = new byte[4096];
                            int bytesRead;
                            InputStream stream = new URL(url).openStream();

                            while ((bytesRead = stream.read(chunk)) > 0) {
                                outputStream.write(chunk, 0, bytesRead);
                            }

                            outputStream.toByteArray();

                            bmp = BitmapFactory.decodeByteArray(
                                    outputStream.toByteArray(), 0,
                                    outputStream.toByteArray().length);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.v("Error", e.toString());
                        }

                        String filename = Environment.getExternalStorageDirectory().getAbsoluteFile() + File.separator + File.separator
                                + new Date().getTime()+ ".png";
                        Log.e("BITMAP", filename);
                        FileOutputStream out = new FileOutputStream(filename);
                        bmp.compress(Bitmap.CompressFormat.PNG, 50, out);

                        Bitmap icon = bmp;

                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "title");
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                        Uri uri = getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                        OutputStream outstream;
                        try {
                            outstream = getContentResolver()
                                    .openOutputStream(uri);
                            icon.compress(Bitmap.CompressFormat.PNG, 60, outstream);
                            outstream.close();
                        } catch (Exception e) {
                            System.err.println(e.toString());
                        }
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                    }
                    startActivity(Intent.createChooser(share, "Share"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}

