package com.app.moodindia;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.app.moodindia.model.ChangePwdPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.ui.publicpolls.PublicHomePage;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.MyApplication;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.Transition;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hanks.htextview.HTextView;
import com.hanks.htextview.HTextViewType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.RetrofitError;
import xyz.hanks.library.SmallBang;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,KenBurnsView.TransitionListener, GoogleApiClient.OnConnectionFailedListener ,ConnectivityReceiver.ConnectivityReceiverListener{
    EditText etemail,etpwd;
    private SmallBang mSmallBang;
    private HTextView hTextView ;
    TextView tvskip,tvforgot,btn_skip_login;
    Button btnlogin,btnfacebook,btnsignup;
    LoginButton fb_login;
    int RC_SIGN_IN=1023;
    String TAG="Login",email,pwd,logintype,name,gender;
    //Facebook
    CallbackManager callbackManager;
    Dialog dialog;
    Button btnGoogle;
    GoogleApiClient mGoogleApiClient;
    SignInButton sign_in_button;
    ConnectionDetector cd;
    private static final int TRANSITIONS_TO_SWITCH = 2;
    LinearLayout llnologin,lllogin;
    TextView txtaddlogin;


    private ViewSwitcher mViewSwitcher;

    private int mTransitionsCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(LoginActivity.this);
        AppEventsLogger.activateApp(LoginActivity.this);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lllogin=(LinearLayout)findViewById(R.id.lllogin);
        llnologin=(LinearLayout)findViewById(R.id.llnologin);
        txtaddlogin=(TextView) findViewById(R.id.txtaddlogin);

        llnologin.setVisibility(View.VISIBLE);
        lllogin.setVisibility(View.GONE);
        txtaddlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llnologin.setVisibility(View.GONE);
                lllogin.setVisibility(View.VISIBLE);
            }
        });





        boolean isConnected = ConnectivityReceiver.isConnected();
        if(isConnected) {

            if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null){
                //Logged in so show the login button
                LoginManager.getInstance().logOut();
                startActivity(new Intent(LoginActivity.this,LoginActivity.class));
                finish();
            }
            else
            {
                getLogin();
            }
        }
        else
        {
            new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("Sorry! Device not connected to internet!")
                    .show();
        }
        mViewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        KenBurnsView img1 = (KenBurnsView) findViewById(R.id.img1);
        img1.setTransitionListener(this);

        KenBurnsView img2 = (KenBurnsView) findViewById(R.id.img2);
        img2.setTransitionListener(this);
        mSmallBang = SmallBang.attach2Window(this);
        TextView tx = (TextView)findViewById(R.id.textView4);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssl.ttf");

        tx.setTypeface(custom_font);

        //TextView tx1 = (TextView)findViewById(R.id.textView5);


        //tx1.setTypeface(custom_font);

        TextView tx2 = (TextView)findViewById(R.id.textView6);

        tx2.setTypeface(custom_font);

        TextView tx3 = (TextView)findViewById(R.id.textView7);


        tx3.setTypeface(custom_font);

        hTextView = (HTextView) findViewById(R.id.text);
        hTextView.setTypeface(Typeface.createFromAsset(getAssets(),  "fonts/ssb.ttf"));
    // be sure to set custom typeface before setting the animate type, otherwise the font may not be updated.
        hTextView.setAnimateType(HTextViewType.SCALE);
        hTextView.animateText("Because you reflect the Nation"); // animate

    }

    @Override
    public void onTransitionStart(Transition transition) {

    }


    @Override
    public void onTransitionEnd(Transition transition) {

        mTransitionsCount++;
        if (mTransitionsCount == TRANSITIONS_TO_SWITCH) {
            //Log.d(TAG, "onTransitionEnd: "+mViewSwitcher.getCurrentView().getResources().getResourceName(mViewSwitcher.getCurrentView().getId()));
            if(mViewSwitcher.getCurrentView().getResources().getResourceName(mViewSwitcher.getCurrentView().getId())=="img1") {
                hTextView.animateText("Because you reflect the Nation"); // animate

            }else{
                hTextView.animateText("Because you reflect the Nation"); // animate
            }

            mViewSwitcher.showNext();
            mTransitionsCount = 0;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    private void initview() {
        etemail=(EditText)findViewById(R.id.etemail);
        etpwd=(EditText)findViewById(R.id.etpwd);
        tvskip=(TextView)findViewById(R.id.tvskip);
        tvforgot=(TextView)findViewById(R.id.tvforgot);
        btn_skip_login=(TextView)findViewById(R.id.btn_skip_login);
        btn_skip_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(M.getFavs(LoginActivity.this).length()>0)
                {
                    Intent intent=new Intent(LoginActivity.this,PublicHomePage.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    Intent it = new Intent(LoginActivity.this, SelectFavsActivity.class);
                    finish();
                    startActivity(it);
                }
            }
        });
        btnlogin=(Button)findViewById(R.id.btnlogin);
        fb_login=(LoginButton)findViewById(R.id.fb_login);
        btnfacebook=(Button)findViewById(R.id.btnfacebook);
        btnsignup=(Button)findViewById(R.id.btnsignup);
        btnGoogle=(Button)findViewById(R.id.btnGoogle);
        sign_in_button=(SignInButton)findViewById(R.id.sign_in_button);
        tvskip.setOnClickListener(this);
        tvforgot.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
        btnfacebook.setOnClickListener(this);
        btnsignup.setOnClickListener(this);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnlogin){
            mSmallBang.bang(view);
            if(etemail.getText().toString().trim().length()<0){
                etemail.setError("Email Required");
            }else if(!isValidEmaillId(etemail.getText().toString())){
                etemail.setError("Invalid Email");
            }else if(etpwd.getText().toString().trim().length()<0){
                etpwd.setError("Password Required");
            }else{
                email=etemail.getText().toString();
                pwd=etpwd.getText().toString();
                login();
            }
        }
        else if(view.getId()==R.id.btnfacebook){
            fb_login.performClick();
        }else if(view.getId()==R.id.btnsignup){
            Intent it=new Intent(LoginActivity.this,RegisterActivity.class);
            startActivity(it);
            overridePendingTransition(0,0);
        }else if(view.getId()==R.id.tvskip){
            Intent it=new Intent(LoginActivity.this,HomeActivity.class);
            finish();
            startActivity(it);
            overridePendingTransition(0,0);
        }else if(view.getId()==R.id.tvforgot){
            dialog = new Dialog(LoginActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_forgot_pwd);
            final EditText edtemail=(EditText)dialog.findViewById(R.id.etemailaddress);
            Button btn=(Button)dialog.findViewById(R.id.btnforgot);
            Button btncancel=(Button)dialog.findViewById(R.id.btncancel);

            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(edtemail.getText().toString().trim().length()<=0)
                        edtemail.setError("Email Required");
                    else if(!isValidEmaillId(edtemail.getText().toString())){
                        edtemail.setError("Invalid Email");
                    }else{
                        String mail=edtemail.getText().toString();
                        forgotPwd(mail);
                    }
                }
            });
            dialog.show();
        }
    }

    private void login() {
        M.showLoadingDialog(LoginActivity.this);
        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.login(email,pwd,new retrofit.Callback<ProfilePojo>() {
            @Override
            public void success(ProfilePojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    //Log.d("res",response.toString());
                    if(pojo.getSuccess().equals("1")) {
                        M.setID(pojo.getUser_id(), LoginActivity.this);
                        M.setUsername(pojo.getName(),LoginActivity.this);
                        M.setEmail(pojo.getEmail(),LoginActivity.this);
                        M.setPhone(pojo.getPhone(),LoginActivity.this);
                        M.setCountry(pojo.getCountry(),LoginActivity.this);
                        M.setBday(pojo.getDate_of_birth(),LoginActivity.this);
                        M.setGender(pojo.getGender(),LoginActivity.this);
                        M.setlogintype(pojo.getLogin_type(),LoginActivity.this);
                        M.setUserpic(pojo.getProfile_picture(),LoginActivity.this);
                        M.hideLoadingDialog();
                        if(M.getFavs(LoginActivity.this).toString().length()>0)
                        {
                            Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                            finish();
                            startActivity(it);
                        }
                        else
                        {
                            Intent it = new Intent(LoginActivity.this, SelectFavsActivity.class);
                            finish();
                            startActivity(it);
                        }

                    }else{
                       Toast.makeText(LoginActivity.this,"Invalid Email or Password...",Toast.LENGTH_SHORT).show();
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    private void social_login() {

        M.showLoadingDialog(LoginActivity.this);

        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.sociallogin(email,logintype,new retrofit.Callback<ProfilePojo>() {
            @Override
            public void success(ProfilePojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess().equalsIgnoreCase("1")) {
                        M.setID(pojo.getUser_id(), LoginActivity.this);
                        M.setUsername(pojo.getName(),LoginActivity.this);
                        M.setEmail(pojo.getEmail(),LoginActivity.this);
                        M.setPhone(pojo.getPhone(),LoginActivity.this);
                        M.setCountry(pojo.getCountry(),LoginActivity.this);
                        M.setBday(pojo.getDate_of_birth(),LoginActivity.this);
                        M.setGender(pojo.getGender(),LoginActivity.this);
                        M.setlogintype(pojo.getLogin_type(),LoginActivity.this);
                        M.hideLoadingDialog();
                        if(M.getFavs(LoginActivity.this).toString().length()>0)
                        {
                            Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                            finish();
                            startActivity(it);
                        }
                        else
                        {
                            Intent it = new Intent(LoginActivity.this, SelectFavsActivity.class);
                            finish();
                            startActivity(it);
                            overridePendingTransition(0,0);
                        }
                    }else{
                        Intent it=new Intent(LoginActivity.this,RegisterActivity.class);
                        it.putExtra("name",name);
                        it.putExtra("email",email);
                        it.putExtra("gender",gender);
                        it.putExtra("logintype",logintype);
                        startActivity(it);
                    }
                }
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    private void forgotPwd(String mail){
        M.showLoadingDialog(LoginActivity.this);

        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.forgotPwd(mail,new retrofit.Callback<ChangePwdPojo>() {
            @Override
            public void success(ChangePwdPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess().equals("1")){

                        dialog.cancel();

                        Toast.makeText(LoginActivity.this,"Password Sent via email.Please Check your email.",Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(LoginActivity.this,"Invalid Email",Toast.LENGTH_SHORT).show();
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            //Log.d("call","fbcall");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else
          callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        //Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            name = acct.getDisplayName();
            email =acct.getEmail();
            gender="Male";
            logintype="Google";
            social_login();
        } else {
            // Signed out, show unauthenticated UI.
           // updateUI(false);
            M.showToast(LoginActivity.this,"Unable to login by google, try other login ways.");
        }
    }
    private boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Bundle extras=getIntent().getExtras();
        if(extras!=null && getIntent().getExtras().containsKey("frompublic"))
        {
            if(M.getID(LoginActivity.this).equalsIgnoreCase("0"))
            {
                Intent it = new Intent(LoginActivity.this, PublicHomePage.class);
                startActivity(it);
            }
        }else
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            getLogin();
        }
        else
        {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }

    }
    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

  public void getLogin()
  {

      callbackManager = CallbackManager.Factory.create();
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      // window.setStatusBarColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryDark));
      initview();
      //google login
      GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
              .requestEmail()
              .requestProfile()
              .build();
      mGoogleApiClient = new GoogleApiClient.Builder(this)
              .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
              .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
              .addOnConnectionFailedListener(this)
              .build();
      //fb login


      fb_login.setReadPermissions("public_profile", "email");
      fb_login.registerCallback(callbackManager,
              new FacebookCallback<LoginResult>() {
                  @Override
                  public void onSuccess(final LoginResult loginResult) {
                      // App code
                      M.showLoadingDialog(LoginActivity.this);
                      GraphRequest request = GraphRequest.newMeRequest(
                              loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                  @Override
                                  public void onCompleted(JSONObject json, GraphResponse response) {
                                      if (response.getError() != null) {
                                          Toast.makeText(getApplicationContext(), response.getError().toString(), Toast.LENGTH_SHORT);
                                      } else {
                                          M.hideLoadingDialog();
                                          json = response.getJSONObject();
                                          try {
                                              name = json.getString("name");
                                              email = json.getString("email");
                                            //
                                              //  gender = json.getString("gender");
                                              logintype = "Facebook";
                                              //Log.d(TAG, "data:" + name + "-" + email + "-" + json.getString("gender"));
                                              social_login();
                                          } catch (JSONException e) {
                                              e.printStackTrace();
                                          }
                                      }
                                  }

                              });
                      Bundle parameters = new Bundle();
                      parameters.putString("fields", "id,name,email,gender");
                      request.setParameters(parameters);
                      request.executeAsync();
                  }

                  @Override
                  public void onCancel() {
                      // App code
                      //Log.d(TAG, "cancel");
                  }

                  @Override
                  public void onError(FacebookException exception) {
                      // App code
                      //Log.d(TAG, "exception" + exception.toString());
                  }
              });
  }
}
