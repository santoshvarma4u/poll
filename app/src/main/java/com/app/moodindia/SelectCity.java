package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.app.moodindia.model.CityPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.LocationAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class SelectCity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    RecyclerView rv_city;
    SearchView sv_city;
    boolean isConnected;
    List<CityPojo> citypojoList;
    Button clearfilter;
    TextView tvpollscitylist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);
        rv_city=(RecyclerView)findViewById(R.id.rv_city);
        sv_city=(SearchView)findViewById(R.id.sv_city);
        clearfilter=(Button) findViewById(R.id.clearFilter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectCity.this);
        rv_city.setLayoutManager(mLayoutManager);
        rv_city.setItemAnimator(new DefaultItemAnimator());
        citypojoList=new ArrayList<>();
        sv_city.setIconified(true);
        tvpollscitylist=(TextView)findViewById(R.id.tvpollscitylist);

        sv_city.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                cla.applyFilter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length()>0)
                {
                    cla.applyFilter(newText);
                }

                return true;
            }
        });
        clearfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();

                //M.showToast(SelectState.this,filterdcstatepojo.get(getAdapterPosition()).getId());
                intent.putExtra("CityName","0");
                intent.putExtra("CityId","0");
                intent.putExtra("fromact","home");
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        checkConnection();
    }
    StateListAdapter cla;
    List<CityPojo> filterdcitypojoList;
    public class StateListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<CityPojo> citypojoList;
        public StateListAdapter(List<CityPojo> citypojoList) {
                filterdcitypojoList=new ArrayList<>();
                this.citypojoList = citypojoList;
                filterdcitypojoList.addAll(citypojoList);
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {
                RecyclerView.ViewHolder viewHolder;
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View vSlider = inflater.inflate(R.layout.item_country, parent, false);
                viewHolder = new ViewHolderFilter(vSlider);
                return viewHolder;
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                ViewHolderFilter vh2 = (ViewHolderFilter) holder;
                configureViewHolderFilter(vh2, position);
        }

        public void applyFilter(String query)
        {
            if(query.length() ==0)
                filterdcitypojoList.addAll(citypojoList);
            else
            {
                filterdcitypojoList.clear();
                for(CityPojo cp:citypojoList)
                {
                    if(cp.getName().toLowerCase().contains(query.toLowerCase()))
                    {
                        filterdcitypojoList.add(cp);
                    }
                }
            }
            notifyDataSetChanged();

        }
        @Override
        public int getItemCount() {
            return filterdcitypojoList.size();
        }
        CityPojo listCountry;
        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCountry=filterdcitypojoList.get(position);
            holder.CountryName.setText(listCountry.getName());
            holder.CountryId=listCountry.getId();
            //holder
        }
    }
    class ViewHolderFilter extends RecyclerView.ViewHolder{
        TextView CountryName;
        String CountryId;
        public ViewHolderFilter(View itemView) {
            super(itemView);
            CountryName=(TextView)itemView.findViewById(R.id.tv_countryname);
            CountryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent();
                    intent.putExtra("fromAct","home");
                    intent.putExtra("CityName",filterdcitypojoList.get(getAdapterPosition()).getName());
                    intent.putExtra("CityId",filterdcitypojoList.get(getAdapterPosition()).getId());
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras().containsKey("register")){
            getCountryRegisterData(getIntent().getExtras().getString("stateid").toString());
        }
        else{
            getCountryData(getIntent().getExtras().getString("stateid").toString());
            tvpollscitylist.setVisibility(View.VISIBLE);
        }

        }


    public void getCountryData(String stateid)
    {
        M.showLoadingDialog(SelectCity.this);
      //  M.showToast(SelectCity.this,stateid);
        LocationAPI mAuthenticationAPI = APIService.createService(LocationAPI.class);
        mAuthenticationAPI.getCities(stateid,new retrofit.Callback<List<CityPojo>>() {
            @Override
            public void success(List<CityPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        citypojoList=pojo;
                        cla=new StateListAdapter(citypojoList);
                        rv_city.setAdapter(cla);
                        M.hideLoadingDialog();
                    }else{

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

    public void getCountryRegisterData(String stateid)
    {
        M.showLoadingDialog(SelectCity.this);
        //  M.showToast(SelectCity.this,stateid);
        LocationAPI mAuthenticationAPI = APIService.createService(LocationAPI.class);
        mAuthenticationAPI.getCitiesRegister(stateid,"register",new retrofit.Callback<List<CityPojo>>() {
            @Override
            public void success(List<CityPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        citypojoList=pojo;
                        cla=new StateListAdapter(citypojoList);
                        rv_city.setAdapter(cla);
                        M.hideLoadingDialog();
                    }else{

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

       // showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
    }
   /* private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }*/
}
