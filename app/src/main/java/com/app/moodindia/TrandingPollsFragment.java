package com.app.moodindia;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.PollAPI;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;


/**
 * to handle interaction events.
 * Use the {@link TrandingPollsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrandingPollsFragment extends Fragment   {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TrandingPollsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TrandingPollsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TrandingPollsFragment newInstance(String param1, String param2) {
        TrandingPollsFragment fragment = new TrandingPollsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    RecyclerView rv_current_polls;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    RecyclerView.LayoutManager mLayoutManager;
    boolean isConnected;
    private TextView tvNopolls;
    SwipeRefreshLayout svlayout;
    private ShimmerRecyclerView shimmerRecycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_polls_trending, container, false);

        rv_current_polls = (RecyclerView)view.findViewById(R.id.pools_recycler);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_current_polls.setLayoutManager(mLayoutManager);
        rv_current_polls.setItemAnimator(new DefaultItemAnimator());
        tvNopolls = (TextView) view.findViewById(R.id.tv_nopolls);
        boolean loading = true;

        shimmerRecycler = (ShimmerRecyclerView)view.findViewById(R.id.shimmer_recycler_view);
        shimmerRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        shimmerRecycler.showShimmerAdapter();
        rv_current_polls.setVisibility(View.GONE);

        svlayout=(SwipeRefreshLayout)view.findViewById(R.id.svlayout);

        svlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(M.getMyCountry(getActivity()).length()>1)
                {
                    poolsList.clear();
                    getPollCategoryData();
                }
                else
                {
                    //  M.showToast(getActivity(),"Select your location, country is mandatory");
                }
                svlayout.setRefreshing(false);
            }
        });



        rv_current_polls.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0)
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();

                }

            }
        });
        checkConnection();
        pla=new PollsAdapter();
        poolsList=new ArrayList<>();
        return view;
    }


    PollsAdapter pla;
    List<PollPojo> poolsList;


    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {

            return poolsList == null ? 0 : poolsList.size();
        }
        PollPojo listCategory;

        String tempCatId="";
        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCategory=poolsList.get(position);

         /*   if(tempCatId==listCategory.getCat_id())
            {
                holder.cv_category.setVisibility(View.GONE);
            }
            else
            {
                tempCatId=listCategory.getCat_id();
                holder.cv_category.setVisibility(View.VISIBLE);
            }


*/

            holder.catLayout.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
           // holder.tvCategPost.setVisibility(View.GONE);
           // holder.tvCateg.setVisibility(View.VISIBLE);
            holder.tvpollcategory.setText("Category : " +listCategory.getCat_name());
            holder.tvcat.setText("Category : " +listCategory.getCat_name());
            holder.tvCateg.setText("In " +listCategory.getCat_name());
            holder.tvincategory.setVisibility(View.VISIBLE);

            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));

                holder.tvincategory.setText(" In "+listCategory.getCat_name());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(getActivity())
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new RoundedSquareTransformation(10,0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost,tvCateg,tvincategory;
        ImageView ivpoll;
        TextView tvpollcategory;
        CardView cv_category;

        LinearLayout catLayout;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
            tvCateg=(TextView) itemView.findViewById(R.id.tvCateg);
            tvincategory=(TextView) itemView.findViewById(R.id.tvincategory);
            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("from","active");
                    it.putExtra("fragName","1");
                    startActivity(it);
                }
            });
            tvviewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), CurrentPolls.class);
                    it.putExtra("catid",poolsList.get(getAdapterPosition()).getCat_id());
                    it.putExtra("catname",poolsList.get(getAdapterPosition()).getCat_name());
                    startActivity(it);
                }
            });
            Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);



        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(M.getMyCountry(getActivity()).length()>1)
        {
            getPollCategoryData();
        }
        else
        {
          //  M.showToast(getActivity(),"Select your location");
        }
    }

    int currentpage=1;

    public void getPollCategoryData()
    {
        shimmerRecycler.showShimmerAdapter();
        rv_current_polls.setVisibility(View.GONE);
        final int page=getCurrentpage();
        String favs=M.getFavs(getActivity()).toString();
        //M.showLoadingDialog(getActivity());
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getTrandingPolls(M.getID(getActivity()),page,M.getMyCountryID(getActivity()),M.getMyStateID(getActivity()),M.getMyCityID(getActivity()),"true",favs,new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        shimmerRecycler.hideShimmerAdapter();
                        rv_current_polls.setVisibility(View.VISIBLE);
                        poolsList=pojo;
                        rv_current_polls.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        //ArrayList<CategoryPojo>) pojo;
                        //lvcat.setAdapter(adapter);
                        tvNopolls.setVisibility(View.GONE);
                    }else{
                        shimmerRecycler.hideShimmerAdapter();

                     /*   new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("There is no trending polls . Come back later!")
                                .show();*/

                        tvNopolls.setVisibility(View.VISIBLE);
                        tvNopolls.setText("Oops!.There is no trending polls . Come back later");
                    }
                }else{

                }
                shimmerRecycler.hideShimmerAdapter();
                rv_current_polls.setVisibility(View.VISIBLE);
              //  M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
               // M.hideLoadingDialog();
                shimmerRecycler.hideShimmerAdapter();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

    public int getCurrentpage() {
        return currentpage;
    }

    public void setCurrentpage(int currentpage)
    {
        this.currentpage = currentpage;
    }


    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
       // showSnack(isConnected);
    }


}
