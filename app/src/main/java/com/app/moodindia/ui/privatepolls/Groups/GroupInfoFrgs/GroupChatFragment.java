package com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.moodindia.R;
import com.app.moodindia.chatmod.adapters.MainAdapter;
import com.app.moodindia.chatmod.models.Message;
import com.app.moodindia.chatmod.utils.ProfanityFilter;
import com.app.moodindia.chatmod.utils.SCUtils;
import com.app.moodindia.model.M;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupChatFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "groupid";
    private static final String ARG_PARAM2 = "chatid";
    protected View rootView;
    protected RecyclerView mainRecyclerView;
    protected EditText editTextMessage;
    protected ImageView imageButtonSend;
    protected ProgressWheel progressWheelsending;
    protected LinearLayout linLayoutText;
    protected LinearLayout lytChatView;
    private DatabaseReference databaseRef;

    private Context mContext;
    private MainAdapter madapter;

    // TODO: Rename and change types of parameters
    private String mGroupId;
    private String mChatId;

    public static final int ANTI_FLOOD_SECONDS = 3; //simple anti-flood
    private boolean IS_ADMIN = false; //set this to true for the admin app.
    String username = "anonymous"; //default username
    private boolean PROFANITY_FILTER_ACTIVE = true;
    private FirebaseDatabase database;
    private RecyclerView main_recycler_view;
    ArrayList<Message> messageArrayList = new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    public GroupChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupChatFragment newInstance(String param1, String param2) {
        GroupChatFragment fragment = new GroupChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getActivity();
        if (getArguments() != null) {
            mGroupId = getArguments().getString(ARG_PARAM1);
            mChatId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_group_chat, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imageButton_send) {

            if(editTextMessage.getText().toString().trim().length()>0)
              getActivity().runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      new backTask().execute();
                  }
              });

        }
    }

    private void initView(View rootView) {
        main_recycler_view = (RecyclerView) rootView.findViewById(R.id.main_recycler_view);
        editTextMessage = (EditText) rootView.findViewById(R.id.editText_message);
        imageButtonSend = (ImageView) rootView.findViewById(R.id.imageButton_send);
        imageButtonSend.setOnClickListener(GroupChatFragment.this);
        progressWheelsending = (ProgressWheel) rootView.findViewById(R.id.progressWheelsending);
        linLayoutText = (LinearLayout) rootView.findViewById(R.id.linLayout_text);
        lytChatView = (LinearLayout) rootView.findViewById(R.id.lytChatView);
        database = FirebaseDatabase.getInstance();
        databaseRef = database.getReference();
        main_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        madapter = new MainAdapter(mContext, messageArrayList);
        main_recycler_view.setAdapter(madapter);
        makeChatReady(mChatId,M.getUsername(mContext));
        //Toast.makeText(mContext, M.getUsername(mContext), Toast.LENGTH_SHORT).show();
    }

    public void makeChatReady(String roomid, final String Pollusername)
    {
        // Log.i("roomid",roomid);
        databaseRef.child(roomid).addChildEventListener(new ChildEventListener() {
            @Override public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //  M.hideLoadingDialog();
                progressWheelsending.setVisibility(View.GONE);
                imageButtonSend.setVisibility(View.VISIBLE);
                Message new_message = dataSnapshot.getValue(Message.class);
                messageArrayList.add(new_message);
                madapter.notifyDataSetChanged();
                main_recycler_view.scrollToPosition(madapter.getItemCount() - 1);
            }

            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                progressWheelsending.setVisibility(View.GONE);
                imageButtonSend.setVisibility(View.VISIBLE);
            }

            @Override public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("REMOVED", dataSnapshot.getValue(Message.class).toString());
                messageArrayList.remove(dataSnapshot.getValue(Message.class));
                madapter.notifyDataSetChanged();

                progressWheelsending.setVisibility(View.GONE);
                imageButtonSend.setVisibility(View.VISIBLE);
            }

            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                progressWheelsending.setVisibility(View.GONE);
                imageButtonSend.setVisibility(View.VISIBLE);
            }

            @Override public void onCancelled(DatabaseError databaseError) {

                progressWheelsending.setVisibility(View.GONE);
                imageButtonSend.setVisibility(View.VISIBLE);
            }
        });

        userID = SCUtils.getUniqueID(mContext);

        databaseRef.child("users").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {

                    String new_username = M.getUsername(mContext);
               /*   if ((!new_username.equals(username)) && (!username.equals("anonymous"))) {
                      process_new_message(username + " changed it's nickname to " + new_username, true);
                  }*/
                    username = new_username;
                    databaseRef.child("users").child(userID).setValue(Pollusername);
                } else {
                    username = dataSnapshot.getValue(String.class);
                    //Snackbar.make(getActivity().findViewById(android.R.id.content), "Logged in as " + username, Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override public void onCancelled(DatabaseError databaseError) {
                Log.w("!!!", "username:onCancelled", databaseError.toException());
            }
        });
    }
    private String userID;

    private void process_new_message(String new_message, boolean isNotification) {
        if (new_message.isEmpty()) {
            return;
        }



        //simple anti-flood protection
       /* if ((System.currentTimeMillis() / 1000L - last_message_timestamp) < ANTI_FLOOD_SECONDS) {
            SCUtils.showErrorSnackBar(mContext, findViewById(android.R.id.content), "You cannot send messages so fast.").show();
            return;
        }
*/
        //yes, admins can swear ;)
        if ((PROFANITY_FILTER_ACTIVE) && (!IS_ADMIN)) {
            new_message = new_message.replaceAll(ProfanityFilter.censorWords(ProfanityFilter.ENGLISH), ":)");
        }



        Message xmessage = new Message(userID, M.getUsername(mContext), new_message, System.currentTimeMillis() / 1000L, IS_ADMIN, isNotification);
        String key = databaseRef.child(mChatId).push().getKey();
        databaseRef.child(mChatId).child(key).setValue(xmessage);

        last_message_timestamp = System.currentTimeMillis() / 1000L;
    }
    private long last_message_timestamp = 0;

    private class backTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            process_new_message(editTextMessage.getText().toString().trim(), false);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressWheelsending.setVisibility(View.VISIBLE);
            imageButtonSend.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            editTextMessage.setText("");
            progressWheelsending.setVisibility(View.GONE);
            imageButtonSend.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
