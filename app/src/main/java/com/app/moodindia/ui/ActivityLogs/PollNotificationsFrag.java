package com.app.moodindia.ui.ActivityLogs;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.helper.JSONParser;
import com.app.moodindia.helper.WrapContentLinearLayoutManager;
import com.app.moodindia.model.M;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.model.UserLog;
import com.app.moodindia.ui.privatepolls.PrivatePollScreen;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PollNotificationsFrag extends Fragment {


    protected View rootView;
    protected RecyclerView rvPollNotifications;
    protected LinearLayout lytNotification;
    protected MagicTextView notificationCountTile;

    public PollNotificationsFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_poll_notifications, container, false);
        initView(rootView);
        return rootView;
    }

    RecyclerView.LayoutManager mLayoutManager;

    private void initView(View rootView) {
        rvPollNotifications = (RecyclerView) rootView.findViewById(R.id.rv_poll_notifications);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvPollNotifications.setLayoutManager(new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPollNotifications.setItemAnimator(new DefaultItemAnimator());
        pla = new MsgAdapter();
        userLogs = new ArrayList<>();
        getPollNotifications();
        notificationCountTile = (MagicTextView) rootView.findViewById(R.id.notificationCountTile);
        lytNotification = (LinearLayout) rootView.findViewById(R.id.lytNotification);
    }

    MsgAdapter pla;
    List<UserLog> userLogs;
    int totalCount = 0;
    SimpleDateFormat defaultfmt = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dtfmt = new SimpleDateFormat("dd MMM yyyy");
    public class MsgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_messages, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return userLogs.size();
        }

        UserLog mUserLog;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            mUserLog = userLogs.get(position);
            holder.tvtitle.setText(mUserLog.getNotification_title());
            holder.tvmsg.setText(mUserLog.getNotification_message());
           // Log.i(mUserLog.getNotification_title(),mUserLog.getRead());
            try {
                holder.date.setText("- "+dtfmt.format(defaultfmt.parse(mUserLog.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (mUserLog.getRead().equalsIgnoreCase("1")) {
                holder.tvtitle.setTextColor(Color.parseColor("#888888"));
                holder.readview.setBackgroundColor(Color.parseColor("#888888"));
            }
        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder {

        TextView tvtitle, tvmsg,date;
        CardView cv_notifications;
        LinearLayout readview;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvtitle = (TextView) itemView.findViewById(R.id.title);
            tvmsg = (TextView) itemView.findViewById(R.id.message);
            date = (TextView) itemView.findViewById(R.id.date);
            cv_notifications = (CardView) itemView.findViewById(R.id.cv_notifications);
            readview = (LinearLayout) itemView.findViewById(R.id.readview);

            tvtitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Poll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();

                        } else {
                            //redirect to poll screen
                            Intent intent = new Intent(getActivity(), PollScreen.class);
                            intent.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            intent.putExtra("from", "active");
                            intent.putExtra("by", "notifications");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        }
                    } else if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("PrivatePoll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();
                        } else {
                            //redirect to poll screen
                            Intent it = new Intent(getActivity(), PrivatePollScreen.class);
                            it.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("upollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("from", "active");
                            it.putExtra("mainview", "mainview");
                            startActivity(it);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());

                        }
                    }
                }
            });
            tvmsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Poll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();
                        } else {
                            //redirect to poll screen
                            Intent intent = new Intent(getActivity(), PollScreen.class);
                            intent.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            intent.putExtra("from", "active");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        }
                    } else if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("PrivatePoll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();
                        } else {
                            //redirect to private poll screen
                            Intent it = new Intent(getActivity(), PrivatePollScreen.class);
                            it.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("upollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("from", "active");
                            it.putExtra("mainview", "mainview");
                            startActivity(it);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        }
                    }
                }
            });
            cv_notifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Poll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing

                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();
                        } else {
                            //redirect to poll screen
                            Intent intent = new Intent(getActivity(), PollScreen.class);
                            intent.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            intent.putExtra("from", "active");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        }
                    } else if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("PrivatePoll")) {
                        if (userLogs.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                            //do nothing
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                            getPollNotifications();
                        } else {
                            //redirect to private poll screen
                            Intent it = new Intent(getActivity(), PrivatePollScreen.class);
                            it.putExtra("pollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("upollid", userLogs.get(getAdapterPosition()).getPollid());
                            it.putExtra("from", "active");
                            it.putExtra("mainview", "mainview");
                            startActivity(it);
                            groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        }
                    }
                }
            });
        }
    }

    public void groupUpdateRead(String notificationId) {

        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.submitReadNotification(notificationId, new Callback<SuccessPojo>() {

            @Override
            public void success(SuccessPojo successPojo, Response response) {
                //sucess
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getPollNotifications() {
        M.showLoadingDialog(getActivity());
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getUserPollLog(M.getID(getActivity()), new Callback<List<UserLog>>() {
            @Override
            public void success(List<UserLog> muserLogs, Response response) {
                M.hideLoadingDialog();
                //Toast.makeText(getActivity(), muserLogs.get(0).getNotification_title(), Toast.LENGTH_SHORT).show();
                userLogs = muserLogs;
                notificationCountTile.setText("You've "+userLogs.get(userLogs.size()-1).getTotalCount()+" unread notifications");

                rvPollNotifications.setAdapter(pla);
                pla.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                // Toast.makeText(getActivity(), error.getBody().toString(), Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }
        });
    }
}
