package com.app.moodindia.ui.privatepolls;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParticipatedPrivatePolls extends Fragment {


    public ParticipatedPrivatePolls() {
        // Required empty public constructor
    }

    RecyclerView rv_current_polls;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    RecyclerView.LayoutManager mLayoutManager;
    boolean  isConnected;
    private TextView tvNopolls;
    SwipeRefreshLayout svlayout;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_participated_private_polls, container, false);
        rv_current_polls = (RecyclerView)view.findViewById(R.id.pools_recycler);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_current_polls.setLayoutManager(mLayoutManager);
        rv_current_polls.setItemAnimator(new DefaultItemAnimator());
        pla=new PollsAdapter();
        tvNopolls = (TextView) view.findViewById(R.id.tv_nopolls);

        boolean loading = true;

        svlayout=(SwipeRefreshLayout)view.findViewById(R.id.svlayout);
        poolsList=new ArrayList<>();
        svlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                poolsList.clear();
                getPrivatePollOwn();

                svlayout.setRefreshing(false);
            }
        });

        getPrivatePollOwn();
        return view;
    }
   /* @Override
    public void onResume() {
        super.onResume();

    }*/

    PollsAdapter pla;
    List<PrivatePollPojo> poolsList;



    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.private_poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return poolsList.size();
        }
        PrivatePollPojo listCategory;

        String tempCatId="";
        int unchangedValue=0;


        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listCategory=poolsList.get(position);

            holder.catLayout.setVisibility(View.GONE);

            holder.tvcat.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
            holder.btn_share_private_poll.setVisibility(View.GONE);
            holder.btn_no_participants.setText(listCategory.getNoofparticipants());
            holder.tvparticipants.setText("Participants : "+listCategory.getNoofparticipants());
            holder.tv_share_pollid.setText("Poll ID : #"+listCategory.getUid());
         //   holder.tvpollcategory.setText(listCategory.getCat_name());
            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(getActivity())
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost,tv_share_pollid,btn_no_participants,btn_share_private_poll;;
        ImageView ivpoll;
        TextView tvpollcategory;
        LinearLayout catLayout;
        CardView cv_category;
        TextView tvparticipants;
        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tv_share_pollid=(TextView)itemView.findViewById(R.id.tv_share_pollid);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
            tvparticipants=(TextView) itemView.findViewById(R.id.tvparticipants);
            btn_no_participants=(TextView) itemView.findViewById(R.id.btn_no_participants);
            btn_share_private_poll=(TextView) itemView.findViewById(R.id.btn_share_private_poll);

            Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
        }
    }

    int currentpage=1;

    public void getPrivatePollOwn()
    {
        final int page=getCurrentpage();
        String favs= M.getFavs(getActivity()).toString();
        //M.showLoadingDialog(getActivity());
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivateParticipatedPolls(M.getID(getActivity()),page,new retrofit.Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> pojo, retrofit.client.Response response) {
                M.hideLoadingDialog();
                if(pojo!=null){
                    if(pojo.size()>0) {
                        poolsList=pojo;

                            rv_current_polls.setAdapter(pla);
                            pla.notifyDataSetChanged();
                            tvNopolls.setVisibility(View.GONE);

                    }else{


                        tvNopolls.setVisibility(View.VISIBLE);

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

    public int getCurrentpage() {
        return currentpage;
    }

    public void setCurrentpage(int currentpage)
    {
        this.currentpage = currentpage;
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {
        if (!isConnected) {
            Intent i=new Intent(getActivity(), Internet.class);
            startActivity(i);
        }
    }
}
