package com.app.moodindia.ui.topics;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adroitandroid.chipcloud.ChipCloud;
import com.app.moodindia.R;
import com.app.moodindia.model.KeywordsPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Hashtags extends Fragment {


    protected View rootView;
    protected ChipCloud chipCloudTags;

    public Hashtags() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_hashtags, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {

    }
    String[] separated;
    public void getHashTags()
    {
        CategoryAPI mCategoryAPI= APIService.createService(CategoryAPI.class);
        mCategoryAPI.getHashTags(M.getID(getActivity()), new Callback<KeywordsPojo>() {

            @Override
            public void success(KeywordsPojo keywordsPojo, Response response) {
                String keywordsList=keywordsPojo.getKeywords();
                separated = keywordsList.split(",");
                for(String keys:separated)
                {
                    if(countWordsInSentence(keys) <=5)
                    {
                        Log.i("tags",keys);
                        chipCloudTags.addChip(keys);
                    }
                }
                // chipCloudTags.addChips(separated);
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
    private int countWordsInSentence(String input) {
        int wordCount = 0;

        if (input.trim().equals("")) {
            return wordCount;
        }
        else {
            wordCount = 1;
        }

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            String str = new String("" + ch);
            if (i+1 != input.length() && str.equals(" ") && !(""+ input.charAt(i+1)).equals(" ")) {
                wordCount++;
            }
        }

        return wordCount;
    }
}
