package com.app.moodindia.ui.privatepolls.Groups;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.ui.privatepolls.MyPrivatePollsActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyGroups extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected View rootView;
    protected CircleImageView defaultimage;
    protected MagicTextView tvMypolls,nogroups;
    protected MagicTextView tvMypollsDesc;
    protected ImageView ivpoll;
    protected MagicTextView topaddpoll;
    protected MagicButton btnBusnGrps;
    protected MagicButton btnPublcGrps;
    protected ImageView btnCreateGroup;
    protected LinearLayout llbusiness;
    protected LinearLayout llpublic;
    protected LinearLayout llpersonal;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MyGroups() {
    }


    protected RecyclerView rvGroups;
    public List<GroupPojo> mGroupPojoList;
    protected TableRow fabadd;
    RecyclerView.LayoutManager mLayoutManager;
    GroupsAdapter mGroupsAdapter;

    public static MyGroups newInstance(String param1, String param2) {
        MyGroups fragment = new MyGroups();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_my_groups, container, false);
        initView(view);
        mGroupPojoList = new ArrayList<>();
        mGroupsAdapter = new GroupsAdapter();
        return view;
    }

    private void initView(View v) {
        rvGroups = (RecyclerView) v.findViewById(R.id.rv_groups);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvGroups.setLayoutManager(mLayoutManager);
        rvGroups.setItemAnimator(new DefaultItemAnimator());
        fabadd = (TableRow) v.findViewById(R.id.fabadd);
        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CreateGroupActivity.class));
            }
        });
        defaultimage = (CircleImageView) v.findViewById(R.id.defaultimage);
        defaultimage.setOnClickListener(MyGroups.this);
        tvMypolls = (MagicTextView) v.findViewById(R.id.tv_mypolls);
        tvMypolls.setOnClickListener(MyGroups.this);
        tvMypollsDesc = (MagicTextView) v.findViewById(R.id.tv_mypolls_desc);
        tvMypollsDesc.setOnClickListener(MyGroups.this);
        ivpoll = (ImageView) v.findViewById(R.id.ivpoll);
        topaddpoll = (MagicTextView) v.findViewById(R.id.topaddpoll);
        nogroups = (MagicTextView) v.findViewById(R.id.nogroups);
        btnBusnGrps = (MagicButton) v.findViewById(R.id.btnBusnGrps);
        btnBusnGrps.setOnClickListener(MyGroups.this);
        btnPublcGrps = (MagicButton) v.findViewById(R.id.btnPublcGrps);
        btnPublcGrps.setOnClickListener(MyGroups.this);
        btnCreateGroup = (ImageView) v.findViewById(R.id.btnCreateGroup);
        btnCreateGroup.setOnClickListener(MyGroups.this);
        llbusiness = (LinearLayout) v.findViewById(R.id.llbusiness);
        llbusiness.setOnClickListener(MyGroups.this);
        llpublic = (LinearLayout) v.findViewById(R.id.llpublic);
        llpublic.setOnClickListener(MyGroups.this);
        llpersonal = (LinearLayout) v.findViewById(R.id.llpersonal);
        llpersonal.setOnClickListener(MyGroups.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.defaultimage) {
            startActivity(new Intent(getActivity(), MyPrivatePollsActivity.class));
        } else if (view.getId() == R.id.tv_mypolls) {
            startActivity(new Intent(getActivity(), MyPrivatePollsActivity.class));
        } else if (view.getId() == R.id.tv_mypolls_desc) {
            startActivity(new Intent(getActivity(), MyPrivatePollsActivity.class));
        } else if (view.getId() == R.id.btnBusnGrps) {
            startActivity(new Intent(getActivity(), BusinessGroups.class));
        } else if (view.getId() == R.id.btnPublcGrps) {
            startActivity(new Intent(getActivity(), PublicGroups.class));
        } else if (view.getId() == R.id.btnCreateGroup) {
            startActivity(new Intent(getActivity(), CreateGroupActivity.class));
        } else if (view.getId() == R.id.llbusiness) {
            startActivity(new Intent(getActivity(), BusinessGroups.class));
        } else if (view.getId() == R.id.llpublic) {
            startActivity(new Intent(getActivity(), PublicGroups.class));
        } else if (view.getId() == R.id.llpersonal) {
            startActivity(new Intent(getActivity(), PersonalGroups.class));
        }
    }

    public class GroupsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_groups, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mGroupPojoList.size();
        }

        GroupPojo mGroupPojo;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            mGroupPojo = mGroupPojoList.get(position);
            holder.tvGroupname.setText(mGroupPojo.getGroupname());
            holder.tvGroupdesc.setText(mGroupPojo.getDescription());

            Picasso.with(getActivity())
                    .load(AppConst.groupimages + mGroupPojo.getImage())
                    .transform(new RoundedSquareTransformation(10, 0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivGroupimage);

            if (mGroupPojo.getType().equalsIgnoreCase("0"))
                holder.iv_business.setImageDrawable(getResources().getDrawable(R.drawable.ic_person));
            else
                holder.iv_business.setImageDrawable(getResources().getDrawable(R.drawable.ic_business_center));

            Log.i("Logd",AppConst.groupimages + mGroupPojo.getImage());
           // Toast.makeText(getActivity(),,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getMyGroups();
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder {
        private ImageView ivGroupimage, iv_business;
        private TextView tvGroupname;
        private TextView tvGroupdesc;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            ivGroupimage = (ImageView) itemView.findViewById(R.id.iv_groupimage);
            iv_business = (ImageView) itemView.findViewById(R.id.iv_business);
            tvGroupname = (TextView) itemView.findViewById(R.id.tv_groupname);
            tvGroupdesc = (TextView) itemView.findViewById(R.id.tv_groupdesc);
            ivGroupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
            tvGroupdesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
            tvGroupname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
        }
    }
    public void getMyGroups() {
        M.showLoadingDialog(getActivity());
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getMyGroups(M.getID(getActivity()), new Callback<List<GroupPojo>>() {
            @Override
            public void success(List<GroupPojo> groupPojos, Response response) {
                if (groupPojos.size() > 0) {
                    mGroupPojoList = groupPojos;
                    rvGroups.setAdapter(mGroupsAdapter);
                    mGroupsAdapter.notifyDataSetChanged();
                }
                else {
                    rvGroups.setVisibility(View.GONE);
                    nogroups.setVisibility(View.VISIBLE);
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "You're not Created nor Participated in any groups. ", Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
                rvGroups.setVisibility(View.GONE);
                nogroups.setVisibility(View.VISIBLE);
            }
        });
    }
}
