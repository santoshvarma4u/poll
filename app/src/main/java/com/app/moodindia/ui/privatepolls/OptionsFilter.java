package com.app.moodindia.ui.privatepolls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivateOptions;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OptionsFilter extends AppCompatActivity implements View.OnClickListener {

    protected MagicButton btnDoneFilter;
    protected RecyclerView rvFilterOptions;
    RecyclerView.LayoutManager mLayoutManager;
    OptionsAdapter ola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_options_filter);
        initView();
    }

    @Override
    public void onClick(View view) {

    }

    private void initView() {

        rvFilterOptions = (RecyclerView) findViewById(R.id.rv_filter_options);

        rvFilterOptions = (RecyclerView) findViewById(R.id.rv_filter_options);
        mLayoutManager = new LinearLayoutManager(OptionsFilter.this);
        rvFilterOptions.setLayoutManager(mLayoutManager);
        rvFilterOptions.setItemAnimator(new DefaultItemAnimator());
        optionsList = new ArrayList<>();
        ola=new OptionsAdapter();
    }

    List<PrivateOptions> optionsList;

    public class OptionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.options_filter_private_poll, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return optionsList.size();
        }

        PrivateOptions listOptions;



        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listOptions = optionsList.get(position);

            holder.tv_filter_option.setText(listOptions.getPollOption());

             Picasso.with(OptionsFilter.this)
                    .load(AppConst.imgurl+listOptions.getImagePath())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.iv_option_image);

        }


        class ViewHolderFilter extends RecyclerView.ViewHolder {

            TextView tv_filter_option;
            ImageView iv_option_image;
            CheckBox cb_option;


            public ViewHolderFilter(View itemView) {
                super(itemView);
                tv_filter_option = (TextView) itemView.findViewById(R.id.tv_filter_option);
                iv_option_image = (ImageView) itemView.findViewById(R.id.iv_option_image);
                cb_option = (CheckBox) itemView.findViewById(R.id.cb_option);

                tv_filter_option.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(OptionsFilter.this, optionsList.get(getAdapterPosition()).getId()+"", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras().containsKey("pollid"))
        {
            getOptions(getIntent().getExtras().getString("pollid"));
        }
        else
        {
            Toast.makeText(OptionsFilter.this, "Unable to get options try again", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void getOptions(String pollid)
    {
        final int page=1;
        M.showLoadingDialog(OptionsFilter.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOptions(pollid, new Callback<List<PrivateOptions>>() {
            @Override
            public void success(List<PrivateOptions> privateOptionses, Response response) {
                M.hideLoadingDialog();
                optionsList=privateOptionses;
                rvFilterOptions.setAdapter(ola);
                ola.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
