package com.app.moodindia.ui.privatepolls.Groups;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.SplashActivity;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.QRCodeGenerator.QRGContents;
import com.app.moodindia.helper.QRCodeGenerator.QRGEncoder;
import com.app.moodindia.helper.simpletabbar.SimpleTabBar;
import com.app.moodindia.helper.simpletabbar.SimpleTabBarAdapter;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.ui.privatepolls.AddPollActivity;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs.GroupChatFragment;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs.GroupParticipantsFragment;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs.PollsGroupFragment;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.bluejamesbond.text.DocumentView;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.google.zxing.WriterException;
import com.ivankocijan.magicviews.views.MagicTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GroupInfoActivity extends AppCompatActivity implements View.OnClickListener {

    protected KenBurnsView kvGroupBackground;
    protected CircleImageView ivGroupImage;
    protected MagicTextView tvGroupname;
    protected SimpleTabBar sampletabs;
    protected ViewPager viewPager;
    public String groupId = "";
    public String uidforchat = "";
    public String groupName = "";
    public String chatId = "0";
    public String noOfParticipants = "0";
    public String noChats = "0";
    public String noPolls = "0";
    protected ImageView btnBackNav;
    protected MagicTextView tvGroupnameToolbar;
    protected ImageView icShare;
    protected ImageView ivShareGroup;
    protected ImageView imageView4;
    protected ImageView icInvite;
    protected ImageView icAdd,iv_scan_qr,share_qr;
    public String groupOwnerId;
    protected MagicTextView tvGroupowner;
    protected MagicTextView tvGroupId;
    private LinearLayout qrLyt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_group_info);
        initView();
        icInvite.setVisibility(View.GONE);
        //check user already joined or not
        //if not joined add user to participants table
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("uidforchat")) {
                //get groupid from this and continue to old process
                getGroup(getIntent().getExtras().getString("uidforchat"));
            } else {
                groupId = getIntent().getExtras().getString("groupid");
                groupOwnerId = getIntent().getExtras().getString("createdby");
                if (groupOwnerId != null && !groupOwnerId.equalsIgnoreCase(M.getID(GroupInfoActivity.this))) {
                    icInvite.setVisibility(View.GONE);
                }
                checkUserInGroup(groupId);
            }
        } else {
            Toast.makeText(this, "Oops this is Invalid group/Closed group", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(GroupInfoActivity.this, HomeActivity.class));
            finish();
        }
    }

    private void initView() {
        kvGroupBackground = (KenBurnsView) findViewById(R.id.kv_group_background);
        ivGroupImage = (CircleImageView) findViewById(R.id.iv_group_image);
        iv_scan_qr = (CircleImageView) findViewById(R.id.iv_scan_qr);
        qrLyt = (LinearLayout) findViewById(R.id.qrLyt);

        tvGroupname = (MagicTextView) findViewById(R.id.tv_groupname);
        tvGroupname.setOnClickListener(GroupInfoActivity.this);
        sampletabs = (SimpleTabBar) findViewById(R.id.sampletabs);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        btnBackNav = (ImageView) findViewById(R.id.btn_back_nav);
        btnBackNav.setOnClickListener(GroupInfoActivity.this);
        tvGroupnameToolbar = (MagicTextView) findViewById(R.id.tv_groupname_toolbar);
        icShare = (ImageView) findViewById(R.id.ic_share);
        icShare.setOnClickListener(GroupInfoActivity.this);
        ivShareGroup = (ImageView) findViewById(R.id.iv_share_group);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        share_qr = (ImageView) findViewById(R.id.share_qr);
        icInvite = (ImageView) findViewById(R.id.ic_invite);
        icInvite.setOnClickListener(GroupInfoActivity.this);
        icAdd = (ImageView) findViewById(R.id.ic_add);
        icAdd.setOnClickListener(GroupInfoActivity.this);
        tvGroupowner = (MagicTextView) findViewById(R.id.tv_groupowner);
        tvGroupowner.setOnClickListener(GroupInfoActivity.this);
        tvGroupId = (MagicTextView) findViewById(R.id.tv_groupId);
        tvGroupId.setOnClickListener(GroupInfoActivity.this);
        share_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClickShare(bitmap);
            }
        });

        qrLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClickShare(bitmap);
            }
        });
    }

    public void OnClickShare(Bitmap bitmap){

        try {
            File file = new File(this.getExternalCacheDir(),"logicchip.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Uri apkURI = FileProvider.getUriForFile(
                    getApplicationContext(),
                    getApplicationContext()
                            .getPackageName() + ".provider", file);
            intent.putExtra(Intent.EXTRA_STREAM, apkURI.fromFile(file));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkURI, "image/png");
            startActivity(Intent.createChooser(intent, "Share Group QR via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager, String groupid, String chatid, String chatCount, String pollCount, String ParticipantCount, String GroupName, String groupType, boolean Owner) {

        GroupChatFragment mGroupChatFragment = new GroupChatFragment();
        Bundle chatArgs = new Bundle();
        chatArgs.putString("groupid", groupid);
        chatArgs.putString("chatid", chatid);
        mGroupChatFragment.setArguments(chatArgs);

        GroupParticipantsFragment mGroupParticipantsFragment = new GroupParticipantsFragment();
        Bundle participantArgs = new Bundle();
        participantArgs.putString("groupid", groupid);
        participantArgs.putString("groupname", GroupName);
        participantArgs.putString("uidforchat", chatid);
        participantArgs.putString("grouptype", groupType);
        participantArgs.putString("isOwner", String.valueOf(Owner));
        mGroupParticipantsFragment.setArguments(participantArgs);

        PollsGroupFragment mPollsGroupFragment = new PollsGroupFragment();
        Bundle pollArgs = new Bundle();
        pollArgs.putString("groupid", groupid);
        pollArgs.putString("groupname", GroupName);
        pollArgs.putString("isOwner", String.valueOf(Owner));
        pollArgs.putString("grouptype", groupType);

        mPollsGroupFragment.setArguments(pollArgs);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mPollsGroupFragment, "Polls", R.color.white, R.color.colorPrimary, R.drawable.ic_list);
        adapter.addFragment(mGroupParticipantsFragment, "People", R.color.white, R.color.colorPrimary, R.drawable.ic_person);
        //  adapter.addFragment(mGroupChatFragment, "Chat", R.color.white, R.color.colorPrimary, R.drawable.ic_message);
        viewPager.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
    }


    Bitmap bitmap;

    private void generateQRCode(String inputValue) {

        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        //  smallerDimension = smallerDimension * 2 / 4;

        QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, smallerDimension);
        try {
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            iv_scan_qr.setImageBitmap(bitmap);

        } catch (WriterException e) {
            Log.v("GroupQR", e.toString());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(GroupInfoActivity.this, HomeActivity.class);
        it.putExtra("fragName", "4");
        startActivity(it);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back_nav) {
            Intent it = new Intent(GroupInfoActivity.this, HomeActivity.class);
            it.putExtra("fragName", "4");
            startActivity(it);
            finish();
        } else if (view.getId() == R.id.ic_share) {

            if (M.isExternalAlert(GroupInfoActivity.this)) {
                String groupJoinURL = "http://moodindia.in/groups/" + uChatId;
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, (groupJoinURL));
                sendIntent.putExtra(Intent.EXTRA_TEXT, (M.getUsername(GroupInfoActivity.this) + " has sent you request to join his/her group on MOODINDIA app. Best app to reflect your true MOOD. Join " + groupName + " group with Group Id:" + uChatId + ". And earn lots of rewards. \n" + groupJoinURL));
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
            } else {
                final DialogPlus notificationDialog = DialogPlus.newDialog(GroupInfoActivity.this)
                        .setGravity(Gravity.CENTER)
                        .setContentHolder(new ViewHolder(R.layout.custom_layout_invite_external))
                        .setCancelable(true)
                        .create();


                View notview = notificationDialog.getHolderView();

                final TextView tvNoticont = (TextView) notview.findViewById(R.id.tv_noticont);
                final TextView tv_notification = (TextView) notview.findViewById(R.id.tv_notification);
                final CheckBox cb_dontsee = (CheckBox) notview.findViewById(R.id.cb_dontsee);
                final DocumentView dv = (DocumentView) notview.findViewById(R.id.blogText);
                Typeface type = Typeface.createFromAsset(getAssets(), "fonts/ssr.ttf");
                dv.getDocumentLayoutParams().setTextTypeface(type);


                tvNoticont.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cb_dontsee.isChecked()) {
                            M.setExternalAlert(true, GroupInfoActivity.this);
                        }
                        String groupJoinURL = "http://moodindia.in/groups/" + uChatId;
                        Intent sendIntent = new Intent(Intent.ACTION_SEND);
                        sendIntent.setType("text/plain");
                        sendIntent.putExtra(Intent.EXTRA_TEXT, (groupJoinURL));
                        sendIntent.putExtra(Intent.EXTRA_TEXT, (M.getUsername(GroupInfoActivity.this) + " has sent you request to join his/her group on MOODINDIA app. Best app to reflect your true MOOD. Join " + groupName + " group with Group Id:" + uChatId + ". And earn lots of rewards. \n" + groupJoinURL));
                        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
                    }
                });
                notificationDialog.show();
            }

        } else if (view.getId() == R.id.ic_invite) {
            Intent intent = new Intent(GroupInfoActivity.this, GetParticipantsToInvite.class);
            intent.putExtra("groupid", groupId);
            intent.putExtra("uidforchat", uChatId);
            intent.putExtra("groupName", groupName);
            startActivity(intent);
        } else if (view.getId() == R.id.ic_add) {
            Intent intent = new Intent(GroupInfoActivity.this, AddPollActivity.class);
            intent.putExtra("groupid", groupId);
            startActivity(intent);
        } else if (view.getId() == R.id.tv_groupname) {

        } else if (view.getId() == R.id.tv_groupowner) {

        } else if (view.getId() == R.id.tv_groupId) {

        }
    }

    class ViewPagerAdapter extends SimpleTabBarAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final List<Integer> mTabColor = new ArrayList<>();
        private final List<Integer> mTextColor = new ArrayList<>();
        private final List<Integer> mIcon = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title, int TabColor, int TextColor, int icon) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            mTabColor.add(TabColor);
            mTextColor.add(TextColor);
            mIcon.add(icon);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getColorResource(int position) {
            return mTabColor.get(position);
        }

        @Override
        public int getTextColorResource(int position) {
            return mTextColor.get(position);
        }

        @Override
        public int getIconResource(int position) {
            return mIcon.get(position);
        }
    }

    public boolean isOwner = false;

    public void getGroupDetails() {
        if (groupId.length() > 0) {
            //  M.showLoadingDialog(GroupInfoActivity.this);
            PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
            mPrivatePollAPI.getGroupDetails(groupId, new Callback<List<GroupPojo>>() {
                @Override
                public void success(List<GroupPojo> groupPojos, Response response) {
                    if (groupPojos.size() > 0) {

                        final GroupPojo mGroupPojo = groupPojos.get(0);
                        groupName = mGroupPojo.getGroupname();
                        tvGroupnameToolbar.setText(groupName);
                        uChatId = mGroupPojo.getUidforchat();
                        generateQRCode("MIGROUP-"+uChatId);
                        Picasso.with(GroupInfoActivity.this)
                                .load(AppConst.groupimages + mGroupPojo.getImage())
                                .placeholder(R.drawable.default_image)
                                .error(R.drawable.default_image)
                                .into(ivGroupImage);

                        tvGroupId.setText("Group Id : "+mGroupPojo.getUidforchat());
                        tvGroupowner.setText("By : "+mGroupPojo.getOwnername());

                        Handler uiHandler = new Handler(Looper.getMainLooper());
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Picasso.with(GroupInfoActivity.this).load(AppConst.groupimages + mGroupPojo.getImage()).into(new Target() {
                                        @Override
                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                            Bitmap afterBlur = fastblur(bitmap, 1, 20);
                                            kvGroupBackground.setImageBitmap(afterBlur);

                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable errorDrawable) {

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                                        }
                                    });


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        noOfParticipants = mGroupPojo.getNoofparticipants();
                        tvGroupname.setText(mGroupPojo.getGroupname());
                        noPolls = mGroupPojo.getPollcount();
                        chatId = mGroupPojo.getUidforchat();
                        if (mGroupPojo.getType().equalsIgnoreCase("1") || mGroupPojo.getType().equalsIgnoreCase("2")) {


                            groupOwnerId = mGroupPojo.getCreated_by().toString();

                            // Toast.makeText(GroupInfoActivity.this, mGroupPojo.getCreated_by().toString()+"from local ---"+M.getID(GroupInfoActivity.this), Toast.LENGTH_LONG).show();

                            if (mGroupPojo.getCreated_by().toString().equalsIgnoreCase((M.getID(GroupInfoActivity.this)))) {
                                isOwner = true;
                                qrLyt.setVisibility(View.VISIBLE);

                            } else {
                                icAdd.setVisibility(View.GONE);
                                icShare.setVisibility(View.GONE);
                                qrLyt.setVisibility(View.GONE);
                            }
                        }else
                        {
                            groupOwnerId = mGroupPojo.getCreated_by().toString();
                            isOwner = true;
                            qrLyt.setVisibility(View.VISIBLE);
                        }

                        //  M.hideLoadingDialog();
                        //getMyPolls(groupId);
                       // Toast.makeText(GroupInfoActivity.this, isOwner+"", Toast.LENGTH_SHORT).show();
                        setupViewPager(viewPager, groupId, chatId, noChats, noPolls, noOfParticipants, mGroupPojo.getGroupname(), mGroupPojo.getType(), isOwner);
                        sampletabs.setupWithViewPager(viewPager);
                    } else {
                        Toast.makeText(GroupInfoActivity.this, "Invalid Group,Please try agian", Toast.LENGTH_SHORT).show();
                        finish();
                    }
//                    M.hideLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    //   M.hideLoadingDialog();
                    Toast.makeText(GroupInfoActivity.this, "Oops,Something went wrong please try again", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }


    public void checkUserInGroup(String groupid) {
        if (groupId.length() > 0) {
            // M.showLoadingDialog(GroupInfoActivity.this);
            PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
            mPrivatePollAPI.checkinuserGroup(M.getID(GroupInfoActivity.this), groupid, new Callback<String>() {
                @Override
                public void success(String statusPojos, Response response) {
                    if (statusPojos.equalsIgnoreCase("success")) {
                        getGroupDetails();
                    } else {
                        //Ask user popup to join the poll
                        showUserPopup();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(GroupInfoActivity.this, "Oops this is Invalid group/Closed group", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(GroupInfoActivity.this, SplashActivity.class));
                    finish();
                }
            });

        } else {
            Toast.makeText(this, "Oops this is Invalid group/Closed group", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(GroupInfoActivity.this, SplashActivity.class));
            finish();
        }
    }

    public void showUserPopup() {
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setButtonsColorRes(R.color.colorPrimaryDark)
                .setIcon(R.drawable.miwhitesmall)
                .setTitle("Join Group")
                .setMessage("It seems your not joined in this group yet. Do you want to join this group?")
                .setPositiveButton("Join", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addUserToGroup();
                    }
                })
                .setNegativeButton("Cancel", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      /*  startActivity(new Intent(GroupInfoActivity.this, GroupInfoActivity.class));
                        finish();*/
                    }
                })
                .show();

     /*   new SweetAlertDialog(GroupInfoActivity.this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Join Group")
                .setConfirmText("Join")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        addUserToGroup();
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        startActivity(new Intent(GroupInfoActivity.this, SplashActivity.class));
                        finish();
                    }
                })
                .setContentText("It seems your not joined in this group yet. Do you want to join this group?")
                .show();*/
    }

    public void addUserToGroup() {
        if (groupId.length() > 0) {
            M.showLoadingDialog(GroupInfoActivity.this);
            PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
            mPrivatePollAPI.addGroupParticipants(groupId, M.getID(GroupInfoActivity.this), "1", new Callback<List<GroupPojo>>() {
                @Override
                public void success(List<GroupPojo> groupPojos, Response response) {
                    M.hideLoadingDialog();
                    Intent intent = new Intent(GroupInfoActivity.this, GroupInfoActivity.class);
                    intent.putExtra("groupid", groupPojos.get(0).getGroupid());
                    intent.putExtra("createdby", groupPojos.get(0).getCreated_by());
                    startActivity(intent);
                    finish();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    Toast.makeText(GroupInfoActivity.this, "Oops this is Invalid group/Closed group", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(GroupInfoActivity.this, SplashActivity.class));
                    finish();
                }
            });
        }
    }

    public String uChatId = "";

    public void getGroup(String uidChat) {
        if (uidChat.length() > 0) {
            M.showLoadingDialog(GroupInfoActivity.this);
            PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
            mPrivatePollAPI.getGroupId(uidChat, new Callback<List<GroupPojo>>() {
                @Override
                public void success(List<GroupPojo> groupPojos, Response response) {
                    M.hideLoadingDialog();
                    if (groupPojos.size() > 0) {
                        groupId = groupPojos.get(0).getGroupid();
                        groupName = groupPojos.get(0).getName();
                        groupOwnerId = groupPojos.get(0).getCreated_by();
                        uChatId = groupPojos.get(0).getUidforchat();
                        if (groupOwnerId != null && !groupOwnerId.equalsIgnoreCase(M.getID(GroupInfoActivity.this))) {
                            icInvite.setVisibility(View.GONE);
                        }
                        checkUserInGroup(groupPojos.get(0).getGroupid());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    Toast.makeText(GroupInfoActivity.this, "Oops this is Invalid group/Closed group", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(GroupInfoActivity.this, SplashActivity.class));
                    finish();
                }
            });
        }
    }

    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);
        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;
        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];
        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {
                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];
                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;
                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];
                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];
                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;
                sir = stack[i + radius];
                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];
                rbs = r1 - Math.abs(i);
                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

}
