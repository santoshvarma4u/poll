package com.app.moodindia.ui.privatepolls;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.moodindia.BrandsActivity;
import com.app.moodindia.InfoActivity;
import com.app.moodindia.LoginActivity;
import com.app.moodindia.R;
import com.app.moodindia.ReportPollActivity;
import com.app.moodindia.SuggestPollActivity;
import com.app.moodindia.adapter.PrivateOptionAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;
import xyz.hanks.library.SmallBang;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class ClosedPrivatePollScreen extends YouTubeBaseActivity implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {

    TextView tvque,tvdate,tvPollCount,txtExp;
    LinearLayout lloptions,lytmap;
    Button btnsubmit;
    //FrameLayout frameLayout;
    //ImageView ivpoll;
    ListView lvoption;
    Bitmap mbitmap;
    File  imagePath;
    String TAG="pollscreen",pollid="",userid="";
    String info,que,start,end,type,img="",queid,seloptions="",totalPolls,twittertag,youtubetag,chattag;
    int single;
    Boolean isvoted;
    Button tvinfo;
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    ArrayList<String> idlist=new ArrayList<>();
    private Button btnViewMaps,tvaddparticipants;
    private Button btnSuggestPoll;
    MaterialDialog materialDialog;
    PrivateOptionAdapter adapter;
    ImageView ivpoll;
    boolean isConnected;
    Button btnParticipatedUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //supportPostponeEnterTransition();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_poll_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Fade().setDuration(500));
            getWindow().setExitTransition(new Fade().setDuration(500));
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // window.setStatusBarColor(ContextCompat.getColor(PrivatePollScreen.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it=new Intent(ClosedPrivatePollScreen.this,PrivatePolls.class);
                startActivity(it);
              //  supportFinishAfterTransition();
                overridePendingTransition(0,0);
            }
        });

//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pollid=getIntent().getExtras().getString("pollid");
        //Log.d("poll",pollid);
        userid= M.getID(ClosedPrivatePollScreen.this);
        tvque=(TextView)findViewById(R.id.tvque);
        tvdate=(TextView)findViewById(R.id.tvdate);
        tvinfo=(Button)findViewById(R.id.tvinfo);
        txtExp=(TextView)findViewById(R.id.txtExp);
        tvPollCount=(TextView)findViewById(R.id.tvPollCount);
        lloptions=(LinearLayout)findViewById(R.id.lloption);
        btnsubmit=(Button)findViewById(R.id.btnsubmit);
        tvaddparticipants = (Button) findViewById(R.id.tvaddparticipants);
        tvaddparticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ClosedPrivatePollScreen.this,ParticipatedUserActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("ques",tvque.getText().toString());
                intent.putExtra("postedtime",start);
                intent.putExtra("pollimage",img);
                startActivity(intent);
            }
        });
        btnParticipatedUsers=(Button)findViewById(R.id.btnParticipatedUsers);
        btnParticipatedUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnsubmit.setVisibility(View.GONE);
       /* frameLayout=(FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);*/
        btnSuggestPoll = (Button) findViewById(R.id.btnSuggestPoll);
        ivpoll=(ImageView) findViewById(R.id.ivpoll);
        // ivpoll.pause();
        lvoption=(ListView)findViewById(R.id.lvotedoption);
        // kvpoll=(KenBurnsView)findViewById(R.id.kvpoll);
        lvoption.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lytmap=(LinearLayout)findViewById(R.id.lytmap);
        setListViewHeightBasedOnChildren(lvoption);
        // getPollData();
        btnsubmit.setOnClickListener(this);
        tvinfo.setOnClickListener(this);
        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        btnViewMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ClosedPrivatePollScreen.this,PrivateResultMapActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("from",getIntent().getExtras().getString("from"));
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ClosedPrivatePollScreen.this,SuggestPollActivity.class);
                startActivity(intent);
            }
        });

        checkConnection();

        if(isConnected)
        {
            if(getIntent().getExtras().containsKey("from"))
                getPollData(getIntent().getExtras().getString("from"));
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public void showParticipatedUsers(){

    }

    @Override
    protected void onResume() {
        super.onResume();



        //moved to on create
       /* if(getIntent().getExtras().containsKey("from"))
            getPollData(getIntent().getExtras().getString("from"));
*/
    }

    public void realtimeLoading()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds

                if(getIntent().getExtras().containsKey("from"))
                {
                    if(getIntent().getExtras().getString("from").equalsIgnoreCase("active"))
                    {
                        getRealTimePollData();
                        handler.postDelayed(this, 10000);
                    }
                }
                //  getPollData(getIntent().getExtras().getString("from"));

            }
        }, 500);
    }

    boolean isVoteLocal=false;

    private SmallBang mSmallBang;

    private void getPollData(String active) {
        // AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
       // M.showLoadingDialog(ClosedPrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOption(userid,pollid,"notActive",new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {

                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            tvinfo.setEnabled(true);
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            if(isvoted)
                            {

                                btnViewMaps.setVisibility(View.VISIBLE);
                                //   frameLayout.setVisibility(View.VISIBLE);

                                if(getIntent().getExtras().containsKey("stats")){
                                    tvinfo.performClick();
                                }
                                else
                                {
                                    tvque.setBackgroundColor(Color.parseColor("#e3e9ef"));
                                    tvque.getBackground().setAlpha(51);
                                    tvque.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    //  tvque.setBackgroundColor(getResources().getColor(R.color.light_green));
                                    //realtimeLoading();
                                }


                            }
                            else
                            {
                                btnViewMaps.setVisibility(View.GONE);

                            }


                            tvque.setText(que);
                            if (img != "") {
                                Picasso.with(ClosedPrivatePollScreen.this)
                                        .load(AppConst.imgurl + img)
                                        .placeholder(R.drawable.default_image)
                                        .error(R.drawable.default_image)
                                        .into(ivpoll);

                            }


                            try {
                                tvdate.setText("Posted on "+dtfmt.format(defaultfmt.parse(start)));
                                Date dt=defaultfmt.parse(end);
                                // txtExp.setVisibility(View.VISIBLE);
                                RelativeTimeTextView v = (RelativeTimeTextView)findViewById(R.id.timestamp);
                                v.setReferenceTime(dt.getTime());


                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                           /* if(!isvoted) {
                                // getBrandDialog();
                                for (final Pollresult data : pojo.getPollresult()) {

                                    final int id = data.getOptionid();

                                    LayoutInflater factory = LayoutInflater.from(ClosedPrivatePollScreen.this);
                                    View myView = factory.inflate(R.layout.view_option_row, null);

                                    final TextView tv_poll_question=(TextView)myView.findViewById(R.id.tvoption);
                                    CircularImageView iv_option=(CircleImageView)myView.findViewById(R.id.option_image);


                                    tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv_poll_question.setBackgroundResource(R.drawable.button_rounded_corner);
                                    tv_poll_question.setText(data.getOptionvalue());

                                    if(data.getOptionimage().length()>0)
                                    {
                                        Picasso.with(ClosedPrivatePollScreen.this)
                                                .load(AppConst.imgurl + data.getOptionimage())
                                                .placeholder(R.drawable.default_image)
                                                .error(R.drawable.default_image)
                                                .resize(40,40)
                                                .into(iv_option);
                                    }
                                    else
                                    {
                                        iv_option.setVisibility(View.INVISIBLE);
                                    }


                                    iv_option.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Dialog dialog=new Dialog(ClosedPrivatePollScreen.this);
                                            dialog.setContentView(R.layout.custom_image_dialog);
                                            ImageView iv_option_image=(ImageView)dialog.findViewById(R.id.iv_full_option_image);
                                            Picasso.with(ClosedPrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .into(iv_option_image);
                                            dialog.show();
                                        }
                                    });


                                    tv_poll_question.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(ClosedPrivatePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(ClosedPrivatePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


                                    lloptions.setOrientation(LinearLayout.VERTICAL);
                                    lloptions.addView(myView, layoutParams);
                                    //  lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);

                                adapter = new PrivateOptionAdapter(ClosedPrivatePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);

                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }*/
                            lloptions.setVisibility(View.GONE);
                            btnsubmit.setVisibility(View.GONE);
                            lvoption.setVisibility(View.VISIBLE);
                            lytmap.setVisibility(View.VISIBLE);

                            adapter = new PrivateOptionAdapter(ClosedPrivatePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);

                            lvoption.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
              //  M.hideLoadingDialog();

                //supportStartPostponedEnterTransition();

                // ivpoll.resume();
            }
            @Override
            public void failure(RetrofitError error) {
              //  M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }



    private void getRealTimePollData() {
        //AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
        // M.showLoadingDialog(ClosedPrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOption(userid,pollid,"active",new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {
                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            //Log.d("info",info);
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");

                            if(!isvoted) {


                                btnViewMaps.setVisibility(View.GONE);

                                for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();

                                    LayoutInflater factory = LayoutInflater.from(ClosedPrivatePollScreen.this);
                                    View myView = factory.inflate(R.layout.view_option_row, null);

                                    final TextView tv_poll_question=(TextView)myView.findViewById(R.id.tvoption);
                                    final CircleImageView iv_option=(CircleImageView)myView.findViewById(R.id.option_image);


                                    tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv_poll_question.setBackgroundResource(R.drawable.button_rounded_corner);
                                    tv_poll_question.setText(data.getOptionvalue());

                                  /*  final TextView tv = new TextView(ClosedPrivatePollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(18);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    final CircularImageView iv_option=new CircularImageView(ClosedPrivatePollScreen.this);*/
                                /*    iv_option.setMaxWidth(40);
                                    iv_option.setMaxHeight(40);*/

                                    if(data.getOptionimage().length()>0)
                                    {
                                        Picasso.with(ClosedPrivatePollScreen.this)
                                                .load(AppConst.imgurl + data.getOptionimage())
                                                .placeholder(R.drawable.default_image)
                                                .error(R.drawable.default_image)
                                                .into(iv_option);
                                    }

                                    iv_option.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Dialog dialog=new Dialog(ClosedPrivatePollScreen.this);
                                            dialog.setContentView(R.layout.custom_image_dialog);
                                            ImageView iv_option_image=(ImageView)dialog.findViewById(R.id.iv_full_option_image);
                                            Picasso.with(ClosedPrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .into(iv_option_image);
                                            dialog.show();
                                        }
                                    });

                                    tv_poll_question.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(ClosedPrivatePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(ClosedPrivatePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                    lloptions.setOrientation(LinearLayout.VERTICAL);

                                    // lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                btnViewMaps.setVisibility(View.VISIBLE);
                                //  frameLayout.setVisibility(View.VISIBLE);
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                PrivateOptionAdapter adapter = new PrivateOptionAdapter(ClosedPrivatePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                // M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnsubmit){
            String ans="";
            for(int i=0;i<idlist.size();i++){
                if(i==0)
                    ans=idlist.get(i);
                else
                    ans=ans+","+idlist.get(i);
            }
            submitresult(ans);
        }else if(view.getId()==R.id.tvinfo){
            Intent it=new Intent(ClosedPrivatePollScreen.this,InfoActivity.class);
            it.putExtra("info",info);
            it.putExtra("question",que);
            it.putExtra("image",img);
            it.putExtra("screen","pollscreen");
            it.putExtra("twittertag",twittertag);
            it.putExtra("youtubetag",youtubetag);
            it.putExtra("chattag",chattag);
            it.putExtra("startdate",start);
            it.putExtra("enddate",end);
            it.putExtra("isvoted",isvoted);
            it.putExtra("from",getIntent().getExtras().getString("from"));
            it.putExtra("pollid",pollid);

            startActivity(it);
        }
    }

    public void submitresult(final String ans){
        //Log.d(TAG,"ans:"+ans);
        M.showLoadingDialog(ClosedPrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.submitPrivateAns(queid,ans,M.getID(ClosedPrivatePollScreen.this),new retrofit.Callback<SubmitAnsPojo>() {
            @Override
            public void success(SubmitAnsPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess()==1){
                        // AppConst.selpoll=null;
                        PollOptionPojo obj=new PollOptionPojo();
                        obj.setEndtime(end);
                        obj.setImage(img);
                        obj.setInfo(info);
                        obj.setIsvoted(true);
                        obj.setSelected_option(ans);
                        obj.setPollid(Integer.valueOf(queid));
                        obj.setPollresult(pojo.getPollresult());
                        obj.setQuestion(que);
                        obj.setStarttime(start);
                        obj.setType(type);
                        AppConst.selpoll=obj;

                        //

                        sendPushToOtherParticipants(queid,M.getID(ClosedPrivatePollScreen.this));


                      /*  new SweetAlertDialog(ClosedPrivatePollScreen.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Mood India")
                                .setContentText("Thanyou for your vote.Do you want to suggest a poll?")
                                .setConfirmText("Yes,I want!")
                                .setCancelText("No")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        pollid=getIntent().getExtras().getString("pollid");

                                        Intent it=new Intent(ClosedPrivatePollScreen.this,PollScreen.class);
                                        it.putExtra("pollid",pollid);
                                        it.putExtra("from","active");
                                        finish();
                                        startActivity(it);
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {

                                        Intent intent=new Intent(ClosedPrivatePollScreen.this,SuggestPollActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .show();*/




                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_poll,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
         if(id==R.id.share){


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ClosedPrivatePollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                }
                else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else {
                        sharepollkitkat();
                    }
                }
            }




            /*Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            //this is the text that will be shared
           // sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));*/
            return true;
        }

        else if(id==R.id.report){
            Intent intent=new Intent(ClosedPrivatePollScreen.this,ReportPollActivity.class);
            intent.putExtra("pollid",pollid);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(ClosedPrivatePollScreen.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getIntent().getExtras().containsKey("fragName"))
        {
            Intent it=new Intent(this,PrivatePolls.class);
            it.putExtra("fragName",getIntent().getExtras().get("fragName").toString());
            startActivity(it);
            //supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else
        {
            Intent it=new Intent(this,PrivatePolls.class);
            startActivity(it);
           // //supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }

    }


    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }
    public void sharepoll(String path) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/gif");
        Uri myUri = Uri.parse("file://" + path);
        sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("New Private Poll is ready for our opinions " +
                "Please participate and express your opinion.  \n"+que+" \n Poll ID:"+pollid+"  \n http://moodindia.in/privatepoll/"+pollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }
    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/privatepoll/"+pollid));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }


    public void getBrandDialog()
    {
        Intent intent=new Intent(ClosedPrivatePollScreen.this,BrandsActivity.class);
        startActivityForResult(intent,1);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }

    }

    public void sendPushToOtherParticipants(String privatepollid,String userid) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(ConsantURL+"app/Http/Controllers/sendPrivateNotifications.php?submitanswer=true&privatepollid="+privatepollid+"&userid="+userid).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {

                if (response.isSuccessful()) {

                    M.hideLoadingDialog();
                    Intent it=new Intent(ClosedPrivatePollScreen.this,PrivatePolls.class);
                    it.putExtra("pollid",pollid);
                    it.putExtra("from","active");
                    finish();
                    startActivity(it);

                }
                else {
                    Toast.makeText(ClosedPrivatePollScreen.this, "Something went wrong!", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}
