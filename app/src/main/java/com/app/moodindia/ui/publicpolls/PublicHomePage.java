package com.app.moodindia.ui.publicpolls;

/**
 * Created by SantoshT on 8/14/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.CurrentPolls;
import com.app.moodindia.HistoryActivity;
import com.app.moodindia.LoginActivity;
import com.app.moodindia.MessagesActivity;
import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.SelectCity;
import com.app.moodindia.SelectState;
import com.app.moodindia.SettingActivity;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.ui.privatepolls.AcceptPollActivity;
import com.app.moodindia.ui.privatepolls.PrivatePollScreen;
import com.app.moodindia.ui.topics.TopicsActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.MyApplication;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hlab.fabrevealmenu.listeners.OnFABMenuSelectedListener;
import com.jaredrummler.android.device.DeviceName;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.sa90.materialarcmenu.ArcMenu;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Picasso;
import com.wooplr.spotlight.SpotlightView;
import com.wooplr.spotlight.utils.SpotlightListener;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;
import com.zplesac.connectionbuddy.models.ConnectivityStrength;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PublicHomePage extends AppCompatActivity implements OnFABMenuSelectedListener,ConnectivityReceiver.ConnectivityReceiverListener, ConnectivityChangeListener {

    TabLayout tabLayout;
    ViewPager viewPager;
    TextView tv_country,tv_state,tv_city,tv_cityup,tv_lowconnection;
    FloatingActionButton fab;
    ImageView tv_cityupicn;
    ImageView priate_poll_search;
    LinearLayout lytlownetwork;
    Button btn_join_poll_by_id,btn_sign_a_pitition;
    ArcMenu fabRevealMenu;
    ImageView homepageHeaderImage;
    TextView networkclose;
    public boolean isFirstTime=false;
    CoordinatorLayout main_content;

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");

    FloatingActionButton archive,categories,profile,notifications,privatepolls;
    String deviceid="",deviceName="";
    boolean isConnected;
    FrameLayout llcorusel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_public_polls);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //window.setStatusBarColor(ContextCompat.getColor(PublicHomePage.this, R.color.colorPrimaryDark));
        deviceid = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
        deviceName = DeviceName.getDeviceName();
        tabLayout=(TabLayout)findViewById(R.id.tablayout);
        lytlownetwork=(LinearLayout)findViewById(R.id.lytlownetwork);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
      //  llcorusel=(FrameLayout) findViewById(R.id.llcorusel);
        tv_country=(TextView)findViewById(R.id.txtCountry);
        networkclose=(TextView)findViewById(R.id.networkclose);
        tv_lowconnection=(TextView)findViewById(R.id.tv_lowconnection);
        btn_join_poll_by_id=(Button) findViewById(R.id.btn_join_poll_by_id);
        btn_sign_a_pitition=(Button) findViewById(R.id.btn_sign_a_pitition);
        tv_state=(TextView)findViewById(R.id.txtState);
        tv_city=(TextView)findViewById(R.id.txtCity);
        tv_cityup=(TextView)findViewById(R.id.txtCityup);
        tv_cityupicn=(ImageView)findViewById(R.id.imageView5);
        homepageHeaderImage=(ImageView)findViewById(R.id.homepageHeaderImage);
        priate_poll_search=(ImageView)findViewById(R.id.priate_poll_search);
        priate_poll_search.setVisibility(View.GONE);
        fabRevealMenu=(ArcMenu) findViewById(R.id.fabMenu);
        archive=(FloatingActionButton)findViewById(R.id.menu_archive);
        categories=(FloatingActionButton)findViewById(R.id.menu_categories);
        profile=(FloatingActionButton)findViewById(R.id.menu_profile);
        notifications=(FloatingActionButton)findViewById(R.id.menu_messages);
        //privatepolls=(FloatingActionButton)findViewById(R.id.menu_private_polls);
        privatepolls=(FloatingActionButton)findViewById(R.id.menu_private_polls);
        main_content=(CoordinatorLayout)findViewById(R.id.main_content);

        final SpaceNavigationView spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("Profile", R.drawable.ic_person_white));
        spaceNavigationView.addSpaceItem(new SpaceItem("Categories", R.drawable.catsarc));
        spaceNavigationView.addSpaceItem(new SpaceItem("Notifications", R.drawable.alarm));
        spaceNavigationView.addSpaceItem(new SpaceItem("Archives", R.drawable.archive));
        spaceNavigationView.showIconOnly();


        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

                Intent intent=new Intent(PublicHomePage.this,LoginActivity.class);
                intent.putExtra("frompublic","true");
                startActivity(intent);
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if(itemName.equalsIgnoreCase("Categories"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else if(itemName.equalsIgnoreCase("Notifications"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else if(itemName.equalsIgnoreCase("Archives"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }

       //  Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                //  Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                if(itemName.equalsIgnoreCase("Categories"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else if(itemName.equalsIgnoreCase("Notifications"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else if(itemName.equalsIgnoreCase("Archives"))
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
                else
                {
                    Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                    it.putExtra("frompublic","true");
                    startActivity(it);
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Explode().setDuration(500));

            getWindow().setExitTransition(new Fade().setDuration(500));
        }


        final ExpandableRelativeLayout expandableLayout
                = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);

        // toggle expand, collapse
        expandableLayout.toggle();
        // expand
        // collapse
        expandableLayout.collapse();

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PublicHomePage.this,SettingActivity.class);
                intent.putExtra("frompublic","true");
                startActivity(intent);
            }
        });
        archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PublicHomePage.this,HistoryActivity.class);
                intent.putExtra("fromact","home");
                startActivity(intent);
            }
        });
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PublicHomePage.this,MessagesActivity.class);
                startActivity(intent);
            }
        });
        categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(PublicHomePage.this, LoginActivity.class);
                it.putExtra("frompublic","true");
                startActivity(it);
            }
        });

         /*    privatepolls.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(PublicHomePage.this,PrivatePolls.class);
                    startActivity(intent);
                }
            });
*/
        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* Intent intent=new Intent(PublicHomePage.this,SelectCountry.class);
                    startActivityForResult(intent,1);*/
            }
        });
        tv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv_country.getText().toString().equalsIgnoreCase("Country")) {
                    M.showToast(PublicHomePage.this,"Please Select Country First");
                }
                else {
                    Intent intent=new Intent(PublicHomePage.this,SelectState.class);
                    intent.putExtra("countryid",M.getMyCountryID(PublicHomePage.this));
                    startActivityForResult(intent,2);
                }
            }
        });
        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_state.getText().toString().equalsIgnoreCase("State")) {
                    M.showToast(PublicHomePage.this,"Please Select State First");
                }
                else {
                    Intent intent=new Intent(PublicHomePage.this,SelectCity.class);
                    intent.putExtra("stateid",M.getMyStateID(PublicHomePage.this));
                    startActivityForResult(intent,3);
                }
            }
        });

        tv_cityup.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                        if(expandableLayout.isExpanded())
                        {
                            //llcorusel.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            // llcorusel.setVisibility(View.GONE);
                        }
                    }
                }

        );
        tv_cityupicn.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                        if(expandableLayout.isExpanded())
                        {
                           // llcorusel.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                           // llcorusel.setVisibility(View.GONE);
                        }
                    }
                }
        );


        networkclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lytlownetwork.setVisibility(View.GONE);
            }
        });
        //Log.i("length",M.getMyState(PublicHomePage.this)+"");
        if(M.getMyCountryID(PublicHomePage.this).length()>1){
            tv_country.setText(M.getMyCountry(PublicHomePage.this));
        }
        else {
            M.setMyCountry("India",PublicHomePage.this);
            M.setMyCountryID("101",PublicHomePage.this);
            tv_country.setText(M.getMyCountry(PublicHomePage.this));
            Intent i = new Intent(PublicHomePage.this, PublicHomePage.class);  //your class
            startActivity(i);
        }
        if(M.getMyState(PublicHomePage.this).length()>1)
        {
            tv_state.setText(M.getMyState(PublicHomePage.this));
            tv_cityup.setText(M.getMyState(PublicHomePage.this));
        }
        if(M.getMyCity(PublicHomePage.this).length()>1)
        {
            tv_city.setText(M.getMyCity(PublicHomePage.this));
            tv_cityup.setText(M.getMyCity(PublicHomePage.this));
        }
        checkConnection();



        Picasso.with(PublicHomePage.this)
                .load("http://moodindia.in/headerImages/intro3.jpeg")
                .noPlaceholder()
                .into(homepageHeaderImage);

        final DialogPlus dialog = DialogPlus.newDialog(PublicHomePage.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_join_poll))
                .setCancelable(true)
                .create();

        final DialogPlus notificationDialog = DialogPlus.newDialog(PublicHomePage.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_push_notification))
                .setCancelable(true)
                .create();


        View view=dialog.getHolderView();

        View notiView=notificationDialog.getHolderView();

        final TextView tvNotiMessage=(TextView) notiView.findViewById(R.id.tv_notification);
        final TextView tvNoticont=(TextView) notiView.findViewById(R.id.tv_noticont);
        final TextView tvNotiCancel=(TextView)notiView.findViewById(R.id.tv_noticancel);
        final TextView tvNotiPollid=(TextView)notiView.findViewById(R.id.tv_noti_pollid);


        if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("from"))
        {
            if(getIntent().getExtras().getString("from").equalsIgnoreCase("push"))
            {
                tvNotiMessage.setText(getIntent().getExtras().getString("message").toString());
                tvNotiPollid.setText(getIntent().getExtras().getString("notipollid").toString());
                notificationDialog.show();
            }
        }

        tvNoticont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getExtras().containsKey("notipollid"))
                {
                    if(getIntent().getExtras().getString("message").toString().toLowerCase().contains("community poll"))
                    {
                        Toast.makeText(PublicHomePage.this,getIntent().getExtras().get("notipollid").toString(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(PublicHomePage.this, PrivatePollScreen.class);
                        intent.putExtra("pollid",getIntent().getExtras().get("notipollid").toString());
                        intent.putExtra("from","active");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Intent intent = new Intent(PublicHomePage.this, PollScreen.class);
                        intent.putExtra("pollid",getIntent().getExtras().get("notipollid").toString());
                        intent.putExtra("from","active");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                }
                else
                {
                    notificationDialog.dismiss();
                }
            }
        });

        tvNotiCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationDialog.dismiss();
            }
        });

        final EditText et_join_poll=(EditText)view.findViewById(R.id.et_join_poll_id);
        final TextView tvJoinDialog=(TextView) view.findViewById(R.id.tvJoinDialog);
        final TextView tvCancelJoinDialog=(TextView)view.findViewById(R.id.tvCancelJoinDialog);
        et_join_poll.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                }
                return false;
            }
        });
        et_join_poll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
            }
        });
        tvJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_join_poll.getText().toString().length() > 0)
                {
                    if(!M.getID(PublicHomePage.this).isEmpty())
                    {
                        validateJoinPoll(et_join_poll.getText().toString(),M.getID(PublicHomePage.this).toString());
                    }
                    else {
                        Toast.makeText(PublicHomePage.this, "You need to login to join in personal polls!", Toast.LENGTH_SHORT).show();
                    }

                  /*  Intent intent=new Intent(PublicHomePage.this, AcceptPollActivity.class);
                    intent.putExtra("poll_id",et_join_poll.getText().toString());
                    startActivity(intent);*/
                }
                else {
                    et_join_poll.setError("Enter a valid personal poll id");
                }

            }
        });

        tvCancelJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing())
                    dialog.dismiss();

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);

            }
        });

        btn_join_poll_by_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        btn_sign_a_pitition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //petitions cat id : 19
                Intent intent=new Intent(PublicHomePage.this,CurrentPolls.class);
                intent.putExtra("act_name","recent");
                intent.putExtra("catid","19");
                intent.putExtra("catname","United India Petitions");
                startActivity(intent);
            }
        });
        priate_poll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* new LovelyTextInputDialog(PublicHomePage.this, R.style.EditTextTintTheme)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle("Mood India")
                        .setMessage("Please Enter Poll Id")
                      //  .setIcon(R.drawable.miwhitesmall)
                        .setInputFilter("Enter valid poll Id", new LovelyTextInputDialog.TextFilter() {
                            @Override
                            public boolean check(String text) {
                                return text.matches("\\w+");
                            }
                        })
                        .setInputType(InputType.TYPE_CLASS_NUMBER)
                        .setConfirmButton("Confirm", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                            @Override
                            public void onTextInputConfirmed(String text) {

                              //  Toast.makeText(PublicHomePage.this, text, Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(PublicHomePage.this, AcceptPollActivity.class);
                                intent.putExtra("poll_id",text);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .show();*/
            }
        });

       /* PreferencesManager mPreferencesManager = new PreferencesManager(PublicHomePage.this);
        mPreferencesManager.resetAll();*/

        spotView = new SpotlightView.Builder(this)
                .introAnimationDuration(400)
                .enableRevealAnimation(true)
                .performClick(true)
                .fadeinTextDuration(400)
                .headingTvColor(Color.parseColor("#eb273f"))
                .headingTvSize(32)
                .headingTvText("Regional Polls")
                .subHeadingTvColor(Color.parseColor("#ffffff"))
                .subHeadingTvSize(16)
                .subHeadingTvText("By clicking here you can filter polls based on Cities and States")
                .maskColor(Color.parseColor("#dc000000"))
                .target(findViewById(R.id.imageView5))
                .lineAnimDuration(400)
                .lineAndArcColor(Color.parseColor("#eb273f"))
                .dismissOnTouch(true)
                .dismissOnBackPress(true)
                .enableDismissAfterShown(true)
                .usageId("rpollsevent")
                .setListener(new SpotlightListener() {
                    @Override
                    public void onUserClicked(String s) {
                        isFirstTime=true;
                        doSequenceForRegionalPolls();
                    }
                })//UNIQUE ID
                .show();


    /*    TutoShowcase.from(this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {

                    }
                })
                .setContentView(R.layout.tuto_sample)
                .on(R.id.imageView5) //a view in actionbar
                .addCircle()
                .withBorder()
                .show();
*/

    }

    public void doSequenceForRegionalPolls()
    {
        tv_cityup.performClick();

         new SpotlightView.Builder(this)
                .introAnimationDuration(400)
                .enableRevealAnimation(true)
                .performClick(true)
                .fadeinTextDuration(400)
                .headingTvColor(Color.parseColor("#eb273f"))
                .headingTvSize(32)
                .headingTvText("State Polls")
                .subHeadingTvColor(Color.parseColor("#ffffff"))
                .subHeadingTvSize(16)
                .subHeadingTvText("Click to filter polls based on State")
                .maskColor(Color.parseColor("#dc000000"))
                .target(tv_state)
                .lineAnimDuration(400)
                .lineAndArcColor(Color.parseColor("#eb273f"))
                .dismissOnTouch(true)
                .dismissOnBackPress(true)
                .enableDismissAfterShown(true)
                .usageId("statefilter")
                .setListener(new SpotlightListener() {
                    @Override
                    public void onUserClicked(String s) {
                        citySpotlightView();
                    }
                })//UNIQUE ID
                .show();
      /*  new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                SpotlightSequence.getInstance(PublicHomePage.this,null)
                        .addSpotlight(tv_state, "State Polls ", "Click to filter polls based on State", "")
                        .addSpotlight(tv_city, "City Polls", "Click to filter polls based on City ", "cityfilter")
                        .startSequence();
            }
        },400);
*/

    }
    public void citySpotlightView()
    {
        new SpotlightView.Builder(this)
                .introAnimationDuration(400)
                .enableRevealAnimation(true)
                .performClick(true)
                .fadeinTextDuration(400)
                .headingTvColor(Color.parseColor("#eb273f"))
                .headingTvSize(32)
                .headingTvText("City Polls")
                .subHeadingTvColor(Color.parseColor("#ffffff"))
                .subHeadingTvSize(16)
                .subHeadingTvText("Click to filter polls based on City")
                .maskColor(Color.parseColor("#dc000000"))
                .target(tv_city)
                .lineAnimDuration(400)
                .lineAndArcColor(Color.parseColor("#eb273f"))
                .dismissOnTouch(true)
                .dismissOnBackPress(true)
                .enableDismissAfterShown(true)
                .usageId("cityfilter")
                .setListener(new SpotlightListener() {
                    @Override
                    public void onUserClicked(String s) {
                        tv_cityup.performClick();
                        int pos = viewPager.getCurrentItem();
                        Fragment fragment = adapter.getRegisteredFragment(pos);
                        switch (pos) {
                            case 0: {
                                ((PublicPollsFragment) fragment).eventRaise();
                                break;
                            }
                        }
                    }
                })//UNIQUE ID
                .show();
    }

    SpotlightView spotView;
    public void svProfile()
    {

    } public void svCategories()
    {

    } public void svCommunityPolls()
    {

    } public void svNotifications()
    {

    } public void svArchives()
    {

    }


    private void validateJoinPoll(final String pollid, String userid)
    {
        M.showLoadingDialog(PublicHomePage.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getAcceptPoll(userid, pollid, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojo, Response response) {
                if(statusPojo.get(0).getStatus().equalsIgnoreCase("A"))
                {
                    M.hideLoadingDialog();
                    //Poll Accepted
                    //Redirect to poll screen
                    Toast.makeText(PublicHomePage.this, "Poll already accepted", Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(PublicHomePage.this,PrivatePollScreen.class);
                    intent.putExtra("from","active");
                    intent.putExtra("pollid",pollid);
                    startActivity(intent);
                }
                else if(statusPojo.get(0).getStatus().equalsIgnoreCase("NA"))
                {
                    M.hideLoadingDialog();
                    getPollDetails(pollid);
                }
                else {
                    M.hideLoadingDialog();
                    Toast.makeText(PublicHomePage.this, "Invalid Poll ID, Try Correct Poll id", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(PublicHomePage.this, "Unknown Error,Please try later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getPollDetails(final String pollid)
    {
        M.showLoadingDialog(PublicHomePage.this);
        final PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getPrivatePollById(pollid, 1, new Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> privatePollPojos, Response response) {

                if(privatePollPojos.size()>0)
                {
                    M.hideLoadingDialog();

                    Intent intent=new Intent(PublicHomePage.this, AcceptPollActivity.class);
                    intent.putExtra("poll_id",pollid);
                    startActivity(intent);

                }
                else
                {
                    M.hideLoadingDialog();
                    Toast.makeText(PublicHomePage.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(PublicHomePage.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();
            }
        });
    }
    ViewPagerAdapter adapter;
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PublicPollsFragment(), " Recent");
        //adapter.addFragment(new PollsFragment(), " Tranding Polls");
        // adapter.addFragment(new PollsFragment(), " Answered Pools");
        adapter.addFragment(new PublicTrandingPollsFragment(), " Trending");
        adapter.addFragment(new JoinUs(), " Join Us");
        viewPager.setAdapter(adapter);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode == RESULT_OK)
        {
            //country call back
            M.setMyCountry(data.getExtras().getString("CountryName").toString(),PublicHomePage.this);
            M.setMyCountryID(data.getExtras().getString("CountryId").toString(),PublicHomePage.this);
            tv_country.setText(data.getExtras().getString("CountryName").toString());
            tv_state.setText("Select State");
            tv_city.setText("Select City");
            M.setMyStateID("",PublicHomePage.this);
            M.setMyState("",PublicHomePage.this);
            M.setMyCity("",PublicHomePage.this);
            M.setMyCityID("",PublicHomePage.this);
            Intent i = new Intent(PublicHomePage.this, PublicHomePage.class);  //your class
            startActivity(i);
            finish();
            //tv_state.performClick();
            //M.showToast(PublicHomePage.this,data.getExtras().getString("CountryId").toString());
        }
        else if(requestCode==2 && resultCode == RESULT_OK)
        {//state call back
            if(data.getExtras().getString("StateName").toString().equals("0")&&data.getExtras().getString("StateId").toString().equals("0"))
            {
                M.setMyStateID("",PublicHomePage.this);
                M.setMyState("",PublicHomePage.this);
                M.setMyCity("",PublicHomePage.this);
                M.setMyCityID("",PublicHomePage.this);
                tv_state.setText("State");
                tv_city.setText("City");
                tv_cityup.setText("Regional Polls");
            }
            else{
                M.setMyState(data.getExtras().getString("StateName").toString(),PublicHomePage.this);
                M.setMyStateID(data.getExtras().getString("StateId").toString(),PublicHomePage.this);
                tv_state.setText(data.getExtras().getString("StateName").toString());
                tv_city.setText("Select City");
                tv_cityup.setText(data.getExtras().getString("StateName").toString());
                M.setMyCityID("",PublicHomePage.this);
                M.setMyCity("",PublicHomePage.this);
                Intent i = new Intent(PublicHomePage.this, PublicHomePage.class);  //your class
                startActivity(i);
                finish();
            }
        }
        else if(requestCode==3 && resultCode == RESULT_OK) {//city call back

            if (data.getExtras().getString("CityName").toString().equals("0") && data.getExtras().getString("CityId").toString().equals("0")) {
                M.setMyCity("", PublicHomePage.this);
                M.setMyCityID("", PublicHomePage.this);
                tv_city.setText("City");
                tv_cityup.setText(M.getMyState(PublicHomePage.this));
            } else {
                M.setMyCity(data.getExtras().getString("CityName").toString(), PublicHomePage.this);
                M.setMyCityID(data.getExtras().getString("CityId").toString(), PublicHomePage.this);
                tv_city.setText(data.getExtras().getString("CityName").toString());
                tv_cityup.setText(data.getExtras().getString("CityName").toString());

                Intent i = new Intent(PublicHomePage.this, PublicHomePage.class);  //your class
                startActivity(i);
                finish();
            }
        }
    }

    @Override
    public void onMenuItemSelected(View view) {
        int id = (int) view.getTag();
        if (id == R.id.menu_profile) {
            Intent intent=new Intent(PublicHomePage.this,SettingActivity.class);
            startActivity(intent);
        } else if (id == R.id.menu_categories) {
            Intent intent=new Intent(PublicHomePage.this,TopicsActivity.class);
            startActivity(intent);
        } else if (id == R.id.menu_messages) {
            Intent intent=new Intent(PublicHomePage.this,MessagesActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.menu_archive) {
            Intent intent=new Intent(PublicHomePage.this,HistoryActivity.class);
            intent.putExtra("fromact","home");
            startActivity(intent);
        }/* else if (id == R.id.menu_private_polls) {
            Intent intent=new Intent(PublicHomePage.this,PrivatePolls.class);
            intent.putExtra("fromact","home");
            startActivity(intent);
        }*/
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {

        if(event.getStrength().getValue()== ConnectivityStrength.POOR)
        {
            tv_lowconnection.setSelected(true);
            lytlownetwork.setVisibility(View.VISIBLE);
            //Crouton.makeText(PublicHomePage.this, "Mood India Detects slow data connection,it may effect on loading time of polls.", Style.ALERT).setConfiguration(CONFIGURATION_INFINITE).show();
        }
        else
        {
            lytlownetwork.setVisibility(View.GONE);
        }
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public Fragment getRegisteredFragment(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }
    @Override
    public void onResume()
    {
        MyApplication.getInstance().setConnectivityListener(this);
        super.onResume();
        // tabLayout.getTabAt(2).select();
        Intent intent = getIntent();
        if(intent.hasExtra("fragName")){
            Bundle bd = getIntent().getExtras();
            if(bd.getString("fragName").equals("0")){
                tabLayout.getTabAt(0).select();
            }
            else if(bd.getString("fragName").equals("1")){
                tabLayout.getTabAt(1).select();
            }
            else if(bd.getString("fragName").equals("2")){
                tabLayout.getTabAt(2).select();
            }
        }
        else
        {
            tabLayout.getTabAt(0).select();

        }


        if(M.getMyCountryID(PublicHomePage.this).length()>1){
            tv_country.setText(M.getMyCountry(PublicHomePage.this));
        }
        else {
            M.setMyCountry("India",PublicHomePage.this);
            M.setMyCountryID("101",PublicHomePage.this);
            tv_country.setText(M.getMyCountry(PublicHomePage.this));
            Intent i = new Intent(PublicHomePage.this, PublicHomePage.class);  //your class
            startActivity(i);
        }
        if(M.getMyState(PublicHomePage.this).length()>1)
        {
            tv_state.setText(M.getMyState(PublicHomePage.this));
            tv_cityup.setText(M.getMyState(PublicHomePage.this));
        }
        if(M.getMyCity(PublicHomePage.this).length()>1)
        {
            tv_city.setText(M.getMyCity(PublicHomePage.this));
            tv_cityup.setText(M.getMyCity(PublicHomePage.this));
        }
    }

    private static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder()
            .setDuration(Configuration.DURATION_INFINITE)
            .build();

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }
}
