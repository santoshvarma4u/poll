package com.app.moodindia.ui.privatepolls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InviteParticipantsByLocation extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
    protected DiscreteSeekBar seekBar;
    protected MagicTextView tvUserCount;
    protected ImageView btnBackNav;
    protected MagicTextView tvGroupnameToolbar;
    protected MagicButton btnCancel;
    protected MagicButton btnInvite;
    GoogleMap mGoogleMap;
    public boolean isMapReady = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_invite_participants_by_location);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        mGoogleMap = googleMap;

        isMapReady = true;
        getUserLocationInfo("1");

    }

    public void addMarkers(double latitude, double longitude) {
        LatLng sydney = new LatLng(latitude, longitude);
        mGoogleMap.addMarker(new MarkerOptions().position(sydney)
                .title("MoodIndia User"));
    }

    public void getUserLocationInfo(String radius) {
        PollAPI mPollAPI = APIService.createService(PollAPI.class);


        mPollAPI.getUserLocations(M.getID(InviteParticipantsByLocation.this), radius, new Callback<List<ProfilePojo>>() {
            @Override
            public void success(List<ProfilePojo> profilePojos, Response response) {
                if (profilePojos.size() > 0) {
                    mGoogleMap.clear();
                    tvUserCount.setText(profilePojos.size() + "");
                    for (ProfilePojo mProfilePojo : profilePojos
                            ) {
                        if (!mProfilePojo.getLatitude().equalsIgnoreCase("0.0")) {
                            Log.i("latlon", mProfilePojo.getLatitude() + mProfilePojo.getLongitude());
                            LatLng sydney = new LatLng(Double.parseDouble(mProfilePojo.getLatitude()), Double.parseDouble(mProfilePojo.getLongitude()));

                            mGoogleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.mapmarker))
                                    .title("Mood India User"));
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));

                        }

                    }

                } else
                    Toast.makeText(InviteParticipantsByLocation.this, "No users found in this range.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void initView() {
        seekBar = (DiscreteSeekBar) findViewById(R.id.seek_bar);
        seekBar.setProgress(1);
        tvUserCount = (MagicTextView) findViewById(R.id.tv_user_count);
        seekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    getUserLocationInfo(String.valueOf(seekBar.getProgress()));
                }
            }
        });
        btnBackNav = (ImageView) findViewById(R.id.btn_back_nav);
        btnBackNav.setOnClickListener(InviteParticipantsByLocation.this);
        tvGroupnameToolbar = (MagicTextView) findViewById(R.id.tv_groupname_toolbar);
        btnCancel = (MagicButton) findViewById(R.id.btnCancel);
        btnInvite = (MagicButton) findViewById(R.id.btnInvite);
        btnInvite.setOnClickListener(InviteParticipantsByLocation.this);
        btnCancel.setOnClickListener(InviteParticipantsByLocation.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back_nav) {
            finish();

        } else if (view.getId() == R.id.btnInvite) {
            //send invitation to selected users
            sendInvitationToLocatedUsers(String.valueOf(seekBar.getProgress()));

        }else if (view.getId() == R.id.btnCancel) {
            finish();
        }
    }
    public void sendInvitationToLocatedUsers(String range)
    {
        M.showLoadingDialog(InviteParticipantsByLocation.this);
        PollAPI mPollAPI = APIService.createService(PollAPI.class);
        mPollAPI.sendLocataionInvite(M.getID(InviteParticipantsByLocation.this), range, new Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
              M.hideLoadingDialog();
                Toast.makeText(InviteParticipantsByLocation.this, "Invitation sent to all selected users", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
            M.hideLoadingDialog();
                Toast.makeText(InviteParticipantsByLocation.this, "Invitation sent to all selected users", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}

