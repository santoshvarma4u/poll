package com.app.moodindia.ui.privatepolls.Groups;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.GetLocation;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BusinessGroups extends AppCompatActivity implements View.OnClickListener {

    protected ImageView btnBackNav;
    protected ImageView imageView4;
    protected MagicTextView tvGroupnameToolbar;
    protected RecyclerView rvGroups;
    public List<GroupPojo> mGroupPojoList;
    RecyclerView.LayoutManager mLayoutManager;
    GroupsAdapter mGroupsAdapter;
    public String radius="0";
    TableRow fabadd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_business_groups);
        initView();
        mGroupsAdapter=new GroupsAdapter();
        Intent locationIntent = new Intent(BusinessGroups.this, GetLocation.class);
        startActivityForResult(locationIntent, 452);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back_nav) {
            Intent it=new Intent(BusinessGroups.this,HomeActivity.class);
            it.putExtra("fragName","4");
            startActivity(it);
            finish();
        }
    }

    private void initView() {
        btnBackNav = (ImageView) findViewById(R.id.btn_back_nav);
        btnBackNav.setOnClickListener(BusinessGroups.this);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        tvGroupnameToolbar = (MagicTextView) findViewById(R.id.tv_groupname_toolbar);
        rvGroups = (RecyclerView) findViewById(R.id.rv_groups);
        fabadd = (TableRow) findViewById(R.id.fabadd);
        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BusinessGroups.this,CreateGroupActivity.class);
                intent.putExtra("from","business");
                startActivity(intent);
            }
        });
        mLayoutManager = new LinearLayoutManager(BusinessGroups.this);
        rvGroups.setLayoutManager(mLayoutManager);
        rvGroups.setItemAnimator(new DefaultItemAnimator());
    }

    public class GroupsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_groups, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mGroupPojoList.size();
        }

        GroupPojo mGroupPojo;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            mGroupPojo = mGroupPojoList.get(position);
            holder.tvGroupname.setText(mGroupPojo.getGroupname());
            holder.tvGroupdesc.setText(mGroupPojo.getDescription());
            holder.tv_distance.setVisibility(View.VISIBLE);

            if(!mGroupPojo.getDistance().equalsIgnoreCase("NA"))
            {
                holder.tv_distance.setText(mGroupPojo.getDistance().toString()+" Km away");
            }
            else
            {
                holder.tv_distance.setText("Too far from you");
            }


            Picasso.with(BusinessGroups.this)
                    .load(AppConst.groupimages + mGroupPojo.getImage())
                    .transform(new RoundedSquareTransformation(10, 0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivGroupimage);

            if(mGroupPojo.getType().equalsIgnoreCase("0"))
                holder.iv_business.setImageDrawable(getResources().getDrawable(R.drawable.ic_person));
            else
                holder.iv_business.setImageDrawable(getResources().getDrawable(R.drawable.ic_business_center));

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getExtras().containsKey("fragName"))
        {
            Intent it=new Intent(BusinessGroups.this,HomeActivity.class);
            it.putExtra("fragName","4");
            startActivity(it);
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //getMyGroups();
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder {
        private ImageView ivGroupimage,iv_business;
        private TextView tvGroupname;
        private TextView tvGroupdesc,tv_distance;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            ivGroupimage = (ImageView) itemView.findViewById(R.id.iv_groupimage);
            iv_business = (ImageView) itemView.findViewById(R.id.iv_business);
            tvGroupname = (TextView) itemView.findViewById(R.id.tv_groupname);
            tvGroupdesc = (TextView) itemView.findViewById(R.id.tv_groupdesc);
            tv_distance = (TextView) itemView.findViewById(R.id.tv_distance);
            ivGroupimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BusinessGroups.this, GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
            tvGroupdesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BusinessGroups.this, GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
            tvGroupname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BusinessGroups.this, GroupInfoActivity.class);
                    intent.putExtra("groupid", mGroupPojoList.get(getAdapterPosition()).getGroupid());
                    intent.putExtra("createdby", mGroupPojoList.get(getAdapterPosition()).getCreated_by());
                    startActivity(intent);
                }
            });
        }
    }

    public void getMyGroups() {
      //  M.showLoadingDialog(BusinessGroups.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getBusinessGroups(M.getID(BusinessGroups.this),radius,latitude,longitude, new Callback<List<GroupPojo>>() {
            @Override
            public void success(List<GroupPojo> groupPojos, Response response) {
                if (groupPojos.size() > 0) {
                    mGroupPojoList = groupPojos;
                    rvGroups.setAdapter(mGroupsAdapter);
                    mGroupsAdapter.notifyDataSetChanged();
                    Toast.makeText(BusinessGroups.this, groupPojos.size()+"", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(BusinessGroups.this, groupPojos.size()+"", Toast.LENGTH_SHORT).show();
                    rvGroups.setVisibility(View.GONE);
                }
             //   M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(BusinessGroups.this, "You're not Created nor Participated in any groups. ", Toast.LENGTH_SHORT).show();
             //   M.hideLoadingDialog();
            }
        });
    }

    public String latitude="0.0";
    public String longitude="0.0";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {

            latitude = data.getExtras().getString("latitude").toString();
            longitude = data.getExtras().getString("longitude").toString();
            if(latitude.equalsIgnoreCase("0.0"))
            {
                Toast.makeText(this, "Location not captured, Unable to show lisitng", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

           latitude="0.0";
           longitude="0.0";

        }
        getMyGroups();
    }
}
