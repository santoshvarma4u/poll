package com.app.moodindia.ui.privatepolls;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.AnyRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.MyCredits;
import com.app.moodindia.R;
import com.app.moodindia.animators.ChatAvatarsAnimator;
import com.app.moodindia.animators.InSyncAnimator;
import com.app.moodindia.animators.RocketAvatarsAnimator;
import com.app.moodindia.animators.RocketFlightAwayAnimator;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.youtubepicker.YoutubePicker;
import com.app.moodindia.model.CreditsPojo;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.PrivateOptions;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.SuggestPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicButton;
import com.redbooth.WelcomeCoordinatorLayout;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import mabbas007.tagsedittext.TagsEditText;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class AddPollActivity extends AppCompatActivity implements View.OnClickListener {

    protected EditText etPollQuestion;
    protected EditText etPollDesc;
    protected ImageView ivOptionAdd;
    protected LinearLayout lytaddoptions;
    protected LinearLayout dynamiclyt;
    protected ImageView ivAddPollImage;
    protected TagsEditText etTags;
    protected MagicButton btnSubmitPoll;
    protected ImageView inputOptionImage;
    protected EditText inputEtOptionValue;
    protected ImageView inputImageConfirm;
    protected RecyclerView rvOptionValues;
    protected RecyclerView rvPreviewOptions;
    protected ImageButton step1go;
    protected ImageButton step2go;
    protected ImageButton step3go;
    protected ImageButton step2back;
    protected ImageButton step3back;
    protected ImageButton step4back;
    private Spinner spnChoiceType;
    private Spinner spnCategory;
    private Spinner spnGroups;

    private LinearLayout preview_full_layout;
    private RelativeLayout optnview;
    JSONArray optionArray;
    JSONObject optionObject;
    RecyclerView.LayoutManager mLayoutManager;
    List<PrivateOptions> mPrivateOptionsList;
    OptionsAdapter mOptionsAdapter;
    OptionsPreviewAdapter mOptionsPreviewAdapter;
    CheckBox cb_pp_iagree,cb_anonymous,cb_identified;
    public ImageView preview_poll_image,ivleftarrow,ivrightarrow,iv_youtube_video;

    public TextView tv_poll_question_preview,video_title_view;
    String validateOptionImages="";

    private boolean animationReady = false;
    private ValueAnimator backgroundAnimator;
    @BindView(R.id.wcoordinator) WelcomeCoordinatorLayout coordinatorLayout;
    private RocketAvatarsAnimator rocketAvatarsAnimator;
    private ChatAvatarsAnimator chatAvatarsAnimator;
    private RocketFlightAwayAnimator rocketFlightAwayAnimator;
    private InSyncAnimator inSyncAnimator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.setContentView(R.layout.addpollwizard);
        ButterKnife.bind(this);
        initializeListeners();
        initializePages();
        initializeBackgroundTransitions();
        initView();
        initGroupSpinner();
        if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("pollid"))
        {
            loadDataFromExistingPollid(getIntent().getExtras().getString("pollid"));
        }


       /* if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("result"))
        {
            ArrayList<String> result=getIntent().getStringArrayListExtra("result");
            if(result.get(2).toString()!=null || !result.get(2).toString().isEmpty()) {
                video_title_view.setText(result.get(2));
            }
            if(result.get(3).toString()!=null || !result.get(3).toString().isEmpty()) {
                youtube_link = result.get(3);
            }
            Toast.makeText(this, result.get(0), Toast.LENGTH_SHORT).show();
            Toast.makeText(this, result.get(1), Toast.LENGTH_SHORT).show();
            Toast.makeText(this, result.get(2), Toast.LENGTH_SHORT).show();
            Toast.makeText(this, result.get(3), Toast.LENGTH_SHORT).show();
        }*/

       // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }
    private void initializePages() {
        final WelcomeCoordinatorLayout coordinatorLayout
                = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
        coordinatorLayout.addPage(R.layout.welcome_page_1,
                R.layout.welcome_page_4);
    }
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_option_add) {
            //addOption();
        } else if (view.getId() == R.id.iv_add_poll_image) {

            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(AddPollActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
            } else {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_PICK);
                // Chooser of file system options.
                Intent openGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE_POLLIMAGE);
            }

        } else if (view.getId() == R.id.btn_submit_poll) {
            submitPoll();
        } else if (view.getId() == R.id.input_option_image) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPollActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                } else {
                    Intent galleryIntent = new Intent();
                    galleryIntent.setType("image/*");
                    galleryIntent.setAction(Intent.ACTION_PICK);
                    // Chooser of file system options.
                    Intent openGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE);
                }
            }

        } else if (view.getId() == R.id.input_image_confirm) {
            if (inputEtOptionValue.getText().toString().length() > 1) {
               // step2go.setVisibility(View.VISIBLE);

                if(mPrivateOptionsList.size()>=1)
                {
                    step2go.setVisibility(View.VISIBLE);
                }


               if(mPrivateOptionsList.size()<=6)
                {
                   // Toast.makeText(AddPollActivity.this, selectedImageUri.toString(), Toast.LENGTH_SHORT).show();

                        addOption(optionImagePath, inputEtOptionValue.getText().toString(),selectedImageUri);
                }
                else
                    Toast.makeText(AddPollActivity.this, "Maximum of 6 options can be add", Toast.LENGTH_SHORT).show();

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputEtOptionValue.getWindowToken(), 0);

            } else {
                //step2go.setVisibility(View.GONE);
                Toast.makeText(AddPollActivity.this, "Option value is mandatory", Toast.LENGTH_SHORT).show();
            }
        } else if(view.getId()==R.id.step1but){
            if(etPollQuestion.getText().length()<=0)
            {
                Toast.makeText(AddPollActivity.this, "Please enter a valid poll question.", Toast.LENGTH_SHORT).show();
            }
           /* else if(etPollDesc.getText().length()<=0)
            {
                Toast.makeText(AddPollActivity.this, "Please enter a valid poll description.", Toast.LENGTH_SHORT).show();
            }*/
            else
            {
                tv_poll_question_preview.setText(etPollQuestion.getText());

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPollQuestion.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(etPollDesc.getWindowToken(), 0);

                final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setCurrentPage(1, true);
            }

        }else if(view.getId()==R.id.step2but){

            if(mPrivateOptionsList.size()<=1)
            {
                Toast.makeText(AddPollActivity.this, "Minimum of 2 options need to be added", Toast.LENGTH_SHORT).show();
            }
            else {
                tv_poll_question_preview.setText(etPollQuestion.getText());

                tv_pp_validation.setVisibility(View.GONE);
                btn_pp_tryagain.setVisibility(View.GONE);
                preview_full_layout.setVisibility(View.VISIBLE);

                final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setCurrentPage(coordinatorLayout.getNumOfPages()-1, true);
            }

        }else if(view.getId()==R.id.step3but){

/*
                final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setCurrentPage(coordinatorLayout.getNumOfPages()-1, true);*/

        }else if(view.getId()==R.id.step2back){


                final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                  coordinatorLayout.setCurrentPage(0, true);
                //coordinatorLayout.setCurrentPage(coordinatorLayout.getNumOfPages()-1, true);


        }else if(view.getId()==R.id.step3back){


               /* final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setCurrentPage(1, true);*/

        }else if(view.getId()==R.id.step4back){
            final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setCurrentPage(0, true);

        }
    }

    @OnClick(R.id.step1but)
    void step1go() {

    }

    public void submitPoll()
    {
        if(cb_anonymous.isChecked() && !isAmountDeducted)
        {
            checkForPaytmCredits();
        }
        else
        {

            String pollques=etPollQuestion.getText().toString();
            String polldesc=etPollDesc.getText().toString();
            String tags = etTags.toString();
            String choicetype="single";
            String optiona="";
            String optionb="";
            String optionc="";
            String optiond="";
            String optione="";
            String optionf="";

            Uri optionimagea=null;
            Uri optionimageb=null;
            Uri optionimagec=null;
            Uri optionimaged=null;
            Uri optionimagee=null;
            Uri optionimagef=null;

            TypedFile typedFilea=null;
            TypedFile typedFileb=null;
            TypedFile typedFilec=null;
            TypedFile typedFiled=null;
            TypedFile typedFilee=null;
            TypedFile typedFilef=null;
            TypedFile poll_image=null;

            if(PollImageselectedImageUri==null)
            {
                //take action here
            }
            else
            {
                poll_image=makeFile(PollImageselectedImageUri.toString());
            }

            if(spnChoiceType.getSelectedItemPosition()==1)
            {
                choicetype="single";
            }
            else if(spnChoiceType.getSelectedItemPosition()==2)
            {
                choicetype="multiple";
            }
            else
            {
                choicetype="single";
            }
            int i=0;

            for (PrivateOptions moptions:mPrivateOptionsList) {
                if(i==0)
                {
                    optiona=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimagea=moptions.getSelectedImageUri();
                        typedFilea=makeFile(optionimagea.toString());
                    }
                }
                if(i==1)
                {
                    optionb=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimageb=moptions.getSelectedImageUri();
                        typedFileb=makeFile(optionimageb.toString());
                    }
                }  if(i==2)
                {
                    optionc=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimagec=moptions.getSelectedImageUri();
                        typedFilec=makeFile(optionimagec.toString());
                    }
                }  if(i==3)
                {
                    optiond=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimaged=moptions.getSelectedImageUri();
                        typedFiled=makeFile(optionimaged.toString());
                    }
                }  if(i==4)
                {
                    optione=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimagee=moptions.getSelectedImageUri();
                        typedFilee=makeFile(optionimagee.toString());
                    }
                }  if(i==5)
                {
                    optionf=moptions.getPollOption();
                    if(moptions.getSelectedImageUri()!=null)
                    {
                        optionimagef=moptions.getSelectedImageUri();
                        typedFilef=makeFile(optionimagef.toString());
                    }
                }
                i++;
            }
            if(cb_pp_iagree.isChecked())
            {
                int anonymous=0;
                if(cb_anonymous.isChecked())
                    anonymous=1;


                String groupid="0";

                if(spnGroups.getSelectedItemPosition()>0)
                {
                    if(mGroupPojoList.size()>0)
                    {
                        groupid=mGroupPojoList.get(spnGroups.getSelectedItemPosition()-1).getGroupid();
                    }
                }


                if(poll_image!=null || validateOptionImages.length()>0)
                {
                    M.showLoadingDialog(AddPollActivity.this);
                    PrivatePollAPI mPrivatePollAPI= APIService.createService(PrivatePollAPI.class);
                    mPrivatePollAPI.submitPrivatePoll(M.getID(AddPollActivity.this), pollques, polldesc, choicetype, optiona, optionb, optionc, optiond, optione,
                            optionf, "",groupid, typedFilea, typedFileb, typedFilec, typedFiled, typedFilee, typedFilef, tags,youtube_link, poll_image, "",String.valueOf(anonymous), new Callback<List<SuggestPojo>>() {
                                @Override
                                public void success(List<SuggestPojo> suggestPojos, Response response) {
                                    //
                                    M.hideLoadingDialog();

                                    Intent intent=new Intent(AddPollActivity.this,PrivatePollScreen.class);
                                    intent.putExtra("pollid",suggestPojos.get(0).getId());
                                    intent.putExtra("upollid",suggestPojos.get(0).getUid());
                                    intent.putExtra("from","active");
                                    intent.putExtra("forshare","true");
                                    intent.putExtra("mainview","mainview");
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    M.hideLoadingDialog();
                                    Toast.makeText(AddPollActivity.this, "Unable submit poll, Please try again", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
                else
                {

                   // Toast.makeText(getApplicationContext(), youtube_link, Toast.LENGTH_SHORT).show();
                    M.showLoadingDialog(AddPollActivity.this);
                    PrivatePollAPI mPrivatePollAPI= APIService.createService(PrivatePollAPI.class);
                    mPrivatePollAPI.submitPrivatePollNoImage(M.getID(AddPollActivity.this), pollques, polldesc, choicetype, optiona, optionb, optionc, optiond, optione,
                            optionf, "",groupid, "", "", "", "", "", "", tags,youtube_link, "", "",String.valueOf(anonymous),new Callback<List<SuggestPojo>>() {
                                @Override
                                public void success(List<SuggestPojo> suggestPojos, Response response) {
                                    //
                                    M.hideLoadingDialog();

                                    Intent intent=new Intent(AddPollActivity.this,PrivatePollScreen.class);
                                    intent.putExtra("pollid",suggestPojos.get(0).getId());
                                    intent.putExtra("upollid",suggestPojos.get(0).getUid());
                                    intent.putExtra("from","active");
                                    intent.putExtra("forshare","true");
                                    intent.putExtra("mainview","mainview");
                                    startActivity(intent);
                                    finish();
                                }
                                @Override
                                public void failure(RetrofitError error) {
                                    //
                                    M.hideLoadingDialog();
                                    Toast.makeText(AddPollActivity.this, "Unable submit poll, Please try again", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
            else
            {
                Toast.makeText(AddPollActivity.this, "Please agree terms and conditions to proceed!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public static final Uri getUriToDrawable(@NonNull Context context,
                                             @AnyRes int drawableId) {
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + context.getResources().getResourcePackageName(drawableId)
                + '/' + context.getResources().getResourceTypeName(drawableId)
                + '/' + context.getResources().getResourceEntryName(drawableId) );
        return imageUri;
        }

    private static final int REQUEST_GALLERY_CODE = 400;
    private static final int REQUEST_GALLERY_CODE_POLLIMAGE = 500;
    private boolean opImage=false;

    public void addOption(String imageUrl, String pollOption,Uri selecteImageUri) {

        PrivateOptions pOptions=new PrivateOptions();

        pOptions.setPollOption(pollOption);
        pOptions.setSelectedImageUri(selecteImageUri);

        if(opImage)
        {
            pOptions.setImagePath(imageUrl);
        }
        else
        {
            pOptions.setImagePath("");
        }

        mPrivateOptionsList.add(pOptions);

        mOptionsAdapter=new OptionsAdapter();
        rvOptionValues.setAdapter(mOptionsAdapter);

        mOptionsAdapter.notifyDataSetChanged();

        mOptionsPreviewAdapter=new OptionsPreviewAdapter();
        rvPreviewOptions.setAdapter(mOptionsPreviewAdapter);
        mOptionsPreviewAdapter.notifyDataSetChanged();

        inputEtOptionValue.setText("");
        Picasso.with(getApplicationContext()).load(R.drawable.addimg).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(inputOptionImage);
        inputEtOptionValue.setHint("Enter option value");
        optionImagePath="";
        opImage=false;

        if(mPrivateOptionsList.size()>1)
        {
            step2go.setVisibility(View.VISIBLE);
        }
    }

    private void initializeListeners() {
        final WelcomeCoordinatorLayout coordinatorLayout
                = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
        coordinatorLayout.setScrollingEnabled(false);
        coordinatorLayout.setOnPageScrollListener(new WelcomeCoordinatorLayout.OnPageScrollListener() {
            @Override
            public void onScrollPage(View v, float progress, float maximum) {
             /* if (!animationReady) {
                    animationReady = true;
                    backgroundAnimator.setDuration((long) maximum);
                }
                backgroundAnimator.setCurrentPlayTime((long) progress); */
            }

            @Override
            public void onPageSelected(View v, int pageSelected) {
                switch (pageSelected) {
                    case 0:
                        if (rocketAvatarsAnimator == null) {
                            rocketAvatarsAnimator = new RocketAvatarsAnimator(coordinatorLayout);
                           // rocketAvatarsAnimator.play();
                        }
                        break;
                    case 1:
                        if (chatAvatarsAnimator == null) {
                            chatAvatarsAnimator = new ChatAvatarsAnimator(coordinatorLayout);
//                            chatAvatarsAnimator.play();
                        }
                        break;
                    case 2:
                        if (inSyncAnimator == null) {
                            inSyncAnimator = new InSyncAnimator(coordinatorLayout);
                          //  inSyncAnimator.play();
                        }
                        break;
                    case 3:
                        if (rocketFlightAwayAnimator == null) {
                            rocketFlightAwayAnimator = new RocketFlightAwayAnimator(coordinatorLayout);
                           // rocketFlightAwayAnimator.play();
                        }
                        break;
                }
            }
        });
    }
    private void initializeBackgroundTransitions() {
        final Resources resources = getResources();
        final int colorPage1 = ResourcesCompat.getColor(resources, R.color.page1, getTheme());
        final int colorPage2 = ResourcesCompat.getColor(resources, R.color.page2, getTheme());
        final int colorPage3 = ResourcesCompat.getColor(resources, R.color.page3, getTheme());
        final int colorPage4 = ResourcesCompat.getColor(resources, R.color.page4, getTheme());
        backgroundAnimator = ValueAnimator
                .ofObject(new ArgbEvaluator(), colorPage1, colorPage2, colorPage3, colorPage4);
        backgroundAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final WelcomeCoordinatorLayout coordinatorLayout
                        = (WelcomeCoordinatorLayout)findViewById(R.id.wcoordinator);
                coordinatorLayout.setBackgroundColor((int) animation.getAnimatedValue());
            }
        });
    }

    public class OptionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.option_layout, parent, false);
            viewHolder = new ViewHodlerOptions(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHodlerOptions vh2 = (ViewHodlerOptions) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mPrivateOptionsList.size();
        }

         public void configureViewHolderFilter(final ViewHodlerOptions holder, final int position) {

             holder.etOptionValue.setText(mPrivateOptionsList.get(position).getPollOption());

             if (!mPrivateOptionsList.get(position).getImagePath().isEmpty() && !mPrivateOptionsList.get(position).getImagePath().contains("http"))
                 Picasso.with(getApplicationContext()).load(new File(mPrivateOptionsList.get(position).getImagePath())).into(holder.optionImage);
           /*  else
                 Picasso.with(getApplicationContext()).load(mPrivateOptionsList.get(position).getImagePath()).into(holder.optionImage);
*/
        }

    }
    public class OptionsPreviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.option_layout, parent, false);
            viewHolder = new ViewHodlerOptions(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHodlerOptions vh2 = (ViewHodlerOptions) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mPrivateOptionsList.size();
        }

         public void configureViewHolderFilter(final ViewHodlerOptions holder, final int position) {

             holder.etOptionValue.setText(mPrivateOptionsList.get(position).getPollOption());

             holder.imageRemoveOption.setVisibility(View.GONE);
             if (!mPrivateOptionsList.get(position).getImagePath().isEmpty())
                 Picasso.with(getApplicationContext()).load(new File(mPrivateOptionsList.get(position).getImagePath())).into(holder.optionImage);
             else
                 holder.optionImage.setVisibility(View.GONE);
        }
    }

    class ViewHodlerOptions extends RecyclerView.ViewHolder{

        private ImageView optionImage;
        private TextView etOptionValue;
        private ImageView imageRemoveOption;
        private TextView indexValue;

        public ViewHodlerOptions(View itemView) {
            super(itemView);
            optionImage = (ImageView)itemView.findViewById(R.id.option_image);
            etOptionValue = (TextView)itemView.findViewById(R.id.et_option_value);
            imageRemoveOption = (ImageView)itemView.findViewById(R.id.image_remove_option);
            indexValue = (TextView)itemView.findViewById(R.id.indexValue);
            imageRemoveOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPrivateOptionsList.remove(getAdapterPosition());
                    mOptionsAdapter.notifyDataSetChanged();
                    mOptionsPreviewAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    Uri selectedImageUri,PollImageselectedImageUri = null;
    String mediaPath, imagepath = "";
    String optionImagePath="";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      //  Toast.makeText(getApplicationContext(), requestCode+""+resultCode+""+data, Toast.LENGTH_SHORT).show();

        if (requestCode == 3345 && resultCode == RESULT_OK)
        {
            String url=data.getExtras().getString("url");
           // Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT).show();
           if(data.getExtras().getString("title") !=null || !data.getExtras().getString("title").isEmpty()) {
                video_title_view.setText(data.getExtras().getString("title"));
            }
            if(data.getExtras().getString("VIDEO_URL")!=null || !data.getExtras().getString("VIDEO_URL").isEmpty()) {
                youtube_link = data.getExtras().getString("VIDEO_URL");

            }
        }
        else if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK && null != data) {
            try {
                //M.showLoadingDialog(AddPollActivity.this);
                // Get real path to make File
                selectedImageUri = Uri.parse(getPath(data.getData()));

                Bitmap bitmap = BitmapFactory.decodeFile(getPath(data.getData()));

/*
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));*/

                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    optionImagePath = cursor.getString(columnIndex);
                    validateOptionImages="true";
                    Picasso.with(getApplicationContext()).load(new File(optionImagePath))
                            .into(inputOptionImage);
                    cursor.close();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                }

                tv_pp_validation.setVisibility(View.GONE);
                btn_pp_tryagain.setVisibility(View.GONE);
                preview_full_layout.setVisibility(View.VISIBLE);
                opImage=true;
                //M.hideLoadingDialog();

            } catch (Exception e) {
                Log.e("Add POll Activity", e.getMessage());
            }
        } else if (requestCode == REQUEST_GALLERY_CODE_POLLIMAGE && resultCode == Activity.RESULT_OK && null != data) {
            try {
              //  M.showLoadingDialog(AddPollActivity.this);
                // Get real path to make File
                PollImageselectedImageUri = Uri.parse(getPath(data.getData()));
                Bitmap bitmap = BitmapFactory.decodeFile(getPath(data.getData()));

                Uri PollImageselectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(PollImageselectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagepath = cursor.getString(columnIndex);

                    Picasso.with(getApplicationContext()).load(new File(imagepath))
                            .into(ivAddPollImage);
                    Picasso.with(getApplicationContext()).load(new File(imagepath))
                            .into(preview_poll_image);
                   // step3go.setVisibility(View.VISIBLE);
                    cursor.close();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                }
               // M.hideLoadingDialog();
                tv_pp_validation.setVisibility(View.GONE);
                btn_pp_tryagain.setVisibility(View.GONE);
                preview_full_layout.setVisibility(View.VISIBLE);
            } catch (Exception e) {
                Log.e("Add POll Activity", e.getMessage());
            }
        }
    }
    public String youtube_link="0";

    private String getPath(Uri uri) throws Exception {
        // this method will be used to get real path of Image chosen from gallery.
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private TypedFile makeFile(String uri) {

        File file = new File(uri);
        File compressedImage = null;
        try {
            Compressor cmp=new Compressor(this);
            cmp.setQuality(40);
            cmp.setMaxHeight(400);
            cmp.setMaxWidth(400);
            compressedImage = cmp.compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        TypedFile typedFile = new TypedFile("image/*", compressedImage);

        return typedFile;
    }
    LinearLayout qtnlyt;
    TextView qtnupdown,txtGroupType;
    FrameLayout opvframelyt;
    private void initView() {
        etPollQuestion = (EditText) findViewById(R.id.et_poll_question);
      /*  etPollQuestion.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                optnview.setVisibility(View.VISIBLE);
                return false;
            }
        });*/
        etPollDesc = (EditText) findViewById(R.id.et_poll_desc);
        ivOptionAdd = (ImageView) findViewById(R.id.iv_option_add);
        ivOptionAdd.setOnClickListener(AddPollActivity.this);
        lytaddoptions = (LinearLayout) findViewById(R.id.lytaddoptions);
        dynamiclyt = (LinearLayout) findViewById(R.id.dynamiclyt);
        ivAddPollImage = (ImageView) findViewById(R.id.iv_add_poll_image);
        iv_youtube_video = (ImageView) findViewById(R.id.iv_youtube_video);
        iv_youtube_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddPollActivity.this
                        , YoutubePicker.class);
                startActivityForResult(i, 3345);
            }
        });
        qtnupdown = (TextView) findViewById(R.id.qtnupdown);
        txtGroupType = (TextView) findViewById(R.id.txtGroupType);
        video_title_view = (TextView) findViewById(R.id.video_title_view);
        ivAddPollImage.setOnClickListener(AddPollActivity.this);
        etTags = (TagsEditText) findViewById(R.id.tagsEditText);
        btnSubmitPoll = (MagicButton) findViewById(R.id.btn_submit_poll);
        btnSubmitPoll.setOnClickListener(AddPollActivity.this);

        optionArray = new JSONArray();
        optionObject = new JSONObject();
        inputOptionImage = (ImageView) findViewById(R.id.input_option_image);
        inputOptionImage.setOnClickListener(AddPollActivity.this);
        inputEtOptionValue = (EditText) findViewById(R.id.input_et_option_value);
        inputImageConfirm = (ImageView) findViewById(R.id.input_image_confirm);
        ivleftarrow = (ImageView) findViewById(R.id.ivleftarrow);
        ivrightarrow = (ImageView) findViewById(R.id.ivrightarrow);
        inputImageConfirm.setOnClickListener(AddPollActivity.this);
        rvOptionValues = (RecyclerView) findViewById(R.id.rvOptionValues);
        mPrivateOptionsList=new ArrayList<>();
        step1go=(ImageButton) findViewById(R.id.step1but);
        step2go=(ImageButton) findViewById(R.id.step2but);
        step3go=(ImageButton) findViewById(R.id.step3but);
        step1go.setOnClickListener(AddPollActivity.this);
        step2go.setOnClickListener(AddPollActivity.this);
        opvframelyt=(FrameLayout)findViewById(R.id.opvframelyt);
        // step3go.setOnClickListener(AddPollActivity.this);
      //  step2back=(ImageButton) findViewById(R.id.step2back);
      //  step3back=(ImageButton) findViewById(R.id.step3back);
        step4back=(ImageButton) findViewById(R.id.step4back);
      //  step2back.setOnClickListener(AddPollActivity.this);
        step4back.setOnClickListener(AddPollActivity.this);
    //    step3back.setOnClickListener(AddPollActivity.this);

        qtnlyt=(LinearLayout)findViewById(R.id.qtnlyt);
        qtnupdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(qtnLayout)
                {
                    qtnlyt.setVisibility(View.GONE);
                    qtnupdown.setText("Edit question info");
                    ivleftarrow.setVisibility(View.VISIBLE);
                    ivrightarrow.setVisibility(View.GONE);
                    qtnLayout=false;
                    optnview.setVisibility(View.VISIBLE);
                    opvframelyt.setVisibility(View.VISIBLE);
                    opvframelyt.setPadding(0,80,0,0);
                }
                else
                {
                    qtnlyt.setVisibility(View.VISIBLE);
                    ivleftarrow.setVisibility(View.GONE);
                    ivrightarrow.setVisibility(View.VISIBLE);
                    qtnupdown.setText("Add poll options");
                    qtnLayout=true;
                    opvframelyt.setVisibility(View.GONE);
                    optnview.setVisibility(View.GONE);
                    opvframelyt.setPadding(0,760,0,0);
                }
            }
        });
        ivleftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qtnlyt.setVisibility(View.VISIBLE);
                ivleftarrow.setVisibility(View.GONE);
                ivrightarrow.setVisibility(View.VISIBLE);
                qtnupdown.setText("Add poll options");
                qtnLayout=true;
                opvframelyt.setVisibility(View.GONE);
                optnview.setVisibility(View.GONE);
                opvframelyt.setPadding(0,760,0,0);
            }
        });
        ivrightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                qtnlyt.setVisibility(View.GONE);
                qtnupdown.setText("Edit question info");
                ivleftarrow.setVisibility(View.VISIBLE);
                ivrightarrow.setVisibility(View.GONE);
                qtnLayout=false;
                optnview.setVisibility(View.VISIBLE);
                opvframelyt.setVisibility(View.VISIBLE);
                opvframelyt.setPadding(0,80,0,0);
            }
        });
        rvOptionValues = (RecyclerView)findViewById(R.id.rvOptionValues);
        mLayoutManager = new LinearLayoutManager(AddPollActivity.this);
        rvOptionValues.setLayoutManager(mLayoutManager);
        rvOptionValues.setItemAnimator(new DefaultItemAnimator());

        spnChoiceType = (Spinner) findViewById(R.id.spn_choice_type);
        spnGroups = (Spinner) findViewById(R.id.spngroups);
        spnCategory = (Spinner) findViewById(R.id.spn_category);
        rvPreviewOptions=(RecyclerView)findViewById(R.id.rv_preview_options);
        preview_poll_image=(ImageView) findViewById(R.id.preview_poll_image);
        tv_poll_question_preview=(TextView) findViewById(R.id.tv_poll_question_preview);


        mLayoutManager = new LinearLayoutManager(AddPollActivity.this);
        rvPreviewOptions.setLayoutManager(mLayoutManager);
        rvPreviewOptions.setItemAnimator(new DefaultItemAnimator());

        preview_full_layout=(LinearLayout)findViewById(R.id.preview_full_layout);
        preview_full_layout.setVisibility(View.GONE);

        tv_pp_validation=(TextView)findViewById(R.id.tv_pp_validation);

        btn_pp_tryagain=(Button) findViewById(R.id.btn_pp_tryagain);
        tv_pp_validation.setVisibility(View.VISIBLE);
        btn_pp_tryagain.setVisibility(View.VISIBLE);

        btn_pp_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddPollActivity.this,AddPollActivity.class));
                finish();
            }
        });

        cb_pp_iagree=(CheckBox)findViewById(R.id.cb_pp_iagree);
        cb_pp_iagree.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                String url = "http://www.moodindia.in/privacy.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

                return true;
            }
        });
        optnview=(RelativeLayout)findViewById(R.id.optnview);
        optnview.setVisibility(View.GONE);

        cb_anonymous=(CheckBox)findViewById(R.id.cb_anonymous);
        cb_identified=(CheckBox)findViewById(R.id.cb_identified);
        cb_anonymous.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    Toast.makeText(AddPollActivity.this, "You will charged 5 INR to make this poll created by anonymous.", Toast.LENGTH_SHORT).show();
                    cb_identified.setChecked(false);
                }
            }
        });
        cb_identified.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    cb_anonymous.setChecked(false);
                }
            }
        });

        mGroupPojoList=new ArrayList<>();
    }
    public boolean qtnLayout=true;
    TextView tv_pp_validation;
    Button btn_pp_tryagain;
    public void initGroupSpinner()
    {
        getUserGroups();
    }
    List<GroupPojo> mGroupPojoList;
    ArrayAdapter<GroupPojo> mGroupPojoArrayAdapter;
    ArrayList<String> mStringArrayList=new ArrayList<>();

    public void getUserGroups()
    {
        mStringArrayList.add("Select group");
        M.showLoadingDialog(AddPollActivity.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getMyGroups(M.getID(AddPollActivity.this), new Callback<List<GroupPojo>>() {
            @Override
            public void success(List<GroupPojo> groupPojos, Response response) {
                if (groupPojos.size() > 0) {
                    mGroupPojoList = groupPojos;

                    for(GroupPojo mGroupPojo:groupPojos)
                    {
                        mStringArrayList.add(mGroupPojo.getGroupname());
                    }
                    ArrayAdapter<String> mStringArrayAdapter=new ArrayAdapter<String>(AddPollActivity.this, android.R.layout.simple_spinner_item,mStringArrayList);
                    //mGroupPojoArrayAdapter = new ArrayAdapter<GroupPojo>(AddPollActivity.this, android.R.layout.simple_spinner_item, mGroupPojoList);
                    mStringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnGroups.setAdapter(mStringArrayAdapter);
                    if(getIntent().getExtras() !=null)
                    {
                       if(getIntent().getExtras().containsKey("groupname"))
                       {
                           int groupIdpos=mStringArrayList.indexOf(mStringArrayAdapter.getPosition(getIntent().getExtras().get("groupname").toString()));
                          // Toast.makeText(AddPollActivity.this, +"", Toast.LENGTH_SHORT).show();
                           spnGroups.setSelection(mStringArrayAdapter.getPosition(getIntent().getExtras().get("groupname").toString()));
                       }

                       if(getIntent().getExtras().containsKey("grouptype"))
                       {
                           txtGroupType.setText("Selected "+getIntent().getExtras().getString("grouptype")+" group");
                       }
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                //Hide or Ask user create a group
                M.hideLoadingDialog();
            }
        });
    }
    public boolean isDuplicated=false;
    public void loadDataFromExistingPollid(String pollid)
    {
        isDuplicated=true;
       // M.showLoadingDialog(AddPollActivity.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        //getting images from server
        mPrivatePollAPI.getPrivatePollOption(M.getID(AddPollActivity.this), pollid, "active", new Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollOptionPojos, Response response) {
            if(pollOptionPojos !=null && pollOptionPojos.size() >0)
            {
               // M.hideLoadingDialog();
                PollOptionPojo mPollOptionPojo=pollOptionPojos.get(0);

                String[] questnArray=mPollOptionPojo.getQuestion().split("-");
                    etPollQuestion.setText(questnArray[0]);


                if(pollOptionPojos.get(0).getImage()!=null && !pollOptionPojos.get(0).getImage().isEmpty())
                    {
                        Picasso.with(getApplicationContext()).load(AppConst.imgurl+pollOptionPojos.get(0).getImage())
                                .into(ivAddPollImage);
                         mMyTask = new DownloadTask()
                                .execute(stringToURL(
                                        AppConst.imgurl+pollOptionPojos.get(0).getImage()
                                ));
                    }

                for(Pollresult mPollresult:mPollOptionPojo.getPollresult())
                {

                    if(mPollresult.getOptionimage()!=null || mPollresult.getOptionimage().isEmpty() || mPollresult.getOptionimage()=="")
                    {
                        optionImageTask = new PollOptionImageTask(mPollresult.getOptionimage(),mPollresult.getOptionvalue())
                                .execute(stringToURL(
                                        AppConst.imgurl+mPollresult.getOptionimage()
                                ));

                       // addOption(mPollresult.getOptionimage(),mPollresult.getOptionvalue(),null);
                    }
                    else
                    {
                        addOption(mPollresult.getOptionimage(),mPollresult.getOptionvalue(),null);
                    }

                    optnview.setVisibility(View.VISIBLE);
                }
            }
       }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
    private AsyncTask mMyTask,optionImageTask;

    public void checkForPaytmCredits()
    {
        M.showLoadingDialog(AddPollActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getCreditDetails(M.getID(AddPollActivity.this), new Callback<List<CreditsPojo>>() {
            @Override
            public void success(List<CreditsPojo> creditsPojos, Response response) {
                if(creditsPojos.size()>0)
                {
                    try{
                        if(Integer.parseInt(creditsPojos.get(creditsPojos.size()-1).getTotalbalance())>=5)
                        {
                            Toast.makeText(AddPollActivity.this, "We deducted 5 MI credits", Toast.LENGTH_SHORT).show();
                            updateBalance("5",M.getID(AddPollActivity.this));
                        }
                        else
                        {
                            Toast.makeText(AddPollActivity.this, "Insufficient MI Credits, Please add", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(AddPollActivity.this, MyCredits.class));
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
    public boolean isAmountDeducted=false;
    public void updateBalance(final String amount, String userid)
    {
        Random r = new Random();
        int i1 = r.nextInt(99999 - 11111) + 11111;

        final String orderid="ORDER"+i1;
         M.showLoadingDialog(AddPollActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.updateCreditsAnon(userid, orderid, amount, new retrofit.Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                isAmountDeducted=true;
                M.hideLoadingDialog();
                submitPoll();

            }

            @Override
            public void failure(RetrofitError error) {
                isAmountDeducted=true;
                M.hideLoadingDialog();
                submitPoll();

            }
        });
    }


    private class DownloadTask extends AsyncTask<URL,Void,Bitmap> {
        // Before the tasks execution
        protected void onPreExecute(){
            // Display the progress dialog on async task start
           M.showLoadingDialog(AddPollActivity.this);
        }

        // Do the task in background/non UI thread
        protected Bitmap doInBackground(URL...urls){
            URL url = urls[0];
            HttpURLConnection connection = null;

            try{
                // Initialize a new http url connection
                connection = (HttpURLConnection) url.openConnection();

                // Connect the http url connection
                connection.connect();

                // Get the input stream from http url connection
                InputStream inputStream = connection.getInputStream();

                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);

                // Return the downloaded bitmap
                return bmp;

            }catch(IOException e){
                e.printStackTrace();
            }finally{
                // Disconnect the http url connection
                connection.disconnect();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result){
            // Hide the progress dialog
          M.hideLoadingDialog();

            if(result!=null){
                // Display the downloaded image into ImageView
                ivAddPollImage.setImageBitmap(result);
                preview_poll_image.setImageBitmap(result);

                // Save bitmap to internal storage
                try {
                    PollImageselectedImageUri = saveImageToInternalStorage(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv_pp_validation.setVisibility(View.GONE);
                btn_pp_tryagain.setVisibility(View.GONE);
                preview_full_layout.setVisibility(View.VISIBLE);

                // Set the ImageView image from internal storage
              //  mImageViewInternal.setImageURI(imageInternalUri);
            }else {
                // Notify user that an error occurred while downloading image
                Toast.makeText(AddPollActivity.this, "Error", Toast.LENGTH_SHORT).show();
               // Snackbar.make(mCLayout,"",Snackbar.LENGTH_LONG).show();
            }
        }
    }

  private class PollOptionImageTask extends AsyncTask<URL,Void,Bitmap> {

        String mOptionImage,moptionVale;
     public PollOptionImageTask(String optionImage,String optionVale)
      {
          super();
          this.mOptionImage=optionImage;
          this.moptionVale=optionVale;
      }
        // Before the tasks execution
        protected void onPreExecute(){
            // Display the progress dialog on async task start
           M.showLoadingDialog(AddPollActivity.this);
        }

        // Do the task in background/non UI thread
        protected Bitmap doInBackground(URL...urls){
            URL url = urls[0];
            HttpURLConnection connection = null;

            try{
                // Initialize a new http url connection
                connection = (HttpURLConnection) url.openConnection();

                // Connect the http url connection
                connection.connect();

                // Get the input stream from http url connection
                InputStream inputStream = connection.getInputStream();

                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);

                // Return the downloaded bitmap
                return bmp;

            }catch(IOException e){
                e.printStackTrace();
            }finally{
                // Disconnect the http url connection
                connection.disconnect();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result){
            // Hide the progress dialog
          M.hideLoadingDialog();

            try {
                //M.showLoadingDialog(AddPollActivity.this);
                // Get real path to make File
                selectedImageUri =saveImageToInternalStorage(result);

                if(selectedImageUri!=null)
                {
                    addOption(mOptionImage,moptionVale,selectedImageUri);
                }
                Bitmap bitmap = result;

                inputOptionImage.setImageBitmap(result);

                tv_pp_validation.setVisibility(View.GONE);
                btn_pp_tryagain.setVisibility(View.GONE);
                preview_full_layout.setVisibility(View.VISIBLE);
                opImage=true;
                //M.hideLoadingDialog();
            } catch (Exception e) {
                Log.e("Add POll Activity", e.getMessage());
            }
        }
    }

    // Custom method to convert string to url
    protected URL stringToURL(String urlString){
        try{
            URL url = new URL(urlString);
            return url;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    // Custom method to save a bitmap into internal storage
    protected Uri saveImageToInternalStorage(Bitmap bitmap) throws Exception {
        // Initialize ContextWrapper
        ContextWrapper wrapper = new ContextWrapper(getApplicationContext());

        // Initializing a new file
        // The bellow line return a directory in internal storage
        File file = wrapper.getDir("Images",MODE_PRIVATE);

        // Create a file to save the image
        file = new File(file,  createTransactionID()+".jpg");

        try{
            // Initialize a new OutputStream
            OutputStream stream = null;

            // If the output file exists, it can be replaced or appended to it
            stream = new FileOutputStream(file);

            // Compress the bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

            // Flushes the stream
            stream.flush();

            // Closes the stream
            stream.close();

        }catch (IOException e) // Catch the exception
        {
            e.printStackTrace();
        }

        // Parse the gallery image url to uri
        Uri savedImageURI = Uri.parse(file.getAbsolutePath());

        // Return the saved image Uri
        return savedImageURI;
    }
    public String createTransactionID() throws Exception{
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
}