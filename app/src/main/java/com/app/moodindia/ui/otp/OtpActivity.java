package com.app.moodindia.ui.otp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.helper.SmsListener;
import com.app.moodindia.helper.SmsReceiver;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.bluejamesbond.text.DocumentView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicEditText;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    protected MagicTextView magicTextView3;
    protected MagicTextView magicTextView4;
    protected MagicEditText etPhonenumber;
    protected MagicButton btnProceed;
    protected RelativeLayout phoneview;
    protected ImageView imageView8,iv_back;
    protected MagicEditText etCode;
    protected MagicButton btnContinue;
    protected RelativeLayout pinview;
    protected MagicTextView tvResendCode;
    private FirebaseAuth mAuth;

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    final String TAG = "Otp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_otp);
        mAuth = FirebaseAuth.getInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(OtpActivity.this, new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, 1257);
            } else {
                initView();
            }
        } else {
            initView();
        }

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {

                M.hideLoadingDialog();
                Log.d(TAG, "onVerificationCompleted:" + credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

                M.hideLoadingDialog();
                Log.w(TAG, "onVerificationFailed", e);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(findViewById(android.R.id.content), "Oops something went wrong, Please try again.",
                                Snackbar.LENGTH_SHORT).show();
                        finish();
                    }
                });


                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                M.hideLoadingDialog();
                } else if (e instanceof FirebaseTooManyRequestsException) {
                M.hideLoadingDialog();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);
                M.hideLoadingDialog();
                Snackbar.make(findViewById(android.R.id.content), "OTP Sent to your mobile number",
                        Snackbar.LENGTH_SHORT).show();
                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                updateUI("OTPSEND");
            }
        };
    }
    String mVerificationId="";

    public void updateUI(String state) {
        if (state.equalsIgnoreCase("OTPSEND")) {
            phoneview.setVisibility(View.GONE);
            pinview.setVisibility(View.VISIBLE);

            waitForSMS();
        }
        else   if (state.equalsIgnoreCase("EDITPHONE")) {
            phoneview.setVisibility(View.VISIBLE);
            pinview.setVisibility(View.GONE);


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initView();
                }
                else
                {
                    Toast.makeText(OtpActivity.this, "Please allow permission to read the Verification Code", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    PhoneAuthProvider.ForceResendingToken mResendToken;
    public static final String OTP_REGEX = "[0-9]{1,6}";

    public void waitForSMS() {
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {

                Log.e("Message", messageText);
                Toast.makeText(OtpActivity.this, "Message: " + messageText, Toast.LENGTH_LONG).show();

                Pattern pattern = Pattern.compile(OTP_REGEX);
                Matcher matcher = pattern.matcher(messageText);
                String otp = null;
                while (matcher.find()) {
                    otp = matcher.group();
                }
                if(otp!=null)
                {
                    verifyUserWithCode(otp);
                }
                Toast.makeText(OtpActivity.this, "OTP Received " + otp, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void verifyUserWithCode(String otpCode) {

        if(mVerificationId.length() > 0)
        {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otpCode);
            signInWithPhoneAuthCredential(credential,etCode.getText().toString());
        }
        else
            Toast.makeText(OtpActivity.this, "Something went worng, please try agian", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.et_phonenumber) {

        } else if (view.getId() == R.id.btnProceed) {
            if (etPhonenumber.getText().length() != 10) {
                Toast.makeText(this, "Please enter valid mobile phone number", Toast.LENGTH_SHORT).show();
            } else {
                M.showLoadingDialog(OtpActivity.this);
                btnProceed.setText("Sending OTP...");
                try {
                    String phoneNumber = "+91" + etPhonenumber.getText().toString();

                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                           phoneNumber,
                            60,
                            TimeUnit.SECONDS,
                            this,
                            mCallbacks);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    Toast.makeText(OtpActivity.this,"Something Went wrong,please try again",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(OtpActivity.this,OtpActivity.class));
                    finish();
                }
                //new backTask().execute();
            }
        } else if (view.getId() == R.id.phoneview) {
        } else if (view.getId() == R.id.et_code) {
        } else if (view.getId() == R.id.btnContinue) {
            if(etCode.getText().length()==6)
            {
                verifyUserWithCode(etCode.getText().toString());
            }
            else
            {
                Toast.makeText(OtpActivity.this, "Please enter a valid code", Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.pinview) {
        } else if (view.getId() == R.id.iv_back) {
            updateUI("EDITPHONE");
        } else if (view.getId() == R.id.tv_resend_code) {
            resendVerificationCode(etPhonenumber.getText().toString(), mResendToken);
        }
    }

    private class backTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            String phoneNumber="+91" + etPhonenumber.getText();

            return null;
        }

    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {

        Snackbar.make(findViewById(android.R.id.content), "Resending OTP, Please wait",
                Snackbar.LENGTH_SHORT).show();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks

    }

    private void initView() {
        magicTextView3 = (MagicTextView) findViewById(R.id.magicTextView3);
        magicTextView4 = (MagicTextView) findViewById(R.id.magicTextView4);
        etPhonenumber = (MagicEditText) findViewById(R.id.et_phonenumber);
        etPhonenumber.setOnClickListener(OtpActivity.this);
        btnProceed = (MagicButton) findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(OtpActivity.this);
        phoneview = (RelativeLayout) findViewById(R.id.phoneview);
        phoneview.setOnClickListener(OtpActivity.this);
        imageView8 = (ImageView) findViewById(R.id.imageView8);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        etCode = (MagicEditText) findViewById(R.id.et_code);
        etCode.setOnClickListener(OtpActivity.this);
        btnContinue = (MagicButton) findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(OtpActivity.this);
        pinview = (RelativeLayout) findViewById(R.id.pinview);
        pinview.setOnClickListener(OtpActivity.this);
        iv_back.setOnClickListener(OtpActivity.this);
        tvResendCode = (MagicTextView) findViewById(R.id.tv_resend_code);
        tvResendCode.setOnClickListener(OtpActivity.this);

        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        if(countryCodeValue.equalsIgnoreCase("in"))
        {

        }
        else
        {
            //outside india popup

            final DialogPlus notificationDialog = DialogPlus.newDialog(OtpActivity.this)
                    .setGravity(Gravity.CENTER)
                    .setContentHolder(new ViewHolder(R.layout.custom_layout_county))
                    .setCancelable(true)
                    .create();


            View view=notificationDialog.getHolderView();

            final TextView tvNoticont=(TextView) view.findViewById(R.id.tv_noticont);
            final TextView tv_notification=(TextView) view.findViewById(R.id.tv_notification);
            final CheckBox cb_dontsee=(CheckBox) view.findViewById(R.id.cb_dontsee);
            final DocumentView dv=(DocumentView)view.findViewById(R.id.blogText) ;
            Typeface type = Typeface.createFromAsset(getAssets(),"fonts/ssr.ttf");
            dv.getDocumentLayoutParams().setTextTypeface(type);


            tvNoticont.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateMobileNumber("1");
                    M.setPhone("1",OtpActivity.this);

                }
            });
            notificationDialog.show();
        }
        etPhonenumber.setText(M.getPhone(OtpActivity.this));
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential,String phoneNumber) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("PhoneVERifier", "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            FirebaseAuth.getInstance().signOut();
                            updateMobileNumber(etPhonenumber.getText().toString());

                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("PhoneVERifier", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                            Toast.makeText(OtpActivity.this, "Invalid OTP Code", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    public void updateMobileNumber(String phoneNumber)
    {
        String userid= M.getID(OtpActivity.this).toString();

       // M.showLoadingDialog(OtpActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.updatePhoneNumber(userid, phoneNumber, new Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                Toast.makeText(OtpActivity.this, "Thank you , Your mobile number verified and updated.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(OtpActivity.this,HomeActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(OtpActivity.this, "Something went worng, please try agian", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(OtpActivity.this,HomeActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
