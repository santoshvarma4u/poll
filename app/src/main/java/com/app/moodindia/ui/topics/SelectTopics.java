package com.app.moodindia.ui.topics;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.app.moodindia.CurrentPolls;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.LoginActivity;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.UserCategories;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectTopics extends Fragment implements ChipListener {


    protected View rootView;
    protected RecyclerView rvTopics;
    protected ChipCloud chips_all_topics;
    protected Button btnSelectAll;



    public SelectTopics() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_select_topics, container, false);
        initView(rootView);

        ((TopicsActivity)getActivity()).setFragmentRefreshListener(new TopicsActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                getUserFollowing();
            }
        });
        return rootView;
    }

    @Override
    public void chipSelected(int i) {
        if(i>=0)
        {
            Intent intent=new Intent(getActivity(),CurrentPolls.class);
            intent.putExtra("act_name","topics");
            intent.putExtra("catid",categoryList.get(i).getCat_id());
            intent.putExtra("catname",categoryList.get(i).getCat_name());
            startActivity(intent);
        }
    }

    @Override
    public void chipDeselected(int i) {
        if(i>=0)
        {
            Intent intent=new Intent(getActivity(),CurrentPolls.class);
            intent.putExtra("act_name","topics");
            intent.putExtra("catid",categoryList.get(i).getCat_id());
            intent.putExtra("catname",categoryList.get(i).getCat_name());
            startActivity(intent);
        }
    }


    protected class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_category_follow, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return  categoryList.size();
        }
        CategoryRealmPojo listCategory;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCategory=categoryList.get(position);
            holder.CategoryName.setText(listCategory.getCat_name());
            holder.CategoryId=listCategory.getCat_id();
            if(listCategory.getFollow().equalsIgnoreCase("1"))
            {
                holder.btnFollow.setText("Unfollow");
                holder.btnFollow.setTextColor(Color.WHITE);
                holder.btnFollow.setBackground(getResources().getDrawable(R.drawable.btn_rounded_white_border));
            }
            /*iv_cat_image*/
            Picasso.with(getActivity())
                    .load(AppConst.imgurl + listCategory.getCat_img())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.iv_cat_image);
        }
    }

    List<CategoryRealmPojo> categoryList;
    CategoryListAdapter cla;


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CategoryName;
        Button btnFollow;
        String CategoryId;
        ImageView iv_cat_image;

       // CheckBox cb_select;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CategoryName=(TextView)itemView.findViewById(R.id.tv_categoryname);
            btnFollow=(Button) itemView.findViewById(R.id.btnFollow);
            iv_cat_image=(ImageView) itemView.findViewById(R.id.iv_cat_image);
            //chipCloud=(ChipCloud) itemView.findViewById(R.id.chip_item);

            CategoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String categoryid= categoryList.get(getAdapterPosition()).getCat_id();
                    Intent intent=new Intent(getActivity(),CurrentPolls.class);
                    intent.putExtra("act_name","topics");
                    intent.putExtra("catid",categoryid);
                    intent.putExtra("catname",CategoryName.getText().toString());
                    startActivity(intent);
                }
            });

            btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if( categoryList.get(getAdapterPosition()).getFollow().equalsIgnoreCase("0"))
                    {
                        updateUserFollowing(categoryList.get(getAdapterPosition()).getCat_id(),"1");
                    }else
                    {
                        updateUserFollowing(categoryList.get(getAdapterPosition()).getCat_id(),"0");
                    }
                }
            });
        }
    }

    public void getUserFollowing()
    {
       // categoryList.clear();
     //   M.showLoadingDialog(getActivity());
        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.getUserFollowing(M.getID(getActivity()), new retrofit.Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(List<CategoryRealmPojo> categoryRealmPojos, Response response) {

                categoryList=categoryRealmPojos;

                for(CategoryRealmPojo crp:categoryRealmPojos)
                {
                    chips_all_topics.addChip(crp.getCat_name());

                }
                int i=0;
                for(CategoryRealmPojo crp:categoryRealmPojos)
                {
                    if(crp.getFollow().equalsIgnoreCase("1"))
                    {
                        chips_all_topics.setSelectedChip(i);
                    }
                    i++;
                }
                chips_all_topics.setChipListener(SelectTopics.this);
               /* rvTopics.setAdapter(cla);

                cla.notifyDataSetChanged();*/

               // M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
              //  M.hideLoadingDialog();
                Toast.makeText(getActivity().getApplication().getApplicationContext(), "Oops, Something went wrong please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void updateUserFollowing(String catid,String status)
    {
        //Toast.makeText(getActivity(), catid, Toast.LENGTH_SHORT).show();
        if(catid.length()>0)
        {
            M.showLoadingDialog(getActivity());
            CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
            mCategoryAPI.updateUserFollowing(M.getID(getActivity()), catid, status, new retrofit.Callback<StatusPojo>() {
                @Override
                public void success(StatusPojo statusPojo, Response response) {
                    //userupdated
                    //getUserFollowing();
                    M.hideLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    // getUserFollowing();
                }
            });
        }
    }

    private void initView(View rootView) {

        chips_all_topics = (ChipCloud)rootView.findViewById(R.id.chips_all_topics);
        btnSelectAll=(Button)rootView.findViewById(R.id.btnSelectAll);

        rvTopics = (RecyclerView) rootView.findViewById(R.id.rv_topics);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvTopics.setLayoutManager(mLayoutManager);
        rvTopics.setItemAnimator(new DefaultItemAnimator());
        categoryList=new ArrayList<>();
        cla=new CategoryListAdapter();
        getUserFollowing();
        btnSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAllTopics();
            }
        });
    }
    public void selectAllTopics()
    {
       String userid = M.getID(getActivity());

        if(userid.length()>0)
        {
            M.showLoadingDialog(getActivity());
            CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
            mCategoryAPI.updateAllUserFollowing(M.getID(getActivity()), new retrofit.Callback<StatusPojo>() {
                @Override
                public void success(StatusPojo statusPojo, Response response) {
                    //userupdated
                    //getUserFollowing();
                    M.hideLoadingDialog();
                    getUserFavsString();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    // getUserFollowing();
                    getUserFavsString();
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(), "Oops Something went wrong with your login. Please Login again", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }
    }

    public void getUserFavsString()
    {

        M.showLoadingDialog(getActivity());

        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.getUserCatgories(M.getID(getActivity()), new retrofit.Callback<List<UserCategories>>() {

            @Override
            public void success(List<UserCategories> userCategories, Response response) {

                if(userCategories.size() > 0)
                {
                    if(  userCategories.get(userCategories.size()-1).getUsercategories().length() > 0)
                    {
                        //  Toast.makeText(TopicsActivity.this, userCategories.get(userCategories.size()-1).getUsercategories(), Toast.LENGTH_SHORT).show();
                        M.setFavs(userCategories.get(userCategories.size()-1).getUsercategories(),getActivity());
                        startActivity(new Intent(getActivity(),HomeActivity.class));
                        getActivity().finish();
                        M.hideLoadingDialog();
                    }
                    else
                    {

                        Toast.makeText(getActivity(), "Please select some topics to explore", Toast.LENGTH_SHORT).show();
                        M.hideLoadingDialog();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "Please select some topics to explore", Toast.LENGTH_SHORT).show();
                    M.hideLoadingDialog();
                }

            }
            @Override
            public void failure(RetrofitError error) {

                Toast.makeText(getActivity(), "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }
        });
    }
}
