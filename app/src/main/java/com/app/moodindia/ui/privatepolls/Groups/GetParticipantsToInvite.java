package com.app.moodindia.ui.privatepolls.Groups;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ParticipantsPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class GetParticipantsToInvite extends AppCompatActivity implements View.OnClickListener {

    protected TextView tvInviteParticipants;
    protected RecyclerView rvParticipantsInvite;
    public String groupid="0";
    public String uidforchat="0";
    LinearLayoutManager mLinearLayoutManager;
    public String groupName="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setContentView(R.layout.activity_get_participants_to_invite);
        initView();
        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("groupid"))
        {
            groupid=getIntent().getExtras().getString("groupid").toString();
            groupName=getIntent().getExtras().getString("groupName").toString();
            uidforchat=getIntent().getExtras().getString("uidforchat").toString();
        }
        getInviteParticipants();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_invite_participants) {
            if(checkedList.size()>0)
                createUserString(checkedList);
            else
                Toast.makeText(GetParticipantsToInvite.this, "Please select participants to invite", Toast.LENGTH_SHORT).show();
        }
    }
    private void initView() {
        tvInviteParticipants = (TextView) findViewById(R.id.tv_invite_participants);
        tvInviteParticipants.setOnClickListener(GetParticipantsToInvite.this);
        rvParticipantsInvite = (RecyclerView) findViewById(R.id.rv_participants_invite);
        mLinearLayoutManager=new LinearLayoutManager(GetParticipantsToInvite.this,LinearLayoutManager.VERTICAL,false);
        rvParticipantsInvite.setLayoutManager(mLinearLayoutManager);
        rvParticipantsInvite.setItemAnimator(new DefaultItemAnimator());
        pla=new PartcipantsAdapter();

}
    PartcipantsAdapter pla;
    public class PartcipantsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_participants, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mParticipantsPojos.size();
        }

        ParticipantsPojo mParticipantsPojo;
        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            mParticipantsPojo=mParticipantsPojos.get(position);


            holder.tvname.setText(mParticipantsPojo.getName());
            holder.phone.setText(mParticipantsPojo.getPhone());

            Picasso.with(GetParticipantsToInvite.this)
                    .load(AppConst.imgurl+mParticipantsPojo.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivProfile);
        }
    }
    List<ParticipantsPojo> mParticipantsPojos;

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvname;
        ImageView ivProfile;
        TextView phone;
        CheckBox cb_invite;
        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvname=(TextView)itemView.findViewById(R.id.tv_username);
            phone=(TextView)itemView.findViewById(R.id.tv_phone);
            ivProfile=(ImageView)itemView.findViewById(R.id.iv_participant);
            cb_invite=(CheckBox)itemView.findViewById(R.id.cb_invite_user);
            cb_invite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                     @Override
                                                     public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                                         if(b)
                                                         {
                                                             checkedList.add(mParticipantsPojos.get(getAdapterPosition()).getUserid());
                                                         }
                                                         else
                                                         {
                                                             checkedList.remove(mParticipantsPojos.get(getAdapterPosition()).getUserid());
                                                         }
                                                     }
                                                 }
            );
        }
    }

    List<String> checkedList = new ArrayList<>();

    public void createUserString(List<String> mStringList)
    {
        String AppliedFilters="";
        for(int i=0;i<mStringList.size();i++)
        {
                AppliedFilters= mStringList.get(i)+","+AppliedFilters;
        }
        if(AppliedFilters.length()>0)
        {
            sendPushToOtherParticipants(groupid,AppliedFilters,groupName,uidforchat);
        }
    }
    public void getInviteParticipants()
    {
        M.showLoadingDialog(GetParticipantsToInvite.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getInviteParticipants(M.getID(GetParticipantsToInvite.this), groupid, new Callback<List<ParticipantsPojo>>() {
            @Override
            public void success(List<ParticipantsPojo> participantsPojos, Response response) {
            M.hideLoadingDialog();
                if(participantsPojos.size()>0)
                {
                    mParticipantsPojos=participantsPojos;
                    rvParticipantsInvite.setAdapter(pla);
                    pla.notifyDataSetChanged();
                }
                else
                {
                    Toast.makeText(GetParticipantsToInvite.this, "No Participants found in your groups", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(GetParticipantsToInvite.this, "Something went wrong, we are facing difficulties to get participants to invite. ", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void sendPushToOtherParticipants(String groupid,String userid,String groupname,String uidforchat) {
        M.showTextLoadingDialog(GetParticipantsToInvite.this,"Please wait sending invitations..");
        OkHttpClient client = new OkHttpClient();
        String generateUrl=ConsantURL+"app/Http/Controllers/sendPrivateNotifications.php?sendGroupInvitation=true&groupid="+groupid+"&userid="+userid+"&uidforchat="+uidforchat+"&groupname="+groupname;
        Request request = new Request.Builder().url(generateUrl).build();
        Log.i("gegerate",generateUrl);
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {

                if (response.isSuccessful()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            M.hideLoadingDialog();
                            Toast.makeText(GetParticipantsToInvite.this, "Invitations sent successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
                else {
                    if (response.isSuccessful()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                M.hideLoadingDialog();
                                Toast.makeText(GetParticipantsToInvite.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                    }
                }
            }
        });
    }
}
