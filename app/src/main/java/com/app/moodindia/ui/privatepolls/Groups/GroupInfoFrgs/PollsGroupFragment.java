package com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.WrapContentLinearLayoutManager;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.ui.privatepolls.AddPollActivity;
import com.app.moodindia.ui.privatepolls.PrivatePollScreen;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;


public class PollsGroupFragment extends Fragment {

    private static final String ARG_PARAM1 = "groupid";
    private static final String ARG_PARAM2 = "groupname";
    private static final String ARG_PARAM3 = "isOwner";
    private static final String ARG_PARAM4 = "grouptype";
    protected View rootView;
    protected RecyclerView rvPolls;
    protected LinearLayout lytPolls;
    protected FloatingActionButton fabadd;
    protected TextView nopolls;
    List<PrivatePollPojo> poolsList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;

    private OnFragmentInteractionListener mListener;


    public PollsGroupFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PollsGroupFragment newInstance(String param1, String param2, String param3, String param4) {
        PollsGroupFragment fragment = new PollsGroupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4= getArguments().getString(ARG_PARAM4);

        }
    }

    private void initView(View rootView) {
        rvPolls = (RecyclerView) rootView.findViewById(R.id.rvPolls);
        lytPolls = (LinearLayout) rootView.findViewById(R.id.lytPolls);
        fabadd = (FloatingActionButton) rootView.findViewById(R.id.fabadd);
        nopolls=(TextView) rootView.findViewById(R.id.nopolls);
        rvPolls.setLayoutManager(new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPolls.setItemAnimator(new DefaultItemAnimator());
        poolsList=new ArrayList<>();
        pla=new PollsAdapter();
        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddPollActivity.class);
                intent.putExtra("groupid", mParam1);
                intent.putExtra("groupname", mParam2);
                intent.putExtra("isOwner", mParam3);

                if(mParam4.equalsIgnoreCase("2"))
                    intent.putExtra("grouptype", "Public");
                else if(mParam4.equalsIgnoreCase("1"))
                    intent.putExtra("grouptype","business");

                startActivity(intent);
            }
        });
        if(mParam3.equalsIgnoreCase("true")){
            fabadd.setVisibility(View.VISIBLE);
        }
        else
        {
            fabadd.setVisibility(View.GONE);
        }

    }
    RecyclerView.LayoutManager mLayoutManager;

    PollsAdapter pla;
    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.group_poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return poolsList == null ? 0 : poolsList.size();

            //return poolsList.size();
        }
        PrivatePollPojo listCategory;



        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listCategory=poolsList.get(position);

            holder.tvpoll.setText(listCategory.getQuestion());
            holder.btn_no_participants.setVisibility(View.VISIBLE);
            holder.tvparticipants.setText(listCategory.getNoofparticipants());

            if(listCategory.getAnonymous().equalsIgnoreCase("0"))
            {
                holder.btn_created_by.setText("By "+listCategory.getUsername());
            }
            else
            {
                holder.btn_created_by.setText("By Anonymous");
            }
            holder.tv_share_pollid.setText("Poll ID : "+listCategory.getUid());

            holder.btn_no_participants.setText(listCategory.getNoofparticipants());

            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(getActivity())
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost,tv_share_pollid,btn_no_participants,btn_share_private_poll,btn_created_by;
        ImageView ivpoll;
        TextView tvpollcategory;
        LinearLayout llpoll;
        CardView cv_category;
        TextView tvparticipants;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            btn_created_by=(TextView)itemView.findViewById(R.id.btn_created_by);
            tv_share_pollid=(TextView)itemView.findViewById(R.id.tv_share_pollid);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
           /* tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);*/
            /*cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;

           */
            llpoll=(LinearLayout) itemView.findViewById(R.id.llpoll);
            tvparticipants=(TextView) itemView.findViewById(R.id.tvparticipants);
           // btn_share_private_poll=(TextView) itemView.findViewById(R.id.btn_share_private_poll);
            btn_no_participants=(TextView) itemView.findViewById(R.id.btn_no_participants);
            btn_no_participants.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

           /* btn_share_private_poll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("plain/text");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, (poolsList.get(getAdapterPosition()).getQuestion() + "\n" + AppConst.downloadurl));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. " +
                            "Please participate and express your opinion. \n"+poolsList.get(getAdapterPosition()).getQuestion()+" \n Poll ID:" + poolsList.get(getAdapterPosition()).getQuestion_id() + "  \nhttp://moodindia.in/privatepoll/"+poolsList.get(getAdapterPosition()).getQuestion_id()+ " \n \n "));
                    startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));

                }
            });*/
            Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/ssr.ttf");

            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("groupview","groupview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("groupview","groupview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
            llpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(getActivity(), PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("groupview","groupview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), p1,p2);
                    startActivity(it, options.toBundle());
                }
            });

        }
    }
    int currentpage=1;


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_polls_group, container, false);
        initView(rootView);
        if(mParam1!=null)
        {
            getMyPolls(mParam1);
        }
        return rootView;
    }

    public void getMyPolls(String groupId)
    {
       // M.showLoadingDialog(getActivity());
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getParticipatedGroupPolls(M.getID(getActivity()),groupId,new retrofit.Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        poolsList=pojo;

                        rvPolls.setAdapter(pla);
                        pla.notifyDataSetChanged();
                      //  M.hideLoadingDialog();

                    }else{
                        rvPolls.setVisibility(View.GONE);
                        nopolls.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), "No polls found in this group", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    rvPolls.setVisibility(View.GONE);
                    nopolls.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "No polls found in this group", Toast.LENGTH_SHORT).show();
                }
              //  M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
              //  M.hideLoadingDialog();
                Toast.makeText(getActivity(), "No polls found in this group", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }
}
