package com.app.moodindia.ui.privatepolls.Groups;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.SplashActivity;
import com.app.moodindia.helper.QRCodeGenerator.QRGContents;
import com.app.moodindia.helper.QRCodeGenerator.QRGEncoder;
import com.app.moodindia.model.M;
import com.google.zxing.WriterException;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

import xyz.hanks.library.SmallBang;

public class GroupSuccessActivity extends AppCompatActivity implements View.OnClickListener {

    protected MagicTextView grouptext;
    protected ImageView ivQRCode;
    protected MagicButton btnSave;
    protected MagicButton btnContinue;
    public String groupId = "0";
    public String groupName = "";
    public String groupuid = "";
    protected ImageView ivthumbsup;
    protected MagicTextView tvGroupid;
    FileOutputStream fileoutputstream;
    ByteArrayOutputStream bytearrayoutputstream;
    File file;
    private SmallBang mSmallBang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_group_success);
        initView();
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("groupid")) {
            groupId = getIntent().getExtras().getString("groupid");
            groupName = getIntent().getExtras().getString("groupname");
            groupuid = getIntent().getExtras().getString("uidforchat");

            if (groupName.length() > 0) {
                grouptext.setText(groupName + " Group Created Successfully");
                tvGroupid.setText("Group Id : "+groupuid);
            }
            //create group QR
            if (groupId.length() > 0)
                generateQRCode("MIGROUP-" + groupuid);
            else {
                Toast.makeText(GroupSuccessActivity.this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(GroupSuccessActivity.this, HomeActivity.class));
                finish();
            }
        } else {
            Toast.makeText(GroupSuccessActivity.this, "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(GroupSuccessActivity.this, HomeActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.grouptext) {

        } else if (view.getId() == R.id.ivQRCode) {

        } else if (view.getId() == R.id.btnSave) {
            if (!groupId.equalsIgnoreCase("0")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(GroupSuccessActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                    } else {
                        saveImageinGallery(bitmap);
                    }
                } else {
                    saveImageinGallery(bitmap);
                }
            }
        } else if (view.getId() == R.id.btnContinue) {
            Intent it = new Intent(GroupSuccessActivity.this, GroupInfoActivity.class);
            it.putExtra("groupid", groupId);
            it.putExtra("groupname", groupName);
            finish();
            startActivity(it);
        } else if (view.getId() == R.id.ivthumbsup) {

        } else if (view.getId() == R.id.tv_groupid) {

        }
    }

    private void initView() {
        grouptext = (MagicTextView) findViewById(R.id.grouptext);
        grouptext.setOnClickListener(GroupSuccessActivity.this);
        ivQRCode = (ImageView) findViewById(R.id.ivQRCode);
        ivQRCode.setOnClickListener(GroupSuccessActivity.this);
        btnSave = (MagicButton) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(GroupSuccessActivity.this);
        btnContinue = (MagicButton) findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(GroupSuccessActivity.this);
        bytearrayoutputstream = new ByteArrayOutputStream();
        mSmallBang = SmallBang.attach2Window(this);
        ivthumbsup = (ImageView) findViewById(R.id.ivthumbsup);
        ivthumbsup.setOnClickListener(GroupSuccessActivity.this);
        mSmallBang.bang(ivthumbsup);
        tvGroupid = (MagicTextView) findViewById(R.id.tv_groupid);
        tvGroupid.setOnClickListener(GroupSuccessActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    saveImageinGallery(bitmap);

                } else {
                    Toast.makeText(getApplicationContext(), "Please Allow Permission to save the file", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(GroupSuccessActivity.this, SplashActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }

    Bitmap bitmap;

    private void generateQRCode(String inputValue) {
        M.showLoadingDialog(GroupSuccessActivity.this);


        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        //  smallerDimension = smallerDimension * 2 / 4;

        QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, smallerDimension);
        try {
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            ivQRCode.setImageBitmap(bitmap);

        } catch (WriterException e) {
            Log.v("GroupQR", e.toString());
        }
        M.hideLoadingDialog();
    }

    public void saveImageinGallery(Bitmap imageBitmap) {
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 60, bytearrayoutputstream);

        file = new File(Environment.getExternalStorageDirectory() + "/" + "MI" + UUID.randomUUID().toString() + ".png");

        try

        {
            file.createNewFile();

            fileoutputstream = new FileOutputStream(file);

            fileoutputstream.write(bytearrayoutputstream.toByteArray());

            fileoutputstream.close();
            Toast.makeText(this, "QR Image saved in gallery successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Unable save image in gallery", Toast.LENGTH_SHORT).show();
        }
    }
}
