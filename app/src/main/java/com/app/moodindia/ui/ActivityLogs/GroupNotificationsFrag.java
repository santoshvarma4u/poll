package com.app.moodindia.ui.ActivityLogs;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.model.M;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.model.UserLog;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupNotificationsFrag extends Fragment {


    protected View rootView;
    protected RecyclerView rvPollNotifications;
    protected MagicTextView notificationCountTile;

    public GroupNotificationsFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_group_notifications, container, false);
        initView(rootView);
        return rootView;
    }

    LinearLayoutManager mLayoutManager;
    MsgAdapter pla;
    List<UserLog> userLogs;

    private void initView(View rootView) {
        rvPollNotifications = (RecyclerView) rootView.findViewById(R.id.rv_poll_notifications);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvPollNotifications.setLayoutManager(mLayoutManager);
        rvPollNotifications.setItemAnimator(new DefaultItemAnimator());

        pla = new MsgAdapter();
        userLogs = new ArrayList<>();
        getGroupNotifications();
        notificationCountTile = (MagicTextView) rootView.findViewById(R.id.notificationCountTile);
    }
    int totalCount = 0;
    SimpleDateFormat defaultfmt = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dtfmt = new SimpleDateFormat("dd MMM yyyy");
            public class MsgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_messages, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return userLogs.size();
        }

        UserLog mUserLog;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            mUserLog = userLogs.get(position);
            holder.tvtitle.setText(mUserLog.getNotification_title());
            holder.tvmsg.setText(mUserLog.getNotification_message());
            //holder.date.setText(mUserLog.getCreated_at());

            try {
                holder.date.setText("- "+dtfmt.format(defaultfmt.parse(mUserLog.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (mUserLog.getRead().equalsIgnoreCase("1")) {
                holder.tvtitle.setTextColor(Color.parseColor("#888888"));
                holder.readview.setBackgroundColor(Color.parseColor("#888888"));
            }
        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder {

        TextView tvtitle, tvmsg,date;
        CardView cv_notifications;
        LinearLayout readview;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvtitle = (TextView) itemView.findViewById(R.id.title);
            tvmsg = (TextView) itemView.findViewById(R.id.message);
            date = (TextView) itemView.findViewById(R.id.date);
            cv_notifications = (CardView) itemView.findViewById(R.id.cv_notifications);
            readview = (LinearLayout) itemView.findViewById(R.id.readview);
            tvtitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Groups")) {
                        groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        if (userLogs.get(getAdapterPosition()).getGroupid().equalsIgnoreCase("0")) {
                            //do nothing
                        } else {
                            //redirect to poll screen
                            Intent it = new Intent(getActivity(), GroupInfoActivity.class);
                            it.putExtra("groupid", userLogs.get(getAdapterPosition()).getGroupid());
                            startActivity(it);
                        }
                    }
                }
            });
            tvmsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Groups")) {
                        groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        if (userLogs.get(getAdapterPosition()).getGroupid().equalsIgnoreCase("0")) {
                            //do nothing
                        } else {
                            //redirect to poll screen
                            Intent it = new Intent(getActivity(), GroupInfoActivity.class);
                            it.putExtra("groupid", userLogs.get(getAdapterPosition()).getGroupid());
                            startActivity(it);
                        }
                    }
                }
            });
            cv_notifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userLogs.get(getAdapterPosition()).getMode().equalsIgnoreCase("Groups")) {
                        groupUpdateRead(userLogs.get(getAdapterPosition()).getNotification_id());
                        if (userLogs.get(getAdapterPosition()).getGroupid().equalsIgnoreCase("0")) {
                            //do nothing
                        } else {
                            //redirect to poll screen
                            Intent it = new Intent(getActivity(), GroupInfoActivity.class);
                            it.putExtra("groupid", userLogs.get(getAdapterPosition()).getGroupid());
                            startActivity(it);
                        }
                    }
                }
            });
        }
    }

    public void groupUpdateRead(String notificationId) {

        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.submitReadNotification(notificationId, new Callback<SuccessPojo>() {

            @Override
            public void success(SuccessPojo successPojo, Response response) {
                //sucess
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getGroupNotifications() {
        // M.showLoadingDialog(getActivity());
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getGroupsLog(M.getID(getActivity()), new Callback<List<UserLog>>() {
            @Override
            public void success(List<UserLog> muserLogs, Response response) {
                //   M.hideLoadingDialog();
                userLogs = muserLogs;
                notificationCountTile.setText("You've "+userLogs.get(userLogs.size()-1).getTotalCount()+" unread notifications");

                rvPollNotifications.setAdapter(pla);
                pla.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                //   M.hideLoadingDialog();
            }
        });
    }
}
