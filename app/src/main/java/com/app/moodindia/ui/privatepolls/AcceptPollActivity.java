package com.app.moodindia.ui.privatepolls;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class AcceptPollActivity extends AppCompatActivity implements View.OnClickListener {

    protected CircleImageView userProfileImage;
    protected CircleImageView pollImage;
    protected LinearLayout linearLayout;
    protected MagicTextView pollQuestion;
    protected MagicButton btnAccept;
    protected MagicButton btnReject;
    String pollid="0";
    String originalpollid="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_accept_poll);
        initView();
    }



    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_accept) {
            submitAcceptance(originalpollid,M.getID(AcceptPollActivity.this));
        } else if (view.getId() == R.id.btn_reject) {
            startActivity(new Intent(AcceptPollActivity.this, HomeActivity.class));
            finish();
        }
    }

    private void initView() {
        userProfileImage = (CircleImageView) findViewById(R.id.user_profile_image);
        pollImage = (CircleImageView) findViewById(R.id.poll_image);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        pollQuestion = (MagicTextView) findViewById(R.id.poll_question);
        btnAccept = (MagicButton) findViewById(R.id.btn_accept);
        btnAccept.setOnClickListener(AcceptPollActivity.this);
        btnReject = (MagicButton) findViewById(R.id.btn_reject);
        btnReject.setOnClickListener(AcceptPollActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras().containsKey("poll_id"))
        {
            getPrivatePollAcceptDetails(getIntent().getExtras().getString("poll_id"),M.getID(AcceptPollActivity.this));
            pollid=getIntent().getExtras().getString("poll_id");
           // originalpollid=getIntent().getExtras().getString("originalpollid");
        }
        else
        {
            Toast.makeText(AcceptPollActivity.this, "Invalid Poll ID, Please try again", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(AcceptPollActivity.this, HomeActivity.class));
            finish();
        }
    }

    public void getPrivatePollAcceptDetails(final String pollid, String userid)
    {
        M.showLoadingDialog(AcceptPollActivity.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getAcceptPoll(userid, pollid, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojo, Response response) {

                System.out.println(statusPojo.get(0).getStatus()+"......................");
                if(statusPojo.get(0).getStatus().equalsIgnoreCase("A"))
                {
                    M.hideLoadingDialog();
                    //Poll Accepted
                    //Redirect to poll screen
                    Toast.makeText(AcceptPollActivity.this, "Poll already accepted", Toast.LENGTH_SHORT).show();
                    originalpollid=statusPojo.get(0).getPollid();
                    Intent intent=new Intent(AcceptPollActivity.this,PrivatePollScreen.class);
                    intent.putExtra("from","active");
                    intent.putExtra("upollid",pollid);
                    intent.putExtra("pollid",originalpollid);
                    startActivity(intent);
                }
                else if(statusPojo.get(0).getStatus().equalsIgnoreCase("NA"))
                {
                   M.hideLoadingDialog();
                     //poll not accepted
                    // keep on same screen
                     //get poll information
                   originalpollid=statusPojo.get(0).getPollid();
                   getPollDetails(pollid);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AcceptPollActivity.this, "Unknown Error,Please try later", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AcceptPollActivity.this, HomeActivity.class));
                finish();
            }
        });
    }


    public void getPollDetails(String pollid)
    {
        M.showLoadingDialog(AcceptPollActivity.this);
        final PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getPrivatePollById(pollid, 1, new Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> privatePollPojos, Response response) {

                if(privatePollPojos.size()>0)
                {
                    M.hideLoadingDialog();
                    PrivatePollPojo mPrivatePollPojo=privatePollPojos.get(0);

                    Picasso.with(AcceptPollActivity.this).load(AppConst.imgurl+mPrivatePollPojo.getProfile_picture()).into(userProfileImage);
                    Picasso.with(AcceptPollActivity.this).load(AppConst.imgurl+mPrivatePollPojo.getImage()).into(pollImage);
                    pollQuestion.setText(mPrivatePollPojo.getQuestion());

                }
                else
                {
                    M.hideLoadingDialog();
                    Toast.makeText(AcceptPollActivity.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AcceptPollActivity.this, HomeActivity.class));
                    finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    public void submitAcceptance(final String pollid, final String userid)
    {
        M.showLoadingDialog(AcceptPollActivity.this);
        final PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.submitAcceptPoll(userid, pollid, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojo, Response response) {
                if(statusPojo.get(0).getStatus().equalsIgnoreCase("success"))
                {
                    sendPushToOwner(pollid,userid);
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(AcceptPollActivity.this, "Unable to accept poll ,please try again", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AcceptPollActivity.this, HomeActivity.class));
        finish();
    }

    public void sendPushToOwner(String privatepollid,String userid) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(ConsantURL+"app/Http/Controllers/sendPrivateNotifications.php?acceptnotification=true&privatepollid="+privatepollid+"&userid="+userid).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {

                if (response.isSuccessful()) {

                    M.hideLoadingDialog();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AcceptPollActivity.this, "Poll Accepted, Redirect to poll screen", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AcceptPollActivity.this,PrivatePollScreen.class);
                            intent.putExtra("from","active");
                            intent.putExtra("pollid",pollid);
                            intent.putExtra("upollid",pollid);
                            startActivity(intent);
                        }
                    });


                }
            }
        });
    }
}
