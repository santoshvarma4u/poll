package com.app.moodindia.ui.privatepolls;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.SuggestPollActivity;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.app.moodindia.R.id.que;

public class PrivateResultMapActivity extends AppCompatActivity   {
    private WebView wvResult;
    String pollid="";
    boolean isConnected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Statistics");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        final Intent it=new Intent(this,PollScreen.class);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
       /*         it.putExtra("pollid",getIntent().getExtras().getString("pollid"));
                it.putExtra("from",getIntent().getExtras().getString("from"));
                it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                finish();
                overridePendingTransition(0,0);*/

                onBackPressed();

            }
        });
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
       //  window.setStatusBarColor(ContextCompat.getColor(PrivateResultMapActivity.this, R.color.colorPrimaryDark));
        checkConnection();
        initView();
    }

    private void initView() {
        wvResult = (WebView) findViewById(R.id.wv_result);
     //   M.showLoadingDialog(PrivateResultMapActivity.this);

        wvResult.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                M.showLoadingDialog(PrivateResultMapActivity.this);
            }
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
              //
                M.hideLoadingDialog();
                view.getUrl();
                super.onPageFinished(view, url);

            }

            @Override
            public void onReceivedHttpError(
                    WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                //Toast.makeText(getApplicationContext(),"httperror",Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                           SslError error) {
               // Toast.makeText(getApplicationContext(),"SslError",Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
                //Your code to do
                Toast.makeText(getApplicationContext(), "Your Internet Connection May not be active Or " + error , Toast.LENGTH_LONG).show();
                M.hideLoadingDialog();


            }
        });
        pollid=getIntent().getExtras().getString("pollid");
        String userid=M.getID(PrivateResultMapActivity.this);
        WebSettings webSettings = wvResult.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        wvResult.getSettings().setJavaScriptEnabled(true);
        wvResult.getSettings().setDomStorageEnabled(true);
        wvResult.getSettings().setPluginState(WebSettings.PluginState.ON);
        if (Build.VERSION.SDK_INT >= 21) {
            wvResult.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager.getInstance().setAcceptThirdPartyCookies(wvResult, true);
        }
        wvResult.loadUrl("http://moodindia.in/public/mView/pindex.php?pid="+pollid+"&uid="+userid);

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getExtras().getString("from").equalsIgnoreCase("push"))
        {
            Intent it=new Intent(this,PrivatePolls.class);
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);

            finish();
            overridePendingTransition(0,0);
        }
        else if(getIntent().getExtras().containsKey("archives")){
            Intent it=new Intent(this,PrivatePolls.class);
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
        else
        {
            Intent it=new Intent(this,PrivatePollScreen.class);
            it.putExtra("pollid",getIntent().getExtras().getString("pollid"));
            it.putExtra("from","Active");
            it.putExtra("upollid",getIntent().getExtras().getString("upollid"));
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.result_map_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.refresh){
            Intent intent=new Intent(PrivateResultMapActivity.this,PrivateResultMapActivity.class);
            intent.putExtra("pollid",getIntent().getExtras().getString("pollid"));
            intent.putExtra("from",getIntent().getExtras().getString("from"));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else if(id==R.id.share)
        {



            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(PrivateResultMapActivity.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                }
                else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }

                }
            }
            else
            {
                sharepollkitkat();
            }

        }
        else if(id==R.id.fullstory)
        {
            Intent intent=new Intent(PrivateResultMapActivity.this,PollScreen.class);
            intent.putExtra("pollid",getIntent().getExtras().getString("pollid"));
            intent.putExtra("from","active");
            intent.putExtra("stats","stats");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }
    public void sharepoll(String path) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/gif");
        Uri myUri = Uri.parse("file://" + path);
        sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. " +
                "Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }

    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(PrivateResultMapActivity.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }



    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(PrivateResultMapActivity.this, Internet.class);
            startActivity(i);
        }


    }
}
