package com.app.moodindia.ui.privatepolls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.CGImagePojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChooseImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_image);
        initViews();
    }

    private  List<CGImagePojo> android;

    ImageAdapter ima;
    public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choose_image_layout, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {

            Log.i("Images",AppConst.IMAGE_URLS_GALLERY+android.get(i).getImage());

            Picasso.with(ChooseImageActivity.this).load(AppConst.IMAGE_URLS_GALLERY+android.get(i).getImage()).into(viewHolder.img_android);
        }

        @Override
        public int getItemCount() {
            return android.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            private ImageView img_android;
            public ViewHolder(View view) {
                super(view);
                img_android = (ImageView) view.findViewById(R.id.img_android);
            }
        }

    }

    RecyclerView recyclerView;
    private void initViews(){
        recyclerView = (RecyclerView)findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),4);
        recyclerView.setLayoutManager(layoutManager);
        android=new ArrayList<>();
        ima=new ImageAdapter();


     /*   ImageAdapter adapter = new ImageAdapter(getApplicationContext(),androidVersions);
        recyclerView.setAdapter(adapter);*/

    }


    @Override
    protected void onResume() {
        super.onResume();
        getAllPhotos();
    }

    public void getAllPhotos()
    {
        PrivatePollAPI mAuthenticationAPI = APIService.createServiceImages(PrivatePollAPI.class);
        mAuthenticationAPI.getAllPhotos("0", new retrofit.Callback<List<CGImagePojo>>() {
            @Override
            public void success(List<CGImagePojo> cgImagePojos, Response response) {
                android=cgImagePojos;

                recyclerView.setAdapter(ima);
                ima.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ChooseImageActivity.this, "Unable to load gallery", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
