package com.app.moodindia.ui.privatepolls.Groups;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.ui.privatepolls.PrivatePollScreen;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.ivankocijan.magicviews.views.MagicTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GroupDetails extends AppCompatActivity implements View.OnClickListener {

    protected KenBurnsView kvGroupBackground;
    protected CircleImageView ivGroupImage;
    protected MagicTextView tvGroupname;
    protected MagicTextView tvParticipants;
    protected MagicTextView tvPolls;
    protected MagicTextView tvChats;
    protected RecyclerView rvParticpants;
    protected LinearLayout lytParticipants;
    protected RecyclerView rvPolls;
    protected LinearLayout lytPolls;
    protected RecyclerView mainRecyclerView;
    protected EditText editTextMessage;
    protected ImageView imageButtonSend;
    protected ProgressWheel progressWheelsending;
    protected LinearLayout linLayoutText;
    protected LinearLayout lytChatView;
    public List<GroupPojo> mGroupPojos;
    public String groupId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_group_details);
        initView();
        mGroupPojos=new ArrayList<>();
        getGroupDetails();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_participants) {

        } else if (view.getId() == R.id.tv_polls) {

        } else if (view.getId() == R.id.tv_chats) {

        }
    }

    private void initView() {
        kvGroupBackground = (KenBurnsView) findViewById(R.id.kv_group_background);
        ivGroupImage = (CircleImageView) findViewById(R.id.iv_group_image);
        tvGroupname = (MagicTextView) findViewById(R.id.tv_groupname);
        tvParticipants = (MagicTextView) findViewById(R.id.tv_participants);
        tvParticipants.setOnClickListener(GroupDetails.this);
        tvPolls = (MagicTextView) findViewById(R.id.tv_polls);
        tvPolls.setOnClickListener(GroupDetails.this);
        tvChats = (MagicTextView) findViewById(R.id.tv_chats);
        tvChats.setOnClickListener(GroupDetails.this);
        rvParticpants = (RecyclerView) findViewById(R.id.rvParticpants);
        lytParticipants = (LinearLayout) findViewById(R.id.lytParticipants);
        rvPolls = (RecyclerView) findViewById(R.id.rvPolls);
        lytPolls = (LinearLayout) findViewById(R.id.lytPolls);
        mainRecyclerView = (RecyclerView) findViewById(R.id.main_recycler_view);
        editTextMessage = (EditText) findViewById(R.id.editText_message);
        imageButtonSend = (ImageView) findViewById(R.id.imageButton_send);
        progressWheelsending = (ProgressWheel) findViewById(R.id.progressWheelsending);
        linLayoutText = (LinearLayout) findViewById(R.id.linLayout_text);
        lytChatView = (LinearLayout) findViewById(R.id.lytChatView);

        if(getIntent().getExtras()!=null)
        {
            groupId=getIntent().getExtras().getString("groupid");
        }

        mLayoutManager = new LinearLayoutManager(GroupDetails.this);
        rvPolls.setLayoutManager(mLayoutManager);
        rvPolls.setItemAnimator(new DefaultItemAnimator());
        poolsList=new ArrayList<>();
        pla=new PollsAdapter();

    }
    RecyclerView.LayoutManager mLayoutManager;

    PollsAdapter pla;
    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.private_poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return poolsList == null ? 0 : poolsList.size();

            //return poolsList.size();
        }
        PrivatePollPojo listCategory;

        String tempCatId="";
        int unchangedValue=0;


        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listCategory=poolsList.get(position);
            holder.catLayout.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
            holder.btn_no_participants.setVisibility(View.VISIBLE);
            holder.tvparticipants.setText(listCategory.getNoofparticipants());

            holder.tv_share_pollid.setText("Poll ID : "+listCategory.getUid());

            holder.btn_no_participants.setText(listCategory.getNoofparticipants());

            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(GroupDetails.this)
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost,tv_share_pollid,btn_no_participants,btn_share_private_poll;
        ImageView ivpoll;
        TextView tvpollcategory;
        LinearLayout catLayout;
        CardView cv_category;
        TextView tvparticipants;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            tv_share_pollid=(TextView)itemView.findViewById(R.id.tv_share_pollid);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
            tvparticipants=(TextView) itemView.findViewById(R.id.tvparticipants);
            btn_share_private_poll=(TextView) itemView.findViewById(R.id.btn_share_private_poll);
            btn_no_participants=(TextView) itemView.findViewById(R.id.btn_no_participants);
            btn_no_participants.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btn_share_private_poll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("plain/text");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, (poolsList.get(getAdapterPosition()).getQuestion() + "\n" + AppConst.downloadurl));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. " +
                            "Please participate and express your opinion. \n"+poolsList.get(getAdapterPosition()).getQuestion()+" \n Poll ID:" + poolsList.get(getAdapterPosition()).getQuestion_id() + "  \nhttp://moodindia.in/privatepoll/"+poolsList.get(getAdapterPosition()).getQuestion_id()+ " \n \n "));
                    startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));

                }
            });
            Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(GroupDetails.this, PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("mainview","mainview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(GroupDetails.this, p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(GroupDetails.this, PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("mainview","mainview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(GroupDetails.this, p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
        }
    }
    int currentpage=1;

    public void getGroupDetails()
    {
        if(groupId.length()>0)
        {
            M.showLoadingDialog(GroupDetails.this);
            PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
            mPrivatePollAPI.getGroupDetails(groupId, new Callback<List<GroupPojo>>() {
                @Override
                public void success(List<GroupPojo> groupPojos, Response response) {
                    if(groupPojos.size()>0)
                    {
                        mGroupPojos=groupPojos;
                        final GroupPojo mGroupPojo=mGroupPojos.get(0);
                        Picasso.with(GroupDetails.this)
                                .load(AppConst.groupimages + mGroupPojo.getImage())
                                .placeholder(R.drawable.default_image)
                                .error(R.drawable.default_image)
                                .into(ivGroupImage);

                        Handler uiHandler = new Handler(Looper.getMainLooper());
                        uiHandler.post(new Runnable(){
                            @Override
                            public void run() {
                                try {
                                    Picasso.with(GroupDetails.this).load(AppConst.groupimages+mGroupPojo.getImage()).into(new Target() {
                                        @Override
                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                            Bitmap afterBlur=fastblur(bitmap,1,20);
                                            kvGroupBackground.setImageBitmap(afterBlur);

                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable errorDrawable) {

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                                        }
                                    });


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        tvParticipants.setText(mGroupPojo.getNoofparticipants());
                        tvGroupname.setText(mGroupPojo.getName());
                        tvPolls.setText(mGroupPojo.getPollcount());
                        M.hideLoadingDialog();
                        getMyPolls(groupId);
                    }
                    else
                    {
                        Toast.makeText(GroupDetails.this, "Invalid Group,Please try agian", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    M.hideLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    Toast.makeText(GroupDetails.this, "Oops,Something went wrong please try again", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }
    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) { return (null); } int w = bitmap.getWidth(); int h = bitmap.getHeight(); int[] pix = new int[w * h]; Log.e("pix", w + " " + h + " " + pix.length); bitmap.getPixels(pix, 0, w, 0, 0, w, h); int wm = w - 1; int hm = h - 1; int wh = w * h; int div = radius + radius + 1; int r[] = new int[wh]; int g[] = new int[wh]; int b[] = new int[wh]; int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; int vmin[] = new int[Math.max(w, h)]; int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) { p = pix[yi + Math.min(wm, Math.max(i, 0))]; sir = stack[i + radius]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) { r[yi] = dv[rsum]; g[yi] = dv[gsum]; b[yi] = dv[bsum]; rsum -= routsum; gsum -= goutsum; bsum -= boutsum; stackstart = stackpointer - radius + div; sir = stack[stackstart % div]; routsum -= sir[0]; goutsum -= sir[1]; boutsum -= sir[2]; if (y == 0) { vmin[x] = Math.min(x + radius + 1, wm); } p = pix[yw + vmin[x]]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) { yi = Math.max(0, yp) + x; sir = stack[i + radius]; sir[0] = r[yi]; sir[1] = g[yi]; sir[2] = b[yi]; rbs = r1 - Math.abs(i); rsum += r[yi] * rbs; gsum += g[yi] * rbs; bsum += b[yi] * rbs; if (i > 0) {
                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];
            } else {
                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];
            }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    List<PrivatePollPojo> poolsList;

    public void getMyPolls(String groupId)
    {
        M.showLoadingDialog(GroupDetails.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getParticipatedGroupPolls(M.getID(GroupDetails.this),groupId,new retrofit.Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        poolsList=pojo;

                        rvPolls.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        M.hideLoadingDialog();

                    }else{
                        Toast.makeText(GroupDetails.this, "No polls found in this category", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(GroupDetails.this, "No polls found in this category", Toast.LENGTH_SHORT).show();
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(GroupDetails.this, "No polls found in this category", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }
}
