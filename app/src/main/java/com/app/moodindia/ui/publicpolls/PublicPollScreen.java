package com.app.moodindia.ui.publicpolls;

/**
 * Created by SantoshT on 8/14/2017.
 */

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.moodindia.BrandsActivity;
import com.app.moodindia.LoginActivity;
import com.app.moodindia.R;
import com.app.moodindia.ResultMapActivity;
import com.app.moodindia.SuggestPollActivity;
import com.app.moodindia.adapter.OptionAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PollAPI;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.picasso.Picasso;
import com.wooplr.spotlight.SpotlightView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;
import xyz.hanks.library.SmallBang;

public class PublicPollScreen extends AppCompatActivity implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {

    TextView tvque,tvdate,tvPollCount,txtExp;
    LinearLayout lloptions,lytmap,llhiddenoption;
    Button btnsubmit;
    //FrameLayout frameLayout;
    //ImageView ivpoll;
    ListView lvoption;
    Bitmap mbitmap;
    File imagePath;
    String TAG="pollscreen",pollid="",userid="";
    String info,que,start,end,type,img="",queid,seloptions="",totalPolls,twittertag,youtubetag,chattag;
    int single;
    Boolean isvoted;
    Button tvinfo;
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    ArrayList<String> idlist=new ArrayList<>();
    private Button btnViewMaps;
    private Button btnSuggestPoll;
    MaterialDialog materialDialog;
    OptionAdapter adapter;
    ImageView ivpoll;
    boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        supportPostponeEnterTransition();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Fade().setDuration(500));
            getWindow().setExitTransition(new Fade().setDuration(500));
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // window.setStatusBarColor(ContextCompat.getColor(PollScreen.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pollid=getIntent().getExtras().getString("pollid");
        //Log.d("poll",pollid);
        userid= "0";//hardcoded for public polls

        tvque=(TextView)findViewById(R.id.tvque);
        tvdate=(TextView)findViewById(R.id.tvdate);
        tvinfo=(Button)findViewById(R.id.tvinfo);
        txtExp=(TextView)findViewById(R.id.txtExp);
        tvPollCount=(TextView)findViewById(R.id.tvPollCount);
        lloptions=(LinearLayout)findViewById(R.id.lloption);
        llhiddenoption=(LinearLayout)findViewById(R.id.llhiddenoption);
        btnsubmit=(Button)findViewById(R.id.btnsubmit);
        btnsubmit.setVisibility(View.GONE);
       /* frameLayout=(FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);*/
        btnSuggestPoll = (Button) findViewById(R.id.btnSuggestPoll);
        ivpoll=(ImageView) findViewById(R.id.ivpoll);
        // ivpoll.pause();
        lvoption=(ListView)findViewById(R.id.lvotedoption);
        // kvpoll=(KenBurnsView)findViewById(R.id.kvpoll);
        lvoption.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lytmap=(LinearLayout)findViewById(R.id.lytmap);
        setListViewHeightBasedOnChildren(lvoption);
        // getPollData();
        btnsubmit.setOnClickListener(this);
        tvinfo.setOnClickListener(this);
        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        btnViewMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PublicPollScreen.this,ResultMapActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("from",getIntent().getExtras().getString("from"));
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PublicPollScreen.this,SuggestPollActivity.class);
                startActivity(intent);
            }
        });

        checkConnection();

        if(isConnected)
        {
            if(getIntent().getExtras().containsKey("from"))
                getPollData(getIntent().getExtras().getString("from"));
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();



        //moved to on create
       /* if(getIntent().getExtras().containsKey("from"))
            getPollData(getIntent().getExtras().getString("from"));
*/
    }

    public void realtimeLoading()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds

                if(getIntent().getExtras().containsKey("from"))
                {
                    if(getIntent().getExtras().getString("from").equalsIgnoreCase("active"))
                    {
                        getRealTimePollData();
                        handler.postDelayed(this, 10000);
                    }
                }
                //  getPollData(getIntent().getExtras().getString("from"));

            }
        }, 500);
    }

    boolean isVoteLocal=false;

    private SmallBang mSmallBang;

    private void getPollData(String active) {
        // AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
        M.showLoadingDialog(PublicPollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid,pollid,active,"0","0",new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {

                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            tvinfo.setEnabled(true);
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            if(isvoted)
                            {

                                btnViewMaps.setVisibility(View.VISIBLE);
                                //   frameLayout.setVisibility(View.VISIBLE);

                                if(getIntent().getExtras().containsKey("stats")){
                                    tvinfo.performClick();
                                }
                                else
                                {
                                    tvque.setBackgroundColor(Color.parseColor("#e3e9ef"));
                                    tvque.getBackground().setAlpha(51);
                                    tvque.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    //  tvque.setBackgroundColor(getResources().getColor(R.color.light_green));
                                  //  realtimeLoading();
                                }
                            }
                            else
                            {
                                btnViewMaps.setVisibility(View.GONE);
                            }


                            tvque.setText(que);
                            if (img != "") {
                                Picasso.with(PublicPollScreen.this)
                                        .load(AppConst.imgurl + img)
                                        .placeholder(R.drawable.default_image)
                                        .error(R.drawable.default_image)
                                        .into(ivpoll);

                            }

                            try {
                                tvdate.setText("Posted on "+dtfmt.format(defaultfmt.parse(start)));
                                Date dt=defaultfmt.parse(end);
                                // txtExp.setVisibility(View.VISIBLE);
                                RelativeTimeTextView v = (RelativeTimeTextView)findViewById(R.id.timestamp);
                                v.setReferenceTime(dt.getTime());


                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(!isvoted) {
                                // getBrandDialog();
                                int i=0;
                                for (final Pollresult data : pojo.getPollresult()) {

                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(PublicPollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(14);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    final TextView hv = new TextView(PublicPollScreen.this);
                                    hv.setTextColor(Color.WHITE);
                                    hv.setText(data.getOptionvalue());
                                    hv.setPadding(20, 20,20, 20);
                                    hv.setTextSize(14);
                                    hv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    hv.setBackgroundResource(R.drawable.button_rounded_corner);
                                    hv.setVisibility(View.INVISIBLE);

                                    if(i==0)
                                    {
                                        new SpotlightView.Builder(PublicPollScreen.this)
                                                .introAnimationDuration(400)
                                                .enableRevealAnimation(true)
                                                .performClick(true)
                                                .fadeinTextDuration(400)
                                                .headingTvColor(Color.parseColor("#eb273f"))
                                                .headingTvSize(32)
                                                .headingTvText("Poll Options")
                                                .subHeadingTvColor(Color.parseColor("#ffffff"))
                                                .subHeadingTvSize(16)
                                                .subHeadingTvText("By clicking here you can vote your opinion on this poll")
                                                .maskColor(Color.parseColor("#dc000000"))
                                                .target(hv)
                                                .lineAnimDuration(400)
                                                .lineAndArcColor(Color.parseColor("#eb273f"))
                                                .dismissOnTouch(true)
                                                .dismissOnBackPress(true)
                                                .enableDismissAfterShown(true)
                                                .usageId("tvpollscreen")
                                                .show();
                                    }

                                    i++;


                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {


                                            final DialogPlus notificationDialog = DialogPlus.newDialog(PublicPollScreen.this)
                                                    .setGravity(Gravity.CENTER)
                                                    .setContentHolder(new ViewHolder(R.layout.custom_layout_no_login))
                                                    .setCancelable(true)
                                                    .create();


                                            View v=notificationDialog.getHolderView();

                                            final TextView tvNoticont=(TextView) v.findViewById(R.id.tv_noticont);
                                            final TextView tv_noticancel=(TextView) v.findViewById(R.id.tv_noticancel);



                                            tvNoticont.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    notificationDialog.dismiss();
                                                    Toast.makeText(PublicPollScreen.this, "You need to Login, To cast your vote", Toast.LENGTH_SHORT).show();
                                                    Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
                                                    it.putExtra("frompublic","true");
                                                    startActivity(it);
                                                    finish();
                                                }
                                            });
                                            tv_noticancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    notificationDialog.dismiss();
                                                }
                                            });

                                            notificationDialog.show();

                                            /*if (!M.getID(PublicPollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }*/
                                        }
                                    });

                                    LinearLayout.LayoutParams hidddenlayoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    hidddenlayoutParams.setMargins(5, 5, 5, 5);
                                    llhiddenoption.addView(hv, hidddenlayoutParams);

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 5);
                                    lloptions.addView(tv, layoutParams);




                                    //  lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);

                                adapter = new OptionAdapter(PublicPollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);

                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                M.hideLoadingDialog();

                supportStartPostponedEnterTransition();

                // ivpoll.resume();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }



    private void getRealTimePollData() {
        //AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
        // M.showLoadingDialog(PollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid,pollid,"active","0","0",new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {
                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            //Log.d("info",info);
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");

                            if(!isvoted) {


                                btnViewMaps.setVisibility(View.GONE);

                                for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(PublicPollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(14);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            final DialogPlus notificationDialog = DialogPlus.newDialog(PublicPollScreen.this)
                                                    .setGravity(Gravity.CENTER)
                                                    .setContentHolder(new ViewHolder(R.layout.custom_layout_no_login))
                                                    .setCancelable(true)
                                                    .create();


                                            View v=notificationDialog.getHolderView();

                                            final TextView tvNoticont=(TextView) v.findViewById(R.id.tv_noticont);
                                            final TextView tv_noticancel=(TextView) v.findViewById(R.id.tv_noticancel);



                                            tvNoticont.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    notificationDialog.dismiss();
                                                    Toast.makeText(PublicPollScreen.this, "You need to Login, To cast your vote", Toast.LENGTH_SHORT).show();
                                                    Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
                                                    it.putExtra("frompublic","true");
                                                    startActivity(it);
                                                }
                                            });
                                            tv_noticancel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    notificationDialog.dismiss();
                                                }
                                            });

                                            notificationDialog.show();


                                          /*  if (!M.getID(PublicPollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }*/
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 5);
                                    lloptions.addView(tv, layoutParams);
                                    lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                btnViewMaps.setVisibility(View.VISIBLE);
                                //  frameLayout.setVisibility(View.VISIBLE);
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                OptionAdapter adapter = new OptionAdapter(PublicPollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                // M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnsubmit){
            String ans="";
            for(int i=0;i<idlist.size();i++){
                if(i==0)
                    ans=idlist.get(i);
                else
                    ans=ans+","+idlist.get(i);
            }
            submitresult(ans);
        }else if(view.getId()==R.id.tvinfo){


            Toast.makeText(PublicPollScreen.this, "You need to Login, To access full story , videos and chat", Toast.LENGTH_SHORT).show();
            Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
            it.putExtra("frompublic","true");
            startActivity(it);
           /* Intent it=new Intent(PublicPollScreen.this,InfoActivity.class);
            it.putExtra("info",info);
            it.putExtra("question",que);
            it.putExtra("image",img);
            it.putExtra("screen","pollscreen");
            it.putExtra("twittertag",twittertag);
            it.putExtra("youtubetag",youtubetag);
            it.putExtra("chattag",chattag);
            it.putExtra("startdate",start);
            it.putExtra("enddate",end);
            it.putExtra("isvoted",isvoted);
            it.putExtra("from",getIntent().getExtras().getString("from"));
            it.putExtra("pollid",pollid);

            startActivity(it);*/
        }
    }

    public void submitresult(final String ans){
        //Log.d(TAG,"ans:"+ans);
        M.showLoadingDialog(PublicPollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.submitAns(queid,ans,M.getID(PublicPollScreen.this),new retrofit.Callback<SubmitAnsPojo>() {
            @Override
            public void success(SubmitAnsPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess()==1){
                        // AppConst.selpoll=null;
                        PollOptionPojo obj=new PollOptionPojo();
                        obj.setEndtime(end);
                        obj.setImage(img);
                        obj.setInfo(info);
                        obj.setIsvoted(true);
                        obj.setSelected_option(ans);
                        obj.setPollid(Integer.valueOf(queid));
                        obj.setPollresult(pojo.getPollresult());
                        obj.setQuestion(que);
                        obj.setStarttime(start);
                        obj.setType(type);
                        AppConst.selpoll=obj;
                        M.hideLoadingDialog();

                        Intent it=new Intent(PublicPollScreen.this, PublicPollScreen.class);
                        it.putExtra("pollid",pollid);
                        it.putExtra("from","active");
                        finish();
                        startActivity(it);

                      /*  new SweetAlertDialog(PollScreen.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Mood India")
                                .setContentText("Thanyou for your vote.Do you want to suggest a poll?")
                                .setConfirmText("Yes,I want!")
                                .setCancelText("No")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        pollid=getIntent().getExtras().getString("pollid");

                                        Intent it=new Intent(PollScreen.this,PollScreen.class);
                                        it.putExtra("pollid",pollid);
                                        it.putExtra("from","active");
                                        finish();
                                        startActivity(it);
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {

                                        Intent intent=new Intent(PollScreen.this,SuggestPollActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .show();*/




                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_poll,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home)
            supportFinishAfterTransition();
        else if(id==R.id.share){


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(PublicPollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else {
                        sharepollkitkat();
                    }
                }
            }
            else
            {
                sharepollkitkat();
            }





            /*Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            //this is the text that will be shared
           // sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));*/
            return true;
        }

        else if(id==R.id.report){

            Toast.makeText(PublicPollScreen.this, "You need to Login, To report a poll", Toast.LENGTH_SHORT).show();
            Intent it = new Intent(PublicPollScreen.this, LoginActivity.class);
            startActivity(it);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(PublicPollScreen.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(this,PublicHomePage.class);
        startActivity(it);
        supportFinishAfterTransition();
        overridePendingTransition(0,0);
        finish();
    }

    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }
    public void sharepoll(String path) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/png");
        Uri myUri = Uri.parse("file://" + path);
        sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. " +
                "Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }
    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }


    public void getBrandDialog()
    {
        Intent intent=new Intent(PublicPollScreen.this,BrandsActivity.class);
        startActivityForResult(intent,1);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {
        if (isConnected) {

        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }
}

