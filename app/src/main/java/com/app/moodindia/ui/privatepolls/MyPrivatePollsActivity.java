package com.app.moodindia.ui.privatepolls;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import retrofit.RetrofitError;

public class MyPrivatePollsActivity extends AppCompatActivity {

    RecyclerView rv_current_polls;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    RecyclerView.LayoutManager mLayoutManager;
    boolean  isConnected;
    private TextView tvNopolls;
    SwipeRefreshLayout svlayout;
    TableRow tbcreate,tbmanage;
    ImageView btn_back_nav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_private_polls);
        rv_current_polls = (RecyclerView)findViewById(R.id.pools_recycler);


        mLayoutManager = new LinearLayoutManager(MyPrivatePollsActivity.this);
        rv_current_polls.setLayoutManager(mLayoutManager);
        rv_current_polls.setItemAnimator(new DefaultItemAnimator());
        pla=new PollsAdapter();
        tvNopolls = (TextView) findViewById(R.id.tv_nopolls);
        tbcreate=(TableRow)findViewById(R.id.tableRowcreate);
        tbmanage=(TableRow)findViewById(R.id.tableRowmanage);
        btn_back_nav=(ImageView)findViewById(R.id.btn_back_nav);
        btn_back_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyPrivatePollsActivity.this, HomeActivity.class));
                finish();
            }
        });
        tbcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyPrivatePollsActivity.this,AddPollActivity.class));
            }
        });

        tbmanage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyPrivatePollsActivity.this,PrivatePolls.class));
            }
        });


        boolean loading = true;

        svlayout=(SwipeRefreshLayout)findViewById(R.id.svlayout);

        svlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pla=new PollsAdapter();
                if(pla.getItemCount()!=0) {

                    poolsList.clear();
                }
                getPrivatePollOwn();

                svlayout.setRefreshing(false);
            }
        });

        getPrivatePollOwn();
    }
    PollsAdapter pla;
    List<PrivatePollPojo> poolsList;


    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.private_poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return poolsList == null ? 0 : poolsList.size();

            //return poolsList.size();
        }
        PrivatePollPojo listCategory;

        String tempCatId="";
        int unchangedValue=0;


        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listCategory=poolsList.get(position);
            holder.catLayout.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
            holder.btn_no_participants.setVisibility(View.VISIBLE);
            holder.tvparticipants.setText(listCategory.getNoofparticipants());

            holder.tv_share_pollid.setText("Poll ID : "+listCategory.getUid());

            holder.btn_no_participants.setText(listCategory.getNoofparticipants());

            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(MyPrivatePollsActivity.this)
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost,tv_share_pollid,btn_no_participants,btn_share_private_poll;
        ImageView ivpoll;
        TextView tvpollcategory;
        LinearLayout catLayout;
        CardView cv_category;
        TextView tvparticipants;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            tv_share_pollid=(TextView)itemView.findViewById(R.id.tv_share_pollid);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
            tvparticipants=(TextView) itemView.findViewById(R.id.tvparticipants);
            btn_share_private_poll=(TextView) itemView.findViewById(R.id.btn_share_private_poll);
            btn_no_participants=(TextView) itemView.findViewById(R.id.btn_no_participants);
            btn_no_participants.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Toast.makeText(MyPrivatePollsActivity.this, getAdapterPosition()+"", Toast.LENGTH_SHORT).show();
                 /*   Intent intent=new Intent(MyPrivatePollsActivity.this,ParticipatedUserActivity.class);
                    intent.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    intent.putExtra("ques",poolsList.get(getAdapterPosition()).getQuestion());
                    intent.putExtra("postedtime",poolsList.get(getAdapterPosition()).getStart_time());
                    intent.putExtra("pollimage",poolsList.get(getAdapterPosition()).getImage());
                    startActivity(intent);*/
                }
            });

            btn_share_private_poll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("plain/text");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, (poolsList.get(getAdapterPosition()).getQuestion() + "\n" + AppConst.downloadurl));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. " +
                            "Please participate and express your opinion. \n"+poolsList.get(getAdapterPosition()).getQuestion()+" \n Poll ID:" + poolsList.get(getAdapterPosition()).getQuestion_id() + "  \nhttp://moodindia.in/privatepoll/"+poolsList.get(getAdapterPosition()).getQuestion_id()+ " \n \n "));
                    startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));

                }
            });
            Typeface custom_font = Typeface.createFromAsset(MyPrivatePollsActivity.this.getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(MyPrivatePollsActivity.this.getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(MyPrivatePollsActivity.this.getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(MyPrivatePollsActivity.this, PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("mainview","mainview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MyPrivatePollsActivity.this, p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(MyPrivatePollsActivity.this, PrivatePollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("upollid",poolsList.get(getAdapterPosition()).getUid());
                    it.putExtra("from","active");
                    it.putExtra("mainview","mainview");
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MyPrivatePollsActivity.this, p1,p2);
                    startActivity(it, options.toBundle());
                }
            });
        }
    }
    int currentpage=1;
    public void getPrivatePollOwn()
    {
        final int page=getCurrentpage();
        String favs= M.getFavs(MyPrivatePollsActivity.this).toString();
        M.showLoadingDialog(MyPrivatePollsActivity.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePolls(M.getID(MyPrivatePollsActivity.this),page,new retrofit.Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        poolsList=pojo;

                        rv_current_polls.setAdapter(pla);
                        pla.notifyDataSetChanged();

                        tvNopolls.setVisibility(View.GONE);

                    }else{

                        tvNopolls.setVisibility(View.VISIBLE);

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();

                return;
            }
        });
    }

    public int getCurrentpage() {
        return currentpage;
    }

    public void setCurrentpage(int currentpage)
    {
        this.currentpage = currentpage;
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (!isConnected) {
            Intent i=new Intent(MyPrivatePollsActivity.this, Internet.class);
            startActivity(i);

        }
    }
}
