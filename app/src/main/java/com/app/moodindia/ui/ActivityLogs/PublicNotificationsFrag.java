package com.app.moodindia.ui.ActivityLogs;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.model.M;
import com.app.moodindia.model.MessagesPojo;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublicNotificationsFrag extends Fragment {

    protected MagicTextView notificationCountTile;
    RecyclerView.LayoutManager mLayoutManager;
    boolean isConnected;
    protected View rootView;
    protected RecyclerView rvMessages;

    public PublicNotificationsFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_public_notifications, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        rvMessages = (RecyclerView) rootView.findViewById(R.id.rv_messages);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvMessages.setLayoutManager(mLayoutManager);
        rvMessages.setItemAnimator(new DefaultItemAnimator());
        pla = new MsgAdapter();
        msgList = new ArrayList<>();
        notificationCountTile = (MagicTextView) rootView.findViewById(R.id.notificationCountTile);
    }

    MsgAdapter pla;
    List<MessagesPojo> msgList;
    int totalCount = 0;
    SimpleDateFormat defaultfmt = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dtfmt = new SimpleDateFormat("dd MMM yyyy");
    public class MsgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_messages, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return msgList.size();
        }

        MessagesPojo listMesaages;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listMesaages = msgList.get(position);


            holder.tvtitle.setText(listMesaages.getTitle());
            holder.tvmsg.setText(listMesaages.getMsg());

            try {
                holder.date.setText("- "+dtfmt.format(defaultfmt.parse(listMesaages.getCreated_at())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(position >= unreadCount)
            {
                holder.tvtitle.setTextColor(Color.parseColor("#888888"));
                holder.readview.setBackgroundColor(Color.parseColor("#888888"));
            }

//            if (listMesaages.getRead().equalsIgnoreCase("1")) {
//                holder.tvtitle.setTextColor(Color.parseColor("#888888"));
//                holder.readview.setBackgroundColor(Color.parseColor("#888888"));
//            }

        }
    }

    int unreadCount = 0;

    class ViewHolderFilter extends RecyclerView.ViewHolder {

        TextView tvtitle, tvmsg,date;
        LinearLayout readview;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvtitle = (TextView) itemView.findViewById(R.id.title);
            tvmsg = (TextView) itemView.findViewById(R.id.message);
            date = (TextView) itemView.findViewById(R.id.date);
            readview = (LinearLayout) itemView.findViewById(R.id.readview);
            tvmsg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (msgList.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                      //  groupUpdateRead(msgList.get(getAdapterPosition()).getNotification_id());
                        //getNotifications();
                    } else {
                        Intent intent = new Intent(getActivity(), PollScreen.class);
                        intent.putExtra("pollid", msgList.get(getAdapterPosition()).getPollid());
                        intent.putExtra("from", "active");
                        intent.putExtra("by", "notifications");

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                        //groupUpdateRead(msgList.get(getAdapterPosition()).getNotification_id());
                    }
                }
            });
            tvtitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), msgList.get(getAdapterPosition()).getPollid(), Toast.LENGTH_SHORT).show();
                    if (msgList.get(getAdapterPosition()).getPollid().equalsIgnoreCase("0")) {
                       // groupUpdateRead(msgList.get(getAdapterPosition()).getNotification_id());
                       // getNotifications();
                    } else {
                        Intent intent = new Intent(getActivity(), PollScreen.class);
                        intent.putExtra("pollid", msgList.get(getAdapterPosition()).getPollid());
                        intent.putExtra("from", "active");
                        intent.putExtra("by", "notifications");

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                      //  groupUpdateRead(msgList.get(getAdapterPosition()).getNotification_id());
                    }
                }
            });
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        getNotifications();
    }

    public void groupUpdateRead(String notificationId) {

        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.submitReadNotificationPublic(notificationId, new Callback<SuccessPojo>() {

            @Override
            public void success(SuccessPojo successPojo, Response response) {
                //sucess
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getNotifications() {
        //  M.showLoadingDialog(getActivity());
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getNotifications(M.getID(getActivity()), new Callback<List<MessagesPojo>>() {
            @Override
            public void success(List<MessagesPojo> pojo, Response response) {
                if (pojo != null) {
                    if (pojo.size() > 0) {

                        msgList = pojo;
                        int serverCount = Integer.parseInt(msgList.get(msgList.size()-1).getTotalCount());
                        Log.i("Notif Server",serverCount+"");
                        Log.i("Notifi  Local",M.getNotificationCount(getActivity().getApplicationContext())+"");

                        if(M.getNotificationCount(getActivity().getApplicationContext())>0)
                        {

                            int localCount =  M.getNotificationCount(getActivity().getApplicationContext());

                             unreadCount = serverCount - localCount ;

                            if(unreadCount <= 0)
                                notificationCountTile.setText("You've no unread notifications");
                            else
                                notificationCountTile.setText("You've "+unreadCount+" unread notifications");
                        }
                        else{
                            notificationCountTile.setText("You've "+serverCount+" unread notifications");

                        }

                        M.setNotificationCount(getActivity().getApplicationContext(),(Integer.parseInt(msgList.get(msgList.size()-1).getTotalCount())));

                        rvMessages.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        //ArrayList<CategoryPojo>) pojo;
                        //lvcat.setAdapter(adapter);
                    } else {

                    }
                } else {

                }
                // M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                // M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }
}
