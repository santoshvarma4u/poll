package com.app.moodindia.ui.privatepolls.Groups;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.model.GroupPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.github.florent37.materialtextfield.MaterialTextField;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicCheckBox;
import com.ivankocijan.magicviews.views.MagicTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.List;

import id.zelory.compressor.Compressor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener {

    protected EditText etgroupname;
    protected EditText etgroupdescription;
    protected MagicTextView grp;
    protected CircleImageView ivGroupImage;
    protected MagicButton btnSubmitGroup;
    protected LinearLayout llt;
    protected ImageView btnBackNav;
    protected ImageView imageView4;
    protected MagicTextView tvGroupnameToolbar;
    protected ImageView icShare;
    protected MagicTextView grptype;
    protected MagicCheckBox cbBusiness;
    protected MagicCheckBox cbPersonal;
    protected MagicCheckBox cbPublic;
    protected String latitude = "0.0";
    protected String longitude = "0.0";
    protected String address = "No Address Added";
    protected MagicTextView tvAddress;
    protected LinearLayout lytAddress;
    protected EditText etaddress;
    protected MaterialTextField mtaddress;
    int PLACE_PICKER_REQUEST = 4353;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_create_group);
        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_group_image) {

            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CreateGroupActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
            } else {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_PICK);
                // Chooser of file system options.
                Intent openGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), 6654);
            }

        } else if (view.getId() == R.id.btn_submit_group) {
            if (typedFilea == null) {
                Toast.makeText(this, "Please select group image to upload", Toast.LENGTH_SHORT).show();
            } else if (etgroupname.getText().length() <= 1 && etgroupdescription.getText().length() <= 2) {
                Toast.makeText(this, "Please maintain proper name and description for your group", Toast.LENGTH_SHORT).show();
            } else {
                if ( isBusiness.equalsIgnoreCase("0") || tvAddress.getText().length() > 15) {
                    CreateGroup();
                } else {
                    Toast.makeText(this, "Please select exact address", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (view.getId() == R.id.btn_back_nav) {
            startActivity(new Intent(CreateGroupActivity.this, HomeActivity.class));
            finish();
        } else if (view.getId() == R.id.etaddress) {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            try {

                startActivityForResult(builder.build(CreateGroupActivity.this), PLACE_PICKER_REQUEST);

            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    Uri selectedImageUri = null;
    String optionImagePath = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 6654 && resultCode == Activity.RESULT_OK && null != data) {
            try {
                selectedImageUri = Uri.parse(getPath(data.getData()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                optionImagePath = cursor.getString(columnIndex);

                Picasso.with(getApplicationContext()).load(new File(optionImagePath))
                        .into(ivGroupImage);
                cursor.close();
            } else {
                Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
            }

            if (selectedImageUri != null) {
                generateImage();
            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                if (place.getLatLng().latitude == 0.0) {
                    Toast.makeText(CreateGroupActivity.this, "For Business polls address is mandatory", Toast.LENGTH_SHORT).show();
                    cbPublic.setChecked(false);
                    isBusiness = "0";
                    lytAddress.setVisibility(View.GONE);
                } else {
                    latitude = String.valueOf(place.getLatLng().latitude);
                    longitude = String.valueOf(place.getLatLng().longitude);
                    lytAddress.setVisibility(View.GONE);
                    address = place.getAddress().toString();
                    tvAddress.setText(address);
                    mtaddress.setVisibility(View.VISIBLE);
                    etaddress.setText(address);
                }
                String toastMsg = String.format("Place: %s", place.getName());
                //String.valueOf(place.getLatLng().latitude),String.valueOf(place.getLatLng().longitude);
                //update latitude and longitude
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String isBusiness = "0";

    private void initView() {
        etgroupname = (EditText) findViewById(R.id.etgroupname);
        etgroupdescription = (EditText) findViewById(R.id.etgroupdescription);
        grp = (MagicTextView) findViewById(R.id.grp);
        ivGroupImage = (CircleImageView) findViewById(R.id.iv_group_image);
        ivGroupImage.setOnClickListener(CreateGroupActivity.this);
        btnSubmitGroup = (MagicButton) findViewById(R.id.btn_submit_group);
        btnSubmitGroup.setOnClickListener(CreateGroupActivity.this);
        llt = (LinearLayout) findViewById(R.id.llt);
        btnBackNav = (ImageView) findViewById(R.id.btn_back_nav);
        btnBackNav.setOnClickListener(CreateGroupActivity.this);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        tvGroupnameToolbar = (MagicTextView) findViewById(R.id.tv_groupname_toolbar);
        icShare = (ImageView) findViewById(R.id.ic_share);
        grptype = (MagicTextView) findViewById(R.id.grptype);
        cbBusiness = (MagicCheckBox) findViewById(R.id.cb_business);
        cbPersonal = (MagicCheckBox) findViewById(R.id.cb_personal);
        cbBusiness.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    lytAddress.setVisibility(View.GONE);
                    cbPersonal.setChecked(false);
                    cbPublic.setChecked(false);

                    Toast.makeText(CreateGroupActivity.this, "Business group needs address", Toast.LENGTH_SHORT).show();
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                    try {

                        startActivityForResult(builder.build(CreateGroupActivity.this), PLACE_PICKER_REQUEST);

                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }

                    isBusiness = "1";
                } else {
                    isBusiness = "0";
                }
            }
        });
        cbPersonal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    lytAddress.setVisibility(View.GONE);
                    cbBusiness.setChecked(false);
                    cbPublic.setChecked(false);
                    isBusiness = "0";
                }

            }
        });
        cbPublic = (MagicCheckBox) findViewById(R.id.cb_public);
        cbPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cbPersonal.setChecked(false);
                    cbBusiness.setChecked(false);
                    isBusiness = "2";

                    Toast.makeText(CreateGroupActivity.this, "Public group needs your location", Toast.LENGTH_SHORT).show();
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                    try {

                        startActivityForResult(builder.build(CreateGroupActivity.this), PLACE_PICKER_REQUEST);

                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                } else {
                    isBusiness = "0";
                }
            }
        });
        tvAddress = (MagicTextView) findViewById(R.id.tvAddress);
        lytAddress = (LinearLayout) findViewById(R.id.lytAddress);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("from")) {
                if (getIntent().getExtras().get("from").toString().equalsIgnoreCase("business")) {
                    cbPersonal.setChecked(false);
                    cbBusiness.setChecked(true);
                    cbPublic.setChecked(false);

                    boolean b = true;
                    //
                    if (b) {
                        lytAddress.setVisibility(View.GONE);
                        cbPersonal.setChecked(false);
                        cbPublic.setChecked(false);

                        Toast.makeText(CreateGroupActivity.this, "Business group needs address", Toast.LENGTH_SHORT).show();
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                        try {

                            startActivityForResult(builder.build(CreateGroupActivity.this), PLACE_PICKER_REQUEST);

                        } catch (GooglePlayServicesRepairableException e) {
                            e.printStackTrace();
                        } catch (GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }

                        isBusiness = "1";
                    } else {
                        isBusiness = "0";
                    }
                } else if (getIntent().getExtras().get("from").toString().equalsIgnoreCase("public")) {
                    cbPersonal.setChecked(false);
                    cbBusiness.setChecked(false);
                    cbPublic.setChecked(true);

                    boolean b = true;
                    if (b) {
                        cbPersonal.setChecked(false);
                        cbBusiness.setChecked(false);
                        isBusiness = "2";

                        Toast.makeText(CreateGroupActivity.this, "Public group needs your location", Toast.LENGTH_SHORT).show();
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                        try {

                            startActivityForResult(builder.build(CreateGroupActivity.this), PLACE_PICKER_REQUEST);

                        } catch (GooglePlayServicesRepairableException e) {
                            e.printStackTrace();
                        } catch (GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }
                    } else {
                        isBusiness = "0";
                    }
                } else {
                    cbPersonal.setChecked(true);
                    cbBusiness.setChecked(false);
                    cbPublic.setChecked(false);
                }
            }
        }
        etaddress = (EditText) findViewById(R.id.etaddress);
        etaddress.setOnClickListener(CreateGroupActivity.this);
        mtaddress = (MaterialTextField) findViewById(R.id.mtaddress);
    }

    public void CreateGroup() {
        M.showLoadingDialog(CreateGroupActivity.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.createGroup(M.getID(CreateGroupActivity.this), etgroupname.getText().toString(), etgroupdescription.getText().toString(), isBusiness, latitude, longitude, address,
                typedFilea, new Callback<List<GroupPojo>>() {
                    @Override
                    public void success(List<GroupPojo> groupPojos, Response response) {
                        Toast.makeText(CreateGroupActivity.this, "Group Created Successfully", Toast.LENGTH_SHORT).show();
                        M.hideLoadingDialog();
                   /*     Intent it = new Intent(CreateGroupActivity.this, GroupInfoActivity.class);
                        it.putExtra("groupid", groupPojos.get(0).getGroupid());
                        finish();
                        startActivity(it); */

                        Intent it = new Intent(CreateGroupActivity.this, GroupSuccessActivity.class);
                        it.putExtra("groupid", groupPojos.get(0).getGroupid());
                        it.putExtra("groupname", groupPojos.get(0).getGroupname());
                        it.putExtra("uidforchat", groupPojos.get(0).getUidforchat());
                        finish();
                        startActivity(it);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(CreateGroupActivity.this, "Unable to create group try again", Toast.LENGTH_SHORT).show();
                        Intent it = new Intent(CreateGroupActivity.this, HomeActivity.class);
                        startActivity(it);
                        finish();
                    }
                });
    }

    TypedFile typedFilea = null;

    public void generateImage() {

        if (selectedImageUri != null) {
            typedFilea = makeFile(selectedImageUri.toString());

        } else
            typedFilea = null;

    }

    private String getPath(Uri uri) throws Exception {
        // this method will be used to get real path of Image chosen from gallery.
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private TypedFile makeFile(String uri) {

        File file = new File(uri);
        File compressedImage = null;
        try {
            Compressor cmp = new Compressor(this);
            cmp.setQuality(40);
            cmp.setMaxHeight(400);
            cmp.setMaxWidth(400);
            compressedImage = cmp.compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        TypedFile typedFile = new TypedFile("image/*", compressedImage);

        return typedFile;
    }


}
