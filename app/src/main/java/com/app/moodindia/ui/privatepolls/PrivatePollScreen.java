package com.app.moodindia.ui.privatepolls;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.app.moodindia.BrandsActivity;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.InfoActivity;
import com.app.moodindia.LoginActivity;
import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.ReportPollActivity;
import com.app.moodindia.SuggestPollActivity;
import com.app.moodindia.adapter.PrivateOptionAdapter;
import com.app.moodindia.chatmod.adapters.MainAdapter;
import com.app.moodindia.chatmod.models.Message;
import com.app.moodindia.chatmod.utils.ProfanityFilter;
import com.app.moodindia.chatmod.utils.SCUtils;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.SimpleGestureFilter;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import de.hdodenhof.circleimageview.CircleImageView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;
import xyz.hanks.library.SmallBang;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class PrivatePollScreen extends YouTubeBaseActivity implements  YouTubePlayer.OnInitializedListener,View.OnClickListener,SimpleGestureFilter.SimpleGestureListener,ConnectivityReceiver.ConnectivityReceiverListener {

    TextView tvque,tvdate,tvPollCount,txtExp,commentscount,catname;
    LinearLayout lloptions,lytmap;
    Button btnsubmit;
    //FrameLayout frameLayout;
    //ImageView ivpoll;
    ListView lvoption;
    Bitmap mbitmap;
    File  imagePath;
    String TAG="pollscreen",pollid="",userid="";
    String upollid;
    String info,que,start,end,type,img="",queid,seloptions="",totalPolls,twittertag,youtubetag,chattag;
    int single;
    Boolean isvoted;
    Button tvinfo;
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    ArrayList<String> idlist=new ArrayList<>();
    private Button btnViewMaps,tvaddparticipants;
    private Button btnSuggestPoll;
    MaterialDialog materialDialog;
    PrivateOptionAdapter adapter;
    ImageView ivpoll,navback,iv_share_poll,iv_report_poll;
    boolean isConnected;
    Button btnParticipatedUsers;

    public static final int ANTI_FLOOD_SECONDS = 3; //simple anti-flood
    private boolean IS_ADMIN = false; //set this to true for the admin app.
    String username = "anonymous"; //default username
    private boolean PROFANITY_FILTER_ACTIVE = true;
    private FirebaseDatabase database;
    private RecyclerView main_recycler_view;
    private PrivatePollScreen mContext;
    private MainAdapter madapter;
    private String userID;
    private DatabaseReference databaseRef;
    private ImageView imageButton_send,iv_chat_window_action;
    private EditText editText_message;
    ArrayList<Message> messageArrayList = new ArrayList<>();
    private ProgressBar progressBar;
    private long last_message_timestamp = 0;
    String nextpollid="0";
    String prevpollid="0";
    RelativeLayout navbtnlayout;
    SlidingUpPanelLayout sliding_layout;
    LinearLayout lytChatView;
    ProgressWheel progressWheelsending;
    private KenBurnsView kv_poll_background;
    public boolean isOwner=false;
    YouTubePlayerView youTubeView;
    private SimpleGestureFilter detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_poll_screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Fade().setDuration(500));
            getWindow().setExitTransition(new Fade().setDuration(500));
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // window.setStatusBarColor(ContextCompat.getColor(PrivatePollScreen.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        detector = new SimpleGestureFilter(this,this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getIntent().getExtras().containsKey("mainview"))
                {
                    Intent it=new Intent(PrivatePollScreen.this,HomeActivity.class);
                    it.putExtra("fragName","3");

                    startActivity(it);

                    overridePendingTransition(0,0);
                }
                else if(getIntent().getExtras().containsKey("groupview"))
                {
                    finish();
                }
                else {
                    Intent it=new Intent(PrivatePollScreen.this,HomeActivity.class);
                    it.putExtra("fragName","3");

                    startActivity(it);
                    //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }
              /*  Intent it=new Intent(PrivatePollScreen.this,PrivatePolls.class);
                startActivity(it);
                supportFinishAfterTransition();
                overridePendingTransition(0,0);*/
            }
        });





     /*   setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        navback=(ImageView)findViewById(R.id.navback);
        iv_share_poll=(ImageView)findViewById(R.id.iv_share_poll);
        iv_report_poll=(ImageView)findViewById(R.id.iv_report_poll);
        iv_share_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(PrivatePollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                        }
                        else {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                                View v1 = getWindow().getDecorView().getRootView();
                                v1.setDrawingCacheEnabled(true);
                                Bitmap myBitmap = Bitmap.createBitmap(v1.getDrawingCache());
                                saveBitmap(myBitmap);
                                v1.setDrawingCacheEnabled(false);
                            }
                            else {
                                sharepollkitkat();
                            }
                        }
                    }
                    else {
                        sharepollkitkat();
                    }
            }
        });

        iv_report_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(PrivatePollScreen.this,ReportPollActivity.class);
                intent.putExtra("pollid",upollid);
                startActivity(intent);
            }
        });
        navback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getExtras().containsKey("mainview"))
                {
                    Intent it=new Intent(PrivatePollScreen.this,HomeActivity.class);
                    it.putExtra("fragName","3");

                    startActivity(it);
                  //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                    finish();
                }
                else if(getIntent().getExtras().containsKey("groupview"))
                {
                    finish();
                }
                else {
                    Intent it=new Intent(PrivatePollScreen.this,HomeActivity.class);
                    it.putExtra("fragName","3");

                    startActivity(it);
                    //  supportFinishAfterTransition();
                    overridePendingTransition(0,0);
                }

               /* Intent it=new Intent(PrivatePollScreen.this,PrivatePolls.class);
                startActivity(it);
                supportFinishAfterTransition();
                overridePendingTransition(0,0);
                finish();*/
            }
        });




        mContext = PrivatePollScreen.this;
        username = M.getUsername(mContext);

        main_recycler_view = (RecyclerView) findViewById(R.id.main_recycler_view);
        imageButton_send = (ImageView) findViewById(R.id.imageButton_send);
        iv_chat_window_action = (ImageView) findViewById(R.id.iv_chat_window_action);
        progressWheelsending = (ProgressWheel) findViewById(R.id.progressWheelsending);
        editText_message = (EditText) findViewById(R.id.editText_message);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        database = FirebaseDatabase.getInstance();
        databaseRef = database.getReference();
        sliding_layout=(SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
        commentscount=(TextView) findViewById(R.id.commentscount);
        lytChatView=(LinearLayout)findViewById(R.id.lytChatView);
        kv_poll_background=(KenBurnsView)findViewById(R.id.kv_poll_background);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        main_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        madapter = new MainAdapter(mContext, messageArrayList);
        main_recycler_view.setAdapter(madapter);


        imageButton_send.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(editText_message.getText().toString().trim().length() > 0)
                        {
                            // M.showLoadingDialog(PollScreen.this);

                            new backTask().execute();
                            //
                        }
                    }
                });
            }
        });

        editText_message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_SEND)) {
                    imageButton_send.performClick();
                }
                return false;
            }
        });



        sliding_layout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);
                if(newState.toString().equalsIgnoreCase("EXPANDED"))
                {

                    iv_chat_window_action.setImageResource(R.drawable.ic_clear);
                }
                else if(newState.toString().equalsIgnoreCase("COLLAPSED"))
                {

                    iv_chat_window_action.setImageResource(R.drawable.ic_arrow_upward);
                }
            }
        });
        iv_chat_window_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sliding_layout.getPanelState().toString().equalsIgnoreCase("EXPANDED"))
                {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                else {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });


        pollid=getIntent().getExtras().getString("pollid");
        upollid=getIntent().getExtras().getString("upollid");
        catname=(TextView)findViewById(R.id.catname);
        //Log.d("poll",pollid);
        userid= M.getID(PrivatePollScreen.this);
        tvque=(TextView)findViewById(R.id.tvque);
        tvdate=(TextView)findViewById(R.id.tvdate);
        tvinfo=(Button)findViewById(R.id.tvinfo);
        txtExp=(TextView)findViewById(R.id.txtExp);
        tvPollCount=(TextView)findViewById(R.id.tvPollCount);
        lloptions=(LinearLayout)findViewById(R.id.lloption);
        btnsubmit=(Button)findViewById(R.id.btnsubmit);
        btnParticipatedUsers=(Button)findViewById(R.id.btnParticipatedUsers);
        btnParticipatedUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnsubmit.setVisibility(View.GONE);
       /* frameLayout=(FrameLayout)findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);*/
        btnSuggestPoll = (Button) findViewById(R.id.btnSuggestPoll);
        ivpoll=(ImageView) findViewById(R.id.ivpoll);
        // ivpoll.pause();
        lvoption=(ListView)findViewById(R.id.lvotedoption);
        // kvpoll=(KenBurnsView)findViewById(R.id.kvpoll);
        lvoption.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lytmap=(LinearLayout)findViewById(R.id.lytmap);
        setListViewHeightBasedOnChildren(lvoption);
        // getPollData();
        btnsubmit.setOnClickListener(this);
        tvinfo.setOnClickListener(this);
        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        tvaddparticipants = (Button) findViewById(R.id.tvaddparticipants);
        btnViewMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PrivatePollScreen.this,PrivateResultMapActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("from",getIntent().getExtras().getString("from"));
                intent.putExtra("upollid",upollid);
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PrivatePollScreen.this,SuggestPollActivity.class);
                startActivity(intent);
            }
        });

        checkConnection();

        if(isConnected)
        {
            if(getIntent().getExtras().containsKey("from"))
                getPollData(getIntent().getExtras().getString("from"));
        }

        tvaddparticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PrivatePollScreen.this,ParticipatedUserActivity.class);
                intent.putExtra("pollid",pollid);
                intent.putExtra("ques",que);
                intent.putExtra("postedtime",start);
                intent.putExtra("pollimage",img);
                intent.putExtra("mainview","mainview");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onSwipe(int direction) {
        switch (direction) {
            case  SimpleGestureFilter.SWIPE_RIGHT : nextPoll();
                break;
            case SimpleGestureFilter.SWIPE_LEFT : prevPoll();
                break;
        }
    }

    @Override
    public void onDoubleTap() {
    }
    public void nextPoll()
    {
        if(getIntent().getExtras().containsKey("searchquery"))
        {
            //swipe disbaled for search results
        }
        else
        {
            if(nextpollid.equalsIgnoreCase("0"))
            {
                Toast.makeText(PrivatePollScreen.this, "No more polls found in this group.", Toast.LENGTH_SHORT).show();
            }
            else {
                // Toast.makeText(this, "Next Poll", Toast.LENGTH_SHORT).show();
                Intent intent1=new Intent(PrivatePollScreen.this,PrivatePollScreen.class);
                intent1.putExtra("from","active");
                intent1.putExtra("pollid",nextpollid);
                intent1.putExtra("upollid",nextpollid);
                finish();
                startActivity(intent1);
                this.overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);

            }
        }
    }
    public void prevPoll()
    {
        if(getIntent().getExtras().containsKey("searchquery"))
        {
            //swipe disbaled for search results
        }
        else
        {
            if(prevpollid.toLowerCase().equalsIgnoreCase("0"))
            {
                Toast.makeText(PrivatePollScreen.this, "No more polls found in this group.", Toast.LENGTH_SHORT).show();
            }
            else {
                //  Toast.makeText(this, "Prev Poll", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(PrivatePollScreen.this,PrivatePollScreen.class);
                intent.putExtra("from","active");
                intent.putExtra("pollid",prevpollid);
                intent.putExtra("upollid",prevpollid);
                finish();
                startActivity(intent);
                this.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        }
    }

    private class backTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {
            process_new_message(editText_message.getText().toString().trim(), false);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressWheelsending.setVisibility(View.VISIBLE);
            imageButton_send.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            editText_message.setText("");
            progressWheelsending.setVisibility(View.GONE);
            imageButton_send.setVisibility(View.VISIBLE);
        }
    }

    public void makeChatReady(String roomid, final String Pollusername)
    {

       // Log.i("roomid",roomid);
        databaseRef.child(roomid).addChildEventListener(new ChildEventListener() {
            @Override public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //  M.hideLoadingDialog();
                progressWheelsending.setVisibility(View.GONE);
                imageButton_send.setVisibility(View.VISIBLE);
                Message new_message = dataSnapshot.getValue(Message.class);
                messageArrayList.add(new_message);
                madapter.notifyDataSetChanged();
                main_recycler_view.scrollToPosition(madapter.getItemCount() - 1);
                commentscount.setText("Comments ("+messageArrayList.size()+")");
            }

            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                progressWheelsending.setVisibility(View.GONE);
                imageButton_send.setVisibility(View.VISIBLE);
            }

            @Override public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("REMOVED", dataSnapshot.getValue(Message.class).toString());
                messageArrayList.remove(dataSnapshot.getValue(Message.class));
                madapter.notifyDataSetChanged();

                progressWheelsending.setVisibility(View.GONE);
                imageButton_send.setVisibility(View.VISIBLE);
            }

            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                progressWheelsending.setVisibility(View.GONE);
                imageButton_send.setVisibility(View.VISIBLE);
            }

            @Override public void onCancelled(DatabaseError databaseError) {

                progressWheelsending.setVisibility(View.GONE);
                imageButton_send.setVisibility(View.VISIBLE);
            }
        });

        userID = SCUtils.getUniqueID(getApplicationContext());

        databaseRef.child("users").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {

                    String new_username = M.getUsername(mContext);
               /*   if ((!new_username.equals(username)) && (!username.equals("anonymous"))) {
                      process_new_message(username + " changed it's nickname to " + new_username, true);
                  }*/
                    username = new_username;
                    databaseRef.child("users").child(userID).setValue(Pollusername);
                } else {
                    username = dataSnapshot.getValue(String.class);
                    //  Snackbar.make(findViewById(android.R.id.content), "Logged in as " + username, Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override public void onCancelled(DatabaseError databaseError) {
                Log.w("!!!", "username:onCancelled", databaseError.toException());
            }
        });
    }

    private void process_new_message(String new_message, boolean isNotification) {
        if (new_message.isEmpty()) {
            return;
        }



        //simple anti-flood protection
       /* if ((System.currentTimeMillis() / 1000L - last_message_timestamp) < ANTI_FLOOD_SECONDS) {
            SCUtils.showErrorSnackBar(mContext, findViewById(android.R.id.content), "You cannot send messages so fast.").show();
            return;
        }
*/
        //yes, admins can swear ;)
        if ((PROFANITY_FILTER_ACTIVE) && (!IS_ADMIN)) {
            new_message = new_message.replaceAll(ProfanityFilter.censorWords(ProfanityFilter.ENGLISH), ":)");
        }



        Message xmessage = new Message(userID, M.getUsername(mContext), new_message, System.currentTimeMillis() / 1000L, IS_ADMIN, isNotification);
        String key = databaseRef.child(upollid).push().getKey();
        databaseRef.child(upollid).child(key).setValue(xmessage);

        last_message_timestamp = System.currentTimeMillis() / 1000L;
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public void showParticipatedUsers(){

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void realtimeLoading()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds

                if(getIntent().getExtras().containsKey("from"))
                {
                    if(getIntent().getExtras().getString("from").equalsIgnoreCase("active"))
                    {
                        getRealTimePollData();
                        handler.postDelayed(this, 10000);
                    }
                }
                //  getPollData(getIntent().getExtras().getString("from"));

            }
        }, 500);
    }

    boolean isVoteLocal=false;

    private SmallBang mSmallBang;
    private String youtubeLink="";
    private String createdUserId="";
    private void getPollData(String active) {
        // AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
        M.showLoadingDialog(PrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOption(userid,pollid,active,new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {

                    if (pollpojo.size() > 0) {
                        for(final PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            tvinfo.setEnabled(true);
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            fromServerUserid=pojo.getCreateuserid();
                            nextpollid=pojo.getNextid();
                            prevpollid=pojo.getPrevid();
                            createdUserId=pojo.getCreateuserid();


                            if(pojo.getYoutubelink().length()>5)
                            {
                                youTubeView.initialize(AppConst.YOUTUBE_API_KEY, PrivatePollScreen.this);
                                youtubeLink=pojo.getYoutubelink().toString().substring(pojo.getYoutubelink().toString().lastIndexOf("=") + 1);
                                Log.d("Youutube",youtubeLink);
                                // =pojo.getYoutubelink().toString();
                            }
                            else
                            {
                                youTubeView.setVisibility(View.GONE);
                                ivpoll.setVisibility(View.VISIBLE);
                            }

                            if(pojo.getGroupid().equalsIgnoreCase("0"))
                            {
                                //do nothing
                            }
                            else{
                                iv_share_poll.setVisibility(View.GONE);
                            }

                            if(M.getID(PrivatePollScreen.this).equalsIgnoreCase(pojo.getCreateuserid()))
                            {
                                isOwner=true;
                                //update UI enable copy poll option
                                //item.setVisible(true);
                            }
                            else
                            {
                               // item.setVisible(false);
                                isOwner=false;
                            }

                            if(pojo.getAnonymous().equalsIgnoreCase("1"))
                                isAnon=true;


                            makeChatReady(upollid,username);

                          if(isvoted)
                            {

                                btnViewMaps.setVisibility(View.VISIBLE);
                                //   frameLayout.setVisibility(View.VISIBLE);

                                if(getIntent().getExtras().containsKey("stats")){
                                    tvinfo.performClick();
                                }
                                else
                                {
                                    tvque.setBackgroundColor(Color.parseColor("#e3e9ef"));
                                    tvque.getBackground().setAlpha(51);
                                    tvque.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    //  tvque.setBackgroundColor(getResources().getColor(R.color.light_green));
                                    realtimeLoading();
                                }


                            }
                            else if(isOwner)
                            {
                                btnViewMaps.setVisibility(View.VISIBLE);
                                tvaddparticipants.setVisibility(View.VISIBLE);

                            }
                            else {
                              btnViewMaps.setVisibility(View.GONE);
                          }


                            tvque.setText(que);
                            if (img != "") {

                                Picasso.with(PrivatePollScreen.this)
                                        .load(AppConst.imgurl + img)
                                        .placeholder(R.drawable.default_image)
                                        .error(R.drawable.default_image)
                                        .into(ivpoll);

                                Handler uiHandler = new Handler(Looper.getMainLooper());
                                uiHandler.post(new Runnable(){
                                    @Override
                                    public void run() {
                                        try {
                                            Picasso.with(PrivatePollScreen.this).load(AppConst.imgurl+img).into(new Target() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                                    Bitmap afterBlur=fastblur(bitmap,1,20);
                                                    kv_poll_background.setImageBitmap(afterBlur);

                                                }

                                                @Override
                                                public void onBitmapFailed(Drawable errorDrawable) {

                                                }

                                                @Override
                                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                                }
                                            });




                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }


                            try {
                                tvdate.setText("Posted on "+dtfmt.format(defaultfmt.parse(start)));
                                Date dt=defaultfmt.parse(end);
                                // txtExp.setVisibility(View.VISIBLE);
                                RelativeTimeTextView v = (RelativeTimeTextView)findViewById(R.id.timestamp);
                                v.setReferenceTime(dt.getTime());


                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if(!isvoted) {
                                // getBrandDialog();
                                for (final Pollresult data : pojo.getPollresult()) {

                                    final int id = data.getOptionid();

                                    LayoutInflater factory = LayoutInflater.from(PrivatePollScreen.this);
                                    View myView = factory.inflate(R.layout.view_option_row, null);

                                    final TextView tv_poll_question=(TextView)myView.findViewById(R.id.tvoption);
                                    CircleImageView iv_option=(CircleImageView)myView.findViewById(R.id.option_image);


                                    tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv_poll_question.setBackgroundResource(R.drawable.button_rounded_corner);
                                    tv_poll_question.setText(data.getOptionvalue());

                                    if(data.getOptionimage().length()>0)
                                    {
                                        if(data.getOptionimage().equalsIgnoreCase("NO"))
                                        {
                                            iv_option.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            Picasso.with(PrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .resize(40,40)
                                                    .into(iv_option);
                                        }

                                    }
                                    else
                                    {
                                        iv_option.setVisibility(View.GONE);
                                    }


                                    iv_option.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Dialog dialog=new Dialog(PrivatePollScreen.this);
                                            dialog.setContentView(R.layout.custom_image_dialog);
                                            ImageView iv_option_image=(ImageView)dialog.findViewById(R.id.iv_full_option_image);
                                            Picasso.with(PrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .into(iv_option_image);
                                            dialog.show();
                                        }
                                    });


                                    tv_poll_question.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(PrivatePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PrivatePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


                                    lloptions.setOrientation(LinearLayout.VERTICAL);
                                    lloptions.addView(myView, layoutParams);
                                    //  lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);

                                adapter = new PrivateOptionAdapter(PrivatePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);

                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }

 new CountDownTimer(4000,1000) {

                                @Override
                                public void onFinish() {
                                    M.hideLoadingDialog();
                                    if(getIntent().getExtras().containsKey("forshare"))
                                    {

                                        if(pojo.getGroupid().equalsIgnoreCase("0"))
                                        {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                                                    ActivityCompat.requestPermissions(PrivatePollScreen.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                                                }
                                                else {

                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                                                        View v1 = getWindow().getDecorView().getRootView();
                                                        v1.setDrawingCacheEnabled(true);
                                                        Bitmap myBitmap = v1.getDrawingCache();
                                                        saveBitmap(myBitmap);
                                                    }
                                                    else {
                                                        sharepollkitkat();
                                                    }
                                                }
                                            }
                                            else {
                                                sharepollkitkat();
                                            }
                                        }

                                    }
                                }
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }
                            }.start();



                        }
                    }
                }


               // supportStartPostponedEnterTransition();

                // ivpoll.resume();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    public String fromServerUserid="";
   private void getRealTimePollData() {
        //AppConst.selpoll=null;
        //Log.d(TAG,"userid"+userid+"-"+pollid);
        // M.showLoadingDialog(PrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOption(userid,pollid,"active",new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if(pollpojo!=null) {
                    if (pollpojo.size() > 0) {
                        for(PollOptionPojo pojo:pollpojo) {
                            AppConst.selpoll=pojo;
                            queid=pojo.getPollid()+"";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            //Log.d("info",info);
                            start = pojo.getStarttime();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions=pojo.getSelected_option();
                            totalPolls=pojo.getPoll_counts();
                            tvPollCount.setText(totalPolls+" Participant(s)");
                            fromServerUserid=pojo.getCreateuserid();
                            if(pojo.getAnonymous().equalsIgnoreCase("1"))
                                isAnon=true;



                            if(!isvoted) {


                               // btnViewMaps.setVisibility(View.GONE);

                                for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();

                                    LayoutInflater factory = LayoutInflater.from(PrivatePollScreen.this);
                                    View myView = factory.inflate(R.layout.view_option_row, null);

                                    final TextView tv_poll_question=(TextView)myView.findViewById(R.id.tvoption);
                                    final CircleImageView iv_option=(CircleImageView)myView.findViewById(R.id.option_image);


                                    tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv_poll_question.setBackgroundResource(R.drawable.button_rounded_corner);
                                    tv_poll_question.setText(data.getOptionvalue());

                                  /*  final TextView tv = new TextView(PrivatePollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(18);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    final CircularImageView iv_option=new CircularImageView(PrivatePollScreen.this);*/
                                /*    iv_option.setMaxWidth(40);
                                    iv_option.setMaxHeight(40);*/

                                    if(data.getOptionimage().length()>0)
                                    {
                                        if(data.getOptionimage().equalsIgnoreCase("NO"))
                                        {
                                            iv_option.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            Picasso.with(PrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .resize(40,40)
                                                    .into(iv_option);
                                        }

                                    }

                                    iv_option.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Dialog dialog=new Dialog(PrivatePollScreen.this);
                                            dialog.setContentView(R.layout.custom_image_dialog);
                                            ImageView iv_option_image=(ImageView)dialog.findViewById(R.id.iv_full_option_image);
                                            Picasso.with(PrivatePollScreen.this)
                                                    .load(AppConst.imgurl + data.getOptionimage())
                                                    .placeholder(R.drawable.default_image)
                                                    .error(R.drawable.default_image)
                                                    .into(iv_option_image);
                                            dialog.show();
                                        }
                                    });

                                    tv_poll_question.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(PrivatePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv_poll_question.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(PrivatePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                    lloptions.setOrientation(LinearLayout.VERTICAL);

                                   // lytmap.setVisibility(View.GONE);
                                }
                            }else{
                                btnViewMaps.setVisibility(View.VISIBLE);
                                //  frameLayout.setVisibility(View.VISIBLE);
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                PrivateOptionAdapter adapter = new PrivateOptionAdapter(PrivatePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
                // M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    private static final int RECOVERY_REQUEST = 1;
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.loadVideo(youtubeLink);
        }
        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

                //youTubePlayer.pause();
            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {

            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        });
        youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
            @Override
            public void onPlaying() {

                //  navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onPaused() {

                //navbtnlayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStopped() {
               // navbtnlayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onBuffering(boolean b) {

                //navbtnlayout.setVisibility(View.GONE);
            }

            @Override
            public void onSeekTo(int i) {

                //navbtnlayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
           /* String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();*/
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(AppConst.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }


    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnsubmit){
            String ans="";
            for(int i=0;i<idlist.size();i++){
                if(i==0)
                    ans=idlist.get(i);
                else
                    ans=ans+","+idlist.get(i);
            }
            submitresult(ans);
        }else if(view.getId()==R.id.tvinfo){
            Intent it=new Intent(PrivatePollScreen.this,InfoActivity.class);
            it.putExtra("info",info);
            it.putExtra("question",que);
            it.putExtra("image",img);
            it.putExtra("screen","pollscreen");
            it.putExtra("twittertag",twittertag);
            it.putExtra("youtubetag",youtubetag);
            it.putExtra("chattag",chattag);
            it.putExtra("startdate",start);
            it.putExtra("enddate",end);
            it.putExtra("isvoted",isvoted);
            it.putExtra("from",getIntent().getExtras().getString("from"));
            it.putExtra("pollid",pollid);

            startActivity(it);
        }
    }

    public void submitresult(final String ans){
        //Log.d(TAG,"ans:"+ans);
        M.showLoadingDialog(PrivatePollScreen.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.submitPrivateAns(queid,ans,M.getID(PrivatePollScreen.this),new retrofit.Callback<SubmitAnsPojo>() {
            @Override
            public void success(SubmitAnsPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess()==1){
                        // AppConst.selpoll=null;
                        PollOptionPojo obj=new PollOptionPojo();
                        obj.setEndtime(end);
                        obj.setImage(img);
                        obj.setInfo(info);
                        obj.setIsvoted(true);
                        obj.setSelected_option(ans);
                        obj.setPollid(Integer.valueOf(queid));
                        obj.setPollresult(pojo.getPollresult());
                        obj.setQuestion(que);
                        obj.setStarttime(start);
                        obj.setType(type);
                        AppConst.selpoll=obj;
                        sendPushToOtherParticipants(queid,M.getID(PrivatePollScreen.this));
                   }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    Menu mMenu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu=menu;
        getMenuInflater().inflate(R.menu.menu_private_poll,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home)
        {

        }


            /*Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            //this is the text that will be shared
           // sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));*/


        else if(id==R.id.report){
            Intent intent=new Intent(PrivatePollScreen.this,ReportPollActivity.class);
            intent.putExtra("pollid",pollid);
            startActivity(intent);
        }
        else if(id==R.id.copy)
        {
            Intent intent=new Intent(PrivatePollScreen.this,AddPollActivity.class);
            intent.putExtra("pollid",pollid);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(PrivatePollScreen.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }
    public boolean isAnon=false;


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getIntent().getExtras().containsKey("mainview"))
        {
            Intent it=new Intent(this,HomeActivity.class);
            it.putExtra("fragName","3");

            startActivity(it);
          //  supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else if(getIntent().getExtras().containsKey("groupview"))
        {
           finish();
        }
        else {
            Intent it=new Intent(this,HomeActivity.class);
            it.putExtra("fragName","3");

            startActivity(it);
            //  supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }



       /* if(getIntent().getExtras().containsKey("fragName"))
        {
            Intent it=new Intent(this,PrivatePolls.class);
            it.putExtra("fragName",getIntent().getExtras().get("fragName").toString());
            startActivity(it);
            supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }
        else
        {
            Intent it=new Intent(this,PrivatePolls.class);
            startActivity(it);
            supportFinishAfterTransition();
            overridePendingTransition(0,0);
        }*/
    }


    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }
    public void sharepoll(String path) {
        if(isAnon)
        {
            Toast.makeText(PrivatePollScreen.this, "You can not share anonymous polls", Toast.LENGTH_SHORT).show();
            /*if(fromServerUserid.equalsIgnoreCase(M.getID(PrivatePollScreen.this)))
            {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("image/gif");
                Uri myUri = Uri.parse("file://" + path);
                sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
                sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
                sendIntent.putExtra(Intent.EXTRA_TEXT, ("New Private Poll is ready for our opinions " +
                        "Please participate and express your opinion.  \n"+que+" \n Poll ID:"+upollid+"  \n http://moodindia.in/privatepoll/"+upollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
            }
            else {
                Toast.makeText(PrivatePollScreen.this, "You can not share anonymous polls", Toast.LENGTH_SHORT).show();
            }*/
        }
        else {
            if(isAnon){
                Toast.makeText(PrivatePollScreen.this, "You can not share anonymous polls", Toast.LENGTH_SHORT).show();
               /* if(fromServerUserid.equalsIgnoreCase(M.getID(PrivatePollScreen.this)))
                {
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("image/gif");
                    Uri myUri = Uri.parse("file://" + path);
                    sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, ("New Private Poll is ready for our opinions " +
                            "Please participate and express your opinion.  \n"+que+" \n Poll ID:"+upollid+"  \n http://moodindia.in/privatepoll/"+upollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
                    startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
                }
                else {
                    Toast.makeText(PrivatePollScreen.this, "You can not share anonymous polls", Toast.LENGTH_SHORT).show();
                }*/
            }
            else {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("image/gif");
                Uri myUri = Uri.parse("file://" + path);
                sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
                sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
                sendIntent.putExtra(Intent.EXTRA_TEXT, ("New Private Poll is ready for our opinions " +
                        "Please participate and express your opinion.  \n"+que+" \n Poll ID:"+upollid+"  \n http://moodindia.in/privatepoll/"+upollid+ " \n \n Download Mood India \n https://play.google.com/store/apps/details?id=com.app.moodindia"));
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
            }

        }


    }
    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n Poll Id: "+upollid+" \n http://moodindia.in/privatepoll/"+upollid));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }


    public void getBrandDialog()
    {
        Intent intent=new Intent(PrivatePollScreen.this,BrandsActivity.class);
        startActivityForResult(intent,1);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }

    }

    public void sendPushToOtherParticipants(String privatepollid,String userid) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(ConsantURL+"app/Http/Controllers/sendPrivateNotifications.php?submitanswer=true&privatepollid="+privatepollid+"&userid="+userid).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {

                if (response.isSuccessful()) {

                    M.hideLoadingDialog();
                    Intent it=new Intent(PrivatePollScreen.this,PrivatePollScreen.class);
                    it.putExtra("pollid",pollid);
                    it.putExtra("upollid",upollid);
                    it.putExtra("from","active");
                    it.putExtra("mainview","mainview");
                    finish();
                    startActivity(it);
                }
            }
        });
    }

    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius){

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) { return (null); } int w = bitmap.getWidth(); int h = bitmap.getHeight(); int[] pix = new int[w * h]; Log.e("pix", w + " " + h + " " + pix.length); bitmap.getPixels(pix, 0, w, 0, 0, w, h); int wm = w - 1; int hm = h - 1; int wh = w * h; int div = radius + radius + 1; int r[] = new int[wh]; int g[] = new int[wh]; int b[] = new int[wh]; int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; int vmin[] = new int[Math.max(w, h)]; int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) { p = pix[yi + Math.min(wm, Math.max(i, 0))]; sir = stack[i + radius]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) { r[yi] = dv[rsum]; g[yi] = dv[gsum]; b[yi] = dv[bsum]; rsum -= routsum; gsum -= goutsum; bsum -= boutsum; stackstart = stackpointer - radius + div; sir = stack[stackstart % div]; routsum -= sir[0]; goutsum -= sir[1]; boutsum -= sir[2]; if (y == 0) { vmin[x] = Math.min(x + radius + 1, wm); } p = pix[yw + vmin[x]]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) { yi = Math.max(0, yp) + x; sir = stack[i + radius]; sir[0] = r[yi]; sir[1] = g[yi]; sir[2] = b[yi]; rbs = r1 - Math.abs(i); rsum += r[yi] * rbs; gsum += g[yi] * rbs; bsum += b[yi] * rbs; if (i > 0) {
                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];
            } else {
                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];
            }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }


    public void checkGroupofPoll()
    {

    }

}
