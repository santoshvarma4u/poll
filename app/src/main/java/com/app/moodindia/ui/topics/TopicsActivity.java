package com.app.moodindia.ui.topics;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.app.moodindia.CurrentPolls;
import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.UserCategories;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TopicsActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout mTabLayout;
    private ImageView mSearchView,btn_back_nav;
    private CardView cv_search;
    private ChipCloud chipCloud;

    public boolean doEvent=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        mTabLayout=(TabLayout)findViewById(R.id.tablayout);
        mSearchView=(ImageView)findViewById(R.id.iv_search);
        cv_search=(CardView)findViewById(R.id.cv_search);
        btn_back_nav=(ImageView)findViewById(R.id.btn_back_nav);
        chipCloud = (ChipCloud) findViewById(R.id.chip_cloud);

        btn_back_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save favs and go to home activity
                getUserFavsString();

            }
        });
        chipCloud.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {

                Intent intent=new Intent(TopicsActivity.this,CurrentPolls.class);
                intent.putExtra("act_name","topics");
                intent.putExtra("catid",mPopularCategories.get(i).getCat_id());
                intent.putExtra("catname",mPopularCategories.get(i).getCat_name());
                startActivity(intent);
               // updateUserFollowing(mPopularCategories.get(i).getCat_id(),"1");
            }

            @Override
            public void chipDeselected(int i) {
                //updateUserFollowing(mPopularCategories.get(i).getCat_id(),"0");
                Intent intent=new Intent(TopicsActivity.this,CurrentPolls.class);
                intent.putExtra("act_name","topics");
                intent.putExtra("catid",mPopularCategories.get(i).getCat_id());
                intent.putExtra("catname",mPopularCategories.get(i).getCat_name());
                startActivity(intent);
            }
        });

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TopicsActivity.this,SearchTopicsActivity.class));
            }
        });

        cv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TopicsActivity.this,SearchTopicsActivity.class));
            }
        });

        setupViewPager(viewPager);
        mTabLayout.setupWithViewPager(viewPager);
        mCategoryRealmPojos=new ArrayList<>();
        mPopularCategories=new ArrayList<>();
        getCategoryData();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SelectTopics(), " All Topics");
     /*   adapter.addFragment(new Hashtags(), " Hash Tags");*/
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    List<CategoryRealmPojo> mCategoryRealmPojos;
    List<CategoryRealmPojo> mPopularCategories;

    public void getCategoryData()
    {
        CategoryAPI mAuthenticationAPI = APIService.createService(CategoryAPI.class);
        mAuthenticationAPI.getCategories("", new Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(List<CategoryRealmPojo> categoryRealmPojos, Response response) {

                for(CategoryRealmPojo mCatPojo : categoryRealmPojos)
                {
                    if(mCatPojo.getPopular().equalsIgnoreCase("1"))
                    {
                        chipCloud.addChip(mCatPojo.getCat_name());
                        mPopularCategories.add(mCatPojo);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(doEvent)
         getUserFavsString();
    }

    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    private FragmentRefreshListener fragmentRefreshListener;
    public interface FragmentRefreshListener{
        void onRefresh();
    }

    public void updateUserFollowing(String catid,String status)
    {
        M.showLoadingDialog(TopicsActivity.this);
        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.updateUserFollowing(M.getID(TopicsActivity.this), catid, status, new Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                //userupdated
                if(getFragmentRefreshListener()!=null){
                    getFragmentRefreshListener().onRefresh();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                if(getFragmentRefreshListener()!=null){
                    getFragmentRefreshListener().onRefresh();
                }
            }
        });
    }
    public void getUserFavsString()
    {
        doEvent=false;
        M.showLoadingDialog(TopicsActivity.this);

        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.getUserCatgories(M.getID(TopicsActivity.this), new retrofit.Callback<List<UserCategories>>() {

            @Override
            public void success(List<UserCategories> userCategories, Response response) {

                doEvent=true;
                if(userCategories.size() > 0)
                {
                    if(  userCategories.get(userCategories.size()-1).getUsercategories().length() > 0)
                    {
                      //  Toast.makeText(TopicsActivity.this, userCategories.get(userCategories.size()-1).getUsercategories(), Toast.LENGTH_SHORT).show();
                        M.setFavs(userCategories.get(userCategories.size()-1).getUsercategories(),TopicsActivity.this);
                        startActivity(new Intent(TopicsActivity.this,HomeActivity.class));
                        finish();
                        M.hideLoadingDialog();
                    }
                    else
                    {

                        Toast.makeText(TopicsActivity.this, "Please select some topics to explore", Toast.LENGTH_SHORT).show();
                        M.hideLoadingDialog();
                    }
                }
                else
                {
                    Toast.makeText(TopicsActivity.this, "Please select some topics to explore", Toast.LENGTH_SHORT).show();
                    M.hideLoadingDialog();
                }

            }
            @Override
            public void failure(RetrofitError error) {
                doEvent=true;
                Toast.makeText(TopicsActivity.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();
            }
        });
    }

}
