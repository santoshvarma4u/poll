package com.app.moodindia.ui.privatepolls;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.SettingActivity;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PrivatePolls extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_polls);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Community Polls");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
       // mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        setupViewPager(mViewPager);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(PrivatePolls.this,AddPollActivity.class));
              /*  Snackbar.make(view, "Add a private poll", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        Button btn_join_poll=(Button)findViewById(R.id.btn_join_poll);


        final DialogPlus dialog = DialogPlus.newDialog(PrivatePolls.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_join_poll))
                .setCancelable(true)
                .create();


        View view=dialog.getHolderView();

        final EditText et_join_poll=(EditText)view.findViewById(R.id.et_join_poll_id);
        final TextView tvJoinDialog=(TextView) view.findViewById(R.id.tvJoinDialog);
        final TextView tvCancelJoinDialog=(TextView)view.findViewById(R.id.tvCancelJoinDialog);

        et_join_poll.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                }
                return false;
            }
        });
        et_join_poll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
            }
        });

        tvJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_join_poll.getText().toString().length() > 0)
                {
                    if(!M.getID(PrivatePolls.this).isEmpty())
                    {
                        validateJoinPoll(et_join_poll.getText().toString(),M.getID(PrivatePolls.this).toString());
                    }
                    else {
                        Toast.makeText(PrivatePolls.this, "You need to login again, for join in personal polls!", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    et_join_poll.setError("Enter a valid personal poll id");
                }
            }
        });

        tvCancelJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing())
                    dialog.dismiss();

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
            }
        });


        btn_join_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.show();
                /*new LovelyTextInputDialog(PrivatePolls.this, R.style.EditTextTintTheme)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle("Mood India")
                        .setMessage("Please Enter Poll Id")
                        //  .setIcon(R.drawable.miwhitesmall)
                        .setInputFilter("Enter valid poll Id", new LovelyTextInputDialog.TextFilter() {
                            @Override
                            public boolean check(String text) {
                                return text.matches("\\w+");
                            }
                        })
                        .setInputType(InputType.TYPE_CLASS_NUMBER)
                        .setConfirmButton("Confirm", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                            @Override
                            public void onTextInputConfirmed(String text) {

                                //  Toast.makeText(HomeActivity.this, text, Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(PrivatePolls.this, AcceptPollActivity.class);
                                intent.putExtra("poll_id",text);
                                startActivity(intent);
                               // finish();
                            }
                        })
                        .show();*/
            }
        });
    }



    private void validateJoinPoll(final String pollid, String userid)
    {
        M.showLoadingDialog(PrivatePolls.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getAcceptPoll(userid, pollid, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojo, Response response) {
                if(statusPojo.get(0).getStatus().equalsIgnoreCase("A"))
                {
                    M.hideLoadingDialog();
                    //Poll Accepted
                    //Redirect to poll screen
                    Toast.makeText(PrivatePolls.this, "Poll already accepted", Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(PrivatePolls.this,PrivatePollScreen.class);
                    intent.putExtra("from","active");
                    intent.putExtra("pollid",pollid);
                    startActivity(intent);
                }
                else if(statusPojo.get(0).getStatus().equalsIgnoreCase("NA"))
                {
                    M.hideLoadingDialog();
                    getPollDetails(pollid);
                }
                else {
                    M.hideLoadingDialog();
                    Toast.makeText(PrivatePolls.this, "Invalid Poll ID, Try Correct Poll id", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(PrivatePolls.this, "Unknown Error,Please try later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getPollDetails(final String pollid)
    {
        M.showLoadingDialog(PrivatePolls.this);
        final PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getPrivatePollById(pollid, 1, new Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> privatePollPojos, Response response) {

                if(privatePollPojos.size()>0)
                {
                    M.hideLoadingDialog();

                    Intent intent=new Intent(PrivatePolls.this, AcceptPollActivity.class);
                    intent.putExtra("poll_id",pollid);
                    startActivity(intent);

                }
                else
                {
                    M.hideLoadingDialog();
                    Toast.makeText(PrivatePolls.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(PrivatePolls.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
     //   adapter.addFragment(new MyPrivatePolls(), " My Polls");
        //adapter.addFragment(new PollsFragment(), " Tranding Polls");
        // adapter.addFragment(new PollsFragment(), " Answered Pools");
        adapter.addFragment(new ParticipatedPrivatePolls(), " Participated");
        adapter.addFragment(new MyEndPolls(), "Closed");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


      /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_private_polls, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PrivatePolls.this, SettingActivity.class));
        finish();
    }
}
