package com.app.moodindia.ui.publicpolls;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.moodindia.LoginActivity;
import com.app.moodindia.R;
import com.thefinestartist.finestwebview.FinestWebView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JoinUs#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JoinUs extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public JoinUs() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JoinUs.
     */
    // TODO: Rename and change types and number of parameters
    public static JoinUs newInstance(String param1, String param2) {
        JoinUs fragment = new JoinUs();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_join_us, container, false);

        TextView tv_joinnow=(TextView)view.findViewById(R.id.tv_joinnow);
        TextView tv_learn_more=(TextView)view.findViewById(R.id.tv_learn_more);

        tv_joinnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), LoginActivity.class);
                it.putExtra("frompublic","true");
                startActivity(it);
            }
        });

        tv_learn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.moodindia.in/about.html";
                new FinestWebView.Builder(getActivity()).show(url);
            }
        });

        return view;
    }

}
