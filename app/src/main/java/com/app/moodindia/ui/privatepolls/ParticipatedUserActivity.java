package com.app.moodindia.ui.privatepolls;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ParticipatedUsersPojo;
import com.app.moodindia.model.PrivateOptions;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class ParticipatedUserActivity extends AppCompatActivity implements View.OnClickListener {


    protected ImageView imageView4;
    protected Toolbar toolbar;
    protected CircleImageView ivpoll;
    protected MagicTextView tvque;
    protected RelativeTimeTextView timestamp;
    protected LinearLayout linearLayout2;
    protected RecyclerView rvUsers;
    protected Button btn_option_filter, btn_end_poll;
    protected Dialog dialog;
    protected ImageView navback;
    protected LinearLayout lay2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_participated_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("Participated Users");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParticipatedUserActivity.this, PrivatePolls.class));
            }
        });
        initView();
        usa = new UsersAdapter();

        if (getIntent().getExtras().containsKey("pollid")) {
            getParticipatedUsers(getIntent().getExtras().getString("pollid"));
            tvque.setText(getIntent().getExtras().getString("ques"));

            try {
                timestamp.setText("Posted on " + dtfmt.format(defaultfmt.parse(getIntent().getExtras().getString("postedtime"))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(ParticipatedUserActivity.this)
                    .load(AppConst.imgurl + getIntent().getExtras().getString("pollimage"))
                    .transform(new RoundedSquareTransformation(10, 0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(ivpoll);

            getOptions(getIntent().getExtras().getString("pollid"));

        } else {
            Toast.makeText(ParticipatedUserActivity.this, "Something went wrong, Please try after sometime", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(ParticipatedUserActivity.this, PrivatePolls.class));
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    UsersAdapter usa;
    List<ParticipatedUsersPojo> usersList;
    SimpleDateFormat defaultfmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt = new SimpleDateFormat("dd MMM yyyy");

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.navback) {
            finish();
        }
    }

    public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.participated_users, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            ViewHolderFilter vh2 = (ViewHolderFilter) viewHolder;
            configureViewHolderFilter(vh2, i);
        }

        @Override
        public int getItemCount() {
            return usersList.size();
        }

        ParticipatedUsersPojo listUsers;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listUsers = usersList.get(position);

            holder.username.setText("Name:  " + listUsers.getName());
            holder.useranswer.setText("Answer:  " + listUsers.getAnswer());
            holder.userphone.setText("Phone:  " + listUsers.getPhone());

            if (listUsers.getBan().equalsIgnoreCase("0"))
                holder.btnbanunban.setText("Ban");
            else
                holder.btnbanunban.setText("Unban");


            Picasso.with(ParticipatedUserActivity.this)
                    .load(AppConst.imgurl + listUsers.getProfilepicture())
                    .transform(new RoundedSquareTransformation(10, 0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivUserProfileImage);
        }
    }


    class ViewHolderFilter extends RecyclerView.ViewHolder {

        private CircleImageView ivUserProfileImage;
        private MagicTextView username;
        private MagicTextView useranswer;
        private MagicTextView userphone;
        private Button btnbanunban;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            ivUserProfileImage = (CircleImageView) itemView.findViewById(R.id.iv_user_profile_image);
            username = (MagicTextView) itemView.findViewById(R.id.username);
            useranswer = (MagicTextView) itemView.findViewById(R.id.useranswer);
            userphone = (MagicTextView) itemView.findViewById(R.id.userphone);
            btnbanunban = (Button) itemView.findViewById(R.id.btnbanunban);
            btnbanunban.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (usersList.get(getAdapterPosition()).getBan().equalsIgnoreCase("0")) {
                        //Ban user
                        banUserPrivatePoll(usersList.get(getAdapterPosition()).getUserid(), getIntent().getExtras().getString("pollid"), "1");
                    } else {
                        //Unban User
                        banUserPrivatePoll(usersList.get(getAdapterPosition()).getUserid(), getIntent().getExtras().getString("pollid"), "0");
                    }
                }
            });

        }
    }

    public void banUserPrivatePoll(String userid, String pollid, final String ban) {
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.BanPrivatePoll(userid, pollid, ban, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojos, Response response) {
                if (ban.equalsIgnoreCase("0"))
                    Toast.makeText(ParticipatedUserActivity.this, "User Banned Successfully", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(ParticipatedUserActivity.this, "User UnBanned Successfully", Toast.LENGTH_SHORT).show();

                usersList.clear();
                getParticipatedUsers(getIntent().getExtras().getString("pollid"));
                usa.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ParticipatedUserActivity.this, "Ban User Failed, TryAgain.", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void getParticipatedUsers(String questionid) {
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollUsers(questionid, 1, new Callback<List<ParticipatedUsersPojo>>() {
            @Override
            public void success(List<ParticipatedUsersPojo> participatedUsersPojos, Response response) {
                if (participatedUsersPojos.size() > 0) {
                    usersList = participatedUsersPojos;
                    rvUsers.setAdapter(usa);
                    usa.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getParticipatedUsersByUserid(String questionid, String optionid) {
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollUsersByUserid(questionid, optionid, 1, new Callback<List<ParticipatedUsersPojo>>() {
            @Override
            public void success(List<ParticipatedUsersPojo> participatedUsersPojos, Response response) {
                dialog.dismiss();
                if (participatedUsersPojos.size() > 0) {
                    usersList.clear();
                    usersList = participatedUsersPojos;
                    rvUsers.setAdapter(usa);
                    usa.notifyDataSetChanged();
                } else {
                    Toast.makeText(ParticipatedUserActivity.this, "No users are voted to this option.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ParticipatedUserActivity.this, "Unable to fetch user data, try after sometime", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void initView() {
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivpoll = (CircleImageView) findViewById(R.id.ivpoll);
        tvque = (MagicTextView) findViewById(R.id.tvque);
        timestamp = (RelativeTimeTextView) findViewById(R.id.timestamp);
        linearLayout2 = (LinearLayout) findViewById(R.id.linearLayout2);
        rvUsers = (RecyclerView) findViewById(R.id.rv_users);
        btn_option_filter = (Button) findViewById(R.id.btn_option_filter);
        btn_end_poll = (Button) findViewById(R.id.btn_end_poll);
        btn_end_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endPersonalPoll(M.getID(ParticipatedUserActivity.this), getIntent().getExtras().getString("pollid").toString());
            }
        });
        mLayoutManager = new LinearLayoutManager(ParticipatedUserActivity.this);
        rvUsers.setLayoutManager(mLayoutManager);
        rvUsers.setItemAnimator(new DefaultItemAnimator());

        dialog = new Dialog(ParticipatedUserActivity.this);

        dialog.setContentView(R.layout.activity_options_filter);
        TextView btn_clear_filter = (TextView) dialog.findViewById(R.id.btn_clear_filter);
        rvFilterOptions = (RecyclerView) dialog.findViewById(R.id.rv_filter_options);

        mLayoutManager = new LinearLayoutManager(ParticipatedUserActivity.this);
        rvFilterOptions.setLayoutManager(mLayoutManager);
        rvFilterOptions.setItemAnimator(new DefaultItemAnimator());
        optionsList = new ArrayList<>();
        ola = new OptionsAdapter();


        btn_option_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.show();
              /*  Intent intent=new Intent(ParticipatedUserActivity.this, class);
                intent.putExtra("pollid",getIntent().getExtras().getString("pollid"));
                startActivityForResult(intent,1452);*/
            }
        });

        btn_clear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParticipatedUsers(getIntent().getExtras().getString("pollid"));
                dialog.dismiss();
            }
        });
        navback = (ImageView) findViewById(R.id.navback);
        navback.setOnClickListener(ParticipatedUserActivity.this);
        lay2 = (LinearLayout) findViewById(R.id.lay2);
    }

    protected RecyclerView rvFilterOptions;
    RecyclerView.LayoutManager mLayoutManager;
    OptionsAdapter ola;
    List<PrivateOptions> optionsList;

    public class OptionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.options_filter_private_poll, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return optionsList.size();
        }

        PrivateOptions listOptions;


        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            listOptions = optionsList.get(position);

            holder.tv_filter_option.setText(listOptions.getPollOption());
            holder.tv_filter_option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getParticipatedUsersByUserid(getIntent().getExtras().get("pollid").toString(), optionsList.get(position).getId());

                    //  Toast.makeText(ParticipatedUserActivity.this, optionsList.get(position).getPollOption(), Toast.LENGTH_SHORT).show();
                }
            });

            Picasso.with(ParticipatedUserActivity.this)
                    .load(AppConst.imgurl + listOptions.getImagePath())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.iv_option_image);

        }

        class ViewHolderFilter extends RecyclerView.ViewHolder {

            TextView tv_filter_option;
            ImageView iv_option_image;
            CheckBox cb_option;


            public ViewHolderFilter(View itemView) {
                super(itemView);
                tv_filter_option = (TextView) itemView.findViewById(R.id.tv_filter_option);
                iv_option_image = (ImageView) itemView.findViewById(R.id.iv_option_image);
                cb_option = (CheckBox) itemView.findViewById(R.id.cb_option);


            }
        }
    }

    public void getOptions(String pollid) {
        final int page = 1;
        M.showLoadingDialog(ParticipatedUserActivity.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.getPrivatePollOptions(pollid, new Callback<List<PrivateOptions>>() {
            @Override
            public void success(List<PrivateOptions> privateOptionses, Response response) {
                M.hideLoadingDialog();
                optionsList = privateOptionses;
                rvFilterOptions.setAdapter(ola);
                ola.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1452 && resultCode == RESULT_OK) {

        }
    }

    public void endPersonalPoll(final String userid, final String pollid) {
        M.showLoadingDialog(ParticipatedUserActivity.this);
        PrivatePollAPI mAuthenticationAPI = APIService.createService(PrivatePollAPI.class);
        mAuthenticationAPI.endPersonalPoll(pollid, userid, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                sendPollPushNotificationToUsers(pollid, userid);
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }

    public void sendPollPushNotificationToUsers(String privatepollid, String userid) {
        OkHttpClient client = new OkHttpClient();
        String reqUrl = ConsantURL + "app/Http/Controllers/sendPrivateNotifications.php?endpoll=true&privatepollid=" + privatepollid + "&userid=" + userid;
        System.out.println(reqUrl);
        Request request = new Request.Builder().url(reqUrl).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                M.hideLoadingDialog();
            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {

                if (response.isSuccessful()) {

                    M.hideLoadingDialog();
                    //
                    Intent it = new Intent(ParticipatedUserActivity.this, PrivatePolls.class);
                    finish();
                    startActivity(it);
                } else {
                    M.hideLoadingDialog();
                    //  Toast.makeText(ParticipatedUserActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(ParticipatedUserActivity.this, PrivatePolls.class);
                    finish();
                    startActivity(it);
                }
            }
        });
    }
}
