package com.app.moodindia.ui.privatepolls.Groups.GroupInfoFrgs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.moodindia.CircleTransform;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.WrapContentLinearLayoutManager;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ParticipantsPojo;
import com.app.moodindia.ui.privatepolls.Groups.GetParticipantsToInvite;
import com.app.moodindia.ui.privatepolls.InviteParticipantsByLocation;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupParticipantsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupParticipantsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "groupid";
    private static final String ARG_PARAM2 = "groupname";
    private static final String ARG_PARAM3 = "uidforchat";
    private static final String ARG_PARAM4 = "grouptype";
    private static final String ARG_PARAM5 = "isOwner";
    protected View rootView;
    protected RecyclerView rvParticipants;
    protected LinearLayout lytPolls;
    protected ImageView ivAddParticipants;
    protected TextView tvAddParticipants;
    protected TableLayout tlAddParticipants;
    protected TableRow fabadd;
    protected ImageView ivpollloc;
    protected MagicTextView topaddpollloc;
    protected TableRow fabaddloc;
    protected TableLayout tlAddByLocation;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private String mParam5;

    private OnFragmentInteractionListener mListener;

    public GroupParticipantsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupParticipantsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupParticipantsFragment newInstance(String param1, String param2, String param3, String param4, String param5) {
        GroupParticipantsFragment fragment = new GroupParticipantsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        args.putString(ARG_PARAM5, param5);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getString(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_group_participants, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        rvParticipants = (RecyclerView) rootView.findViewById(R.id.rvParticipants);
        rvParticipants.setLayoutManager(new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvParticipants.setItemAnimator(new DefaultItemAnimator());
        lytPolls = (LinearLayout) rootView.findViewById(R.id.lytPolls);
        ivAddParticipants = (ImageView) rootView.findViewById(R.id.ivpoll);
        tvAddParticipants = (TextView) rootView.findViewById(R.id.topaddpoll);
        tlAddParticipants = (TableLayout) rootView.findViewById(R.id.tlAddParticipants);
        ivAddParticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GetParticipantsToInvite.class);
                intent.putExtra("groupid", mParam1);
                intent.putExtra("groupName", mParam2);
                intent.putExtra("uidforchat", mParam3);
                startActivity(intent);
            }
        });
        tvAddParticipants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GetParticipantsToInvite.class);
                intent.putExtra("groupid", mParam1);
                intent.putExtra("groupName", mParam2);
                intent.putExtra("uidforchat", mParam3);
                startActivity(intent);
            }
        });
        mParticipantsPojos = new ArrayList<>();
        pla = new PartcipantsAdapter();
        if (mParam1 != null) {
            getParticipants(mParam1);
        }

       /* if (mParam1 != null && !mParam1.equalsIgnoreCase(M.getID(getActivity()))) {
            tlAddParticipants.setVisibility(View.VISIBLE);
        }*/
        fabadd = (TableRow) rootView.findViewById(R.id.fabadd);
        ivpollloc = (ImageView) rootView.findViewById(R.id.ivpollloc);
        topaddpollloc = (MagicTextView) rootView.findViewById(R.id.topaddpollloc);
        fabaddloc = (TableRow) rootView.findViewById(R.id.fabaddloc);
        tlAddByLocation = (TableLayout) rootView.findViewById(R.id.tlAddByLocation);

        if(mParam5!=null && mParam5.equalsIgnoreCase("true"))
        {
            tlAddByLocation.setVisibility(View.VISIBLE);
            tlAddParticipants.setVisibility(View.VISIBLE);
        }
        else
        {
            tlAddByLocation.setVisibility(View.GONE);
            tlAddParticipants.setVisibility(View.GONE);
        }


        tlAddByLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), InviteParticipantsByLocation.class);
                intent.putExtra("groupid",mParam1);
                intent.putExtra("groupname",mParam2);
                startActivity(intent);
            }
        });
    }

    PartcipantsAdapter pla;

    public class PartcipantsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_participant, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return mParticipantsPojos.size();
        }

        ParticipantsPojo mParticipantsPojo;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {

            mParticipantsPojo = mParticipantsPojos.get(position);


            holder.tvname.setText(mParticipantsPojo.getName());
            holder.tvphone.setText(mParticipantsPojo.getPhone());
            holder.tvemail.setText(mParticipantsPojo.getEmail());
            Picasso.with(getActivity())
                    .load(AppConst.imgurl + mParticipantsPojo.getImage())
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivProfile);
        }
    }


    List<ParticipantsPojo> mParticipantsPojos;

    public void getParticipants(String groupid) {
        //M.showLoadingDialog(getActivity());
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getGroupParticipants(groupid, new Callback<List<ParticipantsPojo>>() {
            @Override
            public void success(List<ParticipantsPojo> participantsPojos, Response response) {
                mParticipantsPojos = participantsPojos;
                rvParticipants.setAdapter(pla);
                pla.notifyDataSetChanged();
                // M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                // M.hideLoadingDialog();
            }
        });
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder {

        TextView tvname, tvphone, tvemail;
        ImageView ivProfile;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvname = (TextView) itemView.findViewById(R.id.tv_name);
            tvphone = (TextView) itemView.findViewById(R.id.tv_phone);
            tvemail = (TextView) itemView.findViewById(R.id.tv_email);
            ivProfile = (ImageView) itemView.findViewById(R.id.iv_participant);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
