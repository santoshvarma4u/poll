package com.app.moodindia.ui.topics;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.helper.WrapContentLinearLayoutManager;
import com.app.moodindia.model.KeywordsPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;
import com.app.moodindia.webservices.PollAPI;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchTopicsActivity extends AppCompatActivity {

    protected SearchView svQuery;
    protected ShimmerRecyclerView shimmerRecyclerView;
    protected RecyclerView rvSearchResults;
    protected MagicTextView tvNoSearchResults;
    String[] separated;
    ArrayList<String> scripts = new ArrayList<String>();
    protected View rootView;
    protected ScrollView sv_chips;
    protected ImageView iv_back;

    protected ChipCloud chipCloudTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_search_topics);
        initView();
        svQuery.requestFocus();

        chipCloudTags = (ChipCloud) findViewById(R.id.chip_cloud_tags);
        sv_chips = (ScrollView) findViewById(R.id.sv_chips);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SearchTopicsActivity.this,TopicsActivity.class);
                startActivity(intent);
                finish();
            }
        });
        chipCloudTags.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                if(scripts.size()>0)
                {
                    Intent intent=new Intent(SearchTopicsActivity.this,SearchTopicsActivity.class);
                    intent.putExtra("isTags",scripts.get(i).toString());
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void chipDeselected(int i) {
                if(scripts.size()>0)
                {
                    // Toast.makeText(getActivity(), separated[i], Toast.LENGTH_SHORT).show();
                }
            }
        });
       getHashTags();
    }

    private void initView() {
        svQuery = (SearchView) findViewById(R.id.sv_query);
        shimmerRecyclerView = (ShimmerRecyclerView) findViewById(R.id.shimmer_recycler_view);
        rvSearchResults = (RecyclerView) findViewById(R.id.rv_search_results);

        rvSearchResults.setLayoutManager(new WrapContentLinearLayoutManager(SearchTopicsActivity.this, LinearLayoutManager.VERTICAL, false));
        //mLayoutManager = new LinearLayoutManager(getActivity());
        //rvSearchResults.setLayoutManager(mLayoutManager);
        rvSearchResults.setItemAnimator(new DefaultItemAnimator());

        shimmerRecyclerView = (ShimmerRecyclerView) findViewById(R.id.shimmer_recycler_view);
        shimmerRecyclerView.setLayoutManager(new LinearLayoutManager(SearchTopicsActivity.this));
        shimmerRecyclerView.showShimmerAdapter();
        rvSearchResults.setVisibility(View.GONE);
        tvNoSearchResults = (MagicTextView) findViewById(R.id.tvNoSearchResults);
        svQuery.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length() > 0)
                {
                    svQuery.clearFocus();
                    getSearchResponse(query);
                }
                else
                {
                    Toast.makeText(SearchTopicsActivity.this, "Please type your query in search box to get related stuff", Toast.LENGTH_SHORT).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                /*if(newText.length() > 4)
                {
                    getSearchResponse(newText);
                }*/
                return false;
            }
        });
        svQuery.requestFocus();

        pla=new PollsAdapter();
        poolsList=new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.containsKey("isTags"))
            {
                getTagResponse(extras.getString("isTags"));
                svQuery.setQuery(extras.getString("isTags"),true);
                hideKeyboard();
                svQuery.clearFocus();
                rvSearchResults.requestFocus();
            }
            else if(extras.containsKey("searchquery"))
            {
                getSearchResponse(extras.getString("searchquery"));
                svQuery.setQuery(extras.getString("searchquery"),true);
                hideKeyboard();
                svQuery.clearFocus();
                rvSearchResults.requestFocus();
            }
        }

        svQuery.setIconified(false);

        svQuery.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                poolsList.clear();
                pla.notifyDataSetChanged();
                getHashTags();
                rvSearchResults.setVisibility(View.GONE);
                sv_chips.setVisibility(View.VISIBLE);
                svQuery.onActionViewExpanded();
                return false;
            }
        });
    }

    private void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(svQuery.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
    public void getSearchResponse(String searchQuery) {

        svQuery.clearFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(svQuery.getWindowToken(), 0);

        poolsList.clear();
        pla.notifyDataSetChanged();

        shimmerRecyclerView.showShimmerAdapter();
        rvSearchResults.setVisibility(View.GONE);
        tvNoSearchResults.setVisibility(View.GONE);
        final int page=1;
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getSearchResults(M.getID(SearchTopicsActivity.this), page, searchQuery, new Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pollPojos, Response response) {

                poolsList=pollPojos;

                if(poolsList!=null){
                    if(poolsList.size()>0) {
                        rvSearchResults.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        shimmerRecyclerView.hideShimmerAdapter();
                        rvSearchResults.setVisibility(View.VISIBLE);
                        sv_chips.setVisibility(View.GONE);
                    }
                    else
                    {
                        shimmerRecyclerView.hideShimmerAdapter();
                        rvSearchResults.setVisibility(View.GONE);
                        tvNoSearchResults.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    shimmerRecyclerView.hideShimmerAdapter();
                    rvSearchResults.setVisibility(View.GONE);
                    tvNoSearchResults.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    public void getTagResponse(String searchQuery) {

        svQuery.clearFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(svQuery.getWindowToken(), 0);


        poolsList.clear();
        pla.notifyDataSetChanged();

        shimmerRecyclerView.showShimmerAdapter();
        rvSearchResults.setVisibility(View.GONE);
        tvNoSearchResults.setVisibility(View.GONE);
        final int page=1;
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getTagResults(M.getID(SearchTopicsActivity.this), page, searchQuery, new Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pollPojos, Response response) {

                poolsList=pollPojos;

                if(poolsList!=null){
                    if(poolsList.size()>0) {
                        sv_chips.setVisibility(View.GONE);
                        rvSearchResults.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        shimmerRecyclerView.hideShimmerAdapter();
                        rvSearchResults.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        shimmerRecyclerView.hideShimmerAdapter();
                        rvSearchResults.setVisibility(View.GONE);
                        tvNoSearchResults.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    shimmerRecyclerView.hideShimmerAdapter();
                    rvSearchResults.setVisibility(View.GONE);
                    tvNoSearchResults.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    PollsAdapter pla;
    List<PollPojo> poolsList;

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {

            return poolsList == null ? 0 : poolsList.size();
        }
        PollPojo listCategory;


        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCategory=poolsList.get(position);
            holder.catLayout.setVisibility(View.GONE);

            holder.tvcat.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
            holder.tvpollcategory.setText(listCategory.getCat_name());
            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(SearchTopicsActivity.this)
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new RoundedSquareTransformation(10,0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);
        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{
        TextView tvpoll,tvviewAll,tvcat,tvCategPost;
        ImageView ivpoll;
        TextView tvpollcategory;
        LinearLayout catLayout;
        CardView cv_category;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg) ;
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);

            Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent it=new Intent(SearchTopicsActivity.this, PollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("from","active");
                    it.putExtra("searchquery",svQuery.getQuery().toString());
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchTopicsActivity.this, p1,p2);
                    // startActivity(it, options.toBundle());
                    startActivity(it);
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it=new Intent(SearchTopicsActivity.this, PollScreen.class);
                    it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                    it.putExtra("from","active");
                    it.putExtra("searchquery",svQuery.getQuery().toString());
                    Pair<View, String> p1 = Pair.create((View)ivpoll, "profile");
                    Pair<View, String> p2 = Pair.create((View)tvpoll, "ques");

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchTopicsActivity.this, p1,p2);
                    // startActivity(it, options.toBundle());
                    startActivity(it);
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SearchTopicsActivity.this,TopicsActivity.class));
        this.finish();

    }

    public void getHashTags()
    {
        M.showLoadingDialog(SearchTopicsActivity.this);
        CategoryAPI mCategoryAPI= APIService.createService(CategoryAPI.class);
        mCategoryAPI.getHashTags(M.getID(SearchTopicsActivity.this), new Callback<KeywordsPojo>() {

            @Override
            public void success(KeywordsPojo keywordsPojo, Response response) {
                String keywordsList=keywordsPojo.getKeywords();
                separated = keywordsList.split(",");

                for(String keys:separated)
                {
                    if(countWordsInSentence(keys) <=5)
                    {
                        scripts.add(keys);
                        Log.i("tags",keys);
                        chipCloudTags.addChip(keys);
                    }
                }
                // chipCloudTags.addChips(separated);
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
    private int countWordsInSentence(String input) {
        int wordCount = 0;

        if (input.trim().equals("")) {
            return wordCount;
        }
        else {
            wordCount = 1;
        }

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            String str = new String("" + ch);
            if (i+1 != input.length() && str.equals(" ") && !(""+ input.charAt(i+1)).equals(" ")) {
                wordCount++;
            }
        }

        return wordCount;
    }
}
