package com.app.moodindia;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.moodindia.model.ChangePwdPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;

import retrofit.RetrofitError;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    EditText etcurr,etnew,etconfirm;
    Button btnchange;

    String TAG="change Password",currpwd,newpwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        initview();

    }

    private void initview() {

        etcurr=(EditText)findViewById(R.id.etcurrpwd);
        etnew=(EditText)findViewById(R.id.etnewpwd);
        etconfirm=(EditText)findViewById(R.id.etconpwd);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssr.ttf");
        Typeface custom_fontb = Typeface.createFromAsset(getAssets(),  "fonts/ssb.ttf");
        Typeface custom_fontl = Typeface.createFromAsset(getAssets(),  "fonts/ssl.ttf");

        etcurr.setTypeface(custom_fontb);
        etnew.setTypeface(custom_fontb);
        etconfirm.setTypeface(custom_fontb);

        btnchange=(Button)findViewById(R.id.btnchangepwd);
        btnchange.setOnClickListener(this);
        btnchange.setTypeface(custom_font);

    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.btnchangepwd){
            if(etcurr.getText().toString().trim().length()<=0)
                etcurr.setError("Current Password Required");
            else if(etnew.getText().toString().trim().length()<=0)
                etnew.setError("New Password Required");
            else if(etconfirm.getText().toString().trim().length()<=0)
                etconfirm.setError("Confirm Password Required");
            else if(!etnew.getText().toString().equals(etconfirm.getText().toString()))
                etconfirm.setError("Confirm Password Mismatch");
            else{
                currpwd=etcurr.getText().toString();
                newpwd=etnew.getText().toString();
                changePwd();
            }
        }
    }

    private void changePwd() {
        M.showLoadingDialog(ChangePassword.this);

        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.changePassword(M.getID(ChangePassword.this),currpwd,newpwd,new retrofit.Callback<ChangePwdPojo>() {
            @Override
            public void success(ChangePwdPojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess().equals("1")){
                        Toast.makeText(ChangePassword.this,"Password change Successfully Done",Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }else{
                        Toast.makeText(ChangePassword.this,pojo.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(ChangePassword.this,SettingActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
}
