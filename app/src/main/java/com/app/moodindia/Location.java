package com.app.moodindia;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.app.moodindia.model.CityPojo;
import com.app.moodindia.model.CountryPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatePojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.LocationAPI;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Location extends AppCompatActivity {

    SearchableSpinner sp_country;
    SearchableSpinner sp_state;
    SearchableSpinner sp_city;

    LocationAPI mLocationApi;

    ArrayList<String> al_country = new ArrayList<String>();
    ArrayList<String> al_state = new ArrayList<String>();
    ArrayList<String> al_city = new ArrayList<String>();

    ArrayList<String> al_country_id = new ArrayList<String>();
    ArrayList<String> al_state_id = new ArrayList<String>();
    ArrayList<String> al_city_id = new ArrayList<String>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(Location.this);
        sp_country=(SearchableSpinner)findViewById(R.id.CountrySpinner);
        sp_country.setTitle("Select Country");
        sp_state=(SearchableSpinner)findViewById(R.id.StateSpinner);
        sp_state.setTitle("Select State");
        sp_city=(SearchableSpinner)findViewById(R.id.CitySpinner);
        sp_city.setTitle("Select City");

        mLocationApi = APIService.createService(LocationAPI.class);
        getAllCountries();

        sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               // System.out.println("********************"+al_country_id.get(position).toString());
                getStates(al_country_id.get(position-1).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getCities(al_state_id.get(position-1).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        //getStates("101");
        //getCities("3930");


    }
    public void getAllCountries()
    {
        M.showLoadingDialog(Location.this);
        mLocationApi.getCountries("",new retrofit.Callback<List<CountryPojo>>()
        {

            @Override
            public void success(List<CountryPojo> countryPojos, Response response) {

               // CountryPojo cpojo=new CountryPojo();
                M.hideLoadingDialog();
                al_country_id.clear();
                al_country.clear();
              for(int i=0;i<countryPojos.size();i++)
              {
                  al_country.add(countryPojos.get(i).getName());
                  al_country_id.add(countryPojos.get(i).getId());
              }
                setSpinnerData(Location.this,sp_country,al_country,"");
               // countryadapter.notifyDataSetChanged();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });

    }
    public void getStates(String country_id)
    {
        M.showLoadingDialog(Location.this);
        mLocationApi.getStates(country_id,new retrofit.Callback<List<StatePojo>>()
        {

            @Override
            public void success(List<StatePojo> statePojos, Response response) {

                al_state.clear();
                al_state_id.clear();
                for(int i=0;i<statePojos.size();i++)
                {
                    al_state.add(statePojos.get(i).getName());
                    al_state_id.add(statePojos.get(i).getId());
                }
                setSpinnerData(Location.this,sp_state,al_state,"");

                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }

    public void getCities(String state_id)
    {
        M.showLoadingDialog(Location.this);
        mLocationApi.getCities(state_id,new retrofit.Callback<List<CityPojo>>()
        {

            @Override
            public void success(List<CityPojo> cityPojos, Response response) {

                al_city.clear();
                al_city_id.clear();

                for(int i=0;i<cityPojos.size();i++)
                {
                    al_city.add(cityPojos.get(i).getName());
                    al_city_id.add(cityPojos.get(i).getId());
                }
                setSpinnerData(Location.this,sp_city,al_city,"");

                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }


    public static void setSpinnerData(Context context, SearchableSpinner spn,
                                      ArrayList<String> values, String msg) {

        ArrayAdapter<String> aa = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item);

        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        aa.add(msg);
        for (String val : values)
            aa.add(val);
        spn.setAdapter(aa);
    }
}
