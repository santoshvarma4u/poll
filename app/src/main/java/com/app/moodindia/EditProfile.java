package com.app.moodindia;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.squareup.picasso.Picasso;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.regex.Pattern;

import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {

    EditText etemail,etusername,etphone,etbdate;
    RadioGroup rggender,spinarray;
    RadioButton rbmale,rbfemale,rbsingle,rbmarried;
    //Spinner spncountry;
    Button btnsubmit;
    String TAG="Edit Profile",email,name,phone="",country,bdate,gender,income,marital;
    DatePickerDialog.OnDateSetListener d;
    final Calendar dateAndTime=Calendar.getInstance();
    SimpleDateFormat fmtDateAndTime=new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat fmtdt=new SimpleDateFormat("yyyy-MM-dd");
    Context ctx;
    Spinner spnqualification;
    Spinner spnincome;
    Spinner spnage;
    String qualification,age;
    String countryIntent,stateIntent,cityIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(EditProfile.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        final Intent it=new Intent(this,SettingActivity.class);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
                finish();
                overridePendingTransition(0,0);
            }
        });
        ctx=EditProfile.this;
        initview();
        Intent locationIntent=new Intent(EditProfile.this, GetLocation.class);
        startActivityForResult(locationIntent,452);
    }
    String latitude="0.0",longitude="0.0";
    private Spinner spnmarray;
    private Spinner spnemptype;
    private TextView txtIncomeValue;
    private DiscreteSeekBar seekBar;
    private TextView txtCountry;
    private TextView txtState;
    private TextView txtCity;
    ImageView iv_profile_picture;

    private void initview() {
        iv_profile_picture = (ImageView) findViewById(R.id.iv_profile_picture);
        etemail=(EditText)findViewById(R.id.etusremail);
        etusername=(EditText)findViewById(R.id.etusrname);
        etphone=(EditText)findViewById(R.id.etphone);
        etbdate=(EditText)findViewById(R.id.etbday);
        rggender=(RadioGroup)findViewById(R.id.rgegender);
        spinarray=(RadioGroup)findViewById(R.id.spnmarray);
        rbmale=(RadioButton)findViewById(R.id.rbemale);
        rbfemale=(RadioButton)findViewById(R.id.rbefemale);
        rbsingle=(RadioButton)findViewById(R.id.rbsingle);
        rbmarried=(RadioButton)findViewById(R.id.rbmarried);
        //spncountry=(Spinner)findViewById(R.id.spncountry);
        btnsubmit=(Button)findViewById(R.id.btn_edit);
//        spnmarray = (Spinner) findViewById(R.id.spnmarray);
        spnemptype = (Spinner) findViewById(R.id.spnemptype);
        txtIncomeValue = (TextView) findViewById(R.id.txtIncomeValue);
        seekBar = (DiscreteSeekBar) findViewById(R.id.seek_bar);
        txtCountry = (TextView) findViewById(R.id.txtCountry);
        txtState = (TextView) findViewById(R.id.txtState);
        txtCity = (TextView) findViewById(R.id.txtCity);
        spnqualification = (Spinner) findViewById(R.id.spnqualification);
        spnincome = (Spinner) findViewById(R.id.spnincome);
        spnage = (Spinner) findViewById(R.id.spnage);
        etbdate.setOnClickListener(this);
        btnsubmit.setOnClickListener(this);

        d=new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
            {
                // TODO Auto-generated method stub
                dateAndTime.set(Calendar.YEAR, arg1);
                dateAndTime.set(Calendar.MONTH,arg2);
                dateAndTime.set(Calendar.DAY_OF_MONTH, arg3);

                etbdate.setText(fmtDateAndTime.format(dateAndTime.getTime()));
                bdate=fmtdt.format(dateAndTime.getTime());
            }

        };


        txtCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EditProfile.this,SelectCountry.class);
                intent.putExtra("register", "register");
                startActivityForResult(intent,1);

            }
        });
        txtState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCountry.getText().toString().equalsIgnoreCase("Country"))
                {
                    M.showToast(EditProfile.this,"Please Select Country First");
                }
                else
                {
                    if(tempCountryId.toString().length() > 0)
                    {
                        Intent intent=new Intent(EditProfile.this,SelectState.class);
                        intent.putExtra("countryid",tempCountryId);
                        intent.putExtra("register", "register");
                        startActivityForResult(intent,2);
                    }
                    else
                    {
                        txtCountry.performClick();
                    }

                }

            }
        });
        txtCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtState.getText().toString().equalsIgnoreCase("State"))
                {
                    M.showToast(EditProfile.this,"Please Select State First");
                }
                else
                {
                    if(tempCountryId.toString().length() > 0 && tempStateId.toString().length() > 0)
                    {
                        Intent intent=new Intent(EditProfile.this,SelectCity.class);
                        intent.putExtra("stateid",tempStateId);
                        intent.putExtra("register", "register");
                        startActivityForResult(intent,3);
                    }
                    else
                    {
                        txtCountry.performClick();
                    }

                }
            }
        });

        iv_profile_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  tv_country.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(EditProfile.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                    }
                    else {
                        Intent galleryIntent = new Intent();
                        galleryIntent.setType("image/*");
                        galleryIntent.setAction(Intent.ACTION_PICK);
                        // Chooser of file system options.
                        Intent openGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE);
                    }
                }
            }
        });

        getUserData();
    }
    private static final int REQUEST_GALLERY_CODE = 200;

    String tempCountryId="",tempStateId="",tempCityId="";
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode == RESULT_OK)
        {
            //country call back
           // M.setMyCountry(data.getExtras().getString("CountryName").toString(),EditProfile.this);
           // M.setMyCountryID(data.getExtras().getString("CountryId").toString(),EditProfile.this);
            txtCountry.setText(data.getExtras().getString("CountryName").toString());
            tempCountryId=data.getExtras().getString("CountryId").toString();

            txtState.setText("State");
            // tv_state.performClick();
            //M.showToast(HomeActivity.this,data.getExtras().getString("CountryId").toString());
        }
        else if(requestCode==2 && resultCode == RESULT_OK)
        {
            //country call back
           // M.setMyState(data.getExtras().getString("StateName").toString(),EditProfile.this);
           // M.setMyStateID(data.getExtras().getString("StateId").toString(),EditProfile.this);
            txtState.setText(data.getExtras().getString("StateName").toString());
            tempStateId=data.getExtras().getString("StateId").toString();
            txtCity.setText("City");
            // tv_city.performClick();
            // M.showToast(HomeActivity.this,data.getExtras().getString("StateId").toString());
        }
        else if(requestCode==3 && resultCode == RESULT_OK)
        {
            //country call back
          //  M.setMyCity(data.getExtras().getString("CityName").toString(),EditProfile.this);
          //  M.setMyCityID(data.getExtras().getString("CityId").toString(),EditProfile.this);
            txtCity.setText(data.getExtras().getString("CityName").toString());
            tempCityId=data.getExtras().getString("CityId").toString();
            // M.showToast(RegisterActivity.this,data.getExtras().getString("CityId").toString());
        }
        else if(requestCode==452 && resultCode == RESULT_OK)
        {
                                try {
                                    countryIntent=data.getExtras().getString("country").toString();
                                    stateIntent=data.getExtras().getString("state").toString();
                                    cityIntent=data.getExtras().getString("city").toString();
                                    latitude=data.getExtras().getString("latitude").toString();
                                    longitude=data.getExtras().getString("longitude").toString();

                                if(countryIntent.length()>2)
                                {
                                 txtCountry.setText(countryIntent);
                                }
                                if(stateIntent.length()>2)
                                     {
                                    txtState.setText(stateIntent);
                                }

                        if(cityIntent.length()>2)
                        {
                            txtCity.setText(cityIntent);
                        }
                    }
                    catch (Exception e)
                    {
                        //Log.d("Exception",e.toString());

                        countryIntent = "N";
                        stateIntent = "N";
                        cityIntent = "N";
                    }
        }

        else if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK&& null!=data) {
            try{
                M.showLoadingDialog(EditProfile.this);
                // Get real path to make File
                selectedImageUri = Uri.parse(getPath(data.getData()));
                Bitmap bitmap= BitmapFactory.decodeFile(getPath(data.getData()));
                Picasso.with(getApplicationContext()).load(selectedImageUri).fit().centerCrop().into(iv_profile_picture);
                // relev_pics.setImageBitmap(bitmap);
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    Picasso.with(EditProfile.this).load(new File(cursor.getString(columnIndex)))
                            .into(iv_profile_picture);
                    cursor.close();



                } else {
                    Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                }

                M.hideLoadingDialog();
                //Log.d(TAG,"Image path :- "+selectedImageUri);
            }
            catch (Exception e){
                M.hideLoadingDialog();
                Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                Log.e(TAG,e.getMessage());
            }
        }
    }

    Uri  selectedImageUri=null;

    private String getPath(Uri uri) throws Exception {
        // this method will be used to get real path of Image chosen from gallery.
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    @Override
    public void onClick(View view) {
       if(view.getId()==R.id.btn_edit)
       {
            if(etusername.getText().toString().trim().length()<=0){
                etusername.setError("Username Required");
            }else if(etemail.getText().toString().trim().length()<=0){
                etemail.setError("Email Required");
            }else if(!isValidEmaillId(etemail.getText().toString())){
                etemail.setError("Invalid Email");
            }else if(etbdate.getText().toString().trim().length()<=0){
                etbdate.setError("BirthDate Required");
            }
            /*else if(spnmarray.getSelectedItem().equals("Marital Status")){
                ((TextView)spnmarray.getSelectedView()).setError("Select Marital Status");
            }*/
            else if(spnqualification.getSelectedItem().equals("Qualification")){
                ((TextView)spnqualification.getSelectedView()).setError("Select Qualification");
            }
            else if(spnage.getSelectedItem().equals("Age")){
                ((TextView)spnage.getSelectedView()).setError("Select Age");
            }
            else if(spnincome.getSelectedItem().equals("Income Range")){
                ((TextView)spnincome.getSelectedView()).setError("Select Income Range");
            }
            else if(spnemptype.getSelectedItem().equals("Occupation")){
                ((TextView)spnemptype.getSelectedView()).setError("Select Occupation");
            }

            else {
                email=etemail.getText().toString();
                name=etusername.getText().toString();
                phone=etphone.getText().toString();
                country=txtCountry.getText().toString();

                income=spnincome.getSelectedItem().toString();
                qualification=spnqualification.getSelectedItem().toString();
                age=spnage.getSelectedItem().toString();
                if(rbmarried.isChecked())
                    {
                    marital="Married";
                    }
                else
                    {
                    marital="Single";
                    }
                if(rbfemale.isChecked()) {
                    gender = "Female";
                }
                else {
                    gender = "Male";
                }
                editProfile();
            }
        }
    }

    private void getUserData(){
        //M.showLoadingDialog(EditProfile.this);
        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.getUserData(M.getID(EditProfile.this),new retrofit.Callback<ProfilePojo>() {
            @Override
            public void success(ProfilePojo pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.getSuccess().equalsIgnoreCase("1")) {
                      //  M.hideLoadingDialog();
                        bdate=pojo.getDate_of_birth();
                        try {
                            etbdate.setText(fmtDateAndTime.format(fmtdt.parse(bdate)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                       /* String[] baths = getResources().getStringArray(R.array.maritalarray);
                        spinarray.setSelection(Arrays.asList(baths).indexOf(pojo.getMaritial_status()));*/
                        String[] baths1 = getResources().getStringArray(R.array.emparray);
                        spnemptype.setSelection(Arrays.asList(baths1).indexOf(pojo.getUser_type()));

                        String[] baths2 = getResources().getStringArray(R.array.agearray);
                        spnage.setSelection(Arrays.asList(baths2).indexOf(pojo.getAge()));

                        String[] baths3 = getResources().getStringArray(R.array.qualificationarray);
                        spnqualification.setSelection(Arrays.asList(baths3).indexOf(pojo.getQualification()));


                        String[] baths4 = getResources().getStringArray(R.array.incomearray);
                        spnincome.setSelection(Arrays.asList(baths4).indexOf(pojo.getIncome_level()));

                       // txtIncomeValue.setText(pojo.getIncome_level());

//                        seekBar.setProgress(Integer.parseInt(pojo.getIncome_level()));

                        txtCountry.setText(pojo.getCountry());
                        txtState.setText(pojo.getState());
                        txtCity.setText(pojo.getCity());

                        etemail.setText(pojo.getEmail());
                        etphone.setText(pojo.getPhone());
                        etusername.setText(pojo.getName());

                        Picasso.with(EditProfile.this)
                                .load(AppConst.imgurl + pojo.getProfile_picture())
                                .placeholder(R.drawable.default_image)
                                .error(R.drawable.default_image)
                                .into(iv_profile_picture);

                        if(pojo.getMaritial_status().equalsIgnoreCase("Married"))
                        {
                            rbmarried.setChecked(true);
                            rbsingle.setChecked(false);
                        }
                        if(pojo.getGender().equalsIgnoreCase("female")){
                            rbfemale.setChecked(true);
                            rbmale.setChecked(false);
                        }
                    }
                }
               // M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
             //   M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    private TypedFile makeFile(String uri){

        File file = new File(uri);
        TypedFile typedFile = new TypedFile("image/*",file);
        return typedFile;
    }
    private void editProfile(){

        TypedFile typedFile;
        if(selectedImageUri != null)
        {
            typedFile = makeFile(selectedImageUri.toString());
        }
        else
        {
            Uri path = Uri.parse("android.resource://"+BuildConfig.APPLICATION_ID+"/" + R.drawable.default_image);
            typedFile = makeFile(path.toString());
        }

        M.showLoadingDialog(EditProfile.this);


        if(selectedImageUri ==null)
        {
            AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
            mAuthenticationAPI.editProfile(M.getID(EditProfile.this),name,phone,txtCountry.getText().toString(),bdate,spnage.getSelectedItem().toString(),gender,txtState.getText().toString(),txtCity.getText().toString(),spnemptype.getSelectedItem().toString(),spnincome.getSelectedItem().toString(),marital,latitude,longitude,new retrofit.Callback<ProfilePojo>() {
                @Override
                public void success(ProfilePojo pojo, retrofit.client.Response response) {
                    if(pojo!=null){
                        if(pojo.getSuccess().equalsIgnoreCase("1")) {
                            M.hideLoadingDialog();
                            M.setUsername(pojo.getName(),ctx);
                            M.setEmail(pojo.getEmail(),ctx);
                            M.setPhone(pojo.getPhone(),ctx);
                            M.setCountry(pojo.getCountry(),ctx);
                            M.setBday(pojo.getDate_of_birth(),ctx);
                            M.setGender(pojo.getGender(),ctx);
                            M.setlogintype(pojo.getLogin_type(),ctx);
                            M.setUserpic(pojo.getProfile_picture(),ctx);
                            Toast.makeText(EditProfile.this,"Profile Updated Successfully..",Toast.LENGTH_SHORT).show();
                            Intent it=new Intent(EditProfile.this,SettingActivity.class);
                            finish();
                            startActivity(it);
                        }else{
                            Toast.makeText(EditProfile.this,"Edit Profile failed...",Toast.LENGTH_SHORT).show();
                        }
                    }
                    M.hideLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    //Log.d(TAG,"fail"+error.getMessage());
                    return;
                }
            });
        }
        else
        {
            AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
            mAuthenticationAPI.editProfileWithImage(M.getID(EditProfile.this),name,phone,txtCountry.getText().toString(),bdate,spnage.getSelectedItem().toString(),gender,txtState.getText().toString(),txtCity.getText().toString(),spnemptype.getSelectedItem().toString(),spnincome.getSelectedItem().toString(),marital,latitude,longitude,typedFile,new retrofit.Callback<ProfilePojo>() {
                @Override
                public void success(ProfilePojo pojo, retrofit.client.Response response) {
                    if(pojo!=null){
                        if(pojo.getSuccess().equalsIgnoreCase("1")) {
                            M.hideLoadingDialog();
                            M.setUsername(pojo.getName(),ctx);
                            M.setEmail(pojo.getEmail(),ctx);
                            M.setPhone(pojo.getPhone(),ctx);
                            M.setCountry(pojo.getCountry(),ctx);
                            M.setBday(pojo.getDate_of_birth(),ctx);
                            M.setGender(pojo.getGender(),ctx);
                            M.setlogintype(pojo.getLogin_type(),ctx);
                            Toast.makeText(EditProfile.this,"Profile Updated Successfully..",Toast.LENGTH_SHORT).show();
                            Intent it=new Intent(EditProfile.this,SettingActivity.class);
                            finish();
                            startActivity(it);
                        }else{
                            Toast.makeText(EditProfile.this,"Edit Profile failed...",Toast.LENGTH_SHORT).show();
                        }
                    }
                    M.hideLoadingDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    //Log.d(TAG,"fail"+error.getMessage());
                    return;
                }
            });
        }

    }

    private boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(EditProfile.this,SettingActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
}
