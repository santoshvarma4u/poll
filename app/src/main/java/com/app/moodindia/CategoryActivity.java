package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.app.moodindia.adapter.CategoryAdapter;

public class CategoryActivity extends AppCompatActivity {

    ListView lvcat;
    TextView tvnocat;
    CategoryAdapter adapter;

    String TAG="Category";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        lvcat=(ListView)findViewById(R.id.lvcategory);
        tvnocat=(TextView)findViewById(R.id.nocat);

       // getCategory();
    }

    private void getCategory() {
     /*   M.showLoadingDialog(CategoryActivity.this);

        CategoryAPI mAuthenticationAPI = APIService.createService(CategoryAPI.class);
        mAuthenticationAPI.getCategories("",new retrofit.Callback<List<CategoryPojo>>() {
            @Override
            public void success(List<CategoryPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        lvcat.setVisibility(View.VISIBLE);
                        tvnocat.setVisibility(View.GONE);
                        adapter = new CategoryAdapter(CategoryActivity.this, (ArrayList<CategoryPojo>) pojo);
                        lvcat.setAdapter(adapter);
                    }else{
                        lvcat.setVisibility(View.GONE);
                        tvnocat.setVisibility(View.VISIBLE);
                    }
                }else{
                    lvcat.setVisibility(View.GONE);
                    tvnocat.setVisibility(View.VISIBLE);
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home) {
            Intent it=new Intent(this,HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(this,HomeActivity.class);
        startActivity(it);
        finish();
        overridePendingTransition(0,0);
    }

}
