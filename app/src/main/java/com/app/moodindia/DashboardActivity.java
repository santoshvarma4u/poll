package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.jaredrummler.android.device.DeviceName;

import retrofit.RetrofitError;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
    
    Button btnpoll,btnlogin,btnsetting,btncategory,btnarchvies;
    String TAG="Dashboard",deviceid,deviceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        deviceid = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
        deviceName = DeviceName.getDeviceName();

        initview();

        if(!M.getID(DashboardActivity.this).equals("0"))
            updatefcm();

      /*  M.showToast(DashboardActivity.this,M.getFavs(DashboardActivity.this).toString());*/
    }

    private void initview() {

        btnpoll=(Button)findViewById(R.id.btnpoll);
        btnarchvies=(Button)findViewById(R.id.btnarchives);
        btnlogin=(Button)findViewById(R.id.btn_login);
        btnsetting=(Button)findViewById(R.id.btnsetting);
        btncategory=(Button)findViewById(R.id.btncategory);

        btncategory.setOnClickListener(this);
        btnarchvies.setOnClickListener(this);
        btnsetting.setOnClickListener(this);
        btnlogin.setOnClickListener(this);
        btnpoll.setOnClickListener(this);

        if(M.getID(DashboardActivity.this).equals("0")) {
            btnsetting.setVisibility(View.GONE);
            btnlogin.setVisibility(View.VISIBLE);
        }
        else {
            btnsetting.setVisibility(View.VISIBLE);
            btnlogin.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnpoll){

            Intent it=new Intent(DashboardActivity.this,CurrentPolls.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }else if(v.getId()==R.id.btncategory){
            Intent it=new Intent(DashboardActivity.this,CategoryActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }else if(v.getId()==R.id.btnarchives){
            Intent it=new Intent(DashboardActivity.this,HistoryActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }else if(v.getId()==R.id.btn_login){
            Intent it=new Intent(DashboardActivity.this,LoginActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }else if(v.getId()==R.id.btnsetting){
            Intent it=new Intent(DashboardActivity.this,SettingActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
    }

    public void updatefcm(){
        //Log.d(TAG,"gcmid:"+AppConst.gcm_id);
        if(AppConst.gcm_id.length()>0) {
            AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
            mAuthenticationAPI.updatefcm(M.getID(DashboardActivity.this), AppConst.gcm_id, deviceid, "android", deviceName, new retrofit.Callback<SuccessPojo>() {
                @Override
                public void success(SuccessPojo pojo, retrofit.client.Response response) {
                }

                @Override
                public void failure(RetrofitError error) {
                    //Log.d(TAG, "fail" + error.getMessage());
                    return;
                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

}
