package com.app.moodindia;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ArchivePollScreen;
import com.app.moodindia.adapter.PollAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.ui.topics.TopicsActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.PollAPI;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

public class CurrentPolls extends AppCompatActivity   {
    ListView lvpoll;
    TextView tvnocurrpoll;
    PollAdapter adapter;
    String TAG="currentpoll",userid="",catid="",act_names="null";
    Boolean loadmore=false;
    Boolean initialload = true;
    ArrayList<PollPojo> list=new ArrayList<>();
    Toolbar toolbar;
    boolean isConnected;
    RecyclerView rv_current_polls;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    RecyclerView.LayoutManager mLayoutManager;
    Button btnFollow;

    FragmentManager fragmentManager;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_polls);
        act_names=getIntent().getExtras().getString("act_name");
        //Log.d("act",act_names);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        final Intent it=new Intent(this,HistoryActivity.class);
        final Intent it1=new Intent(this,HomeActivity.class);
        final Intent it2=new Intent(this,HomeActivity.class);
        final Intent it3=new Intent(this,TopicsActivity.class);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(act_names.equals("history")) {
                    it.putExtra("fromact","home");
                    it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                    finish();
                    overridePendingTransition(0,0);
                }
               else if(act_names.equals("recent")) {
                    startActivity(it1);
                    it1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    overridePendingTransition(0,0);
                }else if(act_names.equals("topics")) {
                    startActivity(it3);
                    it1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    overridePendingTransition(0,0);
                }
                else if(act_names.equals("answered")) {
                    startActivity(it2);
                    it2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    overridePendingTransition(0,0);
                }
            }
        });
        tvnocurrpoll=(TextView)findViewById(R.id.nocurrpoll);
        btnFollow=(Button) findViewById(R.id.btnFollow);
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userCatStatus.equalsIgnoreCase("0"))
                {
                    updateUserFollowing(catid,"1");
                }else
                {
                    updateUserFollowing(catid,"0");
                }

            }
        });
        rv_current_polls = (RecyclerView)findViewById(R.id.pools_recycler);
        mLayoutManager = new LinearLayoutManager(CurrentPolls.this);
        rv_current_polls.setLayoutManager(mLayoutManager);
        rv_current_polls.setItemAnimator(new DefaultItemAnimator());
        catid=getIntent().getExtras().getString("catid");
        //Log.d("catid",catid);
        boolean loading = true;
        rv_current_polls.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx > 0)
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();

                }
            }
        });
        checkConnection();
        pla=new PollsAdapter();
        poolsList=new ArrayList<>();
    }
    PollsAdapter pla;
    List<PollPojo> poolsList;
    public class PollsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.poll_row, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return poolsList.size();
        }
        PollPojo listCategory;

        String tempCatId="";
        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCategory=poolsList.get(position);

         /*   if(tempCatId==listCategory.getCat_id())
            {
                holder.cv_category.setVisibility(View.GONE);
            }
            else
            {
                tempCatId=listCategory.getCat_id();
                holder.cv_category.setVisibility(View.VISIBLE);
            }*/


         /*   if(listCategory.isVisible())
                holder.catLayout.setVisibility(View.VISIBLE);
            else*/

            holder.catLayout.setVisibility(View.GONE);
            holder.tvpoll.setText(listCategory.getQuestion());
            holder.tvcat.setVisibility(View.GONE);
            holder.tvpollcategory.setText(listCategory.getCat_name());
            try {
                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(listCategory.getStart_time())));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Picasso.with(CurrentPolls.this)
                    .load(AppConst.imgurl+listCategory.getImage())
                    .transform(new RoundedSquareTransformation(10,0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivpoll);

            if(getIntent().getExtras().containsKey("fromViewAll"))
            {
               // toolbar.setTitle("Participated Polls("+listCategory.getCat_name()+")");
                if(listCategory.getCat_name().toString().contains("United India"))
                {
                    toolbar.setTitle("UIP");
                }
                else
                {
                    toolbar.setTitle(listCategory.getCat_name());
                }

               // toolbar.setTitle(listCategory.getCat_name());
            }
            else {


                if(getIntent().getExtras().containsKey("catname"))
                {
                    if(getIntent().getExtras().getString("catname").toString().contains("United India"))
                    {
                        toolbar.setTitle("UIP");
                    }
                    else
                    {
                        toolbar.setTitle(getIntent().getExtras().getString("catname").toString());
                    }
                    //toolbar.setTitle(getIntent().getExtras().getString("catname").toString());

                }else
                {
                    if(listCategory.getCat_name().toString().contains("United India"))
                    {
                        toolbar.setTitle("UIP");
                    }
                    else
                    {
                        toolbar.setTitle(listCategory.getCat_name());
                    }

                  //  toolbar.setTitle(listCategory.getCat_name());
                }

            }

        }
    }

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvpoll,tvviewAll,tvcat,tvCategPost;
        ImageView ivpoll;
        TextView tvpollcategory;
        CardView cv_category;
        LinearLayout catLayout;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvpoll=(TextView)itemView.findViewById(R.id.tvpoll);
            ivpoll=(ImageView)itemView.findViewById(R.id.ivpoll);
            tvpollcategory = (TextView)itemView.findViewById(R.id.tvpollcategory);
            tvviewAll=(TextView)itemView.findViewById(R.id.tvviewAll);
            tvCategPost=(TextView) itemView.findViewById(R.id.tvCategPost);
            catLayout=(LinearLayout) itemView.findViewById(R.id.cvlayout);

            tvviewAll.setVisibility(View.GONE);
            tvcat=(TextView)itemView.findViewById(R.id.tvCateg);
            cv_category=(CardView)itemView.findViewById(R.id.cv_category);


            Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(getAssets(),  "fonts/ssl.ttf");

            tvcat.setTypeface(custom_fontb);
            tvviewAll.setTypeface(custom_fontb);
            tvpollcategory.setTypeface(custom_fontb);
            tvpoll.setTypeface(custom_font);

            tvpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(act_names.equals("history"))
                    {
                        Intent intetn = new Intent(CurrentPolls.this, ArchivePollScreen.class);
                        intetn.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                        intetn.putExtra("from", "notActive");
                        startActivity(intetn);
                    }
                    else
                    {
                        Intent it=new Intent(CurrentPolls.this, PollScreen.class);
                        it.putExtra("from","active");
                        it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                        startActivity(it);
                    }
                }
            });
            ivpoll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(act_names.equals("history"))
                    {
                        Intent intetn = new Intent(CurrentPolls.this, ArchivePollScreen.class);
                        intetn.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                        intetn.putExtra("from", "notActive");
                        startActivity(intetn);
                    }
                    else
                    {
                        Intent it=new Intent(CurrentPolls.this, PollScreen.class);
                        it.putExtra("from","active");
                        it.putExtra("pollid",poolsList.get(getAdapterPosition()).getQuestion_id());
                        startActivity(it);
                    }
                }
            });
           /* tvviewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // M.showToast(getActivity(),poolsList.get(getAdapterPosition()).getCat_id().toString());
                    Intent it=new Intent(CurrentPolls.this, CurrentPolls.class);
                    it.putExtra("catid",poolsList.get(getAdapterPosition()).getCat_id());
                    startActivity(it);
                }
            });*/
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(M.getMyCountry(CurrentPolls.this).length()>1)
        {
            getPollCategoryData();
        }
        else
        {
         /*   M.showToast(CurrentPolls.this,"Select your location");*/
        }

        if(getIntent().getExtras().containsKey("catname"))
        {
            if(getIntent().getExtras().getString("catname").toString().contains("United India"))
            {
                toolbar.setTitle("UIP");
            }
            else
            {
                toolbar.setTitle(getIntent().getExtras().getString("catname").toString());
            }

        }

    }

    int currentpage=1;

    public void getPollCategoryData()
    {
        final int page=getCurrentpage();

        M.showLoadingDialog(CurrentPolls.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        if(act_names.equalsIgnoreCase("answered"))
        {
            mAuthenticationAPI.getAnsweredPollsAll(M.getID(CurrentPolls.this),catid,page,M.getMyCountryID(CurrentPolls.this),M.getMyStateID(CurrentPolls.this),M.getMyCityID(CurrentPolls.this),"true",new retrofit.Callback<List<PollPojo>>() {
                @Override
                public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                    if(pojo!=null){
                        if(pojo.size()>0) {

                            poolsList=pojo;
                            rv_current_polls.setAdapter(pla);
                            pla.notifyDataSetChanged();
                            //ArrayList<CategoryPojo>) pojo;
                            //lvcat.setAdapter(adapter);

                        }else{
                            new LovelyInfoDialog(CurrentPolls.this)
                                    .setTopColorRes(R.color.colorPrimary)
                                    .setTitle(R.string.app_name)
                                    .setMessage("There are no available Polls")
                                    .show();

                        }
                    }else{

                    }
                     M.hideLoadingDialog();
                    getUserFollowing();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    //Log.d("Favs","fail"+error.getMessage());
                    return;
                }
            });

        }
        else
        {
            mAuthenticationAPI.getPollsV3(M.getID(CurrentPolls.this),page,M.getMyCountryID(CurrentPolls.this),M.getMyStateID(CurrentPolls.this), M.getMyCityID(CurrentPolls.this),catid,act_names,new retrofit.Callback<List<PollPojo>>() {
                @Override
                public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                    if(pojo!=null){
                        if(pojo.size()>0) {

                            poolsList=pojo;
                            rv_current_polls.setAdapter(pla);
                            pla.notifyDataSetChanged();
                            //ArrayList<CategoryPojo>) pojo;
                            //lvcat.setAdapter(adapter);
                        }else{

                            rv_current_polls.setVisibility(View.GONE);
                            tvnocurrpoll.setVisibility(View.VISIBLE);
                        }
                    }else{
                        rv_current_polls.setVisibility(View.GONE);
                        tvnocurrpoll.setVisibility(View.VISIBLE);
                    }
                    M.hideLoadingDialog();
                    getUserFollowing();
                }

                @Override
                public void failure(RetrofitError error) {
                    M.hideLoadingDialog();
                    rv_current_polls.setVisibility(View.GONE);
                    tvnocurrpoll.setVisibility(View.VISIBLE);
                    //Log.d("Favs","fail"+error.getMessage());
                    return;
                }
            });
        }
    }

    public int getCurrentpage() {
        return currentpage;
    }

    public void setCurrentpage(int currentpage)
    {
        this.currentpage = currentpage;
    }



/*    private void getCurrentPoll() {
        final int page=getCurrentpage();
        userid=M.getID(CurrentPolls.this);
        if(page==1) {
            M.showLoadingDialog(CurrentPolls.this);
            list.clear();
        }
        //Log.d(TAG,userid+"-"+page);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPolls(userid,page,new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        loadmore=true;
                        lvpoll.setVisibility(View.VISIBLE);
                        tvnocurrpoll.setVisibility(View.GONE);
                        list .addAll(pojo);
                        updateList((ArrayList<PollPojo>) pojo);
                    }else if(page==1){
                        loadmore=false;
                        lvpoll.setVisibility(View.GONE);
                        tvnocurrpoll.setVisibility(View.VISIBLE);
                    }else{
                        loadmore=false;
                    }
                }else{
                    loadmore=false;
                }
                if(page==1)
                    M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                if(page==1)
                    M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());

                return;
            }
        });
    }*/

    private void getPollByCategory() {
        final int page=getCurrentpage();
        userid=M.getID(CurrentPolls.this);
        if(page==1)
            M.showLoadingDialog(CurrentPolls.this);

        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollsByCategory(userid,page,M.getMyCountryID(CurrentPolls.this),M.getMyStateID(CurrentPolls.this),M.getMyCityID(CurrentPolls.this),catid,new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        loadmore=true;
                        list.addAll(pojo);
                        updateList((ArrayList<PollPojo>) pojo);
                    }else if(page==1){
                        loadmore=false;
                        lvpoll.setVisibility(View.GONE);
                        tvnocurrpoll.setVisibility(View.VISIBLE);
                    }else{
                        loadmore=false;
                    }
                }else{
                    loadmore=false;
                }
                if(page==1)
                    M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                if(page==1)
                    M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                return;
            }
        });
    }

    public void updateList(ArrayList<PollPojo> datalist) {
        if (initialload){
            adapter=new PollAdapter(CurrentPolls.this,R.layout.poll_row,datalist);
            lvpoll.setAdapter(adapter);
            initialload = false;
        } else {
            adapter.addAll(datalist);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==android.R.id.home) {
            Intent it=new Intent(this,HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(act_names.equals("history"))
        {

            Intent it=new Intent(this,HistoryActivity.class);
            it.putExtra("fromact","home");
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
        else if(act_names.equals("recent"))
        {
           Intent it=new Intent(this,HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
        else if(act_names.equals("answered"))
        {

            Intent it=new Intent(this,HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);


        }
        else if(act_names.equals("topics")) {
            Intent it=new Intent(this,TopicsActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0,0);
        }
    }



    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();

    }

    String userCatStatus="0";
    public void getUserFollowing()
    {

        M.showLoadingDialog(CurrentPolls.this);
        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.getUserFollowing(M.getID(CurrentPolls.this), new retrofit.Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(List<CategoryRealmPojo> categoryRealmPojos, Response response) {

                for(CategoryRealmPojo mCategoryRealmPojo : categoryRealmPojos)
                {
                    if(mCategoryRealmPojo.getCat_id().equalsIgnoreCase(catid))
                    {
                        userCatStatus=mCategoryRealmPojo.getFollow();

                       if(mCategoryRealmPojo.getFollow().equalsIgnoreCase("1"))
                       {
                           btnFollow.setText("Unfollow");

                       }else{
                           btnFollow.setText("Follow");
                       }
                    }
                }

                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(CurrentPolls.this, "Oops, Something went wrong please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateUserFollowing(String catid,String status)
    {
        M.showLoadingDialog(CurrentPolls.this);
        CategoryAPI mCategoryAPI=APIService.createService(CategoryAPI.class);
        mCategoryAPI.updateUserFollowing(M.getID(CurrentPolls.this), catid, status, new retrofit.Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                //userupdated
                Toast.makeText(CurrentPolls.this, "Thank you, You have followed this topic. ", Toast.LENGTH_SHORT).show();
                getUserFollowing();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                getUserFollowing();
            }
        });
    }

}
