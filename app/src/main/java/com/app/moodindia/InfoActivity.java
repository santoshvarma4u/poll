package com.app.moodindia;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.adapter.OptionAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InfoActivity extends AppCompatActivity implements View.OnClickListener {

    protected MagicTextView tvdate;
    protected RelativeTimeTextView timestamp;
    protected Button btnParticipate,btnSuggestPoll;
    protected Button btnViewMaps;
    protected ViewPager container;
    protected LinearLayout lytmap;
    protected LinearLayout lloption;
    protected AppBarLayout appbar;
    protected CoordinatorLayout mainContent;
    ImageView iv;
    TextView tvque, tvinfo, tvotes;
    ListView lvoption;
    String question, que_image, que_info;
    PollOptionPojo pojo;
    String TAG = "Info", que = "", pollid;
    int votes = 0;
    String twittertag, youtubetag, chattag, start, end;
    Boolean isvoted;

    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_info);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(InfoActivity.this, R.color.colorPrimaryDark));
        que_info = getIntent().getExtras().getString("info");
        que_image = getIntent().getExtras().getString("image");
        question = getIntent().getExtras().getString("question");
        twittertag = getIntent().getExtras().getString("twittertag");
        youtubetag = getIntent().getExtras().getString("youtubetag");
        chattag = getIntent().getExtras().getString("chattag");
        start = getIntent().getExtras().getString("startdate");
        end = getIntent().getExtras().getString("enddate");
        isvoted = getIntent().getExtras().getBoolean("isvoted");
        pollid = getIntent().getExtras().getString("pollid");
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        viewPager = (ViewPager) findViewById(R.id.container);
viewPager.setOffscreenPageLimit(3);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pojo = AppConst.selpoll;
        initView();
   try {

            if(!start.isEmpty())
            {
                tvdate.setText("Posted on "+dtfmt.format(defaultfmt.parse(start)));
                Date dt=defaultfmt.parse(end);
                // txtExp.setVisibility(View.VISIBLE);
                RelativeTimeTextView v = (RelativeTimeTextView)findViewById(R.id.timestamp);
                v.setReferenceTime(dt.getTime());
            }
            else
            {
                tvdate.setText("");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (pojo != null) {
            que = pojo.getQuestion();
            que = question;
            //  tvque.setVisibility(View.INVISIBLE);
            tvque.setText(que);
            if (que_image != "") {
                Picasso.with(InfoActivity.this)
                        .load(AppConst.imgurl + que_image)//pojo.getImage()
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .into(iv);
            }
            tvinfo.setText(que_info);
            //tvinfo.setText(pojo.getInfo());

            if (getIntent().getExtras() == null) {
                for (Pollresult result : pojo.getPollresult()) {
                    int v = result.getOptioncount();
                    if (v != 0)
                        votes = votes + v;
                }
                if (pojo.getPollresult().size() > 0) {
                    if (votes > 0) {
                        tvotes.setVisibility(View.VISIBLE);
                        tvotes.setText("Votes:" + votes);
                    } else
                        tvotes.setVisibility(View.GONE);
                    lvoption.setVisibility(View.VISIBLE);
                    OptionAdapter adapter = new OptionAdapter(InfoActivity.this, (ArrayList<Pollresult>) pojo.getPollresult(), pojo.getIsvoted(), pojo.getSelected_option());
                    lvoption.setAdapter(adapter);
                } else {
                    lvoption.setVisibility(View.GONE);
                    tvotes.setVisibility(View.GONE);
                }
            } else {
                lvoption.setVisibility(View.GONE);
                tvotes.setVisibility(View.GONE);
                findViewById(R.id.lloption).setVisibility(View.GONE);
            }
        }
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);


        if (!isvoted)
            btnParticipate.setVisibility(View.VISIBLE);

        if(isvoted)
            btnViewMaps.setVisibility(View.VISIBLE);
        else
            btnViewMaps.setVisibility(View.GONE);



        if(getIntent().getExtras().containsKey("screen"))
            if(getIntent().getExtras().getString("screen").equalsIgnoreCase("ArchivePollScreen"))
            {
                btnParticipate.setVisibility(View.GONE);
                btnViewMaps.setVisibility(View.VISIBLE);
            }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new StoryFragment().newInstance(AppConst.imgurl + que_image, que_info.toString()), " Story");
        //adapter.addFragment(new PollsFragment(), " Tranding Polls");
        // adapter.addFragment(new PollsFragment(), " Answered Pools");
        adapter.addFragment(new TweetFragment().newInstance("pollid", twittertag), " Tweets");
        adapter.addFragment(new YoutubeFragment().newInstance("pollid", youtubetag), "Videos");

        if(isvoted)
        {
            //adapter.addFragment(new ChatFragment().newInstance("pollid", pollid), "Chat");
        }
        else if(getIntent().getExtras().containsKey("screen")){
            if(getIntent().getExtras().getString("screen").equalsIgnoreCase("ArchivePollScreen"))
            {
                btnParticipate.setVisibility(View.GONE);
                btnViewMaps.setVisibility(View.VISIBLE);
            }
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnParticipate) {
            finish();
        } else if (view.getId() == R.id.btnViewMaps) {
            Intent intent=new Intent(InfoActivity.this,ResultMapActivity.class);
            intent.putExtra("pollid",pollid);
            intent.putExtra("from",getIntent().getExtras().getString("from"));
            startActivity(intent);
            overridePendingTransition(0,0);
        }
    }

    private void initView() {

        tvdate = (MagicTextView) findViewById(R.id.tvdate);
        timestamp = (RelativeTimeTextView) findViewById(R.id.timestamp);
        btnParticipate = (Button) findViewById(R.id.btnParticipate);
        btnParticipate.setOnClickListener(InfoActivity.this);
        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        btnViewMaps.setOnClickListener(InfoActivity.this);
        tvotes = (MagicTextView) findViewById(R.id.tvotes);
        container = (ViewPager) findViewById(R.id.container);
        lytmap = (LinearLayout) findViewById(R.id.lytmap);
        lvoption = (ListView) findViewById(R.id.lvoption);
        lloption = (LinearLayout) findViewById(R.id.lloption);
        appbar = (AppBarLayout) findViewById(R.id.appbar);
        mainContent = (CoordinatorLayout) findViewById(R.id.main_content);
        btnSuggestPoll=(Button)findViewById(R.id.btnSuggestPoll);

        tvque = (TextView) findViewById(R.id.que);
        tvotes = (TextView) findViewById(R.id.tvotes);
        tvinfo = (TextView) findViewById(R.id.poll_info);
        iv = (ImageView) findViewById(R.id.imgpoll);
        lvoption = (ListView) findViewById(R.id.lvoption);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/ssr.ttf");
        Typeface custom_fontb = Typeface.createFromAsset(getAssets(), "fonts/ssb.ttf");
        Typeface custom_fontl = Typeface.createFromAsset(getAssets(), "fonts/ssl.ttf");

        tvque.setTypeface(custom_font);
        tvinfo.setTypeface(custom_fontb);
        tvotes.setTypeface(custom_font);

    btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(InfoActivity.this,SuggestPollActivity.class);
            startActivity(intent);
        }
    });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void initview() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_poll, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        else if (id == R.id.share) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(InfoActivity.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                }
                else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }

                }
            }
            else
            {
                sharepollkitkat();
            }

         /*   Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            //this is the text that will be shared
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n" + que + "\n" + "http://moodindia.in/p/" + pojo.getPollid()));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
            return true;*/
        }
        else if(id==R.id.report){
            Intent intent=new Intent(InfoActivity.this,ReportPollActivity.class);
            intent.putExtra("pollid",pollid);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        View v1 = getWindow().getDecorView().getRootView();
                        v1.setDrawingCacheEnabled(true);
                        Bitmap myBitmap = v1.getDrawingCache();
                        saveBitmap(myBitmap);
                    }
                    else
                    {
                        sharepollkitkat();
                    }

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(InfoActivity.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }
    public void saveBitmap(Bitmap bitmap) {
        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "Pictures/screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            sharepoll(filePath);
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }
    public void sharepoll(String path) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("image/gif");
        Uri myUri = Uri.parse("file://" + path);
        sendIntent.putExtra(Intent.EXTRA_STREAM, myUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll." +
                " Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid+ " \n \n Download MoodIndia \n https://play.google.com/store/apps/details?id=com.app.moodindia"));

        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }
    public void sharepollkitkat() {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
        sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n"+que+" \n http://moodindia.in/p/"+pollid));
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
