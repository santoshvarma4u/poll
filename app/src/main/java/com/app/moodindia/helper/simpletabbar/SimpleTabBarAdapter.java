package com.app.moodindia.helper.simpletabbar;

/**
 * Created by SantoshT on 11/29/2017.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public abstract class SimpleTabBarAdapter extends FragmentPagerAdapter {

    public SimpleTabBarAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public abstract int getCount();

    public abstract Fragment getItem(int position);

    public abstract CharSequence getPageTitle(int position);

    public abstract int getColorResource(int position);

    public abstract int getTextColorResource(int position);

    public abstract int getIconResource(int position);
}