package com.app.moodindia.helper;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by SantoshT on 10/25/2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    //interface
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();

        PackageManager pm = context.getPackageManager();
        int hasPerm = pm.checkPermission(
                Manifest.permission.READ_SMS,
                context.getPackageName());
        if (hasPerm != PackageManager.PERMISSION_GRANTED) {

            Object[] pdus = (Object[]) data.get("pdus");

            for(int i=0;i<pdus.length;i++){
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

                String sender = smsMessage.getDisplayOriginatingAddress();
                //Check the sender to filter messages which we require to read

                String messageBody = smsMessage.getMessageBody();

                if( messageBody !=null && messageBody.contains("your verification code for"))
                {
                    //Pass the message text to interface
                    mListener.messageReceived(messageBody);
                }

            }
        }


    }
    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}