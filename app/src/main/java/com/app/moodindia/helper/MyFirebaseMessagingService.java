package com.app.moodindia.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Patterns;

import com.app.moodindia.HomeActivity;
import com.app.moodindia.R;
import com.app.moodindia.model.M;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;

//import br.com.goncalves.pugnotification.notification.PugNotification;
import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by ReevaLaptop on 6/29/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
     //   //Log.d(TAG, "message: " + remoteMessage.getNotification().getTitle());
      //  //Log.d(TAG, "From: " + remoteMessage.getFrom());remoteMessage
        //Log.d(TAG, "message: " + remoteMessage.getNotification().getTitle());
        //Log.d(TAG, "From: " + remoteMessage.getFrom());
        //Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
     //   String message = remoteMessage.getNotification().getBody();

        if(remoteMessage.getData().size()>0)
        {
            try{
                        if(M.ispushON(this))
                        {


                            String msg =  remoteMessage.getData().get("body").toString();
                            String pollid=remoteMessage.getData().get("title").toString();
                            String joinUrl="",groupid = "";
                            if(remoteMessage.getData().containsKey("joinurl"))
                            {
                                joinUrl =remoteMessage.getData().get("joinurl").toString();
                            }

                            if(remoteMessage.getData().containsKey("groupid"))
                            {
                                groupid =remoteMessage.getData().get("groupid").toString();
                            }
                            sendNotification("Mood India",msg,pollid,joinUrl,groupid);

                            Log.d(TAG, "Notification Message Body: " +pollid +joinUrl+groupid);
                        }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(remoteMessage.getNotification()!=null)
        {
            if(M.ispushON(this) && remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody()!=null )
                sendNotification("Mood India",remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle(),"","");
        }

        //Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String title,String messageBody,String pollId,String joinUrl,String groupid) {
        Intent intent = new Intent(this, HomeActivity.class);
        Bundle b=new Bundle();
        b.putString("message",messageBody);
        b.putString("from","push");

        intent.putExtra("message",messageBody);
        intent.putExtra("from","push");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if(joinUrl.length()>0)
        {
            if(groupid.length() > 0)
            {
                b.putString("groupid",groupid);
                b.putString("uidforchat",groupid);
                b.putString("togroups","true");

                intent.putExtra("groupid",groupid);
                intent.putExtra("uidforchat",groupid);
                intent.putExtra("togroups","true");
            }
            //link found fetch pollid
        }
        else
        {
            intent.putExtra("notipollid",pollId);
            b.putString("notipollid",pollId);
        }

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

       /* PugNotification.with(this)
                .load()
                .identifier(m)
                .title("Mood India")
                .message(messageBody)
                .smallIcon(R.drawable.miwhite)
                .largeIcon(R.drawable.miwhite)
                .flags(Notification.DEFAULT_ALL)
                .button(R.drawable.miwhite, "Mood India", pendingIntent)
                .sound(defaultSoundUri)
                .autoCancel(true)
                .simple()
                .build();*/

        Notification noti = new Notification.Builder(this)
                .setContentTitle("Mood India")
                .setSmallIcon(R.drawable.miwhite)
                .setContentText(messageBody)
                .setTicker("Mood India")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setExtras(b)
                .setContentIntent(pendingIntent)
                .addExtras(b)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("Mood_India",
                    "Mood India",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);
            channel.setLightColor(Color.parseColor("#233446"));
            channel.enableLights(true);

            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 , noti);
    }
}