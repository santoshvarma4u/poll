package com.app.moodindia.helper.youtubepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;


/**
 * Created by Schemax on 17-10-2017.
 */

public class PlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private YouTubePlayerView playerView;
    Button pick;

    Bundle keys;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_player);
        keys = getIntent().getExtras();
        playerView = (YouTubePlayerView)findViewById(R.id.player_view);


     playerView.initialize(YouTubeConnector.KEY, PlayerActivity.this);
        pick=(Button)findViewById(R.id.pick);
        pick.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

               Intent returnIntent = new Intent();
               returnIntent.putExtra("url",keys.getString("url"));
               returnIntent.putExtra("desc",keys.getString("desc"));
               returnIntent.putExtra("title",keys.getString("title"));
               returnIntent.putExtra("VIDEO_URL",keys.getString("VIDEO_URL"));
               setResult(Activity.RESULT_OK,returnIntent);
               finish();
            }
        });
    }

    @Override
    public void onInitializationFailure(Provider provider,
                                        YouTubeInitializationResult result) {
        Toast.makeText(this, getString(R.string.failed), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player,
                                        boolean restored) {
        if(!restored){
            player.cueVideo(getIntent().getStringExtra("VIDEO_ID"));
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private static final int RECOVERY_REQUEST = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(AppConst.YOUTUBE_API_KEY, this);
        }
    }

   protected YouTubePlayer.Provider getYouTubePlayerProvider() {
       return playerView;
   }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), YoutubePicker.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
