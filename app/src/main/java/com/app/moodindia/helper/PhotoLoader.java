package com.app.moodindia.helper;

/**
 * Created by SantoshT on 1/8/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
public class PhotoLoader implements Target {
    private final String name;
    private ImageView imageView;
    private Context ctx;
    public PhotoLoader(String name, ImageView imageView,Context cts) {
        this.name = name;
        this.imageView = imageView;
        this.ctx=cts;
    }
    @Override
    public void onPrepareLoad(Drawable arg0) {
    }
    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom arg1) {
        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + name);
        try {
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
            ostream.close();
            imageView.setImageBitmap(bitmap);
            Toast.makeText(ctx, "QR Image saved in gallery ("+name+")", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBitmapFailed(Drawable arg0) {
    }
}