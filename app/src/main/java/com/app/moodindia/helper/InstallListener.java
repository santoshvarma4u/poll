package com.app.moodindia.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.app.moodindia.model.M;

/**
 * Created by SantoshT on 10/6/2017.
 */

public class InstallListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String rawReferrerString = intent.getStringExtra("referrer");
        if(rawReferrerString != null) {
            Log.i("MyApp", "Received the following intent " + rawReferrerString);
            Toast.makeText(context, rawReferrerString, Toast.LENGTH_SHORT).show();
            if(rawReferrerString.length()>1) {
                M.setRefId(context, rawReferrerString);
            }
        }
        else
        {
            Toast.makeText(context, "Installed", Toast.LENGTH_SHORT).show();
        }

    }
}
