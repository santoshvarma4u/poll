package com.app.moodindia.helper;

import com.app.moodindia.model.PaytmResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SantoshT on 10/23/2017.
 */

public class JSONParser {
    public static ArrayList<PaytmResponse> mMobiles = new ArrayList<>();

    public static PaytmResponse parseFeed(JSONObject obj) {

        try {
            PaytmResponse mobile = new PaytmResponse();
            mobile.setOrderId(obj.getString("orderId"));
            mobile.setResponse(obj.getString("response"));
            mobile.setStatus(obj.getString("status"));
            mobile.setMetadata(obj.getString("metadata"));

            return mobile;
        } catch (JSONException e1) {
            e1.printStackTrace();
            return null;
        }
    }
}
