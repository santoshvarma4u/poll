package com.app.moodindia.helper;

/**
 * Created by SantoshT on 10/25/2017.
 */

public interface SmsListener {

    public void messageReceived(String messageText);
}
