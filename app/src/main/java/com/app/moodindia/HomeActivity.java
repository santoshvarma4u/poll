package com.app.moodindia;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.transition.Explode;
import android.transition.Fade;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PrivatePollPojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.ui.ActivityLogs.Logs_activity;
import com.app.moodindia.ui.privatepolls.AcceptPollActivity;
import com.app.moodindia.ui.privatepolls.AddPollActivity;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoActivity;
import com.app.moodindia.ui.privatepolls.Groups.MyGroups;
import com.app.moodindia.ui.privatepolls.PrivatePollScreen;
import com.app.moodindia.ui.topics.TopicsActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.MyApplication;
import com.app.moodindia.webservices.PollAPI;
import com.app.moodindia.webservices.PrivatePollAPI;
import com.bluejamesbond.text.DocumentView;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hlab.fabrevealmenu.listeners.OnFABMenuSelectedListener;
import com.jaredrummler.android.device.DeviceName;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.sa90.materialarcmenu.ArcMenu;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;
import com.zplesac.connectionbuddy.models.ConnectivityStrength;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity implements OnFABMenuSelectedListener,ConnectivityReceiver.ConnectivityReceiverListener, ConnectivityChangeListener {

        SmartTabLayout tabLayout;
        ViewPager viewPager;
        TextView tv_country,tv_state,tv_city,tv_cityup,tv_lowconnection;
        FloatingActionButton fab;
        ImageView tv_cityupicn,iv_scan_qr,tv_logo;
        ImageView priate_poll_search;
        LinearLayout lytlownetwork;
        Button btn_join_poll_by_id,btn_sign_a_pitition,btn_join_group_by_id;
        ArcMenu fabRevealMenu;
        ImageView homepageHeaderImage;
        KenBurnsView img1;
        TextView networkclose,tv_invite;
        Button btn_qrcode,btn_suggest;

        CoordinatorLayout main_content;
        RelativeLayout rl_qrview;

        SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");

        FloatingActionButton archive,categories,profile,notifications,privatepolls;
        String deviceid="",deviceName="";
        boolean isConnected;
        FrameLayout llcorusel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_tabbed);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

//        getSupportActionBar().setElevation(0);
            //window.setStatusBarColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark));
            deviceid = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
            deviceName = DeviceName.getDeviceName();
            tabLayout=(SmartTabLayout)findViewById(R.id.tablayout);
            lytlownetwork=(LinearLayout)findViewById(R.id.lytlownetwork);
            //tabLayout.setTabMode(TabLayout.MODE_FIXED);
            viewPager = (ViewPager) findViewById(R.id.container);
            setupViewPager(viewPager);
            tabLayout.setViewPager(viewPager);
            llcorusel=(FrameLayout) findViewById(R.id.llcorusel);
            rl_qrview=(RelativeLayout) findViewById(R.id.rl_qrview);
            tv_country=(TextView)findViewById(R.id.txtCountry);
                tv_invite=(TextView)findViewById(R.id.tv_invite);
             networkclose=(TextView)findViewById(R.id.networkclose);
            tv_lowconnection=(TextView)findViewById(R.id.tv_lowconnection);
            btn_join_poll_by_id=(Button) findViewById(R.id.btn_join_poll_by_id);
            btn_join_group_by_id=(Button) findViewById(R.id.btn_join_group_by_id);
            btn_suggest=(Button) findViewById(R.id.btn_suggest);
            btn_sign_a_pitition=(Button) findViewById(R.id.btn_sign_a_pitition);
            tv_state=(TextView)findViewById(R.id.txtState);
            tv_city=(TextView)findViewById(R.id.txtCity);
            tv_cityup=(TextView)findViewById(R.id.txtCityup);
            tv_cityupicn=(ImageView)findViewById(R.id.imageView5);
            tv_logo=(ImageView)findViewById(R.id.imageView4);
            iv_scan_qr=(ImageView)findViewById(R.id.iv_scan_qr);
             btn_qrcode=(Button) findViewById(R.id.btn_qrcode);
            homepageHeaderImage=(ImageView)findViewById(R.id.homepageHeaderImage);
            img1=(KenBurnsView)findViewById(R.id.img1);
            priate_poll_search=(ImageView)findViewById(R.id.priate_poll_search);
            priate_poll_search.setVisibility(View.GONE);
            fabRevealMenu=(ArcMenu) findViewById(R.id.fabMenu);
            archive=(FloatingActionButton)findViewById(R.id.menu_archive);
            categories=(FloatingActionButton)findViewById(R.id.menu_categories);
            profile=(FloatingActionButton)findViewById(R.id.menu_profile);
            notifications=(FloatingActionButton)findViewById(R.id.menu_messages);
            //privatepolls=(FloatingActionButton)findViewById(R.id.menu_private_polls);
            privatepolls=(FloatingActionButton)findViewById(R.id.menu_private_polls);
            main_content=(CoordinatorLayout)findViewById(R.id.main_content);
        ShortcutBadger.removeCount(this);

        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable(){
            @Override
            public void run() {
                try {
                    Picasso.with(HomeActivity.this).load(AppConst.backgroundImage).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                            Bitmap afterBlur=fastblur(bitmap,1,15);
                            img1.setImageBitmap(afterBlur);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if(M.getPhone(HomeActivity.this).equalsIgnoreCase("1"))
        {
            tv_invite.setVisibility(View.GONE);
        }
        tv_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final DialogPlus notificationDialog = DialogPlus.newDialog(HomeActivity.this)
                        .setGravity(Gravity.CENTER)
                        .setContentHolder(new ViewHolder(R.layout.custom_layout_invite_alert))
                        .setCancelable(true)
                        .create();


                View view=notificationDialog.getHolderView();

                final TextView tvNoticont=(TextView) view.findViewById(R.id.tv_noticont);
                final TextView tv_notification=(TextView) view.findViewById(R.id.tv_notification);
                final CheckBox cb_dontsee=(CheckBox) view.findViewById(R.id.cb_dontsee);
                final DocumentView dv=(DocumentView)view.findViewById(R.id.blogText) ;
                Typeface type = Typeface.createFromAsset(getAssets(),"fonts/ssr.ttf");
                dv.getDocumentLayoutParams().setTextTypeface(type);


                tvNoticont.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onInviteClicked();
                      /*  if(cb_dontsee.isChecked())
                        {
                            M.setNoPollAnswerPopup("1",PollScreen.this);
                        }
                        Intent it=new Intent(PollScreen.this,PollScreen.class);
                        it.putExtra("pollid",pollid);
                        it.putExtra("from","active");
                        finish();
                        startActivity(it);
                        notificationDialog.dismiss();*/
                    }
                });
                notificationDialog.show();

            }
        });
        btn_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivityForResult(new Intent(HomeActivity.this,ScanQRActivtiy.class),9870);
            }
        });

        btn_suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,SuggestPollActivity.class));
            }
        });
            SpaceNavigationView spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
            spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
            spaceNavigationView.addSpaceItem(new SpaceItem("Profile", R.drawable.ic_person_white));
            spaceNavigationView.addSpaceItem(new SpaceItem("Categories", R.drawable.ic_search_white_36dp));
            spaceNavigationView.addSpaceItem(new SpaceItem("Notifications", R.drawable.alarm));
            spaceNavigationView.addSpaceItem(new SpaceItem("Archives", R.drawable.archive));
            spaceNavigationView.showIconOnly();

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

              /*  Intent intent=new Intent(HomeActivity.this,PrivatePolls.class);
                startActivity(intent);*/
                startActivity(new Intent(HomeActivity.this,AddPollActivity.class));
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if(itemName.equalsIgnoreCase("Categories"))
                {
                    Intent intent=new Intent(HomeActivity.this,TopicsActivity.class);
                    startActivity(intent);
                }
                 else if(itemName.equalsIgnoreCase("Notifications"))
                {
                   // Intent intent=new Intent(HomeActivity.this,MessagesActivity.class);
                    Intent intent=new Intent(HomeActivity.this,Logs_activity.class);
                    startActivity(intent);
                }
                 else if(itemName.equalsIgnoreCase("Archives"))
                {
                    Intent intent=new Intent(HomeActivity.this,HistoryActivity.class);
                    intent.putExtra("fromact","home");
                    startActivity(intent);
                }
                else
               {
                   Intent intent=new Intent(HomeActivity.this,SettingActivity.class);
                   startActivity(intent);
                   finish();
               }
              //  Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
              //  Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
                if(itemName.equalsIgnoreCase("Categories"))
                {
                    Intent intent=new Intent(HomeActivity.this,TopicsActivity.class);
                    startActivity(intent);
                }
                else if(itemName.equalsIgnoreCase("Notifications"))
                {
                    //Intent intent=new Intent(HomeActivity.this,MessagesActivity.class);
                    Intent intent=new Intent(HomeActivity.this,Logs_activity.class);
                    startActivity(intent);
                }
                else if(itemName.equalsIgnoreCase("Archives"))
                {
                    Intent intent=new Intent(HomeActivity.this,HistoryActivity.class);
                    intent.putExtra("fromact","home");
                    startActivity(intent);
                }
                else
                {
                    Intent intent=new Intent(HomeActivity.this,SettingActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setEnterTransition(new Explode().setDuration(500));

            getWindow().setExitTransition(new Fade().setDuration(500));
        }


        final ExpandableRelativeLayout expandableLayout
                = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
       // final ExpandableRelativeLayout expandableLayoutbar
        //        = (ExpandableRelativeLayout) findViewById(R.id.expandableLayoutbar);
        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);

    // toggle expand, collapse
           expandableLayout.toggle();
    // expand
    // collapse
           expandableLayout.collapse();

         profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(HomeActivity.this,SettingActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            archive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(HomeActivity.this,HistoryActivity.class);
                    intent.putExtra("fromact","home");
                    startActivity(intent);
                }
            });
            notifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent intent=new Intent(HomeActivity.this,MessagesActivity.class);
                    Intent intent=new Intent(HomeActivity.this,Logs_activity.class);
                    startActivity(intent);
                }
            });
            categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(HomeActivity.this,TopicsActivity.class);
                    startActivity(intent);
                }
            });

         /*    privatepolls.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(HomeActivity.this,PrivatePolls.class);
                    startActivity(intent);
                }
            });
*/
            tv_country.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* Intent intent=new Intent(HomeActivity.this,SelectCountry.class);
                    startActivityForResult(intent,1);*/
                }
            });
            tv_state.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tv_country.getText().toString().equalsIgnoreCase("Country")) {
                        M.showToast(HomeActivity.this,"Please Select Country First");
                    }
                    else {
                        Intent intent=new Intent(HomeActivity.this,SelectState.class);
                        intent.putExtra("countryid",M.getMyCountryID(HomeActivity.this));
                        startActivityForResult(intent,2);
                    }
                }
            });
            tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_state.getText().toString().equalsIgnoreCase("State")) {
                    M.showToast(HomeActivity.this,"Please Select State First");
                }
                else {
                    Intent intent=new Intent(HomeActivity.this,SelectCity.class);
                    intent.putExtra("stateid",M.getMyStateID(HomeActivity.this));
                    startActivityForResult(intent,3);
                }
            }
        });

        tv_cityup.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                        if(expandableLayout.isExpanded())
                        {
                            llcorusel.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            llcorusel.setVisibility(View.GONE);
                        }
                    }
                }

        );
        tv_cityupicn.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                        if(expandableLayout.isExpanded())
                        {
                           llcorusel.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            llcorusel.setVisibility(View.GONE);
                        }
                    }
                }
        );

        tv_logo.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                     //   expandableLayoutbar.toggle();

                    }
                }
        );

        networkclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lytlownetwork.setVisibility(View.GONE);
            }
        });
        //Log.i("length",M.getMyState(HomeActivity.this)+"");
        if(M.getMyCountryID(HomeActivity.this).length()>1){
            tv_country.setText(M.getMyCountry(HomeActivity.this));
        }
        else {
            M.setMyCountry("India",HomeActivity.this);
            M.setMyCountryID("101",HomeActivity.this);
            tv_country.setText(M.getMyCountry(HomeActivity.this));
            Intent i = new Intent(HomeActivity.this, HomeActivity.class);  //your class
            startActivity(i);
        }
        if(M.getMyState(HomeActivity.this).length()>1)
        {
            tv_state.setText(M.getMyState(HomeActivity.this));
            tv_cityup.setText(M.getMyState(HomeActivity.this));
        }
        if(M.getMyCity(HomeActivity.this).length()>1)
        {
            tv_city.setText(M.getMyCity(HomeActivity.this));
            tv_cityup.setText(M.getMyCity(HomeActivity.this));
        }



        final DialogPlus dialog = DialogPlus.newDialog(HomeActivity.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_join_poll))
                .setCancelable(true)
                .create();

        final DialogPlus groupJoinDialog = DialogPlus.newDialog(HomeActivity.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_group_join))
                .setCancelable(true)
                .create();


        final DialogPlus notificationDialog = DialogPlus.newDialog(HomeActivity.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_push_notification))
                .setCancelable(true)
                .create();


        View groupView = groupJoinDialog.getHolderView();

        View view=dialog.getHolderView();

        View notiView=notificationDialog.getHolderView();

        final TextView tvNotiMessage=(TextView) notiView.findViewById(R.id.tv_notification);
        final TextView tvNoticont=(TextView) notiView.findViewById(R.id.tv_noticont);
        final TextView tvNotiCancel=(TextView)notiView.findViewById(R.id.tv_noticancel);
        final TextView tvNotiPollid=(TextView)notiView.findViewById(R.id.tv_noti_pollid);

//        Log.i("Loded",getIntent().getExtras().containsKey("from")+"");

        if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("from"))
        {

            Log.i("Loded","from push ey");
            if(getIntent().getExtras().getString("from").equalsIgnoreCase("push") && getIntent().getExtras().containsKey("notipollid"))
            {
                tvNotiMessage.setText(getIntent().getExtras().getString("message").toString());
                tvNotiPollid.setText(getIntent().getExtras().getString("notipollid").toString());
                notificationDialog.show();
            }
            else if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("togroups") )
            {

                Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                it.putExtra("uidforchat",getIntent().getExtras().getString("groupid") );
                startActivity(it);
            }
            else if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("uidforchat") && getIntent().getExtras().getString("from").equalsIgnoreCase("push"))
            {
                Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                it.putExtra("uidforchat",getIntent().getExtras().getString("uidforchat") );
                startActivity(it);
            }

        }

        tvNoticont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("uidforchat"))
                {
                    Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                    it.putExtra("uidforchat",getIntent().getExtras().getString("uidforchat") );
                    startActivity(it);
                }
               else if(getIntent().getExtras() !=null && getIntent().getExtras().containsKey("notipollid"))
                {
                    if(getIntent().getExtras().getString("message").toString().toLowerCase().contains("community poll"))
                    {
                        Toast.makeText(HomeActivity.this,getIntent().getExtras().get("notipollid").toString(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(HomeActivity.this, PrivatePollScreen.class);
                        intent.putExtra("pollid",getIntent().getExtras().get("notipollid").toString());
                        intent.putExtra("upollid",getIntent().getExtras().get("notipollid").toString());
                        intent.putExtra("from","active");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        Intent intent = new Intent(HomeActivity.this, PollScreen.class);
                        intent.putExtra("pollid",getIntent().getExtras().get("notipollid").toString());
                        intent.putExtra("from","active");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
                else
                {
                    notificationDialog.dismiss();
                }
            }
        });

        tvNotiCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationDialog.dismiss();
            }
        });

        final EditText et_join_poll=(EditText)view.findViewById(R.id.et_join_poll_id);
        final TextView tvJoinDialog=(TextView) view.findViewById(R.id.tvJoinDialog);
        final TextView tvCancelJoinDialog=(TextView)view.findViewById(R.id.tvCancelJoinDialog);
        et_join_poll.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                switch(result) {
                    case EditorInfo.IME_ACTION_DONE:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
                        break;
                }
                return false;
            }
        });
        et_join_poll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);
            }
        });
        tvJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_join_poll.getText().toString().length() > 0)
                {
                    if(!M.getID(HomeActivity.this).isEmpty())
                    {
                        if (et_join_poll.getText().toString().matches("[0-9]+") && et_join_poll.getText().toString().length() > 0) {
                            //only numbers means public polls
                            Intent it=new Intent(HomeActivity.this, PollScreen.class);
                            it.putExtra("pollid",et_join_poll.getText().toString());
                            it.putExtra("from","active");
                            startActivity(it);
                        }
                        else
                        {
                            validateJoinPoll(et_join_poll.getText().toString(),M.getID(HomeActivity.this).toString());
                        }

                    }
                    else {
                        Toast.makeText(HomeActivity.this, "You need to login to join in personal polls!", Toast.LENGTH_SHORT).show();
                    }

                  /*  Intent intent=new Intent(HomeActivity.this, AcceptPollActivity.class);
                    intent.putExtra("poll_id",et_join_poll.getText().toString());
                    startActivity(intent);*/
                }
                else {
                    et_join_poll.setError("Enter a valid personal poll id");
                }

            }
        });

        tvCancelJoinDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog.isShowing())
                    dialog.dismiss();

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_join_poll.getWindowToken(), 0);

            }
        });

        btn_join_poll_by_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        btn_join_group_by_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupJoinDialog.show();
            }
        });

        final EditText et_group_join_id=(EditText)groupView.findViewById(R.id.et_group_join_id);
        final TextView tvJoinGroupDialog=(TextView) groupView.findViewById(R.id.tvJoinGroupDialog);
        final TextView tvCancelGroupDialog=(TextView)groupView.findViewById(R.id.tvCancelGroupDialog);

        et_group_join_id.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(et_group_join_id.getText().length()>0)
                {
                    Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                    it.putExtra("uidforchat",et_group_join_id.getText().toString());
                    startActivity(it);
                }
                return false;
            }
        });

        tvJoinGroupDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_group_join_id.getText().length()>0)
                {
                    Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                    it.putExtra("uidforchat",et_group_join_id.getText().toString());
                    startActivity(it);
                }
            }
        });
        tvCancelGroupDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupJoinDialog.dismiss();
            }
        });

        btn_sign_a_pitition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //petitions cat id : 19
                Intent intent=new Intent(HomeActivity.this,CurrentPolls.class);
                intent.putExtra("act_name","recent");
                intent.putExtra("catid","19");
                intent.putExtra("catname","United India Petitions");
                startActivity(intent);
            }
        });
        priate_poll_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* new LovelyTextInputDialog(HomeActivity.this, R.style.EditTextTintTheme)
                        .setTopColorRes(R.color.colorPrimary)
                        .setTitle("Mood India")
                        .setMessage("Please Enter Poll Id")
                      //  .setIcon(R.drawable.miwhitesmall)
                        .setInputFilter("Enter valid poll Id", new LovelyTextInputDialog.TextFilter() {
                            @Override
                            public boolean check(String text) {
                                return text.matches("\\w+");
                            }
                        })
                        .setInputType(InputType.TYPE_CLASS_NUMBER)
                        .setConfirmButton("Confirm", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                            @Override
                            public void onTextInputConfirmed(String text) {

                              //  Toast.makeText(HomeActivity.this, text, Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(HomeActivity.this, AcceptPollActivity.class);
                                intent.putExtra("poll_id",text);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .show();*/
            }
        });


        checkConnection();
        if(M.getID(HomeActivity.this) !=null && !M.getID(HomeActivity.this).equals("0"))
        {
             updatefcm();
             verifyUserReferral();
//            if(M.getRefId(HomeActivity.this)!=null && M.getRefId(HomeActivity.this).length()>0)
//            {
//                verifyUserReferral();
//            }
//            else
//            {
//                Log.i("fcm","infcm");
//                updatefcm();
//            }
        }

    }


    private void validateJoinPoll(final String pollid, String userid)
    {
        M.showLoadingDialog(HomeActivity.this);
        PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getAcceptPoll(userid, pollid, new Callback<List<StatusPojo>>() {
            @Override
            public void success(List<StatusPojo> statusPojo, Response response) {
                if(statusPojo.get(0).getStatus().equalsIgnoreCase("A"))
                {
                    M.hideLoadingDialog();
                    //Poll Accepted
                    //Redirect to poll screen
                    Toast.makeText(HomeActivity.this, "Poll already accepted", Toast.LENGTH_SHORT).show();

                    Intent intent=new Intent(HomeActivity.this,PrivatePollScreen.class);
                    intent.putExtra("from","active");
                    intent.putExtra("pollid",pollid);
                    intent.putExtra("upollid",pollid);
                    startActivity(intent);
                }
                else if(statusPojo.get(0).getStatus().equalsIgnoreCase("NA"))
                {
                    M.hideLoadingDialog();
                    getPollDetails(pollid);
                }
                else {
                    M.hideLoadingDialog();
                    Toast.makeText(HomeActivity.this, "Invalid Poll ID, Try Correct Poll id", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(HomeActivity.this, "Unknown Error,Please try later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getPollDetails(final String pollid)
    {
        M.showLoadingDialog(HomeActivity.this);
        final PrivatePollAPI mPrivatePollAPI = APIService.createService(PrivatePollAPI.class);
        mPrivatePollAPI.getPrivatePollById(pollid, 1, new Callback<List<PrivatePollPojo>>() {
            @Override
            public void success(List<PrivatePollPojo> privatePollPojos, Response response) {

                if(privatePollPojos.size()>0)
                {
                    M.hideLoadingDialog();

                    Intent intent=new Intent(HomeActivity.this, AcceptPollActivity.class);
                    intent.putExtra("poll_id",pollid);
                    intent.putExtra("upollid",pollid);
                    intent.putExtra("originalpollid",privatePollPojos.get(0).getUid());
                    startActivity(intent);

                }
                else
                {
                    M.hideLoadingDialog();
                    Toast.makeText(HomeActivity.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                Toast.makeText(HomeActivity.this, "Invalid Poll Id, Try another poll", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PollsFragment(), " New");
        adapter.addFragment(new TrandingPollsFragment(), " Top");
        adapter.addFragment(new ResultPools(), "Voted");
       // adapter.addFragment(new MyPrivatePolls(), "Created");
        adapter.addFragment(new MyGroups(), "Groups");
        viewPager.setAdapter(adapter);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode == RESULT_OK)
        {
            //country call back
            M.setMyCountry(data.getExtras().getString("CountryName"),HomeActivity.this);
            M.setMyCountryID(data.getExtras().getString("CountryId"),HomeActivity.this);
            tv_country.setText(data.getExtras().getString("CountryName"));
            tv_state.setText("Select State");
            tv_city.setText("Select City");
            M.setMyStateID("0",HomeActivity.this);
            M.setMyState("0",HomeActivity.this);
            M.setMyCity("0",HomeActivity.this);
            M.setMyCityID("0",HomeActivity.this);
            Intent i = new Intent(HomeActivity.this, HomeActivity.class);  //your class
            startActivity(i);
            finish();
            //tv_state.performClick();
            //M.showToast(HomeActivity.this,data.getExtras().getString("CountryId").toString());
        }
        else if(requestCode==2 && resultCode == RESULT_OK)
        {//state call back
            if(data.getExtras().getString("StateName").toString().equals("0")&&data.getExtras().getString("StateId").toString().equals("0"))
            {
                M.setMyStateID("0",HomeActivity.this);
                M.setMyState("0",HomeActivity.this);
                M.setMyCity("0",HomeActivity.this);
                M.setMyCityID("0",HomeActivity.this);
                tv_state.setText("State");
                tv_city.setText("City");
                tv_cityup.setText("Regional Polls");
            }
            else{
            M.setMyState(data.getExtras().getString("StateName").toString(),HomeActivity.this);
            M.setMyStateID(data.getExtras().getString("StateId").toString(),HomeActivity.this);
            tv_state.setText(data.getExtras().getString("StateName").toString());
            tv_city.setText("Select City");
            tv_cityup.setText(data.getExtras().getString("StateName").toString());
            M.setMyCityID("0",HomeActivity.this);
            M.setMyCity("0",HomeActivity.this);
            Intent i = new Intent(HomeActivity.this, HomeActivity.class);  //your class
            startActivity(i);
            finish();
            }
        }
        else if(requestCode==3 && resultCode == RESULT_OK) {//city call back

            if (data.getExtras().getString("CityName").toString().equals("0") && data.getExtras().getString("CityId").toString().equals("0")) {
                M.setMyCity("0", HomeActivity.this);
                M.setMyCityID("0", HomeActivity.this);
                tv_city.setText("City");
                tv_cityup.setText(M.getMyState(HomeActivity.this));
            } else {
                M.setMyCity(data.getExtras().getString("CityName").toString(), HomeActivity.this);
                M.setMyCityID(data.getExtras().getString("CityId").toString(), HomeActivity.this);
                tv_city.setText(data.getExtras().getString("CityName").toString());
                tv_cityup.setText(data.getExtras().getString("CityName").toString());

                Intent i = new Intent(HomeActivity.this, HomeActivity.class);  //your class
                startActivity(i);
                finish();
            }
        }else if(requestCode==9870 && resultCode == RESULT_OK) {//city call back

            if (data.getExtras().getString("type").equalsIgnoreCase("cats")) {
                   String catid =  data.getExtras().getString("catid").toString();
                if(catid.length()>0)
                {
                    Intent intent=new Intent(HomeActivity.this,CurrentPolls.class);
                    intent.putExtra("act_name","recent");
                    intent.putExtra("catid",catid);
                    intent.putExtra("catname",catid);
                    startActivity(intent);
                }

            }
            else  if (data.getExtras().getString("type").toString().equalsIgnoreCase("polls")) {
                String pollid =  data.getExtras().getString("pollid").toString();
                if(pollid.length()>0)
                {
                    Intent intent = new Intent(HomeActivity.this, PollScreen.class);
                    intent.putExtra("pollid",pollid);
                    intent.putExtra("from","active");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }      else  if (data.getExtras().getString("type").toString().equalsIgnoreCase("groups")) {
                String groupid =  data.getExtras().getString("pollid").toString();
                if(groupid.length()>0)
                {
                    Intent it = new Intent(HomeActivity.this, GroupInfoActivity.class);
                    it.putExtra("uidforchat",groupid);
                    finish();
                    startActivity(it);
                    overridePendingTransition(0,0);
                }
            } else {
                //invalid qr <code></code>
                Toast.makeText(HomeActivity.this, "Invalid QR Image,try again", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void updatefcm(){
        Log.d("App","gcmid:"+ M.getFCMToken(HomeActivity.this));
        if(M.getFCMToken(HomeActivity.this).length()>0) {
                AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
                mAuthenticationAPI.updatefcm(M.getID(HomeActivity.this), M.getFCMToken(HomeActivity.this), deviceid, "android", deviceName, new retrofit.Callback<SuccessPojo>() {
                @Override
                    public void success(SuccessPojo pojo, retrofit.client.Response response) {
                                    verifyUserReferral();
                         }
                    @Override
                public void failure(RetrofitError error) {
                    //Log.d("App", "fail" + error.getMessage());
                    return;
                }
            });
        }
    }

    //verify the referral verification and user should be the first time or not
    public void verifyUserReferral()
    {
        if(M.getRefId(HomeActivity.this).length()>2)
        {
            Toast.makeText(HomeActivity.this,M.getRefId(HomeActivity.this), Toast.LENGTH_SHORT).show();
            String referralId=M.getRefId(HomeActivity.this);
            PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
            mAuthenticationAPI.verifyReferralUser(M.getID(HomeActivity.this),referralId, deviceid, new Callback<StatusPojo>() {
                @Override
                public void success(StatusPojo statusPojo, Response response) {
                  //  updatefcm();
                  /*  if(statusPojo.getStatus().equalsIgnoreCase("success"))
                    {
                        updateReferralBalance();
                    }*/
                }

                @Override
                public void failure(RetrofitError error) {
                    updatefcm();
                }
            });
        }
    }
    //update referral balance to credits
    //if there is any referral <code></code>

    public void updateReferralBalance()
    {
        String referralId=M.getRefId(HomeActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.addCreditsforReferral(referralId, M.getID(HomeActivity.this), new Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                if(statusPojo.getStatus().equalsIgnoreCase("success"))
                {
                    //referaal amount credits added to account
                    updatefcm();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onMenuItemSelected(View view) {
        int id = (int) view.getTag();
        if (id == R.id.menu_profile) {
            Intent intent=new Intent(HomeActivity.this,SettingActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.menu_categories) {
            Intent intent=new Intent(HomeActivity.this,TopicsActivity.class);
            startActivity(intent);
        } else if (id == R.id.menu_messages) {
           // Intent intent=new Intent(HomeActivity.this,MessagesActivity.class);
            Intent intent=new Intent(HomeActivity.this,Logs_activity.class);
            startActivity(intent);
        }
        else if (id == R.id.menu_archive) {
            Intent intent=new Intent(HomeActivity.this,HistoryActivity.class);
            intent.putExtra("fromact","home");
            startActivity(intent);
        }/* else if (id == R.id.menu_private_polls) {
            Intent intent=new Intent(HomeActivity.this,PrivatePolls.class);
            intent.putExtra("fromact","home");
            startActivity(intent);
        }*/
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {

        if(event.getStrength().getValue()==ConnectivityStrength.POOR)
        {
            tv_lowconnection.setSelected(true);
            lytlownetwork.setVisibility(View.VISIBLE);
            //Crouton.makeText(HomeActivity.this, "Mood India Detects slow data connection,it may effect on loading time of polls.", Style.ALERT).setConfiguration(CONFIGURATION_INFINITE).show();
        }
        else
        {
            lytlownetwork.setVisibility(View.GONE);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {

        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }

    }
    @Override
    public void onResume()
    {
        MyApplication.getInstance().setConnectivityListener(this);
        super.onResume();
       // tabLayout.getTabAt(2).select();
        Intent intent = getIntent();

        if(intent.hasExtra("fragName")){
            Bundle bd = getIntent().getExtras();
          Log.i("Fragname",bd.getString("fragName"))  ;
            if(bd.getString("fragName").equals("0")){
               // tabLayout.getTabAt(0).select();
                viewPager.setCurrentItem(1);
                tabLayout.setViewPager(viewPager);
            }
            else if(bd.getString("fragName").equals("1")){
                //tabLayout.getTabAt(1).select();
                viewPager.setCurrentItem(1);
                tabLayout.setViewPager(viewPager);
            }
            else if(bd.getString("fragName").equals("2")){
                //tabLayout.getTabAt(2).select();
                viewPager.setCurrentItem(2);
                tabLayout.setViewPager(viewPager);
            } else if(bd.getString("fragName").equals("3")){
                viewPager.setCurrentItem(3);
                tabLayout.setViewPager(viewPager);
            }else if(bd.getString("fragName").equals("4")){
                viewPager.setCurrentItem(4);
                tabLayout.setViewPager(viewPager);
            }
        }
        else
        {
            //tabLayout.getTabAt(0).select();
        }
        if(M.getMyCountryID(HomeActivity.this).length()>1){
            tv_country.setText(M.getMyCountry(HomeActivity.this));
        }
        else {
            M.setMyCountry("India",HomeActivity.this);
            M.setMyCountryID("101",HomeActivity.this);
            tv_country.setText(M.getMyCountry(HomeActivity.this));
            Intent i = new Intent(HomeActivity.this, HomeActivity.class);  //your class
            startActivity(i);
        }
        if(M.getMyState(HomeActivity.this).length()>1)
        {
            tv_state.setText(M.getMyState(HomeActivity.this));
            tv_cityup.setText(M.getMyState(HomeActivity.this));
        }
        if(M.getMyCity(HomeActivity.this).length()>1)
        {
            tv_city.setText(M.getMyCity(HomeActivity.this));
            tv_cityup.setText(M.getMyCity(HomeActivity.this));
        }
    }

    private static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder()
            .setDuration(Configuration.DURATION_INFINITE)
            .build();

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }

    private void onInviteClicked() {

        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("moodindia")
                .setTitle("MoodIndia Live Polls")
                .setContentDescription("Hey , try this new polls app. Its Awesome")
                .addContentMetadata("referralid", "123456789")
                .addContentMetadata("userName", M.getUsername(HomeActivity.this));

        if(M.getMyReferralID(HomeActivity.this).length()>2)
        {
            LinkProperties linkProperties = new LinkProperties()
                    .setChannel("facebook")
                    .setChannel("whatsapp")
                    .setFeature("sharing")
                    .addControlParameter("referralid",M.getMyReferralID(HomeActivity.this));

            String messageBody="Dear Receiver, "+M.getUsername(HomeActivity.this)+" has invited you to download and register to MOOD INDIA app. Download and get reward of 500 rupees. A minimum of 100 rupees reward to your Paytm registered number is guaranteed. Hurry up, limited time offer. Thank me later.";

            ShareSheetStyle shareSheetStyle = new ShareSheetStyle(HomeActivity.this, "MoodIndia", messageBody)
                    .setCopyUrlStyle(getResources().getDrawable(android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                    .setMoreOptionStyle(getResources().getDrawable(android.R.drawable.ic_menu_search), "Show more")
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.WHATS_APP)
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                    .setAsFullWidthStyle(true)
                    .setSharingTitle("Share With");

            branchUniversalObject.showShareSheet(this,
                    linkProperties,
                    shareSheetStyle,
                    new Branch.BranchLinkShareListener() {

                        @Override
                        public void onShareLinkDialogLaunched() {

                        }

                        @Override
                        public void onShareLinkDialogDismissed() {

                        }

                        @Override
                        public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {

                        }

                        @Override
                        public void onChannelSelected(String channelName) {

                        }
                    });
        }
        else
        {
            Toast.makeText(HomeActivity.this, "Please update the app , to earn referral points", Toast.LENGTH_SHORT).show();
        }


       /* Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText(getString(R.string.invitation_cta))

                .build();
        startActivityForResult(intent, REQUEST_INVITE);*/
    }

    public Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) { return (null); } int w = bitmap.getWidth(); int h = bitmap.getHeight(); int[] pix = new int[w * h]; Log.e("pix", w + " " + h + " " + pix.length); bitmap.getPixels(pix, 0, w, 0, 0, w, h); int wm = w - 1; int hm = h - 1; int wh = w * h; int div = radius + radius + 1; int r[] = new int[wh]; int g[] = new int[wh]; int b[] = new int[wh]; int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; int vmin[] = new int[Math.max(w, h)]; int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) { p = pix[yi + Math.min(wm, Math.max(i, 0))]; sir = stack[i + radius]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) { r[yi] = dv[rsum]; g[yi] = dv[gsum]; b[yi] = dv[bsum]; rsum -= routsum; gsum -= goutsum; bsum -= boutsum; stackstart = stackpointer - radius + div; sir = stack[stackstart % div]; routsum -= sir[0]; goutsum -= sir[1]; boutsum -= sir[2]; if (y == 0) { vmin[x] = Math.min(x + radius + 1, wm); } p = pix[yw + vmin[x]]; sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) { yi = Math.max(0, yp) + x; sir = stack[i + radius]; sir[0] = r[yi]; sir[1] = g[yi]; sir[2] = b[yi]; rbs = r1 - Math.abs(i); rsum += r[yi] * rbs; gsum += g[yi] * rbs; bsum += b[yi] * rbs; if (i > 0) {
                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];
            } else {
                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];
            }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

}
