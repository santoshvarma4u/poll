package com.app.moodindia;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.model.M;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.ui.topics.TopicsActivity;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.bluejamesbond.text.DocumentView;
import com.ivankocijan.magicviews.views.MagicCheckBox;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

import retrofit.RetrofitError;
import retrofit.mime.TypedFile;
import xyz.hanks.library.SmallBang;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    protected RadioGroup spnmarray;
    protected MagicCheckBox cbPrivacy;
    EditText etemail, etpwd, etconfpwd, etusername, etphone, etbdate;
    RadioGroup rggender, spinarray;
    RadioButton rbmale, rbfemale, rbsingle, rbmarried;
    Spinner spnmstatus, spnmemptype;
    Button btnregister;
    TextView tv_state, tv_country, tv_city, txtIncomeValue, tv_social_name, tv_social_email, tv_register_location,tv_privacy,tv_iagree;
    DiscreteSeekBar incomeValue;
    String TAG = "Register", email, pwd = "", name, phone = "", country, bdate, gender, logintype, income;
    LinearLayout socialLoginLyt, emailLoginLyt;
    String state, city, mStatus, user_type;
    DatePickerDialog.OnDateSetListener d;
    final Calendar dateAndTime = Calendar.getInstance();
    SimpleDateFormat fmtDateAndTime = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat fmtdt = new SimpleDateFormat("yyyy-MM-dd");
    Context ctx;
    String latitude = "0.0", longitude = "0.0";
    Spinner spnqualification;
    Spinner spnincome;
    Spinner spnage;
    String tempCountryId = "", tempStateId = "", tempCityId = "";
    String countryIntent = "", stateIntent = "", cityIntent = "", marital;
    ImageView iv_edit_address,iv_profile_picture;
    boolean isConnected;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_register);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // window.setStatusBarColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ctx = RegisterActivity.this;
        initview();
        checkConnection();
        Intent locationIntent = new Intent(RegisterActivity.this, GetLocation.class);
        startActivityForResult(locationIntent, 452);
        Bundle extras=new Bundle();

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("logintype")) {
            etemail.setText(getIntent().getExtras().getString("email"));
            Toast.makeText(getApplicationContext(), etemail.getText().toString(), Toast.LENGTH_SHORT).show();
            etusername.setText(getIntent().getExtras().getString("name"));
            String gender = getIntent().getExtras().getString("gender");
            logintype = getIntent().getExtras().getString("logintype");
            emailLoginLyt.setVisibility(View.GONE);
            socialLoginLyt.setVisibility(View.VISIBLE);
            if (logintype != null && logintype.equalsIgnoreCase("Google")) {
                tv_social_name.setText("You're Logged in from Google");
                M.setLoginType("Google", RegisterActivity.this);
                tv_social_email.setText("Hello, " + getIntent().getExtras().getString("name"));
            } else if (logintype != null && logintype.equalsIgnoreCase("Facebook")) {
                tv_social_name.setText("You're Logged in from Facebook");
                M.setLoginType("Facebook", RegisterActivity.this);
                tv_social_email.setText("Hello, " + getIntent().getExtras().getString("name"));
            }
            else
            {
                logintype = "email";
                emailLoginLyt.setVisibility(View.VISIBLE);
                socialLoginLyt.setVisibility(View.GONE);
                etpwd.setVisibility(View.VISIBLE);
                etconfpwd.setVisibility(View.VISIBLE);
            }
        } else {
            logintype = "email";
            emailLoginLyt.setVisibility(View.VISIBLE);
            socialLoginLyt.setVisibility(View.GONE);
            etpwd.setVisibility(View.VISIBLE);
            etconfpwd.setVisibility(View.VISIBLE);
        }
        //initView();


        final DialogPlus notificationDialog = DialogPlus.newDialog(RegisterActivity.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_registration_alert))
                .setCancelable(true)
                .create();


        View view=notificationDialog.getHolderView();

        final TextView tvNoticont=(TextView) view.findViewById(R.id.tv_noticont);
        final TextView tv_notification=(TextView) view.findViewById(R.id.tv_notification);
        final DocumentView dv=(DocumentView)view.findViewById(R.id.blogText) ;
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/ssr.ttf");
        dv.getDocumentLayoutParams().setTextTypeface(type);

        tvNoticont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationDialog.dismiss();
            }
        });
        notificationDialog.show();
    }

    private SmallBang mSmallBang;

    private void initview() {
        etemail = (EditText) findViewById(R.id.etuseremail);
        iv_profile_picture = (ImageView) findViewById(R.id.iv_profile_picture);
        etpwd = (EditText) findViewById(R.id.etuserpwd);
        etconfpwd = (EditText) findViewById(R.id.etconfirmpwd);
        etusername = (EditText) findViewById(R.id.etusername);
        etphone = (EditText) findViewById(R.id.etuserphone);
        etbdate = (EditText) findViewById(R.id.etuserbday);
        rggender = (RadioGroup) findViewById(R.id.rggender);
        rbmale = (RadioButton) findViewById(R.id.rbmale);
        rbfemale = (RadioButton) findViewById(R.id.rbfemale);
        rbsingle = (RadioButton) findViewById(R.id.rbsingle);
        rbmarried = (RadioButton) findViewById(R.id.rbmarried);
        tv_social_name = (TextView) findViewById(R.id.tv_social_name);
        tv_social_email = (TextView) findViewById(R.id.tv_social_email);
        socialLoginLyt = (LinearLayout) findViewById(R.id.socialLoginLyt);
        emailLoginLyt = (LinearLayout) findViewById(R.id.emailLoginLyt);
        tv_register_location = (TextView) findViewById(R.id.tv_register_location);
        tv_privacy = (TextView) findViewById(R.id.tv_privacy);
        tv_iagree = (TextView) findViewById(R.id.tv_iagree);
        iv_edit_address = (ImageView) findViewById(R.id.iv_edit_address);
        mSmallBang = SmallBang.attach2Window(this);
        tv_state = (TextView) findViewById(R.id.txtState);
        tv_city = (TextView) findViewById(R.id.txtCity);
        tv_country = (TextView) findViewById(R.id.txtCountry);
        txtIncomeValue = (TextView) findViewById(R.id.txtIncomeValue);
        spnqualification = (Spinner) findViewById(R.id.spnqualification);
        spnincome = (Spinner) findViewById(R.id.spnincome);
        spnage = (Spinner) findViewById(R.id.spnage);
        socialLoginLyt = (LinearLayout) findViewById(R.id.socialLoginLyt);
        etusername = (EditText) findViewById(R.id.etusername);
        emailLoginLyt = (LinearLayout) findViewById(R.id.emailLoginLyt);
        spnqualification = (Spinner) findViewById(R.id.spnqualification);
        spnincome = (Spinner) findViewById(R.id.spnincome);
        spnage = (Spinner) findViewById(R.id.spnage);
        txtIncomeValue = (MagicTextView) findViewById(R.id.txtIncomeValue);
        rbmale = (RadioButton) findViewById(R.id.rbmale);
        rbfemale = (RadioButton) findViewById(R.id.rbfemale);
        rggender = (RadioGroup) findViewById(R.id.rggender);
        rbsingle = (RadioButton) findViewById(R.id.rbsingle);
        rbmarried = (RadioButton) findViewById(R.id.rbmarried);
        spnmarray = (RadioGroup) findViewById(R.id.spnmarray);
        cbPrivacy = (MagicCheckBox) findViewById(R.id.cb_privacy);
        btnregister = (Button) findViewById(R.id.btnregister);
        btnregister.setOnClickListener(RegisterActivity.this);
        tv_country.setText("India");
        tempCountryId = "101";
        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.moodindia.in/privacy.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        tv_iagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.moodindia.in/privacy.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        tv_iagree.setPaintFlags(tv_iagree.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        incomeValue = (DiscreteSeekBar) findViewById(R.id.seek_bar);
        incomeValue.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                txtIncomeValue.setText(value + "");
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
                //txtIncomeValue.setText(seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                //  txtIncomeValue.setText(seekBar.getProgress());
            }
        });
        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, SelectCountry.class);
                startActivityForResult(intent, 1);

            }
        });
        tv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_country.getText().toString().equalsIgnoreCase("Country")) {
                    M.showToast(RegisterActivity.this, "Please Select Country First");
                } else {
                    if (tempCountryId.toString().length() > 0) {
                        Intent intent = new Intent(RegisterActivity.this, SelectState.class);
                        intent.putExtra("countryid", tempCountryId);
                        intent.putExtra("register", "register");
                        startActivityForResult(intent, 2);
                    } else {
                        tv_country.performClick();
                    }
                }
            }
        });
        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_state.getText().toString().equalsIgnoreCase("State")) {
                    M.showToast(RegisterActivity.this, "Please Select State First");
                } else {
                    if (tempCountryId.toString().length() > 0 && tempStateId.toString().length() > 0) {
                        Intent intent = new Intent(RegisterActivity.this, SelectCity.class);
                        intent.putExtra("stateid", tempStateId);
                        intent.putExtra("register", "register");
                        startActivityForResult(intent, 3);
                    } else {
                        tv_country.performClick();
                    }
                }
            }
        });

        spinarray = (RadioGroup) findViewById(R.id.spnmarray);
        spnmemptype = (Spinner) findViewById(R.id.spnemptype);
        btnregister = (Button) findViewById(R.id.btnregister);
        etbdate.setOnClickListener(this);
        btnregister.setOnClickListener(this);
        d = new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                dateAndTime.set(Calendar.YEAR, arg1);
                dateAndTime.set(Calendar.MONTH, arg2);
                dateAndTime.set(Calendar.DAY_OF_MONTH, arg3);
                etbdate.setText(fmtDateAndTime.format(dateAndTime.getTime()));
                bdate = fmtdt.format(dateAndTime.getTime());
            }

        };

        iv_edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  tv_country.setVisibility(View.VISIBLE);
                tv_state.setVisibility(View.VISIBLE);
                tv_city.setVisibility(View.VISIBLE);
                tv_country.requestFocus();
            }
        });

   iv_profile_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  tv_country.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                    }
                    else {
                        Intent galleryIntent = new Intent();
                        galleryIntent.setType("image/*");
                        galleryIntent.setAction(Intent.ACTION_PICK);
                        // Chooser of file system options.
                        Intent openGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE);
                    }
                }
            }
        });

    }
    private static final int REQUEST_GALLERY_CODE = 200;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            //country call back
            // M.setMyCountry(data.getExtras().getString("CountryName").toString(),RegisterActivity.this);
            //  M.setMyCountryID(data.getExtras().getString("CountryId").toString(),RegisterActivity.this);
            tv_country.setText(data.getExtras().getString("CountryName").toString());
            tempCountryId = data.getExtras().getString("CountryId").toString();

            completeLocationAddress = data.getExtras().getString("CountryName").toString();
            tv_register_location.setText(completeLocationAddress);

            tv_state.setText("State");
            // tv_state.performClick();
            //M.showToast(HomeActivity.this,data.getExtras().getString("CountryId").toString());
        } else if (requestCode == 2 && resultCode == RESULT_OK) {
            //country call back
            // M.setMyState(data.getExtras().getString("StateName").toString(),RegisterActivity.this);
            //  M.setMyStateID(data.getExtras().getString("StateId").toString(),RegisterActivity.this);
            tv_state.setText(data.getExtras().getString("StateName").toString());
            tempStateId = data.getExtras().getString("StateId").toString();
            tv_city.setText("City");

            completeLocationAddress += data.getExtras().getString("StateName").toString();
            tv_register_location.setText(completeLocationAddress);


            // tv_city.performClick();
            // M.showToast(HomeActivity.this,data.getExtras().getString("StateId").toString());
        } else if (requestCode == 3 && resultCode == RESULT_OK) {
            //country call back
            // M.setMyCity(data.getExtras().getString("CityName").toString(),RegisterActivity.this);
            // M.setMyCityID(data.getExtras().getString("CityId").toString(),RegisterActivity.this);
            tv_city.setText(data.getExtras().getString("CityName").toString());
            tempCityId = data.getExtras().getString("CityId").toString();

            completeLocationAddress += data.getExtras().getString("CityName").toString();
            tv_register_location.setText("At " + completeLocationAddress);

            // M.showToast(RegisterActivity.this,data.getExtras().getString("CityId").toString());
        } else if (requestCode == 452 && resultCode == RESULT_OK) {
            try {
                countryIntent = data.getExtras().getString("country").toString();
                stateIntent = data.getExtras().getString("state").toString();
                cityIntent = data.getExtras().getString("city").toString();
                latitude = data.getExtras().getString("latitude").toString();
                longitude = data.getExtras().getString("longitude").toString();
                if (countryIntent.length() > 2) {
                    tv_country.setText(countryIntent);
                }
                if (stateIntent.length() > 2) {
                    tv_state.setText(stateIntent);
                }
                if (cityIntent.length() > 2) {
                    tv_city.setText(cityIntent);
                }
            } catch (Exception e) {
                countryIntent = "N";
                stateIntent = "N";
                cityIntent = "N";

            }
            completeLocationAddress = cityIntent + "," + stateIntent + "," + countryIntent;
            tv_register_location.setText("At " + completeLocationAddress);


            if (countryIntent.equalsIgnoreCase("N")) {

                Toast.makeText(RegisterActivity.this, "Unable capture location , Please select manually.", Toast.LENGTH_SHORT).show();
                tv_state.setVisibility(View.VISIBLE);
                tv_city.setVisibility(View.VISIBLE);
                tv_country.setVisibility(View.VISIBLE);
                tv_country.performClick();
            }

        }
        else if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK&& null!=data) {
            try{
                M.showLoadingDialog(RegisterActivity.this);
                // Get real path to make File
                selectedImageUri = Uri.parse(getPath(data.getData()));
                Bitmap bitmap= BitmapFactory.decodeFile(getPath(data.getData()));
                Picasso.with(getApplicationContext()).load(selectedImageUri).fit().centerCrop().into(iv_profile_picture);
                // relev_pics.setImageBitmap(bitmap);
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);



                    Picasso.with(RegisterActivity.this).load(new File(cursor.getString(columnIndex)))
                            .into(iv_profile_picture);


                    cursor.close();



                } else {
                    Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                }

                M.hideLoadingDialog();
                //Log.d(TAG,"Image path :- "+selectedImageUri);
            }
            catch (Exception e){
                Log.e(TAG,e.getMessage());
            }
        }
    }

    Uri  selectedImageUri=null;

    private String getPath(Uri uri) throws Exception {
        // this method will be used to get real path of Image chosen from gallery.
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public boolean validateAddress(final String Geolatitude, final String Geolongitude, String Address) {
        final boolean[] validation = new boolean[1];


        //okhttp service
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://maps.google.com/maps/api/geocode/json?address=" + Address + "&sensor=false").build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                // //Log.i("response",response.body());
                ////Log.i("response",response.body().string());

                if (response.isSuccessful()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jobj = new JSONObject(response.body().string().toString());
                                JSONArray jResults = jobj.getJSONArray("results");

                                JSONObject object = jResults.getJSONObject(0);

                                JSONObject geometry = object.getJSONObject("geometry");

                                JSONObject location = geometry.getJSONObject("location");

                                //Log.i("JResults Latitude", location.getString("lat"));
                                //Log.i("JResults Longitude", location.getString("lng"));

                                Location myLocation = new Location("");
                                Location distLocation = new Location("");

                                myLocation.setLatitude(Double.parseDouble(Geolatitude));
                                myLocation.setLongitude(Double.parseDouble(Geolongitude));

                                distLocation.setLatitude(Double.parseDouble(location.getString("lat")));
                                distLocation.setLongitude(Double.parseDouble(location.getString("lng")));


                                Float distance = myLocation.distanceTo(distLocation) * 1000;

                                //Log.i("JResults distance Kms", round(distance, 1) + "");
                                //Log.i("JResults distance Ms", myLocation.distanceTo(distLocation) + "");


                                if (round(distance, 1) <= 20) {
                                    validation[0] = true;
                                } else {
                                    validation[0] = false;
                                }

                                //Log.i("JResults distance Kms", validation[0] + "");

                            } catch (JSONException se) {
                                se.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }
        });

        return true;
    }


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    String completeLocationAddress = "";

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.etuserbday) {
            etbdate.setError(null);
            DatePickerDialog d1 = new DatePickerDialog(RegisterActivity.this, d, dateAndTime.get(Calendar.YEAR), dateAndTime.get(Calendar.MONTH), dateAndTime.get(Calendar.DAY_OF_MONTH));
            d1.show();
        } else if (view.getId() == R.id.btnregister) {

            mSmallBang.bang(view);
            if (etusername.getText().toString().trim().length() <= 0) {
                etusername.setError("Username Required");
            } else if (etemail.getText().toString().trim().length() <= 0) {
                etemail.setError("Email Required");
            } else if (!isValidEmaillId(etemail.getText().toString())) {
                etemail.setError("Invalid Email");
            } else if (tv_country.getText().toString().equalsIgnoreCase("Country") || tv_country.getText().toString().equalsIgnoreCase("")) {
                etbdate.setError("Country Required");
            } else if (tv_state.getText().toString().equalsIgnoreCase("State") || tv_state.getText().toString().equalsIgnoreCase("State")) {
                etbdate.setError("State Required");
            } else if (tv_state.getText().toString().equalsIgnoreCase("City") || tv_state.getText().toString().equalsIgnoreCase("City")) {
                etbdate.setError("City Required");
            }
            /*else if(spnmstatus.getSelectedItem().equals("Marital Status")){
                ((TextView)spnmstatus.getSelectedView()).setError("Select Marital Status");
            }*/
            else if (spnqualification.getSelectedItem().equals("Qualification")) {
                ((TextView) spnqualification.getSelectedView()).setError("Select Qualification");
            } else if (spnage.getSelectedItem().equals("Age")) {
                ((TextView) spnage.getSelectedView()).setError("Select Age");
            } else if (spnincome.getSelectedItem().equals("Income Range")) {
                ((TextView) spnincome.getSelectedView()).setError("Select Income Range");
            } else if (spnmemptype.getSelectedItem().equals("Occupation")) {
                ((TextView) spnmemptype.getSelectedView()).setError("Select Occupation");
            }
            else if (!cbPrivacy.isChecked()) {
                Toast.makeText(RegisterActivity.this,"Please check privacy policy",Toast.LENGTH_SHORT).show();
            }

            else {
                if (logintype.equalsIgnoreCase("email")) {
                    if (etpwd.getText().toString().trim().length() <= 0)
                        etpwd.setError("Password Required");
                    else if (etconfpwd.getText().toString().trim().length() <= 0)
                        etconfpwd.setError("Confirm Password Required");
                    else if (!etpwd.getText().toString().equals(etconfpwd.getText().toString()))
                        etconfpwd.setError("Confirm Password Mismatch");
                    else
                        register();
                } else
                    register();

            }
        }
    }

    String qualification, age,referralId,deviceid;

    private void register() {
        deviceid = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);
        email = etemail.getText().toString();
        name = etusername.getText().toString();
        phone = etphone.getText().toString();
        country = tv_country.getText().toString();
        state = tv_state.getText().toString();
        city = tv_city.getText().toString();

        referralId=M.getRefId(RegisterActivity.this);
       // referralId="F0162E";
        //mStatus=spnmstatus.getSelectedItem().toString();
        user_type = spnmemptype.getSelectedItem().toString();
        income = spnincome.getSelectedItem().toString();
        qualification = spnqualification.getSelectedItem().toString();
        age = spnage.getSelectedItem().toString();
        if (logintype.equalsIgnoreCase("email"))
            pwd = etpwd.getText().toString();
        if (rbfemale.isChecked())
            gender = "Female";
        else
            gender = "Male";
        if (rbmarried.isChecked()) {
            marital = "Married";
        } else {
            marital = "Single";
        }
        TypedFile typedFile;
        if(selectedImageUri != null)
        {
            typedFile = makeFile(selectedImageUri.toString());
        }
        else
        {
            Uri path = Uri.parse("android.resource://"+BuildConfig.APPLICATION_ID+"/" + R.drawable.default_image);
            typedFile = makeFile(path.toString());
        }


        if(selectedImageUri==null)
        {
            if (validateAddress(latitude, longitude, city + "," + state + "," + country)) {
                M.showLoadingDialog(RegisterActivity.this);
                AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
                mAuthenticationAPI.register_no_image(email, name, pwd, phone, country, gender, logintype, state, marital, city, income, user_type, age, qualification, latitude, longitude,referralId,deviceid, new retrofit.Callback<ProfilePojo>() {
                    @Override
                    public void success(ProfilePojo pojo, retrofit.client.Response response) {
                        if (pojo != null) {
                            //Log.d(TAG, "res:" + pojo.getSuccess());
                            if (pojo.getSuccess().toString().trim().equals("1")) {
                                M.hideLoadingDialog();
                                Toast.makeText(RegisterActivity.this, "Registration Successfully Done...", Toast.LENGTH_SHORT).show();
                                if (logintype.equalsIgnoreCase("facebook")) {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                } else if (logintype.equalsIgnoreCase("google")) {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                } else {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                }
                            } else if (pojo.getSuccess().equals("0")) {
                                Toast.makeText(RegisterActivity.this, "Email Already exist...", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Registration Failed...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        M.hideLoadingDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        //Log.d(TAG, "fail" + error.getMessage());
                        return;
                    }
                });
            }
        }
        else
        {
            if (validateAddress(latitude, longitude, city + "," + state + "," + country)) {
                M.showLoadingDialog(RegisterActivity.this);
                AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
                mAuthenticationAPI.register(email, name, pwd, phone, country, gender, logintype, state, marital, city, income, user_type, age, qualification, latitude, longitude,typedFile,referralId,deviceid, new retrofit.Callback<ProfilePojo>() {
                    @Override
                    public void success(ProfilePojo pojo, retrofit.client.Response response) {
                        if (pojo != null) {
                            //Log.d(TAG, "res:" + pojo.getSuccess());
                            if (pojo.getSuccess().toString().trim().equals("1")) {
                                M.hideLoadingDialog();
                                Toast.makeText(RegisterActivity.this, "Registration Successfully Done...", Toast.LENGTH_SHORT).show();
                                if (logintype.equalsIgnoreCase("facebook")) {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                } else if (logintype.equalsIgnoreCase("google")) {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                } else {
                                    M.setID(pojo.getUser_id(), ctx);
                                    M.setUsername(pojo.getName(), ctx);
                                    M.setEmail(pojo.getEmail(), ctx);
                                    M.setPhone(pojo.getPhone(), ctx);
                                    M.setCountry(pojo.getCountry(), ctx);
                                    M.setBday(pojo.getDate_of_birth(), ctx);
                                    M.setGender(pojo.getGender(), ctx);
                                    M.setlogintype(pojo.getLogin_type(), ctx);
                                    Intent it = new Intent(RegisterActivity.this, TopicsActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0, 0);
                                }
                            } else if (pojo.getSuccess().equals("0")) {
                                Toast.makeText(RegisterActivity.this, "Email Already exist...", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Registration Failed...", Toast.LENGTH_SHORT).show();
                            }
                        }
                        M.hideLoadingDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        M.hideLoadingDialog();
                        //Log.d(TAG, "fail" + error.getMessage());
                        return;
                    }
                });
            }
        }



/*       M.showToast(RegisterActivity.this,age);
        M.showToast(RegisterActivity.this,qualification);
        M.showToast(RegisterActivity.this,latitude);
*/

    }

    private TypedFile makeFile(String uri){

        File file = new File(uri);
        TypedFile typedFile = new TypedFile("image/*",file);
        return typedFile;
    }
    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        if (isConnected) {

        } else {
            Intent i = new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }

    private void initView() {

    }
}
