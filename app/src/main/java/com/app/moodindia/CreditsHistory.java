package com.app.moodindia;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.moodindia.model.CreditsPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreditsHistory extends Fragment {

    RecyclerView rvCreditsHistory;
    LinearLayoutManager mLayoutManager;
    public CreditsHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_credits_history, container, false);

        rvCreditsHistory=(RecyclerView)view.findViewById(R.id.rv_credits);

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvCreditsHistory.setLayoutManager(mLayoutManager);
        rvCreditsHistory.setItemAnimator(new DefaultItemAnimator());

        transList=new ArrayList<>();
        tar=new TransAdapter();
        getTransData();
        //call get data


        return view;
    }

   // PollsAdapter pla;
   TransAdapter tar;
    List<CreditsPojo> transList;
    public class TransAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_paytm_transaction, parent, false);
            viewHolder = new ViewHolderTrans(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            configureViewHolderTrans((ViewHolderTrans) holder,position);
        }

        @Override
        public int getItemCount() {
            return transList.size();
        }
        CreditsPojo creditsPojo;
        public void configureViewHolderTrans(final ViewHolderTrans holder, final int position) {

            creditsPojo=transList.get(position);

            if(creditsPojo.getCr().equalsIgnoreCase("0"))
            {
                //its dr
                holder.tv_trans_amount.setText("-₹ "+creditsPojo.getDr());
            }
            else
            {
                //its cr
                holder.tv_trans_amount.setText("+₹ "+creditsPojo.getCr());
            }

            holder.tv_narration.setText(creditsPojo.getNarration());
            try {
                holder.tv_transdate.setText(dtfmt.format(defaultfmt.parse(creditsPojo.getDate_time())));
            } catch (ParseException e) {
                e.printStackTrace();
                holder.tv_transdate.setText("");
            }
            holder.tv_transid.setText("Transaction : "+creditsPojo.getTransid());

        }
    }
    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");
    class ViewHolderTrans extends RecyclerView.ViewHolder{

        TextView tv_narration,tv_trans_amount,tv_transid,tv_transdate;

        public ViewHolderTrans(View itemView) {
            super(itemView);
            tv_narration=(TextView)itemView.findViewById(R.id.tv_narration);
            tv_trans_amount=(TextView)itemView.findViewById(R.id.tv_trans_amount);
            tv_transid=(TextView)itemView.findViewById(R.id.tv_transid);
            tv_transdate=(TextView)itemView.findViewById(R.id.tv_transdate);
        }
    }

    public void getTransData()
    {
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getCreditDetails(M.getID(getActivity()), new Callback<List<CreditsPojo>>() {
            @Override
            public void success(List<CreditsPojo> creditsPojos, Response response) {
                if(creditsPojos.size()>0 &&creditsPojos.get(0).getTransid() !=null)
                {
                    transList=creditsPojos;
                    rvCreditsHistory.setAdapter(tar);
                    tar.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
