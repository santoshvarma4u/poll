package com.app.moodindia;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.transition.Fade;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ArchivePollScreen;
import com.app.moodindia.adapter.PollAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.PollPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.PollAPI;
import com.app.moodindia.webservices.SelectcategoryActivity;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.sa90.materialarcmenu.ArcMenu;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
public class HistoryActivity extends AppCompatActivity   {

    ListView lvpoll;
    TextView tvnohistory,tv_categories,tv_country,tv_state,tv_city,tv_cityup;
    PollAdapter adapter;
    String TAG = "History",fromact="";
    Boolean isLoading = true,loadmore = false;
    Boolean initialload = true;
    int currentpage = 1;
    ArrayList<PollPojo> list = new ArrayList<>();
    EditText search;
    ImageView tv_cityupicn,backarchive,placeholder;
    ImageView Searchtext;
    CardView searchborder;
    ArcMenu fabRevealMenu;
    View views;
    FloatingActionButton archive,categories,profile,notifications;
    boolean isConnected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        tv_categories = (TextView) findViewById(R.id.txtCategories);
        lvpoll = (ListView) findViewById(R.id.lvhistory);
        tv_country=(TextView)findViewById(R.id.txtCountry);
        tv_state=(TextView)findViewById(R.id.txtState);
        tv_city=(TextView)findViewById(R.id.txtCity);
        tvnohistory = (TextView) findViewById(R.id.nohistory);
        Searchtext=(ImageView)findViewById(R.id.txtsearch);
        placeholder=(ImageView)findViewById(R.id.placeholder);
        searchborder=(CardView)findViewById(R.id.cv_category) ;
        search=(EditText)findViewById(R.id.search);
        fabRevealMenu=(ArcMenu) findViewById(R.id.fabMenu);
        fabRevealMenu=(ArcMenu) findViewById(R.id.fabMenu);
        archive=(FloatingActionButton)findViewById(R.id.menu_archive);
        categories=(FloatingActionButton)findViewById(R.id.menu_categories);
        profile=(FloatingActionButton)findViewById(R.id.menu_profile);
        notifications=(FloatingActionButton)findViewById(R.id.menu_messages);

        tv_cityup=(TextView)findViewById(R.id.txtCityup);
        tv_cityupicn=(ImageView)findViewById(R.id.imageView5);
        backarchive=(ImageView)findViewById(R.id.backarchive);
        backarchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(HistoryActivity.this, HomeActivity.class);
                startActivity(it);
                finish();
                overridePendingTransition(0, 0);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            getWindow().setEnterTransition(new Explode().setDuration(500));

            getWindow().setExitTransition(new Fade().setDuration(500));
        }

        AppBarLayout appBarLayout=(AppBarLayout) findViewById(R.id.appbar);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior =  new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return false;
            }
        });
        params.setBehavior(behavior);


        final ExpandableRelativeLayout expandableLayout
                = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);

// toggle expand, collapse
        expandableLayout.toggle();
// expand

// collapse
        expandableLayout.collapse();


        tv_cityup.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                    }
                }

        );
        tv_cityupicn.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        expandableLayout.toggle();
                        rippleBackground.startRippleAnimation();
                    }
                }

        );




        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HistoryActivity.this,SettingActivity.class);
                startActivity(intent);
            }
        });
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HistoryActivity.this,MessagesActivity.class);
                startActivity(intent);
            }
        });
        categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HistoryActivity.this,SelectFavsActivity.class);
                startActivity(intent);
            }
        });
        Intent intent = getIntent();


      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Archives");
        getSupportActionBar().setDisplayShowTitleEnabled(true);*/
        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* Intent intent=new Intent(HomeActivity.this,SelectCountry.class);
                    startActivityForResult(intent,1);*/
            }
        });
        tv_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tv_country.getText().toString().equalsIgnoreCase("Country")) {
                    M.showToast(HistoryActivity.this,"Please Select Country First");
                }
                else {
                    Intent intent=new Intent(HistoryActivity.this,SelectState.class);
                    intent.putExtra("countryid",M.getMyCountryID(HistoryActivity.this));
                    startActivityForResult(intent,25);
                }
            }
        });
        tv_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tv_country.getText().toString().equalsIgnoreCase("State")) {
                    M.showToast(HistoryActivity.this,"Please Select State First");
                }
                else {
                    Intent intent=new Intent(HistoryActivity.this,SelectCity.class);
                    intent.putExtra("stateid",M.getMyStateID(HistoryActivity.this));
                    startActivityForResult(intent,3);
                }
            }
        });
        //Log.i("length",M.getMyState(HistoryActivity.this)+"");
        if(M.getMyCountryID(HistoryActivity.this).length()>1){
            tv_country.setText(M.getMyCountry(HistoryActivity.this));
        }
        else {
            M.setMyCountry("India",HistoryActivity.this);
            M.setMyCountryID("101",HistoryActivity.this);
            tv_country.setText(M.getMyCountry(HistoryActivity.this));
            Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);  //your class
            startActivity(i);
        }
        if(M.getMyState(HistoryActivity.this).length()>1)
        {
            tv_state.setText(M.getMyState(HistoryActivity.this));
        }
        if(M.getMyCity(HistoryActivity.this).length()>1)
        {
            tv_city.setText(M.getMyCity(HistoryActivity.this));
            tv_cityup.setText(M.getMyCity(HistoryActivity.this));
        }
        checkConnection();
        Searchtext.setOnClickListener(new View.OnClickListener() {
         @Override
        public void onClick(View v) {

                 search.setText("");
                 Searchtext.setImageResource(R.drawable.ic_search_white_36dp);

             setCurrentpage(1);
             getHistory();

        }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               /* if(s.length()>1)
                {
                    mSearchEnable=true;
                    Searchtext.setImageResource(R.drawable.ic_clear);
                     getFilterHistoryData(search.getText().toString());
                }
                else
                {
                    mSearchEnable=false;
                    Searchtext.setImageResource(R.drawable.ic_search_white_36dp);
                }*/

                if(s.length()>3)
                {
                    search.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>3)
                {
                    mSearchEnable=true;
                    Searchtext.setImageResource(R.drawable.ic_clear);
                    getFilterHistoryData(search.getText().toString());
                }
                else if(s.length()==0)
                {
                    setCurrentpage(1);
                    getHistory();
                }
                else
                {
                    mSearchEnable=false;
                    Searchtext.setImageResource(R.drawable.ic_search_white_36dp);
                }
                search.setEnabled(true);

            }
        });

        tv_categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryActivity.this, SelectcategoryActivity.class);
                startActivityForResult(intent, 100);
            }
        });
        if (currentpage == 1&&intent.getStringExtra("fromact").equals("home")) {
            setCurrentpage(1);
            getHistory();
        }
        lvpoll.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (lvpoll.getLastVisiblePosition() >= lvpoll.getCount() - 1) {
                        if (loadmore) {
                            setCurrentpage(getCurrentpage() + 1);
                            getHistory();
                        }
                    }
                }
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        lvpoll.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long viewId = view.getId();
                if (viewId == R.id.tvpoll) {
                    Intent it = new Intent(HistoryActivity.this, ArchivePollScreen.class);
                    it.putExtra("pollid", list.get(i).getQuestion_id());
                    it.putExtra("from", "notActive");
                    startActivity(it);
                }
                else if (viewId == R.id.tvviewAll) {
                    Intent it=new Intent(HistoryActivity.this, CurrentPolls.class);
                    //Log.d("logis",list.get(i).getCat_id());
                    it.putExtra("act_name","history");
                    it.putExtra("catid",list.get(i).getCat_id());
                    it.putExtra("catname",list.get(i).getCat_name());
                    startActivity(it);
                }
            }
        });
        if(intent.getStringExtra("fromact").equals("history")){
            tv_categories.setText(M.getMyCategory(HistoryActivity.this));
            getHistoryData();
        }
        else
        {
        }

        Picasso.with(HistoryActivity.this)
                .load("http://moodindia.in/headerImages/intro3.jpeg")
                .noPlaceholder()
                .into(placeholder);

    }

    public boolean mSearchEnable=false;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            if (data.getExtras().getString("Categoryname").toString().equals("0") && data.getExtras().getString("CategoryId").toString().equals("0")) {
                //Log.d("name", data.getExtras().getString("Categoryname").toString());
                //country call back
                M.setMyCategory("", HistoryActivity.this);
                M.setMyCategoryID("", HistoryActivity.this);
                tv_categories.setText("Categories");
                fromact="home";
                //   tv_state.setText("Select State");
                Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);
                i.putExtra("fromact",fromact);
                //your class
                startActivity(i);
                finish();
            }
                else
                {
                    //Log.d("name", data.getExtras().getString("Categoryname").toString());
                    //country call back
                    M.setMyCategory(data.getExtras().getString("Categoryname").toString(), HistoryActivity.this);
                    M.setMyCategoryID(data.getExtras().getString("CategoryId").toString(), HistoryActivity.this);
                    tv_categories.setText(data.getExtras().getString("Categoryname").toString());
                    fromact="history";
                    //   tv_state.setText("Select State");
                    Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);
                    i.putExtra("fromact",fromact);
                    //your class
                    startActivity(i);
                    finish();
                }

            //tv_state.performClick();
            //M.showToast(HomeActivity.this,data.getExtras().getString("CountryId").toString());
        }
        if(requestCode==1 && resultCode == RESULT_OK)
        {
            //country call back
            M.setMyCountry(data.getExtras().getString("CountryName").toString(),HistoryActivity.this);
            M.setMyCountryID(data.getExtras().getString("CountryId").toString(),HistoryActivity.this);
            tv_country.setText(data.getExtras().getString("CountryName").toString());
            tv_state.setText("Select State");
            tv_city.setText("Select City");
            M.setMyStateID("",HistoryActivity.this);
            M.setMyState("",HistoryActivity.this);
            M.setMyCity("",HistoryActivity.this);
            M.setMyCityID("",HistoryActivity.this);
            Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);  //your class
            startActivity(i);
            finish();
            //tv_state.performClick();
            //M.showToast(HomeActivity.this,data.getExtras().getString("CountryId").toString());
        }
        else if(requestCode==25 && resultCode == RESULT_OK) {//state call back
            if (data.getExtras().getString("StateName").toString().equals("0") && data.getExtras().getString("StateId").toString().equals("0")) {
                M.setMyStateID("", HistoryActivity.this);
                M.setMyState("", HistoryActivity.this);
                M.setMyCity("", HistoryActivity.this);
                M.setMyCityID("", HistoryActivity.this);
                tv_state.setText("State");
                tv_city.setText("City");
                tv_cityup.setText("Select City");
            } else {
                M.setMyState(data.getExtras().getString("StateName").toString(), HistoryActivity.this);
                M.setMyStateID(data.getExtras().getString("StateId").toString(), HistoryActivity.this);
                tv_state.setText(data.getExtras().getString("StateName").toString());
                tv_city.setText("Select City");
                M.setMyCityID("", HistoryActivity.this);
                M.setMyCity("", HistoryActivity.this);
                fromact="home";
                Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);
                i.putExtra("fromact",fromact);
                //your class
                startActivity(i);
                finish();
            }
        }
        else if(requestCode==3 && resultCode == RESULT_OK) {//country call back

            if (data.getExtras().getString("CityName").toString().equals("0") && data.getExtras().getString("CityId").toString().equals("0")) {
                M.setMyCity("", HistoryActivity.this);
                M.setMyCityID("", HistoryActivity.this);
                tv_city.setText("City");
                tv_cityup.setText("Select City");
            }
            else
            {
                M.setMyCity(data.getExtras().getString("CityName").toString(),HistoryActivity.this);
                M.setMyCityID(data.getExtras().getString("CityId").toString(),HistoryActivity.this);
                tv_city.setText(data.getExtras().getString("CityName").toString());
                tv_cityup.setText(M.getMyCity(HistoryActivity.this));
                fromact="home";
                Intent i = new Intent(HistoryActivity.this, HistoryActivity.class);
                i.putExtra("fromact",fromact);
                //your class
                startActivity(i);
                finish();
            }
        }
    }
    public int getCurrentpage() {
        return currentpage;
    }
    public void setCurrentpage(int currentpage) {
        this.currentpage = currentpage;
    }
    private void getHistory() {
        SharedPreferences preferences =getSharedPreferences("mycategoryid",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        final int page = getCurrentpage();
       /* if (page == 1)
            M.showLoadingDialog(HistoryActivity.this);*/

        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getHistory(page,M.getMyCountryID(getApplicationContext()),M.getMyStateID(getApplicationContext()),M.getMyCityID(getApplicationContext()), new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                if (pojo != null) {
                    if (pojo.size() > 0) {
                        list.clear();
                        list.addAll(pojo);
                        updateList();
                    } else if (page == 1) {
                        loadmore = false;
                        lvpoll.setVisibility(View.GONE);
                        tvnohistory.setVisibility(View.VISIBLE);

                    } else {
                        loadmore = false;
                    }
                } else {
                    tvnohistory.setVisibility(View.VISIBLE);
                    lvpoll.setVisibility(View.GONE);
                    loadmore = false;
                }
                //if (page == 1)
                  //  M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                if (page == 1)
                  //  M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }

    public void updateList() {
        if (initialload) {
            adapter = new PollAdapter(HistoryActivity.this, R.layout.poll_row, list);
            lvpoll.setAdapter(adapter);
            initialload = false;
        } else {
            adapter.addAll(list);
            adapter.notifyDataSetChanged();
        }
        isLoading = false;
    }
    private void getPollData(String pollid) {
        AppConst.selpoll = null;
        //Log.d(TAG, "id" + pollid);
        M.showLoadingDialog(HistoryActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getHistoryOption(pollid, M.getID(HistoryActivity.this), "0", new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if (pollpojo != null) {
                    if (pollpojo.size() > 0) {
                        for (PollOptionPojo pojo : pollpojo) {
                            AppConst.selpoll = pojo;

                            Intent it = new Intent(HistoryActivity.this, PollScreen.class);
                            it.putExtra("pollid", pojo.getPollid() + "");
                            startActivity(it);
                        }
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent it = new Intent(this, HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0, 0);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(this, HomeActivity.class);
        startActivity(it);
        finish();
        overridePendingTransition(0, 0);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }*/

    @Override
    public void onResume() {
       // MyApplication.getInstance().setConnectivityListener(this);

        super.onResume();
    }
/*
    SharedPreferences preferences =getSharedPreferences("mycategoryid",Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = preferences.edit();
    editor.clear();
    editor.commit();*/
    public void getHistoryData() {
        final int page = getCurrentpage();
        if (page == 1)
            M.showLoadingDialog(HistoryActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getCategoryHistory(page,M.getMyCategoryID(HistoryActivity.this),M.getMyCountryID(getApplicationContext()),M.getMyStateID(getApplicationContext()),M.getMyCityID(getApplicationContext()), new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo, retrofit.client.Response response) {
                if (pojo != null) {
                    if (pojo.size() > 0) {
                        list.clear();
                        list.addAll(pojo);
                        //Log.d("taghis",list.toString());
                        adapter = new PollAdapter(HistoryActivity.this, R.layout.poll_row, list);
                        lvpoll.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        //  updateList();
                    } else if (page == 1) {
                        loadmore = false;
                        lvpoll.setVisibility(View.GONE);
                        tvnohistory.setVisibility(View.VISIBLE);
                    } else {
                        loadmore = false;
                    }
                } else {
                    tvnohistory.setVisibility(View.VISIBLE);
                    lvpoll.setVisibility(View.GONE);
                    loadmore = false;
                }
                if (page == 1)
                    M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                if (page == 1)
                    M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }
    public void  getFilterHistoryData(String text)
    {
        String filtertxt=text;

        final int page = getCurrentpage();
        if (page == 1)
            M.showLoadingDialog(HistoryActivity.this);

        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getFilterCategoryHistory(page,M.getMyCategoryID(HistoryActivity.this),filtertxt, new retrofit.Callback<List<PollPojo>>() {
            @Override
            public void success(List<PollPojo> pojo1, retrofit.client.Response response) {
                if (pojo1 != null) {

                    if (pojo1.size() > 0) {
                        adapter.clear();
                    //Log.d("pojolist",pojo1.toString());
                    list.addAll(pojo1);

                    adapter = new PollAdapter(HistoryActivity.this, R.layout.poll_row, list);
                    lvpoll.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                        //
                        // updateList();
                    } else if (page == 1) {
                        loadmore = false;
                        lvpoll.setVisibility(View.GONE);
                        tvnohistory.setVisibility(View.VISIBLE);

                    } else {
                        loadmore = false;
                    }
                } else {
                    tvnohistory.setVisibility(View.VISIBLE);
                    lvpoll.setVisibility(View.GONE);
                    loadmore = false;
                }
                if (page == 1)
                    M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                if (page == 1)
                    M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }


    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);        }
    }
}
