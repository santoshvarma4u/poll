package com.app.moodindia.Paytm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.app.moodindia.MyCredits;
import com.app.moodindia.R;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TransactionActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

       // PaytmPGService Service = PaytmPGService.getStagingService();
        PaytmPGService Service = PaytmPGService.getProductionService();


        final String amount = getIntent().getExtras().getString("amount");
        String Custid=getIntent().getExtras().getString("Custid");

        final String orderid=getIntent().getExtras().getString("orderid");
        String email=getIntent().getExtras().getString("email");
         String phone=getIntent().getExtras().getString("phone");
        final String checksum=getIntent().getExtras().getString("checksumhash");


        Map<String, String> paramMap = new HashMap<String,String>();
        paramMap.put( "MID" , PaytmConfiguration.PAYTM_MID_PRODUCTION);
        paramMap.put( "ORDER_ID" , orderid);
        paramMap.put( "CUST_ID" , Custid);
        paramMap.put( "INDUSTRY_TYPE_ID" , PaytmConfiguration.PAYTM_Industry_type_ID_PRODUCTION);
        paramMap.put( "CHANNEL_ID" , "WAP");
        paramMap.put( "TXN_AMOUNT" , amount);
        paramMap.put( "WEBSITE" , PaytmConfiguration.PAYTM_WEBSITE_PRODUCTION);
        paramMap.put( "CALLBACK_URL" , "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp");
        paramMap.put( "EMAIL" , email);
        paramMap.put( "MOBILE_NO" , phone);
        paramMap.put( "CHECKSUMHASH" ,checksum);

        PaytmOrder Order = new PaytmOrder(paramMap);


        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionResponse(Bundle bundle) {
              getCheckSumHash(bundle.get("ORDERID").toString(),bundle.get("TXNAMOUNT").toString());


            }
            @Override
            public void networkNotAvailable() {
                //Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }

            @Override
            public void clientAuthenticationFailed(String s) {
              //  Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }

            @Override
            public void someUIErrorOccurred(String s) {
                //Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
               // Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }

            @Override
            public void onBackPressedCancelTransaction() {
               // Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }

            @Override
            public void onTransactionCancel(String s, Bundle bundle) {
                //Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
            }
        });
    }
    public void updateBalance(final String amount, String userid, String orderid)
    {
//        M.showLoadingDialog(TransactionActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.addCredits(userid, orderid, amount, new Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {

                Toast.makeText(TransactionActivity.this, "Credits added successfully", Toast.LENGTH_SHORT).show();
                //Log.d("LOG", "Payment Transaction is successful " + bundle);
                // Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));

                finish();
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(TransactionActivity.this, "Payment failed try again", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(TransactionActivity.this, MyCredits.class));
                finish();
               // M.hideLoadingDialog();
            }
        });
    }
    public void verifyTransactionStatus(final String Orderid, String checksumhash, final String amount)
    {
        JSONObject jObjectType = new JSONObject();
        try {
            jObjectType.put("MID",PaytmConfiguration.PAYTM_MID_PRODUCTION);
            jObjectType.put("ORDERID",Orderid);
            jObjectType.put("CHECKSUMHASH",checksumhash);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("verifyurl","https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?JsonData="+jObjectType.toString());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?JsonData="+jObjectType.toString()).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(com.squareup.okhttp.Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                // //Log.i("response",response.body());
                ////Log.i("response",response.body().string());

                if (response.isSuccessful()) {
                  //  updateBalance(redeembaleAmount,M.getID(getActivity()));
                    Log.e("respose",response.body().string());
                    //  updateBalance();
                    updateBalance(amount,M.getID(TransactionActivity.this),Orderid);

                }
            }
        });
    }
    public void getCheckSumHash(final String orderid,final String amount)
    {
        //okhttp service
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://moodindia.in/Paytm/generateTransactionCheckSum.php?OrderId="+orderid).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(com.squareup.okhttp.Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                //Log.i("response",response.body());

                String checksumhas=response.body().string();
                    verifyTransactionStatus(orderid,checksumhas,amount);

            }
        });
    }
}
