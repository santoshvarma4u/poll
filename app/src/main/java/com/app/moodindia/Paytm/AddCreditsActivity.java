package com.app.moodindia.Paytm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.app.moodindia.R;
import com.ivankocijan.magicviews.views.MagicEditText;

import br.com.bloder.magic.view.MagicButton;

public class AddCreditsActivity extends AppCompatActivity implements View.OnClickListener {

    protected MagicEditText etCredits;
    protected MagicButton btn10rs;
    protected MagicButton btn50rs;
    protected MagicButton btn100rs;
    protected MagicButton btnProceedpayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_add_credits);
        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn10rs) {


        } else if (view.getId() == R.id.btn50rs) {

        } else if (view.getId() == R.id.btn100rs) {

        } else if (view.getId() == R.id.btnProceedpayment) {

        }
    }

    private void initView() {
        etCredits = (MagicEditText) findViewById(R.id.et_credits);
        btn10rs = (MagicButton) findViewById(R.id.btn10rs);
        btn10rs.setOnClickListener(AddCreditsActivity.this);
        btn50rs = (MagicButton) findViewById(R.id.btn50rs);
        btn50rs.setOnClickListener(AddCreditsActivity.this);
        btn100rs = (MagicButton) findViewById(R.id.btn100rs);
        btn100rs.setOnClickListener(AddCreditsActivity.this);
        btnProceedpayment = (MagicButton) findViewById(R.id.btnProceedpayment);
        btnProceedpayment.setOnClickListener(AddCreditsActivity.this);
    }
}
