package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.app.moodindia.model.M;
import com.app.moodindia.model.StatePojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.LocationAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class SelectState extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    RecyclerView rv_state;
    SearchView sv_state;
    boolean isConnected;
    List<StatePojo> statepojoList;
    Button clearfilter;
    TextView tvpollsstatelist;


    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_state);
        rv_state=(RecyclerView)findViewById(R.id.rv_state);
        clearfilter=(Button) findViewById(R.id.clearFilter);
        sv_state=(SearchView)findViewById(R.id.sv_state);
        tvpollsstatelist=(TextView)findViewById(R.id.tvpollsstatelist);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectState.this);
        rv_state.setLayoutManager(mLayoutManager);
        rv_state.setItemAnimator(new DefaultItemAnimator());
        statepojoList=new ArrayList<>();
        sv_state.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length()  > 0){
                    cla.applyFilter(query);
                    return false;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length() > 0 )
                {
                    cla.applyFilter(newText);
                    return true;
                }
                return true;
            }
        });
        clearfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();

                //M.showToast(SelectState.this,filterdcstatepojo.get(getAdapterPosition()).getId());
                intent.putExtra("StateName","0");
                intent.putExtra("StateId","0");
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        /*clearfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                //M.showToast(SelectState.this,filterdcstatepojo.get(getAdapterPosition()).getId());
                intent.putExtra("StateName","0");
                intent.putExtra("StateId","0");
                setResult(RESULT_OK,intent);
            }
        });*/
    }

    StateListAdapter cla;
    List<StatePojo> filterdcstatepojo;

    public class StateListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        List<StatePojo> mStatePojo;



        public StateListAdapter(List<StatePojo> mStatePojo) {

            filterdcstatepojo=new ArrayList<>();
            this.mStatePojo = mStatePojo;
            filterdcstatepojo.addAll(mStatePojo);
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_country, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }



        public void applyFilter(String query)
        {
            if(query.length() ==0) {
                filterdcstatepojo.clear();
                filterdcstatepojo.addAll(mStatePojo);
            }
            else
            {
                filterdcstatepojo.clear();
                for(StatePojo cp:mStatePojo)
                {
                    if(cp.getName().toLowerCase().contains(query.toLowerCase()))
                    {
                        filterdcstatepojo.add(cp);
                    }
                }

            }
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return filterdcstatepojo.size();
        }
        StatePojo listState;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listState=filterdcstatepojo.get(position);
            holder.CountryName.setText(listState.getName());
            holder.CountryId=listState.getId();


           /* holder.CountryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    M.showToast(SelectState.this,listState.getId());
                    Intent intent=new Intent();
                    intent.putExtra("StateName",listState.getName());
                    intent.putExtra("StateId",listState.getId());
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });*/

        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CountryName;
        String CountryId;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CountryName=(TextView)itemView.findViewById(R.id.tv_countryname);
            CountryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent();
                    intent.putExtra("fromAct","home");
                    //M.showToast(SelectState.this,filterdcstatepojo.get(getAdapterPosition()).getId());
                    intent.putExtra("StateName",filterdcstatepojo.get(getAdapterPosition()).getName());
                    intent.putExtra("StateId",filterdcstatepojo.get(getAdapterPosition()).getId());
                    setResult(RESULT_OK,intent);
                    finish();


                   // M.showToast(SelectState.this,filterdcstatepojo.get(getAdapterPosition()).getId().toString());
                }
            });
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getExtras().containsKey("register")){
            getCountryRegisterData(getIntent().getExtras().getString("countryid").toString());
        }
        else{
            getCountryData(getIntent().getExtras().getString("countryid").toString());
            tvpollsstatelist.setVisibility(View.VISIBLE);
        }

        
    }

    public void getCountryData(String countryid)
    {
        M.showLoadingDialog(SelectState.this);
        LocationAPI mAuthenticationAPI = APIService.createService(LocationAPI.class);
        mAuthenticationAPI.getStates(countryid,new retrofit.Callback<List<StatePojo>>() {
            @Override
            public void success(List<StatePojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        statepojoList=pojo;
                        cla=new StateListAdapter(statepojoList);
                        rv_state.setAdapter(cla);
                        M.hideLoadingDialog();
                    }else{

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

     public void getCountryRegisterData(String countryid)
      {
        M.showLoadingDialog(SelectState.this);
        LocationAPI mAuthenticationAPI = APIService.createService(LocationAPI.class);
        mAuthenticationAPI.getStatesRegister(countryid,"register",new retrofit.Callback<List<StatePojo>>() {
            @Override
            public void success(List<StatePojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        statepojoList=pojo;
                        cla=new StateListAdapter(statepojoList);
                        rv_state.setAdapter(cla);
                        M.hideLoadingDialog();
                    }else{

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {

        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }


    }
}
