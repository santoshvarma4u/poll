package com.app.moodindia;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.model.CreditsPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class RedeemCredits extends Fragment {


    Button btnRedeem;
    public RedeemCredits() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_redeem_credits, container, false);
        btnRedeem=(Button)view.findViewById(R.id.btnRedeem);


        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_debt_credits))
                .setCancelable(true)
                .create();

        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();

            }
        });
        View view1=dialog.getHolderView();

        et_add=(TextView)view1.findViewById(R.id.tvredeem);
        final TextView tvAddmoney=(TextView) view1.findViewById(R.id.tvConfirmRedeem);
        final TextView tvCancel=(TextView)view1.findViewById(R.id.tvCancel);

        tvAddmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try
                {
                    final int ReddemValue = Integer.parseInt(redeembaleAmount);
                    final int branchCredits = M.getCredits(getActivity());
                    if(ReddemValue+branchCredits >=10)
                    {
                       int crdts =   ReddemValue+branchCredits;
                        dialog.dismiss();
                        makeTransaction( M.getPhone(getActivity()),String.valueOf(crdts));
                        Branch.getInstance().loadRewards(new Branch.BranchReferralStateChangedListener() {
                            @Override
                            public void onStateChanged(boolean changed, BranchError error) {
                                if (error == null && Branch.getInstance().getCredits() >= branchCredits) {
                                    Branch.getInstance().redeemRewards(branchCredits);
                                    M.setCredits(getActivity(),0);
                                }
                            }
                        });

                    }
                    else
                    {
                        //
                        Toast.makeText(getActivity(), "You need minimum 10 MI Credits to redeem", Toast.LENGTH_SHORT).show();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        getTransData();

        return view;
    }
    TextView et_add;
    public void makeTransaction(String phone,String amount)
    {

        byte[] newKey=(String.valueOf(phone)+String.valueOf(amount)).getBytes();
        String keyValue= Base64.encodeToString(newKey,0);
        String Keyurl="http://moodindia.in/Paytm/Gratification/gratification.php?phone="+phone+"&amount="+amount+"&key="+keyValue;
        //okhttp service
        Log.i("Redeem",Keyurl);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(Keyurl).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(com.squareup.okhttp.Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                // //Log.i("response",response.body());
                ////Log.i("response",response.body().string());

                if (response.isSuccessful()) {
                    if(response.body().string().equalsIgnoreCase("Transaction Invalid"))
                    {
                        Toast.makeText(getActivity(), "Invalid transaction please try later", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        updateBalance(redeembaleAmount,M.getID(getActivity()));
                        Log.e("respose",response.body().string());
                        //  updateBalance();
                    }
                }
            }
        });
    }

    public void updateBalance(final String amount, String userid)
    {
        Random r = new Random();
        int i1 = r.nextInt(99999 - 11111) + 11111;

        final String orderid="ORDER"+i1;

//        M.showLoadingDialog(getActivity());
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.removeCredits(userid, orderid, amount, new retrofit.Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, Response response) {
                if(statusPojo.getStatus().equalsIgnoreCase("success"))
                {
                    Toast.makeText(getActivity(), "Added to paytm wallet, Verify your wallet amount on Paytm App", Toast.LENGTH_SHORT).show();
                    //Log.d("LOG", "Payment Transaction is successful " + bundle);
                    // Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getActivity(), MyCredits.class));

                }
               // M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "Credits processed to Paytm wallet, Verify your wallet amount on Paytm App after few minutes. ", Toast.LENGTH_SHORT).show();
                //Log.d("LOG", "Payment Transaction is successful " + bundle);
                // Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
                startActivity(new Intent(getActivity(), MyCredits.class));

               // M.hideLoadingDialog();
            }
        });
    }
    String redeembaleAmount="0";

    public void getTransData()
    {
        //M.showLoadingDialog(getActivity());
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getCreditDetails(M.getID(getActivity()), new retrofit.Callback<List<CreditsPojo>>() {
            @Override
            public void success(List<CreditsPojo> creditsPojos, Response response) {
                if(creditsPojos.size()>0)
                {
                    creditsPojos.get(creditsPojos.size()-1).getTotalbalance();
                    redeembaleAmount= creditsPojos.get(creditsPojos.size()-1).getTotalbalance();
                    et_add.setText("MI Will redeem amount of "+redeembaleAmount+" INR into your paytm wallet.");
                }
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
}
