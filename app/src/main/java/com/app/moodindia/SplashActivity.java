package com.app.moodindia;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.app.moodindia.model.M;
import com.app.moodindia.ui.privatepolls.AcceptPollActivity;
import com.app.moodindia.ui.privatepolls.Groups.GroupInfoActivity;
import com.app.moodindia.ui.publicpolls.PublicHomePage;
import com.jaredrummler.android.widget.AnimatedSvgView;
import com.vansuita.library.CheckNewAppVersion;

import org.json.JSONObject;

import javax.security.auth.login.LoginException;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(SplashActivity.this, R.color.colorPrimaryDark));
        AnimatedSvgView svgView = (AnimatedSvgView) findViewById(R.id.animated_svg_view);
        svgView.start();
     //   M.setRefId(SplashActivity.this,"D6A678");

        //selecting all topics here only
       // M.setRefId(SplashActivity.this,"F0162E");
        M.setFavs("23,6,7,8,9,10,11,22,12,13,15,16,17,21,18,14,19",SplashActivity.this);

        new CountDownTimer(2000,1000) {
            /** This method will be invoked on finishing or expiring the timer */
            @Override
            public void onFinish() {
                /** Creates an intent to start new activity */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, 1257);
                    }
                    else {

                        checkforUpdates();
                    }
                }
                else {
                    checkforUpdates();
                }
            }
            @Override
            public void onTick(long millisUntilFinished) {

            }
        }.start();
    }
    public void checkforUpdates()
    {
        /*final ProgressDialog pd=new ProgressDialog(SplashActivity.this);
        pd.setMessage("Check for updates....");
        pd.show();*/

        new CheckNewAppVersion(SplashActivity.this).setOnTaskCompleteListener(new CheckNewAppVersion.ITaskComplete() {
            @Override
            public void onTaskComplete(CheckNewAppVersion.Result result) {

              //  Toast.makeText(SplashActivity.this, result.getNewVersionCode()+" -----Yhhhh", Toast.LENGTH_SHORT).show();
                //Checks if there is a new version available on Google Play Store.
                if(result.hasNewVersion())
                {

                  //  pd.dismiss();
                    result.openUpdateLink();
                }
                else {
                   // pd.dismiss();
                    init();
                }

               /* //Get the new published version code of the app.
                result.getNewVersionCode();

                //Get the app current version code.
                result.getOldVersionCode();

                //Opens the Google Play Store on your app page to do the update.
             */
            }
        }).execute();
    }
    public void init(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if( M.getID(SplashActivity.this).equals("0")) {

                   /* Intent it = new Intent(SplashActivity.this, LoginActivity.class);
                    finish();
                    startActivity(it);
                    overridePendingTransition(0,0);*/
                    if(M.getFavs(SplashActivity.this).toString().length()>0)
                    {
                        Intent it = new Intent(SplashActivity.this, PublicHomePage.class);
                        it.putExtra("frompublic","true");
                        startActivity(it);
                    }
                    else
                    {
                        Intent it = new Intent(SplashActivity.this, SelectFavsActivity.class);
                        finish();
                        startActivity(it);
                        overridePendingTransition(0,0);
                    }

                   /* Intent it = new Intent(SplashActivity.this, LoginActivity.class);
                    finish();
                    startActivity(it);
                    overridePendingTransition(0,0);*/
                }else{
                    if(M.getFavs(SplashActivity.this).toString().length()>0)
                    {
                        if(getIntent().getData() != null)
                        {
                            try{
                                Uri uri=getIntent().getData();
                                String scheme=uri.getPath().toString();
                                if(scheme.contains("privatepoll"))
                                {
                                    String result = scheme.substring(scheme.lastIndexOf('/') + 1).trim();
                                    if(result.length()>0)
                                    {
                                        Intent it = new Intent(SplashActivity.this, AcceptPollActivity.class);
                                        it.putExtra("poll_id",result);
                                        it.putExtra("poll_id",result);
                                        finish();
                                        startActivity(it);
                                        overridePendingTransition(0,0);
                                    }
                                }
                                else if(scheme.contains("groups"))
                                {
                                    String result = scheme.substring(scheme.lastIndexOf('/') + 1).trim();
                                    if(result.length()>0)
                                    {
                                        Intent it = new Intent(SplashActivity.this, GroupInfoActivity.class);
                                        it.putExtra("uidforchat",result);
                                        finish();
                                        startActivity(it);
                                        overridePendingTransition(0,0);
                                    }
                                }
                                else
                                {
                                    Intent it = new Intent(SplashActivity.this, HomeActivity.class);
                                    finish();
                                    startActivity(it);
                                    overridePendingTransition(0,0);
                                }
                            }catch (ArrayIndexOutOfBoundsException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            Intent it = new Intent(SplashActivity.this, HomeActivity.class);
                            finish();
                            startActivity(it);
                            overridePendingTransition(0,0);
                        }
                    }
                    else {
                        Intent it = new Intent(SplashActivity.this, SelectFavsActivity.class);
                        finish();
                        startActivity(it);
                        overridePendingTransition(0,0);
                    }
                }
            }
        }, 2000);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    init();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permissions", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(SplashActivity.this, SplashActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        final Branch branch = Branch.getInstance();


        branch.getInstance(getApplicationContext()).loadRewards(new Branch.BranchReferralStateChangedListener() {
            @Override
            public void onStateChanged(boolean changed, BranchError error) {
                // changed boolean will indicate if the balance changed from what is currently in memory

                // will return the balance of the current user's credits
                int credits = branch.getCredits();
                //Toast.makeText(SplashActivity.this,credits+"",Toast.LENGTH_LONG).show();
                if(credits > 0)
                {
                    M.setCredits(SplashActivity.this, credits);
                }
            }
        });

        branch.initSession(new Branch.BranchUniversalReferralInitListener() {
            @Override
            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    try{
                        if(linkProperties != null )
                        {
                            if(linkProperties.getControlParams() != null && linkProperties.getControlParams().containsKey("referralid"))
                            {
                                String referrerId = linkProperties.getControlParams().get("referralid").toString();
                                M.setRefId(SplashActivity.this,referrerId);
                               // Toast.makeText(SplashActivity.this, referrerId, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            M.setRefId(SplashActivity.this,"0");
                        }
                    }
                    catch (Exception e)
                    {
                        M.setRefId(SplashActivity.this,"0");
                        e.printStackTrace();
                    }
                } else {
                    M.setRefId(SplashActivity.this,"0");
                    Log.i("MyApp", error.getMessage());
                }
            }
        });

    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }
}
