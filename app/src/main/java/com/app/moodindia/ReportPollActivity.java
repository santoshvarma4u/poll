package com.app.moodindia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.moodindia.model.M;
import com.app.moodindia.model.SuccessPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicEditText;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ReportPollActivity extends AppCompatActivity implements View.OnClickListener {

    protected Spinner spinnerReasons;
    protected MagicEditText etReportDesc;
    protected MagicButton btnsubmitreport;
    protected MagicButton btncancelreport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setContentView(R.layout.activity_report_poll);

        this.setFinishOnTouchOutside(false);


        initView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnsubmitreport) {
            String pollid = getIntent().getExtras().getString("pollid").toString();
            String userid = M.getID(ReportPollActivity.this);

            if (spinnerReasons.getSelectedItemPosition() > 0) {
                submitReport(userid, pollid, spinnerReasons.getSelectedItem().toString(), etReportDesc.getText().toString());
            } else {
                Toast.makeText(ReportPollActivity.this, "Please select type of report", Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.btncancelreport) {
                this.finish();
        }
    }

    private void initView() {
        spinnerReasons = (Spinner) findViewById(R.id.spinnerReasons);
        etReportDesc = (MagicEditText) findViewById(R.id.et_report_desc);
        btnsubmitreport = (MagicButton) findViewById(R.id.btnsubmitreport);
        btnsubmitreport.setOnClickListener(ReportPollActivity.this);
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Report Poll");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0,0);
            }
        });*/


    /*    ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.reportarray, R.layout.spinner_item);

        spinnerReasons.setAdapter(adapter);*/
        btncancelreport = (MagicButton) findViewById(R.id.btncancelreport);
        btncancelreport.setOnClickListener(ReportPollActivity.this);

    }


    public void submitReport(String userid, String pollid, String report, String description) {
        M.showLoadingDialog(ReportPollActivity.this);
        PollAPI mPollAPI = APIService.createService(PollAPI.class);
        mPollAPI.reportPoll(userid, report, pollid, description, new Callback<SuccessPojo>() {
            @Override
            public void success(SuccessPojo successPojo, Response response) {
                Toast.makeText(ReportPollActivity.this, "Report Submitted successfully, Thanks", Toast.LENGTH_SHORT).show();

                M.hideLoadingDialog();
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ReportPollActivity.this, "Report Submission failed, Try again", Toast.LENGTH_SHORT).show();
                M.hideLoadingDialog();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
