package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.model.CountryPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.Internet;
import com.app.moodindia.webservices.LocationAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class SelectCountry extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    RecyclerView rv_country;
    SearchView sv_country;
    boolean isConnected;
    List<CountryPojo> countrypojoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_country);
        rv_country=(RecyclerView)findViewById(R.id.rv_country);
        sv_country=(SearchView)findViewById(R.id.sv_country);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectCountry.this);
        rv_country.setLayoutManager(mLayoutManager);
        rv_country.setItemAnimator(new DefaultItemAnimator());
        countrypojoList=new ArrayList<>();

        sv_country.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                cla.applyFilter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                    cla.applyFilter(newText);
                return true;
            }
        });
        checkConnection();
    }

    CountryListAdapter cla;

    List<CountryPojo> filterdcountrypojoList;

    public class CountryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



        List<CountryPojo> countrypojoList;


        public CountryListAdapter(List<CountryPojo> countrypojoList) {

            filterdcountrypojoList=new ArrayList<>();

            this.countrypojoList = countrypojoList;
            filterdcountrypojoList.addAll(countrypojoList);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)  {


            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_country, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        public void applyFilter(String query)
        {
            if(query.length() ==0)

                filterdcountrypojoList.addAll(countrypojoList);
            else
            {
                filterdcountrypojoList.clear();
                for(CountryPojo cp:countrypojoList)
                {
                    if(cp.getName().toLowerCase().contains(query.toLowerCase()))
                    {
                        filterdcountrypojoList.add(cp);
                    }
                }

            }
            notifyDataSetChanged();

        }

        @Override
        public int getItemCount() {
            return filterdcountrypojoList.size();
        }
        CountryPojo listCountry;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCountry=filterdcountrypojoList.get(position);
            holder.CountryName.setText(listCountry.getName());
            holder.CountryId=listCountry.getId();
        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CountryName;
        String CountryId;


        public ViewHolderFilter(View itemView) {
            super(itemView);
            CountryName=(TextView)itemView.findViewById(R.id.tv_countryname);


            CountryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent();
                    intent.putExtra("CountryName",filterdcountrypojoList.get(getAdapterPosition()).getName());
                    intent.putExtra("CountryId",filterdcountrypojoList.get(getAdapterPosition()).getId());
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getCountryData();
    }


    public void getCountryData()
    {
        M.showLoadingDialog(SelectCountry.this);
        LocationAPI mAuthenticationAPI = APIService.createService(LocationAPI.class);
        mAuthenticationAPI.getCountries("",new retrofit.Callback<List<CountryPojo>>() {
            @Override
            public void success(List<CountryPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        countrypojoList=pojo;

                        cla=new CountryListAdapter(countrypojoList);

                        rv_country.setAdapter(cla);

                        M.hideLoadingDialog();
                    }else{
                        Toast.makeText(getApplicationContext(),"There are no polls available",Toast.LENGTH_LONG).show();
                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {

        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }
    }
}
