package com.app.moodindia;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.ProfilePojo;
import com.app.moodindia.model.StatusPojo;
import com.app.moodindia.ui.privatepolls.PrivatePolls;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.AuthenticationAPI;
import com.app.moodindia.webservices.PollAPI;
import com.bluejamesbond.text.DocumentView;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;
import retrofit.RetrofitError;

import static com.app.moodindia.helper.AppConst.ConsantURL;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    protected MagicTextView tvfaq,invitetv,uname;
    protected MagicTextView tvuserguide;
    protected MagicTextView tvcontactus;
    protected MagicTextView tvEmailVerified;
    protected MagicTextView tv_partcommunitypolls;
    protected MagicButton btnVerify;
    MagicTextView app_version;

    protected Switch sw_profilevisibility;
    LinearLayout tvmycredts,tv_invitenearn;
    protected LinearLayout lytverification;
    protected LinearLayout contentSetting;
    TextView tvprofile, tvpwd, tvlogout, tvshare, tvaboutus,tv_credits;
    LinearLayout changepwdLyt;
    SwitchCompat swpush;
    String TAG = "Setting";
    ImageView iv_profile_picture;
    private static final int REQUEST_INVITE = 0;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Account");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        initview();
       /* if (M.getLoginType(SettingActivity.this).equalsIgnoreCase("Google") || M.getLoginType(SettingActivity.this).equalsIgnoreCase("Facebook"))
            changepwdLyt.setVisibility(View.GONE);
        else
            changepwdLyt.setVisibility(View.VISIBLE);*/

           getUserDetails();
    }


    private void onInviteClicked() {

        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("moodindia")
                .setTitle("MoodIndia Live Polls")
                .setContentDescription("Hey , try this new polls app. Its Awesome")
                .addContentMetadata("referralid", "123456789")
                .addContentMetadata("userName", M.getUsername(SettingActivity.this));

        if(M.getMyReferralID(SettingActivity.this).length()>2)
        {
            LinkProperties linkProperties = new LinkProperties()
                    .setChannel("facebook")
                    .setChannel("whatsapp")
                    .setFeature("sharing")
                    .addControlParameter("referralid",M.getMyReferralID(SettingActivity.this));

            String messageBody="Dear Receiver, "+M.getUsername(SettingActivity.this)+" has invited you to download and register to MOOD INDIA app. Download and get reward of 500 rupees. A minimum of 100 rupees reward to your Paytm registered number is guaranteed. Hurry up, limited time offer. Thank me later.";

            ShareSheetStyle shareSheetStyle = new ShareSheetStyle(SettingActivity.this, "MoodIndia", messageBody)
                    .setCopyUrlStyle(getResources().getDrawable(android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                    .setMoreOptionStyle(getResources().getDrawable(android.R.drawable.ic_menu_search), "Show more")
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.WHATS_APP)
                    .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                    .setAsFullWidthStyle(true)
                    .setSharingTitle("Share With");

            branchUniversalObject.showShareSheet(this,
                    linkProperties,
                    shareSheetStyle,
                    new Branch.BranchLinkShareListener() {

                        @Override
                        public void onShareLinkDialogLaunched() {

                        }

                        @Override
                        public void onShareLinkDialogDismissed() {

                        }

                        @Override
                        public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {

                        }

                        @Override
                        public void onChannelSelected(String channelName) {

                        }
                    });
        }
        else
        {
            Toast.makeText(SettingActivity.this, "Please update the app , to earn referral points", Toast.LENGTH_SHORT).show();
        }


       /* Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                .setMessage(getString(R.string.invitation_message))
                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText(getString(R.string.invitation_cta))

                .build();
        startActivityForResult(intent, REQUEST_INVITE);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // [START_EXCLUDE]
              //  showMessage(getString(R.string.send_failed));
                // [END_EXCLUDE]
            }
        }
        else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                //String.valueOf(place.getLatLng().latitude),String.valueOf(place.getLatLng().longitude);
                //update latitude and longitude
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    int PLACE_PICKER_REQUEST = 4353;
    private void initview() {

        tvprofile = (TextView) findViewById(R.id.tveditprofile);
        tvlogout = (TextView) findViewById(R.id.tvlogout);
        tvpwd = (TextView) findViewById(R.id.tvchangepwd);
        tvshare = (TextView) findViewById(R.id.tvshare);
        tvaboutus = (TextView) findViewById(R.id.tvaboutus);
       // tv_credits = (TextView) findViewById(R.id.tv_credits);//uname,tvmycredts,tv_invitenearn;
        uname = (MagicTextView) findViewById(R.id.uname);
        tvmycredts = (LinearLayout) findViewById(R.id.tvmycredts);
        tv_invitenearn = (LinearLayout) findViewById(R.id.tv_invitenearn);
        //tv_credits = (TextView) findViewById(R.id.tv_credits);

        swpush = (SwitchCompat) findViewById(R.id.swpush);
      //  changepwdLyt = (LinearLayout) findViewById(R.id.changepwdLyt);
        tvaboutus = (MagicTextView) findViewById(R.id.tvaboutus);
        tv_partcommunitypolls = (MagicTextView) findViewById(R.id.tv_partcommunitypolls);
        tvaboutus.setOnClickListener(SettingActivity.this);
        tv_partcommunitypolls.setOnClickListener(SettingActivity.this);
        tvshare = (MagicTextView) findViewById(R.id.tvshare);
        tvshare.setOnClickListener(SettingActivity.this);
        tvfaq = (MagicTextView) findViewById(R.id.tvfaq);
       // invitetv = (MagicTextView) findViewById(R.id.invitetv);

        tvfaq.setOnClickListener(SettingActivity.this);
        //invitetv.setOnClickListener(SettingActivity.this);
        tvuserguide = (MagicTextView) findViewById(R.id.tvuserguide);
        tvuserguide.setOnClickListener(SettingActivity.this);
        tvcontactus = (MagicTextView) findViewById(R.id.tvcontactus);
        tvcontactus.setOnClickListener(SettingActivity.this);
        tvlogout = (MagicTextView) findViewById(R.id.tvlogout);
        tvlogout.setOnClickListener(SettingActivity.this);
        tvEmailVerified = (MagicTextView) findViewById(R.id.tvEmailVerified);
        app_version = (MagicTextView) findViewById(R.id.app_version);
        btnVerify = (MagicButton) findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(SettingActivity.this);
        lytverification = (LinearLayout) findViewById(R.id.lytverification);
        sw_profilevisibility=(Switch) findViewById(R.id.sw_profilevisibility);

        iv_profile_picture=(ImageView) findViewById(R.id.iv_profile_picture);
        tvpwd.setOnClickListener(this);
        tvprofile.setOnClickListener(this);
        tvlogout.setOnClickListener(this);
        tvaboutus.setOnClickListener(this);
        tvshare.setOnClickListener(this);
       // tv_credits.setOnClickListener(this);
        swpush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                M.setpush(b, SettingActivity.this);
            }
        });
        sw_profilevisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    if(latitude.equalsIgnoreCase("0.0"))
                    {
                        //update address

                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                        try {

                            startActivityForResult(builder.build(SettingActivity.this), PLACE_PICKER_REQUEST);

                        } catch (GooglePlayServicesRepairableException e) {
                            e.printStackTrace();
                        } catch (GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }
                    }else
                    {

                        updateProfileVisibilty("0");
                    }
                }

                else {
                        updateProfileVisibilty("1");
                }
            }
        });
        app_version.setText("App Version: V"+BuildConfig.VERSION_NAME+"."+BuildConfig.VERSION_CODE);
        mProfilePojo=new ProfilePojo();
        uname.setText(M.getUsername(SettingActivity.this));
        tvmycredts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mnVerificationStatus.equalsIgnoreCase("0"))
                {
                    new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("Currently reward system and in-app purchases are only available for users in India")
                            .show();
                }
                else {
                    Intent it = new Intent(SettingActivity.this, MyCredits.class);
                    startActivity(it);
                    overridePendingTransition(0, 0);
                }
            }
        }); //  ;,tv_invitenearn;
        tv_invitenearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final DialogPlus notificationDialog = DialogPlus.newDialog(SettingActivity.this)
                        .setGravity(Gravity.CENTER)
                        .setContentHolder(new ViewHolder(R.layout.custom_layout_invite_alert))
                        .setCancelable(true)
                        .create();


                View view=notificationDialog.getHolderView();

                final TextView tvNoticont=(TextView) view.findViewById(R.id.tv_noticont);
                final TextView tv_notification=(TextView) view.findViewById(R.id.tv_notification);
                final CheckBox cb_dontsee=(CheckBox) view.findViewById(R.id.cb_dontsee);
                final DocumentView dv=(DocumentView)view.findViewById(R.id.blogText) ;
                Typeface type = Typeface.createFromAsset(getAssets(),"fonts/ssr.ttf");
                dv.getDocumentLayoutParams().setTextTypeface(type);


                tvNoticont.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onInviteClicked();
                      /*  if(cb_dontsee.isChecked())
                        {
                            M.setNoPollAnswerPopup("1",PollScreen.this);
                        }
                        Intent it=new Intent(PollScreen.this,PollScreen.class);
                        it.putExtra("pollid",pollid);
                        it.putExtra("from","active");
                        finish();
                        startActivity(it);
                        notificationDialog.dismiss();*/
                    }
                });
                notificationDialog.show();

             //   onInviteClicked();
            }
        });



        if(M.getPhone(SettingActivity.this).equalsIgnoreCase("1"))
        {
            tvmycredts.setVisibility(View.GONE);
            tv_invitenearn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tveditprofile) {
            Intent it = new Intent(SettingActivity.this, EditProfile.class);
            startActivity(it);
            overridePendingTransition(0, 0);
        }else if (view.getId() == R.id.tvchangepwd) {
            Intent it = new Intent(SettingActivity.this, ChangePassword.class);
            startActivity(it);
            overridePendingTransition(0, 0);
        } else if (view.getId() == R.id.tvlogout) {
            new AlertDialog.Builder(SettingActivity.this)
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            M.logOut(SettingActivity.this);
                            Intent it = new Intent(SettingActivity.this, LoginActivity.class);
                            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(it);
                            finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        } else if (view.getId() == R.id.tvaboutus) {
            String url = "http://www.moodindia.in/about.html";

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
          //  new FinestWebView.Builder(SettingActivity.this).show(url);
          /*  Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);*/
        /*    Intent it = new Intent(SettingActivity.this, AboutUs.class);
            finish();
            startActivity(it);*/
            overridePendingTransition(0, 0);
        } else if (view.getId() == R.id.tvshare) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            //this is the text that will be shared
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("Download Mood India App, for more www.moodindia.in"));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));
        } else if (view.getId() == R.id.tvfaq) {
            String url = "http://www.moodindia.in/faq.html";
            new FinestWebView.Builder(SettingActivity.this).show(url);
          /*  Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);*/
        } else if (view.getId() == R.id.tvuserguide) {
            String url = "http://www.moodindia.in/user-guide/index.html";
            new FinestWebView.Builder(SettingActivity.this).show(url);
         /*   Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);*/

        } else if (view.getId() == R.id.tvcontactus) {
            String url = "http://moodindia.in/blog/?page_id=111";
            new FinestWebView.Builder(SettingActivity.this).show(url);
          /*  Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);*/

        } else if (view.getId() == R.id.btnVerify) {
                //send verification email
        }else if (view.getId() == R.id.tv_partcommunitypolls) {

            Intent it = new Intent(SettingActivity.this, PrivatePolls.class);
            startActivity(it);
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent it = new Intent(this, HomeActivity.class);
            startActivity(it);
            finish();
            overridePendingTransition(0, 0);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(this, HomeActivity.class);
        startActivity(it);
        finish();
        overridePendingTransition(0, 0);
    }

    public void getverifyEmail(String mEmail) {
        OkHttpClient client = new OkHttpClient();
        //System.out.println("http://139.59.41.254/app/Http/Controllers/UserEmailVarificationStatus.php?email=" + mEmail);
        Request request = new Request.Builder().url(ConsantURL+"app/Http/Controllers/UserEmailVarificationStatus.php?email=" + mEmail).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(final Response response) throws IOException {
                // //Log.i("response",response.body());
                ////Log.i("response",response.body().string());

                if (response.isSuccessful()) {
                    //processSliderImages(response.body().string());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (response.body().string().toString().equalsIgnoreCase("0")) {
                                    //email not verified
                                    lytverification.setVisibility(View.VISIBLE);
                                } else {
                                    //email verified
                                    lytverification.setVisibility(View.GONE);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    public String latitude="0.0";
    public String longitude="0.0";

    ProfilePojo mProfilePojo;
    public void getUserDetails()
    {
        AuthenticationAPI mAuthenticationAPI = APIService.createService(AuthenticationAPI.class);
        mAuthenticationAPI.getUserData(M.getID(SettingActivity.this), new retrofit.Callback<ProfilePojo>() {
            @Override
            public void success(ProfilePojo profilePojo, retrofit.client.Response response) {
                getverifyEmail(M.getEmail(SettingActivity.this).toString());
                mProfilePojo=profilePojo;

                Picasso.with(SettingActivity.this)
                        .load(AppConst.imgurl + mProfilePojo.getProfile_picture().toString())
                        .placeholder(R.drawable.avatar_fred)
                        .error(R.drawable.avatar_fred)
                        .into(iv_profile_picture);

                mnVerificationStatus=mProfilePojo.getMnverfistatus();
                latitude=mProfilePojo.getLatitude();
                longitude=mProfilePojo.getLongitude();


                if(latitude.equalsIgnoreCase("0.0"))
                {
                    sw_profilevisibility.setChecked(false);
                    if((mProfilePojo.getProfileType().equalsIgnoreCase("0")))
                    {
                        //update profile visibiltiy to one
                        updateProfileVisibilty("1");
                    }
                }
                else
                {
                    if(mProfilePojo.getProfileType().equalsIgnoreCase("0"))
                        sw_profilevisibility.setChecked(true);
                    else
                        sw_profilevisibility.setChecked(false);
                }



            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
    public void updateProfileVisibilty(String visiblity)
    {
        PollAPI mPollAPI = APIService.createService(PollAPI.class);
        mPollAPI.updateProfileVisiblity(M.getID(SettingActivity.this), visiblity, new retrofit.Callback<StatusPojo>() {
            @Override
            public void success(StatusPojo statusPojo, retrofit.client.Response response) {
                Toast.makeText(SettingActivity.this, "Profile visibility updated successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SettingActivity.this, "Profile visibility updated successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public String mnVerificationStatus="1";


}
