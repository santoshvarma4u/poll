package com.app.moodindia.model;

/**
 * Created by SantoshT on 11/10/2017.
 */

public class UserCategories {
    String usercategories;

    public String getUsercategories() {
        return usercategories;
    }

    public void setUsercategories(String usercategories) {
        this.usercategories = usercategories;
    }
}
