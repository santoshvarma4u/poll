package com.app.moodindia.model;

/**
 * Created by tskva on 11/12/2017.
 */

public class KeywordsPojo {

    String keywords;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
}
