package com.app.moodindia.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;


public class M {
    static ProgressDialog pDialog;
    private static SharedPreferences mSharedPreferences;

    public static void showLoadingDialog(Context mContext) {
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(mContext.getString(R.string.please_wait));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

  public static void showTextLoadingDialog(Context mContext,String text) {
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(text);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void hideLoadingDialog() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    public static void showToast(Context mContext, String message) {

      Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();

    }

    public static void T(Context mContext, String Message) {
        Toast.makeText(mContext, Message, Toast.LENGTH_SHORT).show();
    }

    public static void L(String Message) {
        Log.e(AppConst.TAG, Message);
    }



    public static boolean setpush(boolean ispushON, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("ispushON", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("ispushON", ispushON);
        return editor.commit();
    }

    public static boolean ispushON(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("ispushON", 0);
        return mSharedPreferences.getBoolean("ispushON", true);
    }

    public static boolean setExternalAlert(boolean ispushON, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("isExternalAlert", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean("isExternalAlert", ispushON);
        return editor.commit();
    }

    public static boolean isExternalAlert(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("isExternalAlert", 0);
        return mSharedPreferences.getBoolean("isExternalAlert", false);
    }


    public static boolean setUsername(String username, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("username", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("username", username);
        return editor.commit();
    }

    public static String getUsername(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("username", 0);
        return mSharedPreferences.getString("username", "");
    }


    public static boolean setEmail(String email, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("email", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("email", email);
        return editor.commit();
    }

    public static String getEmail(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("email", 0);
        return mSharedPreferences.getString("email", "");
    }

    public static boolean setMyReferralID(String email, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("myreferralid", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("myreferralid", email);
        return editor.commit();
    }

    public static String getMyReferralID(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("myreferralid", 0);
        return mSharedPreferences.getString("myreferralid", "");
    }

 public static boolean setLoginType(String logintype, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("logintype", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("logintype", logintype);
        return editor.commit();
    }

    public static String getLoginType(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("logintype", 0);
        return mSharedPreferences.getString("logintype", null);
    }


    public static boolean setPhone(String phone, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("phone", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("phone", phone);
        return editor.commit();
    }

    public static String getPhone(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("phone", 0);
        return mSharedPreferences.getString("phone", "");
    }

    public static boolean setFCMToken(String phone, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("fcmtoken", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("fcmtoken", phone);
        return editor.commit();
    }

    public static String getFCMToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("fcmtoken", 0);
        return mSharedPreferences.getString("fcmtoken", "");
    }


    public static boolean setCountry(String country, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("country", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("country", country);
        return editor.commit();
    }

    public static String getCountry(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("country", 0);
        return mSharedPreferences.getString("country", "");
    }

    public static boolean setBday(String bday, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("bday", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("bday", bday);
        return editor.commit();
    }

    public static String getBday(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("bday", 0);
        return mSharedPreferences.getString("bday", "");
    }

    public static boolean setGender(String gender, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("gender", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("gender", gender);
        return editor.commit();
    }

    public static String getGender(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("gender", 0);
        return mSharedPreferences.getString("gender", "");
    }

    public static boolean setUserpic(String userpic, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("userpic", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("userpic", userpic);
        return editor.commit();
    }

    public static String getUserpic(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("userpic", 0);
        return mSharedPreferences.getString("userpic", "");
    }

    public static boolean setlogintype(String logintype, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("logintype", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString("logintype", logintype);
        return editor.commit();
    }

    public static String getlogintype(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("logintype", 0);
        return mSharedPreferences.getString("logintype", "");
    }

    public static void logOut(Context mContext) {
        setID("0", mContext);
        setUsername(null, mContext);
        setPhone(null,mContext);
        setEmail(null,mContext);
        setCountry(null,mContext);
        setBday(null,mContext);
        setGender(null,mContext);
        setlogintype(null,mContext);
        AppConst.selpoll=null;
        AppConst.gcm_id="";
        mSharedPreferences.getAll().clear();
    }

    public static boolean setID(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("userid", ID);
        return editor.commit();
    }

    public static String getID(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", 0);
        return mSharedPreferences.getString("userid", "0");
    }


    public static boolean setNoPollAnswerPopup(String ID, Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("pollanswerpopup", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("pollanswerpopup", ID);
        return editor.commit();
    }

    public static String getPollAnswerPopup(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("pollanswerpopup", 0);
        return mSharedPreferences.getString("pollanswerpopup", "0");
    }


    public static String loadSavedPreferences(String etvalue,Context ctx) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext());
        String value=mSharedPreferences.getString(etvalue, "0");
        return value;
    }


    //Save String Value
    public static void savePreferences(String key, String value,Context ctx) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext());
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(key, value);
        edit.commit();
    }


    public static boolean isValidUrl(String text) {
        return Patterns.WEB_URL.matcher(text).matches();
    }


    public static void showNotification(Context mContext, Intent resultIntent, String title, String text, int id) {
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);

        if(M.getUsername(mContext) != null) {
            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            long when = System.currentTimeMillis();
            mNotifyBuilder = new NotificationCompat.Builder(mContext)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setWhen(when)
                    .setSmallIcon(R.mipmap.ic_launcher);

            mNotifyBuilder.setContentIntent(resultPendingIntent);

            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            mNotifyBuilder.setAutoCancel(true);

            mNotificationManager.notify(id, mNotifyBuilder.build());
        }
    }

    public static SharedPreferences.Editor editSharedPref(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        return editor;
    }
    public static SharedPreferences getSharedPref(Context mContext) {
        return  mContext.getSharedPreferences("settings", 0);
    }

    public static boolean setGCM(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("GCMregistered", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putBoolean("GCMregistered", false);
        return editor.commit();
    }

    public static String getToken(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("settings", 0);
        return mSharedPreferences.getString("token", "");
    }

    public static boolean setRefId(Context mContext,String rawString) {
        mSharedPreferences = mContext.getSharedPreferences("ReqRefid", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString("ReqRefid", rawString);
        return editor.commit();
    }

    public static String getRefId(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("ReqRefid", 0);
        return mSharedPreferences.getString("ReqRefid", "");
    }
 public static boolean setCredits(Context mContext,int rawString) {
        mSharedPreferences = mContext.getSharedPreferences("branchcredits", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putInt("branchcredits", rawString);
        return editor.commit();
    }

    public static Integer getCredits(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("branchcredits", 0);
        return mSharedPreferences.getInt("branchcredits", 0);
    }

 public static boolean setNotificationCount(Context mContext,int rawString) {
        mSharedPreferences = mContext.getSharedPreferences("notificationCount", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt("notificationCount", rawString);
        return editor.commit();
    }

    public static int getNotificationCount(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences("notificationCount", 0);
        return mSharedPreferences.getInt("notificationCount", 0);
    }


    public static boolean setFavs(String favs,Context mContext){

        mSharedPreferences=mContext.getSharedPreferences("favourites",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("favourites",favs);
        return editor.commit();

    }

    public static String getFavs(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("favourites", 0);
        return mSharedPreferences.getString("favourites", "");
    }

    public static boolean setMyCountry(String countryName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycountry",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycountry",countryName);
        return editor.commit();
    }

    public static boolean setMyCategory(String categoryName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycategory",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycategory",categoryName);
        return editor.commit();
    }

    public static boolean setMyCategoryID(String categoryid,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycategoryid",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycategoryid",categoryid);
        return editor.commit();
    }

    public static boolean setMyState(String stateName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mystate",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mystate",stateName);
        return editor.commit();
    }

    public  static void RemoveMyState(Context mcontext)
    {
        mSharedPreferences =PreferenceManager.getDefaultSharedPreferences(mcontext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("mystate");
        editor.clear();
        editor.apply();

    }
    public static boolean setMyCity(String cityName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycity",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycity",cityName);
        return editor.commit();
    }
    public  static void RemoveMyCity(Context mcontext)
    {
        mSharedPreferences =PreferenceManager.getDefaultSharedPreferences(mcontext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("mycity");
        editor.clear();
        editor.apply();

    }
    public static boolean setMyCountryID(String countryName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycountryid",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycountryid",countryName);
        return editor.commit();
    }

    public static boolean setMyStateID(String stateName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mystateid",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mystateid",stateName);
        return editor.commit();
    }
    public  static void RemoveMyStateID(Context mcontext)
    {
        mSharedPreferences =PreferenceManager.getDefaultSharedPreferences(mcontext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("mystateid");
        editor.clear();
        editor.apply();

    }
    public static boolean setMyCityID(String cityName,Context mContext)
    {
        mSharedPreferences=mContext.getSharedPreferences("mycityid",0);
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString("mycityid",cityName);
        return editor.commit();
    }
    public  static void RemoveMyCityID(Context mcontext)
    {
        mSharedPreferences =PreferenceManager.getDefaultSharedPreferences(mcontext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("mycityid");
        editor.clear();
        editor.apply();

    }
    public static String getMyCountry(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycountry", 0);
        return mSharedPreferences.getString("mycountry", "");
    }

    public static String getMyState(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mystate", 0);
        return mSharedPreferences.getString("mystate", "");
    }
    public static String getMyCity(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycity", 0);
        return mSharedPreferences.getString("mycity", "");
    }
    public static String getMyCountryID(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycountryid", 0);
        return mSharedPreferences.getString("mycountryid", "");
    }

    public static String getMyStateID(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mystateid", 0);
        return mSharedPreferences.getString("mystateid", "");
    }
    public static String getMyCityID(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycityid", 0);
        return mSharedPreferences.getString("mycityid", "");
    }
    public static String getMyCategoryID(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycategoryid", 0);
        return mSharedPreferences.getString("mycategoryid", "");
    }
    public static String getMyCategory(Context mContext)
    {
        mSharedPreferences = mContext.getSharedPreferences("mycategory", 0);
        return mSharedPreferences.getString("mycategory", "");
    }
    public  static void RemoveMycategoryID(Context mcontext)
    {
        mSharedPreferences =PreferenceManager.getDefaultSharedPreferences(mcontext);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove("mycategoryid");
        editor.clear();
        editor.apply();

    }



}
