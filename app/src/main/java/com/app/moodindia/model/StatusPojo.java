package com.app.moodindia.model;

/**
 * Created by SantoshT on 6/21/2017.
 */

public class StatusPojo {
    String status;
    String pollid;
    String referralid;

    public String getReferralid() {
        return referralid;
    }

    public void setReferralid(String referralid) {
        this.referralid = referralid;
    }

    public String getPollid() {
        return pollid;
    }

    public void setPollid(String pollid) {
        this.pollid = pollid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
