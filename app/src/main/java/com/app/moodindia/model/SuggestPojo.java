package com.app.moodindia.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SantoshT on 1/11/2017.
 */

public class SuggestPojo {

    String id;
    String user_id;
    String poll_question;
    String poll_description;
    String poll_optionA;
    String poll_optionB;
    String poll_optionC;
    String poll_optionD;
    String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoll_optionA() {
        return poll_optionA;
    }

    public void setPoll_optionA(String poll_optionA) {
        this.poll_optionA = poll_optionA;
    }

    public String getPoll_optionB() {
        return poll_optionB;
    }

    public void setPoll_optionB(String poll_optionB) {
        this.poll_optionB = poll_optionB;
    }

    public String getPoll_optionC() {
        return poll_optionC;
    }

    public void setPoll_optionC(String poll_optionC) {
        this.poll_optionC = poll_optionC;
    }

    public String getPoll_optionD() {
        return poll_optionD;
    }

    public void setPoll_optionD(String poll_optionD) {
        this.poll_optionD = poll_optionD;
    }

    public String getPoll_moreinfo() {
        return poll_moreinfo;
    }

    public void setPoll_moreinfo(String poll_moreinfo) {
        this.poll_moreinfo = poll_moreinfo;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    String poll_moreinfo;
    @SerializedName("image_path")
    @Expose
    String image_path;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPoll_question() {
        return poll_question;
    }

    public void setPoll_question(String poll_question) {
        this.poll_question = poll_question;
    }

    public String getPoll_description() {
        return poll_description;
    }

    public void setPoll_description(String poll_description) {
        this.poll_description = poll_description;
    }
}
