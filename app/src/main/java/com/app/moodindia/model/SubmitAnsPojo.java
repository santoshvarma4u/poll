package com.app.moodindia.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubmitAnsPojo {

    private Integer success;
    private List<Pollresult> pollresult = new ArrayList<Pollresult>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     * The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     *
     * @return
     * The pollresult
     */
    public List<Pollresult> getPollresult() {
        return pollresult;
    }

    /**
     *
     * @param pollresult
     * The pollresult
     */
    public void setPollresult(List<Pollresult> pollresult) {
        this.pollresult = pollresult;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}