package com.app.moodindia.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by SantoshT on 5/31/2017.
 */

public class PrivatePollOptionPojo {
    private List<PrivatePollResult> pollresult = new ArrayList<PrivatePollResult>();
    private Integer pollid;
    private String info;
    private String question;
    private String starttime;
    private String endtime;
    private String type;
    private String image;
    private Boolean isvoted;
    String selected_option;
    String twittertag,youtubetag,chattag;

    public String getTwittertag() {
        return twittertag;
    }

    public void setTwittertag(String twittertag) {
        this.twittertag = twittertag;
    }

    public String getYoutubetag() {
        return youtubetag;
    }

    public void setYoutubetag(String youtubetag) {
        this.youtubetag = youtubetag;
    }

    public String getChattag() {
        return chattag;
    }

    public void setChattag(String chattag) {
        this.chattag = chattag;
    }

    public String getPoll_counts() {
        return poll_counts;
    }

    public void setPoll_counts(String poll_counts) {
        this.poll_counts = poll_counts;
    }

    String poll_counts;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The pollresult
     */
    public List<PrivatePollResult> getPollresult() {
        return pollresult;
    }

    /**
     *
     * @param pollresult
     * The pollresult
     */
    public void setPollresult(List<PrivatePollResult> pollresult) {
        this.pollresult = pollresult;
    }

    /**
     *
     * @return
     * The pollid
     */
    public Integer getPollid() {
        return pollid;
    }

    /**
     *
     * @param pollid
     * The pollid
     */
    public void setPollid(Integer pollid) {
        this.pollid = pollid;
    }

    /**
     *
     * @return
     * The info
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     *
     * @return
     * The question
     */
    public String getQuestion() {
        return question;
    }

    /**
     *
     * @param question
     * The question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     *
     * @return
     * The starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     *
     * @param starttime
     * The starttime
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     *
     * @return
     * The endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     *
     * @param endtime
     * The endtime
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public String getSelected_option() {
        return selected_option;
    }

    public void setSelected_option(String selected_option) {
        this.selected_option = selected_option;
    }

    /**
     *
     * @return
     * The isvoted
     */
    public Boolean getIsvoted() {
        return isvoted;
    }

    /**
     *
     * @param isvoted
     * The isvoted
     */
    public void setIsvoted(Boolean isvoted) {
        this.isvoted = isvoted;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
