package com.app.moodindia.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PollOptionPojo {

    private List<Pollresult> pollresult = new ArrayList<Pollresult>();
    private Integer pollid;
    private String info;
    private String question;
    private String starttime;
    private String endtime;
    private String type;
    private String image;
    private Boolean isvoted;
    String selected_option;
    String twittertag,youtubetag,chattag;
    String username;
    String youtubelink;
    String nextid,previd;
    String reaction;
    String cat_name;
    String likecount;
    String createuserid;
    String anonymous;
    String groupid;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    String pollCity,pollState;

    public String getCreateuserid() {
        return createuserid;
    }

    public void setCreateuserid(String createuserid) {
        this.createuserid = createuserid;
    }

    public String getPollCity() {
        return pollCity;
    }

    public void setPollCity(String pollCity) {
        this.pollCity = pollCity;
    }

    public String getPollState() {
        return pollState;
    }

    public void setPollState(String pollState) {
        this.pollState = pollState;
    }

    public String getLikecount() {
        return likecount;
    }

    public void setLikecount(String likecount) {
        this.likecount = likecount;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getNextid() {
        return nextid;
    }

    public void setNextid(String nextid) {
        this.nextid = nextid;
    }

    public String getPrevid() {
        return previd;
    }

    public void setPrevid(String previd) {
        this.previd = previd;
    }

    public String getYoutubelink() {
        return youtubelink;
    }

    public void setYoutubelink(String youtubelink) {
        this.youtubelink = youtubelink;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTwittertag() {
        return twittertag;
    }

    public void setTwittertag(String twittertag) {
        this.twittertag = twittertag;
    }

    public String getYoutubetag() {
        return youtubetag;
    }

    public void setYoutubetag(String youtubetag) {
        this.youtubetag = youtubetag;
    }

    public String getChattag() {
        return chattag;
    }

    public void setChattag(String chattag) {
        this.chattag = chattag;
    }

    public String getPoll_counts() {
        return poll_counts;
    }

    public void setPoll_counts(String poll_counts) {
        this.poll_counts = poll_counts;
    }

    String poll_counts;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The pollresult
     */
    public List<Pollresult> getPollresult() {
        return pollresult;
    }

    /**
     *
     * @param pollresult
     * The pollresult
     */
    public void setPollresult(List<Pollresult> pollresult) {
        this.pollresult = pollresult;
    }

    /**
     *
     * @return
     * The pollid
     */
    public Integer getPollid() {
        return pollid;
    }

    /**
     *
     * @param pollid
     * The pollid
     */
    public void setPollid(Integer pollid) {
        this.pollid = pollid;
    }

    /**
     *
     * @return
     * The info
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     *
     * @return
     * The question
     */
    public String getQuestion() {
        return question;
    }

    /**
     *
     * @param question
     * The question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     *
     * @return
     * The starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     *
     * @param starttime
     * The starttime
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     *
     * @return
     * The endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     *
     * @param endtime
     * The endtime
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public String getSelected_option() {
        return selected_option;
    }

    public void setSelected_option(String selected_option) {
        this.selected_option = selected_option;
    }

    /**
     *
     * @return
     * The isvoted
     */
    public Boolean getIsvoted() {
        return isvoted;
    }

    /**
     *
     * @param isvoted
     * The isvoted
     */
    public void setIsvoted(Boolean isvoted) {
        this.isvoted = isvoted;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
