package com.app.moodindia.model;

import java.io.Serializable;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public class CategoryPojo implements Serializable {

    String cat_id;
    String cat_name;
    boolean isSelected;
    String popular;


    public String getPopular() {
        return popular;
    }

    public void setPopular(String popular) {
        this.popular = popular;
    }

    private static final long serialVersionUID = 1L;

    public CategoryPojo()
    {

    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setCat_img(String cat_img) {
        this.cat_img = cat_img;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    String cat_img;

    public String getCat_id() {
        return cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getCat_img() {
        return cat_img;
    }
}
