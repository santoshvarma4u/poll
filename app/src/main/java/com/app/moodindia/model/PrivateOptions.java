package com.app.moodindia.model;

import android.net.Uri;

/**
 * Created by SantoshT on 6/14/2017.
 */

public class PrivateOptions {

    String imagePath,pollOption;
    Uri selectedImageUri;
    String id;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Uri getSelectedImageUri() {
        return selectedImageUri;
    }

    public void setSelectedImageUri(Uri selectedImageUri) {
        this.selectedImageUri = selectedImageUri;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPollOption() {
        return pollOption;
    }

    public void setPollOption(String pollOption) {
        this.pollOption = pollOption;
    }
}
