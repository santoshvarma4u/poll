package com.app.moodindia.model;

/**
 * Created by Santosh Varma on 18/10/16.
 */

public class ChangePwdPojo {

    String success,message;

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
