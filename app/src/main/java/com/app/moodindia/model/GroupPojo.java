package com.app.moodindia.model;

/**
 * Created by SantoshT on 11/24/2017.
 */

public class GroupPojo {

    String ownername,groupid,groupname,created_by,name,description,image,pollcount,noofparticipants,uidforchat,type,latitude,longitude,address,distance;

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUidforchat() {
        return uidforchat;
    }

    public void setUidforchat(String uidforchat) {
        this.uidforchat = uidforchat;
    }

    public String getPollcount() {
        return pollcount;
    }
    public void setPollcount(String pollcount) {
        this.pollcount = pollcount;
    }
    public String getNoofparticipants() {
        return noofparticipants;
    }
    public void setNoofparticipants(String noofparticipants) {
        this.noofparticipants = noofparticipants;
    }
    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
