package com.app.moodindia.model;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public class SuccessPojo {

    String success,user_id,likecount,referralid,status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getReferralid() {
        return referralid;
    }

    public void setReferralid(String referralid) {
        this.referralid = referralid;
    }

    public String getLikecount() {
        return likecount;
    }

    public void setLikecount(String likecount) {
        this.likecount = likecount;
    }

    public String getSuccess() {
        return success;
    }

    public String getUser_id() {
        return user_id;
    }
}
