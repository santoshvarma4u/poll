package com.app.moodindia.model;

/**
 * Created by SantoshT on 9/5/2017.
 */

public class CGImagePojo {

    String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
