package com.app.moodindia.model;

/**
 * Created by Santosh Varma on 14/10/16.
 */

public class PollPojo {

    String question_id;
    String info;
    String question;
    String image;
    String mnverfistatus;

    public String getMnverfistatus() {
        return mnverfistatus;
    }

    public void setMnverfistatus(String mnverfistatus) {
        this.mnverfistatus = mnverfistatus;
    }

    public String getMyreferralid() {
        return myreferralid;
    }

    public void setMyreferralid(String myreferralid) {
        this.myreferralid = myreferralid;
    }

    String myreferralid;
    String cat_id;
    String cat_name;
    String twittertag;
    String youtubetag;
    String chattag;

    String login_status;




    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }




    public void setInfo(String info) {
        this.info = info;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTwittertag() {
        return twittertag;
    }

    public void setTwittertag(String twittertag) {
        this.twittertag = twittertag;
    }

    public String getYoutubetag() {
        return youtubetag;
    }

    public void setYoutubetag(String youtubetag) {
        this.youtubetag = youtubetag;
    }

    public String getChattag() {
        return chattag;
    }

    public void setChattag(String chattag) {
        this.chattag = chattag;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    String start_time;
    boolean isVisible;

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public String getInfo() {
        return info;
    }

    public String getQuestion() {
        return question;
    }

    public String getImage() {
        return image;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }
}
