
package com.app.moodindia.model;

import java.util.HashMap;
import java.util.Map;


public class Pollresult {

    private Integer optionid;
    private String optionvalue;
    private String optionimage;
    private Integer optioncount;
    private Integer optionpercentage;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getOptionimage() {
        return optionimage;
    }

    public void setOptionimage(String optionimage) {
        this.optionimage = optionimage;
    }

    /**
     *
     * @return
     * The optionid
     */
    public Integer getOptionid() {
        return optionid;
    }

    /**
     *
     * @param optionid
     * The optionid
     */
    public void setOptionid(Integer optionid) {
        this.optionid = optionid;
    }

    /**
     *
     * @return
     * The optionvalue
     */
    public String getOptionvalue() {
        return optionvalue;
    }

    /**
     *
     * @param optionvalue
     * The optionvalue
     */
    public void setOptionvalue(String optionvalue) {
        this.optionvalue = optionvalue;
    }

    /**
     *
     * @return
     * The optioncount
     */
    public Integer getOptioncount() {
        return optioncount;
    }

    /**
     *
     * @param optioncount
     * The optioncount
     */
    public void setOptioncount(Integer optioncount) {
        this.optioncount = optioncount;
    }

    /**
     *
     * @return
     * The optionpercentage
     */
    public Integer getOptionpercentage() {
        return optionpercentage;
    }

    /**
     *
     * @param optionpercentage
     * The optionpercentage
     */
    public void setOptionpercentage(Integer optionpercentage) {
        this.optionpercentage = optionpercentage;
    }



    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
