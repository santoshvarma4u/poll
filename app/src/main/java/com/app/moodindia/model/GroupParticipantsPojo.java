package com.app.moodindia.model;

/**
 * Created by SantoshT on 11/24/2017.
 */

public class GroupParticipantsPojo {

    String group_id,user_id,status;

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
