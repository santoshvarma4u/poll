package com.app.moodindia.model;

/**
 * Created by SantoshT on 5/31/2017.
 */

public class PrivatePollPojo {

    String question_id,info,question,image;
    String cat_id;
    String userid;
    String login_status;
    String profile_picture;
    String noofparticipants;
    String anonymous;
    String uid;
    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(String anonymous) {
        this.anonymous = anonymous;
    }

    public String getNoofparticipants() {
        return noofparticipants;
    }

    public void setNoofparticipants(String noofparticipants) {
        this.noofparticipants = noofparticipants;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }





    public void setInfo(String info) {
        this.info = info;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    String start_time;
    boolean isVisible;

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public String getInfo() {
        return info;
    }

    public String getQuestion() {
        return question;
    }

    public String getImage() {
        return image;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }


}
