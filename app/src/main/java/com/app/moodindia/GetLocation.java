package com.app.moodindia;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.app.moodindia.model.M;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GetLocation extends AppCompatActivity  implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    String TAG = "Location";
    GoogleApiClient googleApiClient;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    protected LocationRequest mLocationRequest;
    protected Boolean mRequestingLocationUpdates;
    protected Location mCurrentLocation, mLastLocation;
    boolean isConnected;
    protected String mLastUpdateTime;
    protected LocationSettingsRequest mLocationSettingsRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);
        checkConnection();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)&&locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //Log.d("hi", "enabled");
           // M.showLoadingDialog(GetLocation.this);
            geocoder = new Geocoder(this, Locale.getDefault());
            updateValuesFromBundle(savedInstanceState);
            createLocationRequest();
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(AppIndex.API)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .enableAutoManage(this, this)
                    .build();
            buildLocationSettingsRequest();
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Log.d("hi", "not enabled");
            showGPSDisabledAlertToUser();
            geocoder = new Geocoder(this, Locale.getDefault());
            updateValuesFromBundle(savedInstanceState);
            createLocationRequest();
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(AppIndex.API)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .enableAutoManage(this, this)
                    .build();
            buildLocationSettingsRequest();
        }
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        //Log.d("hi", "hi1");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(true)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setPositiveButton(" Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                checkLocationSettings();
                            }
                        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onStart(){
        super.onStart();
        // M.showLoadingDialog(GetLocation.this);
        Log.d(TAG, "onStart fired ..............");
        googleApiClient.connect();
    }

    List<Address> addresses;
    Geocoder geocoder;
    public void setLocation(final double lat, final double lon) throws IOException {
         geocoder = new Geocoder(this, Locale.getDefault());

        try {
            geocoder = new Geocoder(GetLocation.this, Locale.ENGLISH);
            addresses = geocoder.getFromLocation(lat, lon, 1);

            if (geocoder.isPresent()) {
               /* Toast.makeText(getApplicationContext(),
                        "geocoder present", Toast.LENGTH_SHORT).show();*/
                if (addresses == null || addresses.size()  == 0 ) {
                    Toast.makeText(getApplicationContext(),"Invalid address",Toast.LENGTH_LONG).show();
                    M.showToast(GetLocation.this, "Unable to get proper location, please try manually");
                    M.hideLoadingDialog();
                    Intent intent = new Intent();
                    intent.putExtra("country", "N");
                    intent.putExtra("state", "N");
                    intent.putExtra("city", "N");
                    intent.putExtra("latitude", String.valueOf(lat));
                    intent.putExtra("longitude", String.valueOf(lon));
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else
                {
                    if(addresses.get(0).getAdminArea()==null || addresses.get(0).getAdminArea().isEmpty())
                    {
                        //okhttp service
                       OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder().url("http://139.59.41.254/app/Http/Controllers/GetStateManuallyController.php?cityname="+addresses.get(0).getLocality()+"&countryname="+addresses.get(0).getCountryName()).build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(com.squareup.okhttp.Request request, IOException e) {

                            }

                            @Override
                            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                                // //Log.i("response",response.body());
                                ////Log.i("response",response.body().string());

                                if (response.isSuccessful()) {
                                    //processSliderImages(response.body().string());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent();
                                            intent.putExtra("country", addresses.get(0).getCountryName());
                                            try {
                                                intent.putExtra("state", response.body().string().toString());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            intent.putExtra("city", addresses.get(0).getLocality());
                                            intent.putExtra("latitude", String.valueOf(lat));
                                            intent.putExtra("longitude", String.valueOf(lon));
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    });
                                }
                            }
                        });
                    }

                    else
                    {
                        Intent intent = new Intent();
                        intent.putExtra("country", addresses.get(0).getCountryName());
                        intent.putExtra("state", addresses.get(0).getAdminArea());
                        intent.putExtra("city", addresses.get(0).getLocality());
                        intent.putExtra("latitude", String.valueOf(lat));
                        intent.putExtra("longitude", String.valueOf(lon));
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            } else {
             /*   Toast.makeText(getApplicationContext(),
                        "geocoder not present", Toast.LENGTH_SHORT).show();*/

                if(addresses.get(0).getAdminArea()==null || addresses.get(0).getAdminArea().isEmpty())
                {
                    //okhttp service
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url("http://139.59.41.254/app/Http/Controllers/GetStateManuallyController.php?cityname="+addresses.get(0).getLocality()+"&countryname="+addresses.get(0).getCountryName()).build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(com.squareup.okhttp.Request request, IOException e) {

                        }

                        @Override
                        public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                            // //Log.i("response",response.body());
                            ////Log.i("response",response.body().string());

                            if (response.isSuccessful()) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent();
                                        intent.putExtra("country", addresses.get(0).getCountryName());
                                        try {
                                            intent.putExtra("state", response.body().string().toString());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        intent.putExtra("city", addresses.get(0).getLocality());
                                        intent.putExtra("latitude", String.valueOf(lat));
                                        intent.putExtra("longitude", String.valueOf(lon));
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    }
                                });
                            }
                        }
                    });
                }

                else {
                    Intent intent = new Intent();
                    intent.putExtra("country", addresses.get(0).getCountryName());
                    intent.putExtra("state", addresses.get(0).getAdminArea());
                    intent.putExtra("city", addresses.get(0).getLocality());
                    intent.putExtra("latitude", String.valueOf(lat));
                    intent.putExtra("longitude", String.valueOf(lon));
                    setResult(RESULT_OK, intent);
                    finish();
                }



            }
        } catch (IOException e) {
// TODO Auto-generated catch block

            Log.e("tag", e.getMessage());
        }

    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");
        googleApiClient.disconnect();
        //Log.d(TAG, "isConnected ...............: " + googleApiClient.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        Log.d(TAG, "onConnected - isConnected ...............: " + googleApiClient.isConnected());

        startLocationUpdates();
    }

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }




    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    protected void checkLocationSettings() {
        M.showLoadingDialog(GetLocation.this);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        googleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
    public void callGoogleClinet()
    {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getApplicationContext()).addApi(LocationServices.API).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback() {
                @Override
                public void onResult(Result result) {
                    final Status status = result.getStatus();
                    //final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(GetLocation.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }

            });

        }
    }


    @Override
    public void onLocationChanged(Location location) {
      mLastLocation=location;
        try {

                setLocation(location.getLatitude(),location.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
        }
 Log.d(TAG, "onLocation changed ..............");
        /*try {
            if(location.getLatitude() > 1 && location.getLongitude() > 1)
                setLocation( location.getLatitude(),  location.getLongitude());
            else
                setLocation(0.0,0.0);

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                //Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
           /*     Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");*/

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(GetLocation.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
               /* //Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");*/
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        //Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }
    private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            new LovelyInfoDialog(GetLocation.this)
                    .setIcon(R.drawable.ic_info_outline_white_36dp)
                    .setTopColorRes(R.color.colorPrimary)
                    .setMessage("Sorry! Not connected to internet")
                    .show();
        }
    }
}