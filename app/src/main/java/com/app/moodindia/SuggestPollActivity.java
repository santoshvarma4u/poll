package com.app.moodindia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.model.SuggestPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;
import com.app.moodindia.webservices.PollAPI;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class SuggestPollActivity extends AppCompatActivity {
    private LinearLayout activitySuggestPoll,option_layout,moredetail_layout;
    private EditText etPollQuestion;
    private EditText  etoptiona, etoptionb, etoptionc, etoptiond, etmoreinfo,name;
    private Button btnSuggest, btnchoose;
    private Spinner sp_pollpreference;
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    Uri  selectedImageUri=null;
    ImageView relev_pics,iv_close;
    List<CategoryRealmPojo> list = new ArrayList<>();
    private String TAG = SuggestPollActivity.class.getSimpleName();
    String mediaPath,imagepath;
    private Spinner etPollDesc;
    Context mContext;
    TypedFile typedFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_poll);
        relev_pics = (ImageView)findViewById(R.id.image_pic);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(SuggestPollActivity.this, R.color.colorPrimaryDark));
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);*/
        final Intent it = new Intent(this, HomeActivity.class);
      /*  toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(it);
                finish();
                overridePendingTransition(0, 0);
            }
        });*/
        getCategory();
        initView();
    }
    private void initView(){

        activitySuggestPoll = (LinearLayout) findViewById(R.id.activity_suggest_poll);
        option_layout = (LinearLayout) findViewById(R.id.option_layout);
        moredetail_layout = (LinearLayout) findViewById(R.id.moredetail_layout);

        etPollQuestion = (EditText) findViewById(R.id.et_poll_question);
        etPollDesc = (Spinner) findViewById(R.id.et_poll_desc);
        etoptiona = (EditText) findViewById(R.id.optiona);
        etoptionb = (EditText) findViewById(R.id.optionb);
        etoptionc = (EditText) findViewById(R.id.optionc);
        etoptiond = (EditText) findViewById(R.id.optiond);
        etmoreinfo = (EditText) findViewById(R.id.moreinfo);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        sp_pollpreference=(Spinner) findViewById(R.id.sp_pollpreference);

        name=(EditText)findViewById(R.id.name);
        btnSuggest = (Button) findViewById(R.id.btnSuggest);
        btnchoose = (Button) findViewById(R.id.ch_picture);
        btnchoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SuggestPollActivity.this, new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1257);
                    }
                    else {
                        Intent galleryIntent = new Intent();
                        galleryIntent.setType("image/*");
                        galleryIntent.setAction(Intent.ACTION_PICK);
                        // Chooser of file system options.
                        Intent openGallery = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE);
                    }
                }

            }
        });
        btnSuggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){


                if(etPollQuestion.getText().toString().trim().length() <=0){
                    etPollQuestion.setError("Question Required");
                }else if(etoptiona.getText().toString().trim().length()<=0){
                    etoptiona.setError("Option Required");
                }
                else if(etoptionb.getText().toString().trim().length()<=0){
                    etoptionb.setError("Option Required");
                }
                else if(etoptionc.getText().toString().trim().length()<=0){
                    etoptionc.setError("Option Required");
                }
                else if(etoptiond.getText().toString().trim().length()<=0){
                    etoptiond.setError("Option Required");
                }
                else  if(etPollDesc.getSelectedItem().toString().equals("Please Select"))
                {
                    etoptiond.setError("Select category ");

                }
                else  if(sp_pollpreference.getSelectedItemPosition()==0)
                {
                    Toast.makeText(SuggestPollActivity.this, "Please Select Location Preference", Toast.LENGTH_SHORT).show();
                }

                else if(selectedImageUri == null){
                    suggestPollWithoutImage(etPollQuestion.getText().toString(),etoptiona.getText().toString(),etPollDesc.getSelectedItem().toString(),etoptionb.getText().toString(), etoptionc.getText().toString(), etoptiond.getText().toString(),etmoreinfo.getText().toString(),name.getText().toString(),Preferredtype,PreferredState,PreferredCity);
                }
                else
                {
                    TypedFile typedFile = makeFile(selectedImageUri.toString());
                    suggestPoll(etPollQuestion.getText().toString(),etoptiona.getText().toString(),etPollDesc.getSelectedItem().toString(),etoptionb.getText().toString(), etoptionc.getText().toString(), etoptiond.getText().toString(),etmoreinfo.getText().toString(),typedFile,name.getText().toString(),Preferredtype,PreferredState,PreferredCity);
                }

            }});


        sp_pollpreference.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position==1)
                {
                    Preferredtype="National";
                }
                else if(position==2){
                    Preferredtype="Regional";
                    Intent intent=new Intent(SuggestPollActivity.this,SelectState.class);
                    intent.putExtra("countryid",M.getMyCountryID(SuggestPollActivity.this));
                    intent.putExtra("register", "register");
                    startActivityForResult(intent,2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etPollQuestion.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                option_layout.setVisibility(View.VISIBLE);
                return false;
            }
        });
        etoptiona.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v,MotionEvent event) {
                moredetail_layout.setVisibility(View.VISIBLE);
                btnSuggest.setVisibility(View.VISIBLE);
                return false;
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(SuggestPollActivity.this, HomeActivity.class);
                finish();
                startActivity(it);
                overridePendingTransition(0, 0);
            }
        });
    }

    String Preferredtype="No Preference";

    String PreferredState="",PreferredCity="";



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1257:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent galleryIntent = new Intent();
                    galleryIntent.setType("image/*");
                    galleryIntent.setAction(Intent.ACTION_PICK);
                    // Chooser of file system options.
                    Intent openGallery = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(openGallery, "Open Gallery"), REQUEST_GALLERY_CODE);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Allow Permission", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(SuggestPollActivity.this, SuggestPollActivity.class);
                    finish();
                    startActivity(it);
                }
                break;
        }
    }

    public void suggestPoll(String pollQuestion, String pollDesc, String optiona, String optionb, String optionc, String optiond, String moreinfo, TypedFile filePart,String name,String mPreferredType,String mPreferedState,String mPreferredCity) {
        M.showLoadingDialog(SuggestPollActivity.this);
        btnSuggest.setVisibility(View.GONE);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.suggestPoll(M.getID(SuggestPollActivity.this), pollQuestion, pollDesc, optiona, optionb, optionc, optiond, moreinfo,mPreferredType,mPreferedState,mPreferredCity,filePart,name, new Callback<List<SuggestPojo>>() {
            @Override
            public void success(List<SuggestPojo> pojo, Response response) {
//                SuggestPojo pojo1= (SuggestPojo) response.getBody();
                ////Log.d("res",pojo1.toString());
                Toast.makeText(getApplicationContext(),"Thanks a lot for taking time out and submitting your poll, we will review and update most relevant polls",Toast.LENGTH_LONG).show();
                finish();
                M.hideLoadingDialog();
                btnSuggest.setVisibility(View.VISIBLE);
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs", "fail" + error.getMessage());
                return;
            }
        });
    }
    public void suggestPollWithoutImage(String pollQuestion, String pollDesc, String optiona, String optionb, String optionc, String optiond, String moreinfo,String name,String mPreferredType,String mPreferedState,String mPreferredCity) {
        M.showLoadingDialog(SuggestPollActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.suggestPollWithoutImage(M.getID(SuggestPollActivity.this), pollQuestion, pollDesc, optiona, optionb, optionc, optiond, moreinfo,mPreferredType,mPreferedState,mPreferredCity,name, new Callback<List<SuggestPojo>>() {
            @Override
            public void success(List<SuggestPojo> pojo, Response response) {
//                SuggestPojo pojo1= (SuggestPojo) response.getBody();
                ////Log.d("res",pojo1.toString());
                Toast.makeText(getApplicationContext(),"Thanks a lot for taking time out and submitting your poll, we will review and update most relevant polls",Toast.LENGTH_LONG).show();
                finish();
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs", "fail" + error.getMessage());
                return;
            }
        });
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(SuggestPollActivity.this, HomeActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK&& null!=data) {
            try{
                M.showLoadingDialog(SuggestPollActivity.this);
                // Get real path to make File
                selectedImageUri = Uri.parse(getPath(data.getData()));
                Bitmap bitmap=BitmapFactory.decodeFile(getPath(data.getData()));
                relev_pics.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(selectedImageUri).fit().centerCrop().into(relev_pics);
               // relev_pics.setImageBitmap(bitmap);
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagepath = cursor.getString(columnIndex);

                    Picasso.with(mContext).load(new File(imagepath))
                            .into(relev_pics);


                    cursor.close();


                    relev_pics.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to load  image", Toast.LENGTH_LONG).show();
                }

                M.hideLoadingDialog();
                //Log.d(TAG,"Image path :- "+selectedImageUri);
            }
            catch (Exception e){
                Log.e(TAG,e.getMessage());
            }

        }
        else if (requestCode == 2 && resultCode == RESULT_OK) {//state call back
            if (data.getExtras().getString("StateName").toString().equals("0") && data.getExtras().getString("StateId").toString().equals("0")) {
                sp_pollpreference.setSelection(0);
            } else {
                Intent intent = new Intent(SuggestPollActivity.this, SelectCity.class);
                intent.putExtra("stateid", data.getExtras().getString("StateId").toString());
                intent.putExtra("register", "register");
                PreferredState = data.getExtras().getString("StateName").toString();
                startActivityForResult(intent, 3);
            }
        } else if (requestCode == 3 && resultCode == RESULT_OK) {//city call back

            if (data.getExtras().getString("CityName").toString().equals("0") && data.getExtras().getString("CityId").toString().equals("0")) {
                sp_pollpreference.setSelection(0);
            } else {

                PreferredCity = data.getExtras().getString("CityName").toString();

            }
        }

    }
    private String getPath(Uri uri) throws Exception {
        // this method will be used to get real path of Image chosen from gallery.
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getCategory() {
        M.showLoadingDialog(SuggestPollActivity.this);

        CategoryAPI mAuthenticationAPI = APIService.createService(CategoryAPI.class);
        mAuthenticationAPI.getCategories("",new retrofit.Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(List<CategoryRealmPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {
                        list.addAll(pojo);
                        String[] items = new String[pojo.size()];
                        for(int i=1; i<list.size(); i++){
                            //Storing names to string array
                            items[0]="Please Select";
                            items[i]= list.get(i).getCat_name();
                            items[list.size()-1]="Others";
                        }

                        ArrayAdapter adapter=new ArrayAdapter(SuggestPollActivity.this,android.R.layout.simple_dropdown_item_1line,items );
                        etPollDesc.setAdapter(adapter);
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG,"fail"+error.getMessage());
                M.showToast(SuggestPollActivity.this, "Thank you for Submitting your Poll");
                if(error.getMessage().toString().contains("Expected Begin"))
                {
                    M.showToast(SuggestPollActivity.this, "Thank you for Submitting your Poll");
                }
                return;
            }
        });
    }
    private TypedFile makeFile(String uri){

        File file = new File(uri);
        TypedFile typedFile = new TypedFile("image/*",file);
        return typedFile;
    }
}














