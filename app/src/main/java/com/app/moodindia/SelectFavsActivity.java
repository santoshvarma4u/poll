package com.app.moodindia;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.app.moodindia.model.CategoryRealmPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.CategoryAPI;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import retrofit.RetrofitError;


public class SelectFavsActivity extends AppCompatActivity implements LocationListener{

    RecyclerView rv_categoriesList;
    Button btn_continue;
    CheckBox cbselectall;


    private static final String TAG = "Location";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;

    LocationRequest mLocationRequest;
    Location mCurrentLocation;

    Double finalLatitude=0.0,finalLongitude=0.0;

   // BubblePicker picker;

   /* @Override
    protected void onResume() {
        super.onResume();
        picker.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        picker.onPause();
    }*/

  //  Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_favs);
        ButterKnife.bind(this);

     //   picker=(BubblePicker)findViewById(R.id.picker) ;

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(SelectFavsActivity.this, R.color.colorPrimaryDark));


       /// realm = Realm.getDefaultInstance();

        rv_categoriesList=(RecyclerView)findViewById(R.id.recycler_view);
        btn_continue=(Button)findViewById(R.id.btnContinue);

        cbselectall=(CheckBox)findViewById(R.id.cbselectall);




        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectFavsActivity.this);
        rv_categoriesList.setLayoutManager(mLayoutManager);
        rv_categoriesList.setItemAnimator(new DefaultItemAnimator());
        categoryList=new ArrayList<>();
        cla=new CategoryListAdapter();
        geocoder = new Geocoder(this, Locale.getDefault());

        createLocationRequest();

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent it = new Intent(SelectFavsActivity.this, HomeActivity.class);
                finish();
                startActivity(it);
                overridePendingTransition(0,0);

            }
        });

        getCategoryData();
    }


    /*public void checkFilters(RealmResults<CategoryRealmPojo> results)
    {
        String AppliedFilters="";
        //save favourites in sharedpreferences in M class

        for(int i=0;i<results.size();i++)
        {
            if(results.get(i).isSelected())
            {
                AppliedFilters=results.get(i).getCat_id()+","+AppliedFilters;
            }

        }

        M.setFavs(AppliedFilters.substring(0,AppliedFilters.length()-1),SelectFavsActivity.this);

        if(M.getID(SelectFavsActivity.this).equals("0")) {

            Intent it = new Intent(SelectFavsActivity.this, PublicHomePage.class);
            finish();
            startActivity(it);
        }
        else{
            Intent it = new Intent(SelectFavsActivity.this, HomeActivity.class);
            finish();
            startActivity(it);
            overridePendingTransition(0,0);
        }
    }*/


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    List<CategoryRealmPojo> categoryList;
    CategoryListAdapter cla;

    @Override
    public void onStart() {
        super.onStart();
        //Log.d(TAG, "onStart fired ..............");

    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.d(TAG, "onStop fired ..............");

    }


    @Override
    public void onLocationChanged(Location location) {

        //Log.d(TAG, "onLocation changed ..............");
        try {
            finalLongitude=location.getLongitude();
            finalLatitude=location.getLatitude();

            setLocation( location.getLatitude(),  location.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    List<Address> addresses;
    Geocoder geocoder;
    public void setLocation(double lat, double lon) throws IOException {
        geocoder = new Geocoder(this, Locale.getDefault());
        addresses = geocoder.getFromLocation(lat, lon, 1);
       // address = addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getAddressLine(1);
        M.setMyCity(addresses.get(0).getLocality().toString(),SelectFavsActivity.this); // city = addresses.get(0).getLocality();
        M.setMyState(addresses.get(0).getAdminArea().toString(),SelectFavsActivity.this);        //state = addresses.get(0).getAdminArea();
        M.setMyCountry(addresses.get(0).getCountryName().toString(),SelectFavsActivity.this);//country = addresses.get(0).getCountryName();

    }


    public class CategoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_favs, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }
        CategoryRealmPojo listCategory;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listCategory=categoryList.get(position);
            holder.CategoryName.setText(listCategory.getCat_name());
            holder.CategoryId=listCategory.getCat_id();
            if(listCategory.isSelected())
            {
                holder.cb_select.setChecked(true);
            }
            else
                holder.cb_select.setChecked(false);
        }
    }

    List<String> checkedList = new ArrayList<>();

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView CategoryName;
        String CategoryId;
        CheckBox cb_select;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            CategoryName=(TextView)itemView.findViewById(R.id.tv_categoryname);
            cb_select=(CheckBox)itemView.findViewById(R.id.cb_selectFavs);

            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                //   categoryList.get(getAdapterPosition()).setSelected(isChecked);

                    if(isChecked)
                        checkedList.add(categoryList.get(getAdapterPosition()).getCat_id());
                    else
                        checkedList.remove(categoryList.get(getAdapterPosition()).getCat_id());

                }
            });


            CategoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String categoryid= categoryList.get(getAdapterPosition()).getCat_id();
                    Intent intent=new Intent(SelectFavsActivity.this,CurrentPolls.class);
                    intent.putExtra("act_name","recent");
                    intent.putExtra("catname",CategoryName.getText().toString());
                    intent.putExtra("catid",categoryid);
                    startActivity(intent);
                }
            });

/*
            cb_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {

                        checkedList.add(categoryList.get(getAdapterPosition()).getCat_id());
                    }
                    else {

                         checkedList.remove(categoryList.get(getAdapterPosition()).getCat_id());
                    }
                }
            });*/

          /*  String str=M.getFavs(SelectFavsActivity.this).toString();
            if(!str.contains("18"))
            {
                checkedList.add("18");
            }*/
        }
    }

    public void getCategoryData()
    {
        M.showLoadingDialog(SelectFavsActivity.this);
        CategoryAPI mAuthenticationAPI = APIService.createService(CategoryAPI.class);
        mAuthenticationAPI.getCategories("",new retrofit.Callback<List<CategoryRealmPojo>>() {
            @Override
            public void success(final List<CategoryRealmPojo> pojo, retrofit.client.Response response) {
                if (pojo != null) {
                    if (pojo.size() > 0) {

                        //setBubbles(pojo);



                        //add empty values to recycler view
                        int i = 0;
                        for (CategoryRealmPojo catp : pojo) {
                            catp.setSelected(false);

                            categoryList.add(i, catp);
                            i++;
                        }

                        rv_categoriesList.setAdapter(cla);

                     /*else {

                            //get selected one and add flags based on the local and server value

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    int i = 0;
                                    RealmResults<CategoryRealmPojo> preResults = realm.where(CategoryRealmPojo.class).findAll();
                                    for (CategoryRealmPojo mCategoryRealmPojo : preResults) {
                                        Log.e("PrevData:", mCategoryRealmPojo.getCat_id() + ":" + mCategoryRealmPojo.isSelected());
                                        categoryList.add(i, mCategoryRealmPojo);

                                        if (mCategoryRealmPojo.isSelected())
                                            checkedList.add(i, mCategoryRealmPojo.getCat_id());


                                        i++;
                                    }
                                }
                            });


                            // categoryList=pojo;



                        *//* for(CategoryPojo catp:pojo)
                            {
                                String str=M.getFavs(SelectFavsActivity.this).toString();
                                if(!str.isEmpty())
                                {
                                 List<String> favsList =  Arrays.asList(str.split(","));

                                for(String favs:favsList)
                                {
                                    //  checkedList.add(listCategory.getCat_id().toString());
                                    if(favs.equalsIgnoreCase(catp.getCat_id().toString()))
                                    {
                                        catp.setSelected(true);
                                    }

                                }
                               categoryList.add(catp);
                            }

                        }*//*

                            //ArrayList<CategoryPojo>) pojo;
                            //lvcat.setAdapter(adapter);
                        }*/
                    }
                    M.hideLoadingDialog();
                }
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

 /*   public void reloadCats(final List<CategoryRealmPojo> pojo)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(CategoryRealmPojo.class);
            }
        });
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                for(CategoryRealmPojo catp:pojo) {
                    CategoryRealmPojo crp= realm.createObject(CategoryRealmPojo.class);
                    crp.setCat_id(catp.getCat_id());
                    crp.setCat_name(catp.getCat_name());
                    crp.setSelected(false);//by default setting all categories as false
                }
            }
        });

        //add empty values to recycler view
        int i=0;
        for(CategoryRealmPojo catp:pojo)
        {
            catp.setSelected(false);

            categoryList.add(i,catp);
            i++;
        }
    }*/


    /*public void setBubbles(final List<CategoryPojo> listCats)
    {
        final TypedArray colors = getResources().obtainTypedArray(R.array.bubblecolors);
        picker.setAdapter(new BubblePickerAdapter() {
            @Override
            public int getTotalCount() {
                return listCats.size();
            }

            @NotNull
            @Override
            public PickerItem getItem(int position) {
                PickerItem item = new PickerItem();
                item.setTitle(listCats.get(position).getCat_name());
                item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
                        colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
               // item.setTypeface(mediumTypeface);
                item.setTextColor(ContextCompat.getColor(SelectFavsActivity.this, android.R.color.white));
                //item.setBackgroundImage(ContextCompat.getDrawable(SelectFavsActivity.this, images.getResourceId(position, 0)));
                return item;
            }
        });
    }*/
}
