package com.app.moodindia.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.moodindia.CurrentPolls;
import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.CategoryPojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter {

    private ArrayList<CategoryPojo> listData;
    private LayoutInflater layoutInflater;
    Context context;

    public CategoryAdapter(Context aContext, ArrayList<CategoryPojo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        context=aContext;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.category_row, null);
            holder = new ViewHolder();
            holder.tvnm = (TextView) convertView.findViewById(R.id.tvcatnm);
            holder.ivcat=(ImageView)convertView.findViewById(R.id.ivcat);
            holder.llcat=(LinearLayout)convertView.findViewById(R.id.llcat);

            Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(context.getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(context.getAssets(),  "fonts/ssl.ttf");

            holder.tvnm .setTypeface(custom_fontb);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final CategoryPojo cat=listData.get(position);

        holder.tvnm.setText(cat.getCat_name());
        if(cat.getCat_img().toString().length()>0)
            Picasso.with(context)
                .load(AppConst.imgurl+cat.getCat_img())
                .transform(new RoundedSquareTransformation(10,0))
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(holder.ivcat);
        else
            Picasso.with(context)
                    .load(R.drawable.default_image)
                    .transform(new RoundedSquareTransformation(10,0))
                    .placeholder(R.drawable.default_image)
                    .error(R.drawable.default_image)
                    .into(holder.ivcat);

        holder.llcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it=new Intent(context, CurrentPolls.class);
                it.putExtra("catid",cat.getCat_id());
                it.putExtra("catname",cat.getCat_name());
                context.startActivity(it);
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView tvnm;
        ImageView ivcat;
        LinearLayout llcat;
    }

}


