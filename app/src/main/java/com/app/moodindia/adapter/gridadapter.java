package com.app.moodindia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.model.Brandpojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by vinay on 28/02/2017.
 */

public class gridadapter extends RecyclerView.Adapter<gridadapter.ViewHolder> {
    private ArrayList<Brandpojo> android;
    private Context context;

    public gridadapter(Context context, ArrayList<Brandpojo> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public gridadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(gridadapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_android.setText(android.get(i).getAndroid_version_name());
        viewHolder.tvpoints.setText(android.get(i).getAndroid_version_name());
        Picasso.with(context).load(android.get(i).getAndroid_image_url()).resize(540, 500).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_android;
        private TextView tvpoints;
        private ImageView img_android;

        public ViewHolder(View view) {
            super(view);

            tv_android = (TextView) view.findViewById(R.id.text);
            img_android = (ImageView) view.findViewById(R.id.brandlogo);
            tvpoints=(TextView)view.findViewById(R.id.text2);
        }
    }
}
