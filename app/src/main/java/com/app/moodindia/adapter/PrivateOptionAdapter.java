package com.app.moodindia.adapter;

/**
 * Created by SantoshT on 7/26/2017.
 */

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.Pollresult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PrivateOptionAdapter extends BaseAdapter {

    private ArrayList<Pollresult> listData;
    private LayoutInflater layoutInflater;
    Context context;
    private Handler mHandler = new Handler();
    Boolean isvoted;
    String seloptions;

    public PrivateOptionAdapter(Context aContext, ArrayList<Pollresult> listData, Boolean isvoted, String seloptions) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        context = aContext;
        this.isvoted = isvoted;
        this.seloptions = seloptions;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.priavte_poll_option_row, null);
            holder = new ViewHolder();
            holder.tvoption = (TextView) convertView.findViewById(R.id.tvoption);
            holder.tvotes = (TextView) convertView.findViewById(R.id.tvvotes);
            holder.tvper = (TextView) convertView.findViewById(R.id.tvper);
            holder.option_image = (ImageView) convertView.findViewById(R.id.option_image);


            Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(context.getAssets(), "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(context.getAssets(), "fonts/ssl.ttf");


            holder.tvper.setTypeface(custom_fontb);
            holder.tvotes.setTypeface(custom_fontb);
            holder.tvoption.setTypeface(custom_font);

            holder.pb = (ProgressBar) convertView.findViewById(R.id.pb);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Pollresult poll = listData.get(position);

        holder.tvoption.setText(poll.getOptionvalue());
        if (isvoted) {
            String[] items = seloptions.split(",");
            List<String> options = Arrays.asList(items);
            //Log.d("options",options.toString());
            if (options.contains(poll.getOptionid() + "")) {
                holder.tvoption.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.selected, 0);
                holder.tvoption.setTextColor(Color.parseColor("#e39400"));
                holder.tvoption.setCompoundDrawablePadding(5);
            } else {
                holder.tvoption.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                holder.tvoption.setCompoundDrawablePadding(0);
                holder.tvoption.setTextColor(Color.parseColor("#FFFFFF"));
            }
        } else {
            holder.tvoption.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            holder.tvoption.setCompoundDrawablePadding(0);
            holder.tvoption.setTextColor(Color.parseColor("#FFFFFF"));
        }

        holder.tvotes.setText(poll.getOptioncount() + " votes");
        holder.tvper.setText(poll.getOptionpercentage() + "%");
        if (poll.getOptioncount() > 0) {
            holder.pb.setVisibility(View.VISIBLE);
            final int[] mProgressStatus = {0};
            new Thread(new Runnable() {
                public void run() {
                    while (mProgressStatus[0] < poll.getOptionpercentage()) {
                        SystemClock.sleep(20);
                        mProgressStatus[0] += 1;
                        // Update the progress bar
                        mHandler.post(new Runnable() {
                            public void run() {

                                holder.pb.setProgress(mProgressStatus[0]);

                            }
                        });
                    }
                }
            }).start();
        }
        if (poll.getOptioncount() == 0) {
            holder.pb.setVisibility(View.GONE);
        }
        holder.option_image.setMaxWidth(40);
        holder.option_image.setMaxHeight(40);
        if(poll.getOptionimage().length()>0)
        {
            if(poll.getOptionimage().equalsIgnoreCase("NO"))
            {
                holder.option_image.setVisibility(View.GONE);
            }
            else
            {
                Picasso.with(context)
                        .load(AppConst.imgurl + poll.getOptionimage())
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .into(holder.option_image);
            }
        }
        else
        {
            holder.option_image.setVisibility(View.GONE);
        }
        holder.option_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog=new Dialog(context);
                dialog.setContentView(R.layout.custom_image_dialog);
                ImageView iv_option_image=(ImageView)dialog.findViewById(R.id.iv_full_option_image);
                Picasso.with(context)
                        .load(AppConst.imgurl + poll.getOptionimage())
                        .placeholder(R.drawable.default_image)
                        .error(R.drawable.default_image)
                        .into(iv_option_image);
                dialog.show();
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView tvoption, tvotes, tvper;
        ProgressBar pb;
        ImageView option_image;
    }
}