package com.app.moodindia.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.helper.RoundedSquareTransformation;
import com.app.moodindia.model.PollPojo;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.app.moodindia.R.id.tvpollcategory;

public class PollAdapter extends ArrayAdapter<PollPojo> implements Filterable {
    private List<PollPojo> originalList;
    ArrayList<PollPojo> list=new ArrayList<>();
    Context ctx;
    private LayoutInflater layoutInflater;

    public PollAdapter(Context context, int textViewResourceId,ArrayList<PollPojo> list) {
        super(context, textViewResourceId, list);
        this.list=list;
        ctx=context;
        layoutInflater=LayoutInflater.from(context);
        this.originalList=list;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;
            if(row==null){
                row=layoutInflater.inflate(R.layout.poll_row, null);
                holder = new ViewHolder();
                holder.cvpollcategory = (TextView)row.findViewById(tvpollcategory);
                holder.tvnm = (TextView) row.findViewById(R.id.tvpoll);
                holder.tvviewAll=(TextView)row.findViewById(R.id.tvviewAll);
                holder.cvlayout=(LinearLayout)row.findViewById(R.id.cvlayout);
                holder.iv=(ImageView)row.findViewById(R.id.ivpoll);
                holder.ll=(LinearLayout)row.findViewById(R.id.llpoll);
                holder.cvcategory=(CardView)row.findViewById(R.id.cv_category);
                holder.tvcat=(TextView)row.findViewById(R.id.tvCateg) ;
                holder.tvCategPost=(TextView) row.findViewById(R.id.tvCategPost);

                Typeface custom_font = Typeface.createFromAsset(ctx.getAssets(),  "fonts/ssr.ttf");
                Typeface custom_fontb = Typeface.createFromAsset(ctx.getAssets(),  "fonts/ssb.ttf");
                Typeface custom_fontl = Typeface.createFromAsset(ctx.getAssets(),  "fonts/ssl.ttf");

                holder.tvnm.setTypeface(custom_fontb);
                holder.tvviewAll.setTypeface(custom_fontb);
                holder.cvpollcategory.setTypeface(custom_fontb);
                holder.tvnm.setTypeface(custom_font);

                row.setTag(holder);
                }
                else{
                    holder = (ViewHolder) row.getTag();
                }
        holder.tvnm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });

        holder.tvviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });
                    PollPojo poll=list.get(position);
                    //Log.d("poll",poll.toString());
                    if(poll.isVisible()) {
                        holder.cvlayout.setVisibility(View.VISIBLE);

                    }
                    else
                        holder.cvlayout.setVisibility(View.GONE);

                         holder.tvcat.setVisibility(View.GONE);
                        holder.tvnm.setText(poll.getQuestion());
                        holder.cvpollcategory.setText(poll.getCat_name());

                        if(poll.getStart_time()== null)
                        {

                        }
                        else
                        {
                            try {
                                holder.tvCategPost.setText("Posted on "+dtfmt.format(defaultfmt.parse(poll.getStart_time())));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }


        if(poll.getImage().toString().length()>0)
                           Picasso.with(ctx)
                            .load(AppConst.imgurl+poll.getImage())
                            .transform(new RoundedSquareTransformation(10,0))
                            .placeholder(R.drawable.default_image)
                            .error(R.drawable.default_image)
                            .into(holder.iv);
                       else
                           Picasso.with(ctx)
                            .load(R.drawable.default_image)
                            .transform(new RoundedSquareTransformation(10,0))
                            .placeholder(R.drawable.default_image)
                            .error(R.drawable.default_image)
                            .into(holder.iv);
        return row;
    }
                        @Override
                        public int getCount() {
                            return originalList.size();
                        }
                        @Override
                        public PollPojo getItem(int position) {
                            return originalList.get(position);
                        }
                        @Override
                        public long getItemId(int position) {
                            return position;
                        }




    SimpleDateFormat defaultfmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt=new SimpleDateFormat("dd MMM yyyy");


    class ViewHolder {
        TextView tvnm,tvviewAll,tvcat,tvCategPost;
        ImageView iv;
        LinearLayout ll;
        TextView cvpollcategory;
        CardView cvcategory;
        LinearLayout cvlayout;
}

}



