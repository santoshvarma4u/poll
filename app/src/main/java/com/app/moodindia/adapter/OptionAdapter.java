package com.app.moodindia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.moodindia.R;
import com.app.moodindia.model.Pollresult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptionAdapter extends BaseAdapter {

    private ArrayList<Pollresult> listData;
    private LayoutInflater layoutInflater;
    Context context;
    private Handler mHandler = new Handler();
    Boolean isvoted;
    String seloptions;

    public OptionAdapter(Context aContext, ArrayList<Pollresult> listData,Boolean isvoted,String seloptions) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
        context=aContext;
        this.isvoted=isvoted;
        this.seloptions=seloptions;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.poll_option_row, null);
            holder = new ViewHolder();
            holder.tvoption = (TextView) convertView.findViewById(R.id.tvoption);
            holder.tvotes = (TextView) convertView.findViewById(R.id.tvvotes);
            holder.tvper = (TextView) convertView.findViewById(R.id.tvper);

            Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "fonts/ssr.ttf");
            Typeface custom_fontb = Typeface.createFromAsset(context.getAssets(),  "fonts/ssb.ttf");
            Typeface custom_fontl = Typeface.createFromAsset(context.getAssets(),  "fonts/ssl.ttf");


            holder.tvper.setTypeface(custom_fontb);
            holder.tvotes.setTypeface(custom_fontb);
            holder.tvoption.setTypeface(custom_font);

            holder.pb=(ProgressBar)convertView.findViewById(R.id.pb);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Pollresult poll=listData.get(position);

        holder.tvoption.setText(poll.getOptionvalue());
        if(isvoted){
            String [] items = seloptions.split(",");
            List<String> options = Arrays.asList(items);
            //Log.d("options",options.toString());
            if(options.contains(poll.getOptionid()+"")){
               holder.tvoption.setCompoundDrawablesWithIntrinsicBounds( 0, 0, R.drawable.selected, 0);
                holder.tvoption.setTextColor(Color.parseColor("#e39400"));
               holder.tvoption.setCompoundDrawablePadding(5);
            }else{
                holder.tvoption.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
                holder.tvoption.setCompoundDrawablePadding(0);
                holder.tvoption.setTextColor(Color.parseColor("#FFFFFF"));
            }
        }else{
            holder.tvoption.setCompoundDrawablesWithIntrinsicBounds( 0, 0, 0, 0);
            holder.tvoption.setCompoundDrawablePadding(0);
            holder.tvoption.setTextColor(Color.parseColor("#FFFFFF"));
        }

        holder.tvotes.setText(poll.getOptioncount()+" votes");
        holder.tvper.setText(poll.getOptionpercentage()+"%");
        if(poll.getOptioncount()>0) {
            holder.pb.setVisibility(View.VISIBLE);
            final int[] mProgressStatus = {0};
            new Thread(new Runnable() {
                public void run() {
                    while (mProgressStatus[0] < poll.getOptionpercentage()) {
                        SystemClock.sleep(20);
                        mProgressStatus[0] += 1;
                        // Update the progress bar
                        mHandler.post(new Runnable() {
                            public void run() {

                                holder.pb.setProgress(mProgressStatus[0]);

                            }
                        });
                    }
                }
            }).start();
        }if(poll.getOptioncount()==0){
            holder.pb.setVisibility(View.GONE);
        }
        return convertView;
    }

    class ViewHolder {
        TextView tvoption,tvotes,tvper;
        ProgressBar pb;
    }

}


