package com.app.moodindia;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.app.moodindia.model.M;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if( M.getID(IntroActivity.this).equals("0")) {
            setDepthAnimation();

            addSlide(AppIntroFragment.newInstance("India","ssl.ttf","The world’s biggest democracy and most diversified nation, now have its own democratic platform to raise the voice of commons on most pressing regional and national issues.","ssr.ttf",R.drawable.sintro1,Color.WHITE,Color.BLACK,Color.BLACK));
            addSlide(AppIntroFragment.newInstance("Let’s raise our voice", "In real time on issues ranging from political, social, economic to product, entertainment, sports & many more via live POLLS. ", R.drawable.sintro2,Color.WHITE,Color.BLACK,Color.BLACK));
            addSlide(AppIntroFragment.newInstance("Awesome Platform", "Where you can see real time transparent statistics, trends and have flexibility to analyze data using your own preferred geographic, age, gender and many more filters", R.drawable.sintro3,Color.WHITE,Color.BLACK,Color.BLACK));

        }
        else
        {

            Intent it = new Intent(IntroActivity.this, SplashActivity.class);
            finish();
            startActivity(it);
            overridePendingTransition(0,0);
        }

        setColorSkipButton(Color.BLACK);
        setColorDoneText(Color.BLACK);
        setNextArrowColor(Color.BLACK);
        setIndicatorColor(Color.BLACK,Color.LTGRAY);
       // setNavBarColor(Color.BLACK);

       }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
                            Intent it = new Intent(IntroActivity.this, SplashActivity.class);
                            finish();
                            startActivity(it);
                            overridePendingTransition(0,0);
    }
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent it = new Intent(IntroActivity.this, SplashActivity.class);
        finish();
        startActivity(it);
        overridePendingTransition(0,0);
    }
}
