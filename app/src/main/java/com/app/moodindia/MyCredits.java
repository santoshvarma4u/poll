package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.moodindia.Paytm.TransactionActivity;
import com.app.moodindia.model.CreditsPojo;
import com.app.moodindia.model.M;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyCredits extends AppCompatActivity {

    SmartTabLayout tabLayout;
    ViewPager viewPager;
    FloatingActionButton fabAddCredits;
    TextView tv_credits;
    ImageView backarchive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_credits);
        tabLayout=(SmartTabLayout)findViewById(R.id.tablayout);
        fabAddCredits=(FloatingActionButton) findViewById(R.id.fabAddCredits);
        tv_credits=(TextView)findViewById(R.id.tv_credits);
        backarchive=(ImageView)findViewById(R.id.backarchive);
        backarchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MyCredits.this, HomeActivity.class);
                startActivity(it);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        //tabLayout.setTabMode(TabLayout.MODE_FIXED);
        viewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);
        tabLayout.setViewPager(viewPager);

        final DialogPlus dialog = DialogPlus.newDialog(MyCredits.this)
                .setGravity(Gravity.CENTER)
                .setContentHolder(new ViewHolder(R.layout.custom_layout_add_credits))
                .setCancelable(true)
                .create();

        View view=dialog.getHolderView();

        final EditText et_add=(EditText)view.findViewById(R.id.et_amount);
        final TextView tvAddmoney=(TextView) view.findViewById(R.id.tvAddMoney);
        final TextView tvCancel=(TextView)view.findViewById(R.id.tvCancel);
        tvAddmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_add.getText().length()>0)
                {
                   String transactionAmount = et_add.getText().toString();

                    getCheckSumHash(transactionAmount);

                }
                else
                {
                    et_add.setError("Please enter valid amount");
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        fabAddCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //take user into transaction page
                //payment gateway
                dialog.show();
             //   startActivity(new Intent(MyCredits.this, AddCreditsActivity.class));

            }
        });
        getTransData();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RedeemCredits(), " Redeem");
        adapter.addFragment(new CreditsHistory(), "Transaction History");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    public void getTransData()
    {
        M.showLoadingDialog(MyCredits.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getCreditDetails(M.getID(MyCredits.this), new Callback<List<CreditsPojo>>() {
            @Override
            public void success(List<CreditsPojo> creditsPojos, Response response) {
                if(creditsPojos.size()>0)
                {
                    int totalcredits = Integer.parseInt(creditsPojos.get(creditsPojos.size()-1).getTotalbalance())+M.getCredits(MyCredits.this);
                    tv_credits.setText("₹ "+totalcredits);

                }
                M.hideLoadingDialog();
            }
            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
            }
        });
    }
    public void getCheckSumHash(final String amount)
    {
        final String Custid="CUST"+ M.getID(MyCredits.this);

        Random r = new Random();
        int i1 = r.nextInt(99999 - 11111) + 11111;
        final String orderid="ORDER"+Integer.toString(i1);
        final String email=M.getEmail(MyCredits.this);
        final String phone=M.getPhone(MyCredits.this);

        //okhttp service
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://moodindia.in/Paytm/generateChecksum.php?OrderId="+orderid+"&amount="+amount+"&Email="+email+"&MobileNo="+phone+"&CustID="+Custid).build();
        client.newCall(request).enqueue(new com.squareup.okhttp.Callback() {
            @Override
            public void onFailure(com.squareup.okhttp.Request request, IOException e) {

            }

            @Override
            public void onResponse(final com.squareup.okhttp.Response response) throws IOException {
                // //Log.i("response",response.body());
                ////Log.i("response",response.body().string());
                    M.hideLoadingDialog();
                if (response.isSuccessful()) {

                    Intent intent=new Intent(getApplicationContext(),TransactionActivity.class);
                    intent.putExtra("amount",amount);
                    intent.putExtra("Custid",Custid);
                    intent.putExtra("orderid",orderid);
                    intent.putExtra("email",email);
                    intent.putExtra("phone",phone);
                    intent.putExtra("checksumhash",response.body().string());
                    startActivity(intent);

                    Log.e("respose",response.body().string());
                }
            }
        });
    }
}
