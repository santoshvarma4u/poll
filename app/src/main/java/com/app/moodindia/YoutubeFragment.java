package com.app.moodindia;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link YoutubeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class YoutubeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public YoutubeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment YoutubeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static YoutubeFragment newInstance(String param1, String param2) {
        YoutubeFragment fragment = new YoutubeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    protected View rootView;
    protected WebView wvTlk;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        initView(rootView);
        return rootView;
    }
    private void initView(View rootView) {
        wvTlk = (WebView) rootView.findViewById(R.id.wv_tlk);
        WebSettings mWebSettings = wvTlk.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);
        mWebSettings.setLoadWithOverviewMode(true);
        // Other webview options
        mWebSettings.setLoadWithOverviewMode(true);

        wvTlk.setScrollbarFadingEnabled(false);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setPluginState(WebSettings.PluginState.ON);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setRenderPriority(
                WebSettings.RenderPriority.HIGH);
        mWebSettings.setCacheMode(
                WebSettings.LOAD_NO_CACHE);
        mWebSettings.setAppCacheEnabled(true);
        wvTlk.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings
                .setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setSavePassword(true);
        mWebSettings.setSaveFormData(true);
        mWebSettings.setEnableSmoothTransition(true);//http://moodindia.in/storyapi/yt/?q=raja
        //System.out.println("http://moodindia.in/storyapi/yt/index.php?q="+mParam2);
        wvTlk.loadUrl("http://moodindia.in/storyapi/yt/index.php?q="+mParam2);
    }
}
