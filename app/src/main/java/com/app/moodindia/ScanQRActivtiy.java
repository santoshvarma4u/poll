package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanQRActivtiy extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }
    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }
    @Override
    public void handleResult(Result result) {
        // Do something with the result here
        Log.v("text", result.getText());
        // Prints scan results
        if(result.getText().contains("MITOPIC") )
        {
            String catid=result.getText().substring(result.getText().lastIndexOf("-")+1).toString();
            String pollid="0";
            Intent intent=new Intent();
            intent.putExtra("catid",catid);
            intent.putExtra("pollid","0");
            intent.putExtra("type","cats");
            setResult(RESULT_OK,intent);
            finish();
        }
        else if(result.getText().contains("MIPOLL"))
        {
            String pollid=result.getText().substring(result.getText().lastIndexOf("-")+1).toString();
            String catid="0";
            Intent intent=new Intent();
            intent.putExtra("catid","0");
            intent.putExtra("pollid",pollid);
            intent.putExtra("type","polls");
            setResult(RESULT_OK,intent);
            finish();
        } else if(result.getText().contains("MIGROUP"))
        {
            String groupid=result.getText().substring(result.getText().lastIndexOf("-")+1).toString();
            String catid="0";
            Intent intent=new Intent();
            intent.putExtra("catid","0");
            intent.putExtra("pollid",groupid);
            intent.putExtra("type","groups");
            setResult(RESULT_OK,intent);
            finish();
        }
        else
        {
            Toast.makeText(ScanQRActivtiy.this, "Incorrect Code, Try again", Toast.LENGTH_SHORT).show();
            mScannerView.resumeCameraPreview(this);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
