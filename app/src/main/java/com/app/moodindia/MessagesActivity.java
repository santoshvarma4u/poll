package com.app.moodindia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.app.moodindia.model.M;
import com.app.moodindia.model.MessagesPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.ConnectivityReceiver;
import com.app.moodindia.webservices.PollAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

public class MessagesActivity extends AppCompatActivity {
    private RecyclerView rvMessages;
    RecyclerView.LayoutManager mLayoutManager;
    boolean  isConnected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);


        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //  window.setStatusBarColor(ContextCompat.getColor(MessagesActivity.this, R.color.colorPrimaryDark));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        toolbar.setNavigationIcon(R.drawable.md_nav_back);

        final Intent it=new Intent(this,HomeActivity.class);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(it);
                finish();
                overridePendingTransition(0,0);
            }
        });


        rvMessages = (RecyclerView) findViewById(R.id.rv_messages);

        mLayoutManager = new LinearLayoutManager(MessagesActivity.this);
        rvMessages.setLayoutManager(mLayoutManager);
        rvMessages.setItemAnimator(new DefaultItemAnimator());
        checkConnection();
        pla=new MsgAdapter();
        msgList=new ArrayList<>();

    }

    MsgAdapter pla;
    List<MessagesPojo> msgList;


    public class MsgAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View vSlider = inflater.inflate(R.layout.item_messages, parent, false);
            viewHolder = new ViewHolderFilter(vSlider);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolderFilter vh2 = (ViewHolderFilter) holder;
            configureViewHolderFilter(vh2, position);
        }

        @Override
        public int getItemCount() {
            return msgList.size();
        }
        MessagesPojo listMesaages;

        public void configureViewHolderFilter(final ViewHolderFilter holder, final int position) {
            listMesaages=msgList.get(position);


            holder.tvtitle.setText(listMesaages.getTitle());
            holder.tvmsg.setText(listMesaages.getMsg());

        }
    }

    class ViewHolderFilter extends RecyclerView.ViewHolder{

        TextView tvtitle,tvmsg;

        public ViewHolderFilter(View itemView) {
            super(itemView);
            tvtitle=(TextView)itemView.findViewById(R.id.title);
            tvmsg = (TextView)itemView.findViewById(R.id.message);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

            getNotifications();
    }

    public void getNotifications()
    {

        M.showLoadingDialog(MessagesActivity.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getNotifications(M.getID(MessagesActivity.this),new retrofit.Callback<List<MessagesPojo>>() {
            @Override
            public void success(List<MessagesPojo> pojo, retrofit.client.Response response) {
                if(pojo!=null){
                    if(pojo.size()>0) {

                        msgList=pojo;
                        rvMessages.setAdapter(pla);
                        pla.notifyDataSetChanged();
                        //ArrayList<CategoryPojo>) pojo;
                        //lvcat.setAdapter(adapter);
                    }else{

                    }
                }else{

                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d("Favs","fail"+error.getMessage());
                return;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(MessagesActivity.this,HomeActivity.class);
        startActivity(it);
        overridePendingTransition(0,0);
    }


    private void checkConnection() {
        isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
    }
   /* private void showSnack(boolean isConnected) {

        if (isConnected) {


        } else {
            Intent i=new Intent(getApplicationContext(), Internet.class);
            startActivity(i);
        }


    }*/
}
