package com.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.moodindia.HistoryActivity;
import com.app.moodindia.InfoActivity;
import com.app.moodindia.PollScreen;
import com.app.moodindia.R;
import com.app.moodindia.ResultMapActivity;
import com.app.moodindia.SuggestPollActivity;
import com.app.moodindia.adapter.OptionAdapter;
import com.app.moodindia.helper.AppConst;
import com.app.moodindia.model.M;
import com.app.moodindia.model.PollOptionPojo;
import com.app.moodindia.model.Pollresult;
import com.app.moodindia.model.SubmitAnsPojo;
import com.app.moodindia.webservices.APIService;
import com.app.moodindia.webservices.PollAPI;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by SantoshT on 2/6/2017.
 */

public class ArchivePollScreen  extends YouTubeBaseActivity implements View.OnClickListener, YouTubePlayer.OnInitializedListener {

    TextView tvque,  tvdate, tvPollCount,txtExp;
    LinearLayout lloptions, lytmap;
    Button btnsubmit;
    ImageView ivpoll,iv_backnav,ic_report,ic_share;
    ListView lvoption;
    Button tvinfo1;
    String TAG = "ArchivePollScreen", pollid = "", userid = "",twittertag,youtubetag,chattag;
    String info, que, start, end, type, img = "", queid, seloptions = "", totalPolls;
    int single;
    Boolean isvoted;
    SimpleDateFormat defaultfmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dtfmt = new SimpleDateFormat("dd MMM yyyy");
    ArrayList<String> idlist = new ArrayList<>();
    private Button btnViewMaps;
    private Button btnSuggestPoll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_archives);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(ArchivePollScreen.this, R.color.colorPrimaryDark));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
   /*     setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        //toolbar.setLogo(R.drawable.mood);
        pollid = getIntent().getExtras().getString("pollid");
        userid = M.getID(ArchivePollScreen.this);
        tvque = (TextView)findViewById(R.id.tvque);
        tvdate = (TextView)findViewById(R.id.tvdate);
        tvinfo1 = (Button)findViewById(R.id.tvinfo);
        tvPollCount = (TextView) findViewById(R.id.tvPollCount);
        txtExp=(TextView) findViewById(R.id.txtExp);
        lloptions = (LinearLayout) findViewById(R.id.lloption);
        btnsubmit = (Button) findViewById(R.id.btnsubmit);
        btnsubmit.setVisibility(View.GONE);
        btnSuggestPoll = (Button) findViewById(R.id.btnSuggestPoll);
        ivpoll = (ImageView) findViewById(R.id.ivpoll);
        iv_backnav = (ImageView) findViewById(R.id.iv_backnav);
        iv_backnav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ArchivePollScreen.this,HistoryActivity.class);
                intent.putExtra("fromact","home");
                startActivity(intent);
            }
        });
        lvoption = (ListView) findViewById(R.id.lvotedoption);
        lvoption.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lytmap = (LinearLayout) findViewById(R.id.lytmap);
        setListViewHeightBasedOnChildren(lvoption);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        // getPollData();
        btnsubmit.setOnClickListener(this);
        tvinfo1.setOnClickListener(this);


        btnViewMaps = (Button) findViewById(R.id.btnViewMaps);
        btnViewMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArchivePollScreen.this, ResultMapActivity.class);
                intent.putExtra("pollid", pollid);
                intent.putExtra("from", getIntent().getExtras().getString("from"));
                intent.putExtra("archives","archives");
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        btnSuggestPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArchivePollScreen.this, SuggestPollActivity.class);
                startActivity(intent);
            }
        });
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getExtras().containsKey("from"))
            getPollData(getIntent().getExtras().getString("from"));
    }
/*
    public void realtimeLoading() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds

                if (getIntent().getExtras().containsKey("from")) {
                    if (getIntent().getExtras().getString("from").equalsIgnoreCase("active")) {
                        getRealTimePollData();
                        handler.postDelayed(this, 10000);
                    }
                }
                //  getPollData(getIntent().getExtras().getString("from"));


            }
        }, 500);
    }*/

    boolean isVoteLocal = false;

    private void getPollData(String active) {
        AppConst.selpoll = null;
        ////Log.d(TAG, "userid" + userid + "-" + pollid);
        M.showLoadingDialog(ArchivePollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid, pollid, active,M.getMyStateID(ArchivePollScreen.this),M.getMyCityID(ArchivePollScreen.this), new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {

                if (pollpojo != null) {
                    if (pollpojo.size() > 0) {
                        for (PollOptionPojo pojo : pollpojo) {
                            AppConst.selpoll = pojo;
                            queid = pojo.getPollid() + "";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions = pojo.getSelected_option();
                            totalPolls = pojo.getPoll_counts();
                            tvPollCount.setText("Total Poll Votes: " + totalPolls);
                           /* if (isvoted) {
                               /* tvque.setTextColor(Color.parseColor("#000"));
                                tvque.setBackgroundColor(Color.parseColor("#fff"));
                                tvque.setBackgroundColor(Color.parseColor("#e3e9ef"));
                                tvque.getBackground().setAlpha(51);
                                tvque.setTextColor(Color.parseColor("#ffffff"));
                                //  tvque.setBackgroundColor(getResources().getColor(R.color.light_green));
                                //realtimeLoading();

                            }
                            else
                            {

                            }
*/
                           if(pojo.getYoutubelink().length()>5)
                            {
                                youTubeView.initialize(AppConst.YOUTUBE_API_KEY, ArchivePollScreen.this);
                                youtubeLink=pojo.getYoutubelink().toString().substring(pojo.getYoutubelink().toString().lastIndexOf("=") + 1);
                                Log.d("Youutube",youtubeLink);
                                // =pojo.getYoutubelink().toString();
                            }
                            else
                            {
                                youTubeView.setVisibility(View.GONE);
                                ivpoll.setVisibility(View.VISIBLE);
                            }


                            tvque.setText(que);
                            if (img != "") {
                                Picasso.with(ArchivePollScreen.this)
                                        .load(AppConst.imgurl + img)
                                        .placeholder(R.drawable.default_image)
                                        .error(R.drawable.default_image)
                                        .into(ivpoll);
                            }

                            try {
                                tvdate.setText("Posted on " + dtfmt.format(defaultfmt.parse(start)));
                                txtExp.setText("Expired on");
                                Date dt = defaultfmt.parse(end);
                                RelativeTimeTextView v = (RelativeTimeTextView) findViewById(R.id.timestamp);
                                v.setReferenceTime(dt.getTime());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            lloptions.setVisibility(View.GONE);
                            btnsubmit.setVisibility(View.GONE);
                            lvoption.setVisibility(View.VISIBLE);
                            lytmap.setVisibility(View.VISIBLE);
                            OptionAdapter adapter = new OptionAdapter(ArchivePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(), true, seloptions);
                            lvoption.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                          /*  if (!isvoted) {
                                for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(ArchivePollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20, 20, 20);
                                    tv.setTextSize(18);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(ArchivePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(ArchivePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 5);
                                    lloptions.addView(tv, layoutParams);
                                    lytmap.setVisibility(View.GONE);
                                }
                            } else {
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                OptionAdapter adapter = new OptionAdapter(ArchivePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(), isvoted, seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }*/
                        }
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }


    private void getRealTimePollData() {
        AppConst.selpoll = null;
        //Log.d(TAG, "userid" + userid + "-" + pollid);
        // M.showLoadingDialog(ArchivePollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.getPollOption(userid, pollid, "active",M.getMyStateID(ArchivePollScreen.this),M.getMyCityID(ArchivePollScreen.this), new retrofit.Callback<List<PollOptionPojo>>() {
            @Override
            public void success(List<PollOptionPojo> pollpojo, retrofit.client.Response response) {
                if (pollpojo != null) {
                    if (pollpojo.size() > 0) {
                        for (PollOptionPojo pojo : pollpojo) {
                            AppConst.selpoll = pojo;
                            queid = pojo.getPollid() + "";
                            info = pojo.getInfo();
                            que = pojo.getQuestion();
                            start = pojo.getStarttime();
                            twittertag=pojo.getTwittertag();
                            youtubetag=pojo.getYoutubetag();
                            chattag=pojo.getChattag();
                            end = pojo.getEndtime();
                            type = pojo.getType();
                            img = pojo.getImage();
                            isvoted = pojo.getIsvoted();
                            seloptions = pojo.getSelected_option();
                            totalPolls = pojo.getPoll_counts();
                            tvPollCount.setText("Total Poll Votes: " + totalPolls);

                            lloptions.setVisibility(View.GONE);
                            btnsubmit.setVisibility(View.GONE);
                            lvoption.setVisibility(View.VISIBLE);
                            lytmap.setVisibility(View.VISIBLE);
                            OptionAdapter adapter = new OptionAdapter(ArchivePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(), true, seloptions);
                            lvoption.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                 /*           if(!isvoted) {
                                *//*for (final Pollresult data : pojo.getPollresult()) {
                                    final int id = data.getOptionid();
                                    final TextView tv = new TextView(ArchivePollScreen.this);
                                    tv.setTextColor(Color.WHITE);
                                    tv.setText(data.getOptionvalue());
                                    tv.setPadding(20, 20,20, 20);
                                    tv.setTextSize(18);

                                    tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                    tv.setBackgroundResource(R.drawable.button_rounded_corner);

                                    tv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (!M.getID(ArchivePollScreen.this).equals("0")) {
                                                if (type.equalsIgnoreCase("single")) {
                                                    tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                    single = id;
                                                    submitresult(single + "");
                                                } else {
                                                    if (idlist.contains(id + "")) {
                                                        tv.setTextColor(getResources().getColor(R.color.unselect_txt));
                                                        idlist.remove(id + "");
                                                    } else {
                                                        tv.setTextColor(getResources().getColor(R.color.select_txt));
                                                        idlist.add(id + "");
                                                    }
                                                    if (idlist.size() > 0)
                                                        btnsubmit.setVisibility(View.VISIBLE);
                                                    else
                                                        btnsubmit.setVisibility(View.GONE);
                                                }
                                            } else {
                                                Intent it = new Intent(ArchivePollScreen.this, LoginActivity.class);
                                                finish();
                                                startActivity(it);
                                            }
                                        }
                                    });

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParams.setMargins(5, 5, 5, 5);
                                    lloptions.addView(tv, layoutParams);
                                    lytmap.setVisibility(View.GONE);
                                }
                            *//*}else{
                                lloptions.setVisibility(View.GONE);
                                btnsubmit.setVisibility(View.GONE);
                                lvoption.setVisibility(View.VISIBLE);
                                lytmap.setVisibility(View.VISIBLE);
                                OptionAdapter adapter = new OptionAdapter(ArchivePollScreen.this, (ArrayList<Pollresult>) pojo.getPollresult(),isvoted,seloptions);
                                lvoption.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }*/
                        }
                    }
                }
                // M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnsubmit) {
            String ans = "";
            for (int i = 0; i < idlist.size(); i++) {
                if (i == 0)
                    ans = idlist.get(i);
                else
                    ans = ans + "," + idlist.get(i);
            }
            submitresult(ans);
        } else if (view.getId() == R.id.tvinfo) {

            Intent it = new Intent(ArchivePollScreen.this, InfoActivity.class);
            it.putExtra("info",info);
            it.putExtra("question",que);
            it.putExtra("image",img);
            it.putExtra("screen","pollscreen");
            it.putExtra("twittertag",twittertag);
            it.putExtra("youtubetag",youtubetag);
            it.putExtra("chattag",chattag);
            it.putExtra("startdate",start);
            it.putExtra("enddate",end);
            it.putExtra("isvoted",isvoted);
            it.putExtra("from",getIntent().getExtras().getString("from"));
            it.putExtra("pollid",pollid);

            // it.putExtra("screen","pollscreen");
            it.putExtra("screen", "ArchivePollScreen");
            startActivity(it);
        }
    }

    public void submitresult(final String ans) {
        //Log.d(TAG, "ans:" + ans);
        M.showLoadingDialog(ArchivePollScreen.this);
        PollAPI mAuthenticationAPI = APIService.createService(PollAPI.class);
        mAuthenticationAPI.submitAns(queid, ans, M.getID(ArchivePollScreen.this), new retrofit.Callback<SubmitAnsPojo>() {
            @Override
            public void success(SubmitAnsPojo pojo, retrofit.client.Response response) {
                if (pojo != null) {
                    if (pojo.getSuccess() == 1) {
                        AppConst.selpoll = null;
                        PollOptionPojo obj = new PollOptionPojo();
                        obj.setEndtime(end);
                        obj.setImage(img);
                        obj.setInfo(info);
                        obj.setIsvoted(true);
                        obj.setSelected_option(ans);
                        obj.setPollid(Integer.valueOf(queid));
                        obj.setPollresult(pojo.getPollresult());
                        obj.setQuestion(que);
                        obj.setStarttime(start);
                        obj.setType(type);
                        AppConst.selpoll = obj;
                        M.hideLoadingDialog();

                        pollid = getIntent().getExtras().getString("pollid");


                        Intent it = new Intent(ArchivePollScreen.this, ArchivePollScreen.class);
                        it.putExtra("pollid", pollid);
                        it.putExtra("from", "notActive");
                        finish();
                        startActivity(it);
                    }
                }
                M.hideLoadingDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                M.hideLoadingDialog();
                //Log.d(TAG, "fail" + error.getMessage());
                return;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_poll, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        else if (id == R.id.share) {
            Intent sendIntent = new Intent();
          /*  sendIntent.setAction(Intent.ACTION_SEND);
            //this is the text that will be shared
            // sendIntent.putExtra(Intent.EXTRA_TEXT, (que + "\n" + AppConst.downloadurl));
            sendIntent.putExtra(Intent.EXTRA_TEXT, ("I just saw this awesome poll. Please participate and express your opinion. \n" + que + " \n http://moodindia.in/p/" + pollid));
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.share_header)));*/
            Toast.makeText(ArchivePollScreen.this, "This Poll is Archived , It cannot be shared", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(this, HistoryActivity.class);
        it.putExtra("fromact","home");
        startActivity(it);
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.loadVideo(youtubeLink);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private String youtubeLink="";


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(AppConst.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

}